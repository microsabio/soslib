:<(UI) Message Dialog Subroutines
    MSGBOX.SBX - Generic pop-up message (GUI or text)
        TSTMSG - Sample/test program for MSGBOX.SBX
    EVTMSG.SBX - Simplified event message w/ logging option (GUI only)
        TSTEVM - Sample/test program for EVTMSG.SBX
    XTRMSG.SBX - Display a text file in a dialog (GUI only)
        TSTXMS - Sample/test program for XTRMSG.SBX
    SCRSTS.SBX - output message to scrolling status window (GUI or text)
        TSTSCR - Sample/test program for SCRSTS.SBX
>
 
