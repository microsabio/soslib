XTRMSG.SBX - Display a file in a pop-up dialog
---------------------------------------------------------------------
USAGE:
    XCALL XTRMSG,TITLE,SROW,SCOL,EROW,ECOL,FLAGS,SOURCE, &
            EXITCODE{,HEADER{,XFLAGS}}

WHERE:
    TITLE is title of dialog box
    SROW,SCOL,EROW,ECOL are the boundaries of the dialog box
    FLAGS:
        1 = first row of file to be used as header
        2 = HEADER uses XTREE advanced syntax
    SOURCE  (fspec of file to display)
    EXITCODE (returned)
    HEADER (optional column header of XTREE)
    XFLAGS (If specified and non-zero, is used for the XTREE flags.
           Otherwise, certain default XTREE flags are used.  Note that
           we ignore FLAGS 2 if XFLAGS is non-zero)
NOTES:
    Uses XTREE #9 (must not already be in use!)
    Due to an ideosyncracy of XTREE, blank lines in the input file
	are ignored.  To display blank lines, add at least one space
	to the blank line.

FILES:
    XTRMSG.BP   Source
    XTRMSG.SBX  Subroutine
    TSTXMS.BP   Sample/test program

COMPATIBILITY & REQUIREMENTS:
    A-Shell 4.9+ GUI support (ATE ok)
    EVTMSG.SBX

!-------------------------------------------------------------------
!EDIT HISTORY
![100] 24-Jan-05 Created /jdm
!-------------------------------------------------------------------

