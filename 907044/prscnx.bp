program PRSCNX,1.0(100) 	! SBX to print screen or window
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
! ------------------------------------------------------------------------
! Notes:
!   Wrapper for UmZero's Netcaller screen capture routine.
!   It creates a PNG file (which, oddly enough, has a WMF extension)
!   and then we print it by creating a GDI print file.  Requires 5.1.1116
!   or higher (for the PNG/WMF support).
!
! Usage:
!   XCALL PRSCNX,OPSRC,OPDST$,STATUS{,FLAGS,WIDTH,HEIGHT,TITLE$,NOTES$}
!
! where
!   OPSRC [num, in] 0 = entire screen, 1 = active window
!    
!   OPDST$ [str, in] name of printer to send it to
!       Default filename is PRSCNX.WMF.  To just create the file,
!       set OPDST$ to a filespec, or ".\" (= ".\prscnx.wmf").  Note:
!       filespec must contain a . or : or will be treated as printer name.
!
!   STATUS [F, out] return status  (0=ok)
!
!   FLAGS [num, in] misc flags
!           &h08000  - landscape
!           &h80000  - preview
!
!   WIDTH [num, in] width of paper in LOENGLISH units (1/100 inch)
!   HEIGHT [num, in] height of paper in LOENGLISH units
!       These dimensions are mainly needed if using TITLE$ or NOTES$, 
!       so that we can come up with a reasonable size and position for 
!       size and position of the image and text.  Include only
!       the printable area, and take into account orientation. 
!       For example, standard letter size paper in portrait should use 
!       WIDTH=800, HEIGHT=1050.  For landscape, reverse those.  If you
!       want a smaller image, reduce them. 
!
!   TITLE$ [str, in] text to display below image 
!
!   NOTES$ [str, in] text to display below image
!
! More Notes:
!   Print file will be named same as image file but with lst extension
! --------------------------------------------------------------------
! {Edit History}                        Edited by
! [100] June 9, 2008 02:31 PM      jdm
!       Created
! --------------------------------------------------------------------

++include ashinc:ashell.def

map1 params
    map2 opsrc,b,1
    map2 opdst$,s,32
    map2 status,f
    map2 flags,b,4
    map2 width,b,2
    map2 height,b,2
    map2 title$,s,160
    map2 notes$,s,512

map1 misc
    map2 imgfile$,s,160
    map2 prtfile$,s,160
    map2 ch,b,2
    map2 x,f
    map2 top,b,2
    map2 bottom,b,2

    on error goto trap

    xgetargs opsrc,opdst$,status,flags,width,height,title$,notes$

    if opsrc # 0 and opsrc # 1 then
        status = -2     ! bad parameter value
        goto return'to'caller
    endif
    
    ! set imgfile$
    if instr(1,opdst$,".")>0 or instr(1,opdst$,"\")>0 then
        imgfile$ = Fn'FQFS$(opdst$)
    else
        imgfile$ = "prscnx.wmf"
    endif

    ! launch netcaller.exe to perform the screen capture, and wait for it
    xcall miamex, MX_SHELLEX, status, &
        "%miame%\bin\netcaller.exe", "", &
        "WINDOWS.SCREENCAPTURE "+str(opsrc) + " " + imgfile$, &
        "", SW_HIDE, 1

    if status = 0 then  ! success
        ch = Fn'NextCh(60000)

        prtfile$ = imgfile$
        x = instr(3,prtfile$,".")
        if x > 0 then
            prtfile$ = prtfile$[1,x] + "lst"
        else
            prtfile$ = prtfile$ + ".lst"
        endif
        open #ch, prtfile$, output
        ? #ch, "//SETMAPMODE,LOENGLISH"

        ! calculate position of image, based on title, notes$
        if title$ # "" then
            ? #ch, "//TEXTOUT, 1,1,";title$
            top = 100
        else
            top = 0
        endif

        if notes$ # "" and height # 0 then
            bottom = height - 100
            ? #ch, "//TEXTOUT,1,";bottom+50;",";notes$
        else
            bottom = 0
        endif

        ? #ch, "//IMAGE,";imgfile$;",1,";str(top);",0,";str(bottom);",8"
        close #ch
        if instr(1,opdst$,".")<1 and instr(1,opdst$,"\")<1 then
            xcall spool, prtfile$, opdst$, flags
        endif
    endif

return'to'caller:
    return(status)
    xputarg 3,status        
    end		

! -------------------------------------------------------------------
trap:
    ? "Error #";err(0);" in PRSCNX.SBX!!!"
    xcall SLEEP,5
    xcall ASFLAG,128	! set ^C in caller
    end			! return to caller


++include sosfunc:fnfqfs.bsi        ! Fn'FQFS$(spec)
++include sosfunc:fnextch.bsi       ! Fn'NextCh(ch)
