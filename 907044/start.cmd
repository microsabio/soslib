:<(Screen Print)

    PRSCNX.BP/SBX  - Capture/print screen (using Firmaware's netcaller.exe)
    APRNTSCRN.BP/SBX - new capture/print screen utility (now considered
                        'standard' with A-Shell). Doesn't require netcaller;
                        includes "ink saver" feature.
    APRNTLOG.BP - Example of how to update aprntscrn.cfg on ATE to have
                        it email the ashlog.log from the server.
>
