SOSFUNC: function directory

Function           File            Description
----------         ------------    ---------------------------------------------------
Fn'AddBase36$      fnbase36.bsi    Add offset to base36 string
Fn'ANSI'to'UTF8$   fnansiutf8.bsi  Convert ANSI string to UTF8 string
Fn'AshNet'Error$   fnashnet.bsi    Convert ASHNET (FTP2,HTTP) err codes to descr
Fn'ATE'SBX         fnatesbx.bsi    Wrapper to execute an SBX remotely on ATE client
Fn'aui'name2id     fnname2id.bsi   Retrieve numeric ID of AUI control by name
Fn'aui'id2name$    fnname2id.bsi   Retrieve name of AUI control by number
Fn'B64'??code'xxx  fnb64.bsi       Base64 encode/decode routines
Fn'Base36ToDec     fnbase36.bsi    Convert base36 to decimal
Fn'BasicAuthHeader$ fnauthdr.bsi   Create HTTP Basic Authorization header
Fn'Bit???$         fnbits.bsi      Various bit field ops: set, clear toggle, test
Fn'Date'To'MM'DD'CCYY$ fndatetime.bsi Convert various inputs to MM/DD/CCYY
Fn'DateTimeOffset$ fndtoffset.bsi  Return formatted date/time offset by days/hours
Fn'Dec2Hex$        fndec2hex.bsi   Convert Decimal notation to hex
Fn'DecB64          fndecb64.bsi    Decimal to Base 64 conversions
Fn'DecToBase36$    fnbase36.bsi    Decimal to Base 36 conversion
Fn'Directory_exp'env$ fndirexp.bsi Expand environment variable within path
Fn'Dir'Scan        fndirscan.bsi   Scan directory tree (using callback func)
Fn'Explode         fnexplode.bsi   Split a string into tokens
Fn'FileAge         fnfileage.bsi   Get age of file 
Fn'FileSock'???    fnfilesock.bsi  Open a file socket for input/output
Fn'FileToStr$      fnfilestr.bsi   Load file into S0 or X0 var
Fn'FileVer$        fnfilever.bsi   Return file version string
Fn'FQFS$           fnfqfs.bsi      Expand spec to fully qualified form
Fn'GCD             fngcd.bsi       Greatest Common Denominator (recursive)
Fn'Get'Switches    fnswitches.bsi  Parse a typical command line and return switches
Fn'Hex2Dec         fnhex2dec.bsi   Convert Hex notation to decimal
Fn'H..'Read'Event  fnfhooklog.bsi  Read event from LOGFIL: file hook
Fn'H..'Event'Descr$     "    "     Return name of hook event
Fn'Hostex          fnhostex.bsi    Wrapper for HOSTEX.SBR 
Fn'HttpGet         fnhttpget.bsi   Get a file from an HTTP server.  (See HTTPGET.BP[908,25])
Fn'ImgErr$         fnimgerr.bsi    Display error msg for AUI_IMAGE code
Fn'Instr           fninstr.bsi     Enhanced version of instr(); supports right-to-left search
Fn'is???           fnistype.bsi    Check if a character is alpha, digit, etc
Fn'Is'Running'???  fnisrun.bsi     Check if program/pid/job running
Fn'LogArchive      fnlogarc        Archive/roll-over log files 
Fn'MAPI'Error$     fnmapi          Convert MAPI error status to description
Fn'MinAshVer       fnminasver.bsi  Check if A-Shell version meets minimum specified level
Fn'MLJ'Load        fnmlistjsn.bsi  Load JSON doc into MLIST
Fn'MLJ'Serialize   fnmlistjsn.bsi  Serialize to file
Fn'MLJ'Traverse         "   "      Traverse a branch of JSON doc with callback
Fn'MLJ'GetNamedParam$   "   "      Return value of JSON item by path
Fn'MLJ'SetNamedParam$   "   "      Change value of JSON item by path
Fn'Mon'To'Num      fndatetime.bsi  Convert month name/abbr to number
Fn'MoveWindow      fnmovewin.bsi   Reposition external app window
Fn'Name'Ext$       fnameext.bsi    Extract name.ext from a complete fspec
Fn'NextCh          fnnextch.bsi    Return next available file channel
Fn'NumInRangeList  fnselrange.bsi  Check if numeric value in list of ranges
Fn'Parse'Date'Time fndatetime.bsi  Validate/parse date and/or time 
Fn'ProgInfo        fnproginfo.bsi  Return program info (mem req, version, etc.)
Fn'PrtTok          fnprttok.bsi    Print string, using tab(x,y) for individual tokens
Fn'PrtUsing$       fnprtusing.bsi  Function equiv to PRINT USING MASK,VAR1,...VARN
Fn'RGB             fnrgb.bsi       Convert R,G,B to single B4 value
Fn'Reverse'B?      fnswap.bsi      Reverse order of bytes (B2-B6)
Fn'SetScrollBack   fnscrlbck.bsi   Show/hide/toggle scroll bar
Fn'SmartScrollBack fnscrlbck.bsi   Activate scrollback if string not visible
Fn'SplitPath$      fnsplitpth.bsi  Split a path into directory and fname
Fn'Sprintf$        fnsprintf.bsi   Create a formatted string (ala C sprintf() function)
Fn'StrInRangeList  fnselrange.bsi  Check if string value in list of ranges
Fn'StrToFile       fnfilestr.bsi   Output string or xvar to file
fn'str'replace$    strrpl.bsi      Replace all occurrences of pattern in string
Fn'String'Replace$ fnstrrpl.bsi    Simple version of fn'str'replace$
fn'str_pad$        fnstrpad.bsi    Pad either/both ends of string with char
Fn'Swap'Words      fnswap.bsi      Swap HW and LW (0123 -> 2301)
Fn'Sync'SBX'to'ATE fnsyncsbx.bsi   Sync an SBX from server to ATE client
Fn'TCPrecv$        fntcprecv.bsi   Receive a string from a TCP channel
Fn'Temp'File'xxx   fntempfile.bsi  Generate (unique) temp file name
Fn'To'Amos         fntoamos.bsi    Convert native spec back to AMOS format
Fn'To'Host$        fntohost.bsi    Convert AMOS-style spec to native path
Fn'URL'Decode$     fnurlenc.bsi    Decode a URL-encoded string to ansi
Fn'URL'Encode$     fnurlenc.bsi    URL encode an ansi string
Fn'UTF8toGDI$      utf8togdi.bsi   Convert UTF8 to GDI entity ref notation
Fn'Val2Bool        fnbool.bsi      Convert 1/0, T/F etc. to .TRUE/.FALSE
Fn'ValMinMax       fnminmax.bsi    Return value limited by both min and max
Fn'VerATE$         fnverate.bsi    Return ATE version string (and components)
Fn'XFLTR'Dlg       fnxfltr.bsi     XTREE-based dialog to input filter criteria
Fn'XFLTR'*         fnxfltr.bsi     Data filter routines related to Fn'XFLTR'Dlg
Fn'XMList'Load'Doc fnmlistxml.bsi  Load/parse xmldoc$ into $xml()
Fn'XMList'Save'Doc fnmlistxml.bsi  Unload xml from Mlist to disk file
Fn'XMList'Get'Item fnmlistxml.bsi  Retrieve element, possible sublist, by key
Fn'XMList'Get'Stats    "    "      Retrieve current status info
Fn'XMList'Walk     fnmlistxml.bsi  Walk doc in memory with callback for each element
Fn'ZT'ESC'Supp     fnztsupp.bsi    Check if ZTERM Dev ESC sequences supported (ZTERM or ATE)
-----------------------------------------------------------------------------
! Functions in uzstrings.fnp:
!
! FN'1char'ucs$(pos) 
!       returns A-Z for 1-26
! FN'1char'lcs$(pos) 
!       returns a-z for 1-26
! FN'abcd'pos(letra$) 
!       returns ordinal position of char in alphabet
! FN'alfa'num$(variavel$,keep'spaces) 
!       removes punct, converts accented to non-accented, ucs
! FN'alfa'num'sign$(variavel$,keep'spaces) 
!       like FN'alfa'num$ but allows some numeric punct chars
! FN'excel'column$(col) 
!       convert col # 1-702 to Excel column code A-ZZ
! FN'excel'col'letter2num(col$,col) 
!       convert excel col code A-ZZ back to number 1-702
! FN'concatenate'if'value$(value$,prefix$,suffix$,zero'as'blank) 
!       conditional concatenation
! FN'count'str(searched'text$,text2search$) 
!       count occurrences of substring
! FN'if'else'value$(value'if$,value'else$,zero'as'blank) 
!       return value'if$ or value'else$ depending on blank/zero value'if$
! FN'if'then'else'value$(value'if$,value'then$,value'else$,zero'as'blank) 
!       return value'then$ or value'else$ depending on blank/zero value'if$
! FN'mid'str$(searched'text$,left'delimiter$,right'delimiter$,left'incr'decr,right'incr'decr) 
!       return middle section of string between two delimters, else left or right part, with offsets
! FN'if'mid'str$(searched'text$,left'delimiter$,right'delimiter$) 
!       return middle section of string between two delimiters if found; else ""
! FN'soma'string(val1,val2) 
!       val1 + val2 (mainly useful for strings with numeric values)
! FN'prepare'search(text$)
!       prepare for multi-substring search
! FN'search'string(text2search$,adjust'text) 
!       carry out multi-substring search
! FN'left'str$(searched'text$,text2search$,incr'decr,nr'of'occurrences) 
!       return left part of string up to nth substring match
! FN'right'str$(searched'text$,text2search$,incr'decr,nr'of'occurrences)
!       return right part of string up to nth substring match
