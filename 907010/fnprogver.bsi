!fnprogver.bsi [103] - get version of a RUN or SBX
!------------------------------------------------------------------------
!EDIT HISTORY
! [103] 06-Sep-23 / jdm / Update/combine with/from fnfilever.bsi; add Fn'Prog'Compare'Versions()
! [102] 20-Feb-23 / jdm / Revise workaround for tricky filenames; misc modernization
! [101] 02-Sep-15 / jdm / Add the ++ifndef, include fnextch
!-------------------------------------------------------------------------
! NOTES:
!   An equivalent function, Fn'FileVer$() in fnfilever.bsi has been 
!   redirected here (see defaliases below)
!
!!KEYWORDS: program version
!
!------------------------------------------------------------------------
!PUBLIC FUNCTIONS
!   Fn'ProgVer$(fspec$,format) - return version of program
!   Fn'Prog'Compare'Versions(ver1$,ver2$) - compare two program versions
!------------------------------------------------------------------------ 
++ifnlbl Fn'ProgVer$()                 ! [102] def INC_PROGVER_
![103] treat the old fnfilever.bsi routines as aliases to versions herein
defalias Fn'FileVer$() = Fn'ProgVer$()
defalias Fn'CompareFileVers() = Fn'Prog'Compare'Versions()
++include'once ashinc:ashell.def         
++include'once sosfunc:fnextch.bsi     ! [101]
!-------------------------------------------------------------------------
!Function: Fn'ProgVer$(progspec$)
!Parameters:
!   progspec$ [string, in] - amos or native spec of program to get ver of
!                             [103] if blank, return current RUN or SBX ver
!   format   (num) [in] - optional format option :          [103]     
!                           0 = M.m{S}(eee){-p} (default)
!                           1 = M.m.p.eee
!Returns:
!   version string (e.g. "1.2A(100)" or 1.2.100 depending on format), or "" if n/a
!Globals:
!   none
!Error handling:
!  none
!Notes:
!   Code basically lifted from DIR.LIT.  (Oddly, there is no
!   XCALL to get the version of a program other than the currently
!   running program)
!
!   requires ++include sosfunc:fnextch.bsi
!
!-------------------------------------------------------------------------
Function Fn'ProgVer$(progspec$ as s260:inputonly, format=0 as b1) as s16
    map1 locals
        map2 ch,f
        map2 vrecord                    ! version record
            map3 vbinary(8),b,1         ! Individual byte
        map2 vreturn,f,6                ! # bytes read
        map2 vmajor,b,1
        map2 vminor,b,1
        map2 vsub,b,1
        map2 vedit,b,2
        map2 vwho,b,1
        map2 dirsep$,s,1            ! [101]
        map2 x,i,2                  ! [103] 
    ! [103] if no progspec$, use current program
    if progspec$ = "" then
        xcall GETVER, .fn
        if format then          ! convert M.m{S}(eee){-p} to M.m.p.eee
            x = instr(1, .fn, "-")
            if x then
                .fn[x;1] = "."
            endif
            x = instr(1,.fn,")")
            if x then
                .fn = .fn[1,x-1] + .fn[x+1,-1]
            endif
            x = instr(1,.fn,"(")
            if x then
                .fn = .fn[1,x-1] + "." + .fn[x+1,-1]
            endif
            x = instr(1, .fn, "[A-Z]", 0)
            if x then
                .fn = .fn[1,x-1] + .fn[x+1,-1]
            endif
        endif
        exitfunction
    endif
     ! check for tricky native filenames which A-Shell might try to
    ! treat as A-Shell filenames, and prepend a ./ to them
    ![102] if len(progspec$)>14 or instr(1,progspec$,"_")>0 then
    if instr(1,progspec$,"_")>0 or instr(1,progspec$," ")>0 then    ! [102] 
        if progspec$[1,1] # "." then
            xcall MIAMEX,MX_DIRSEP,dirsep$
            progspec$ = "." + dirsep$ +progspec$
        endif
    endif
    if (lookup(progspec$) > 0) then
        ch = Fn'NextCh(60000) 
        open #ch,progspec$,input
        xcall GET,vrecord,ch,8,vreturn
        if vreturn = 8 and vbinary(1) >= 254 and vbinary(2) = 255 then
            vmajor = vbinary(4)
            vminor = vbinary(3) and 15
            vsub = int(vbinary(6)/16)
            vedit = (vbinary(6) and 15)*256+vbinary(5)
            vwho = int(vbinary(3)/16)
            .fn = str(vmajor)+"."+str(vminor)
            if (format = 0) then           ! [103]
                if vsub then .fn += chr(64+vsub)
                if vedit then .fn += "(" + str(vedit) + ")"
                if vwho then .fn += "-" + str(vwho)
            else                            ! [103]
                .fn = str(vmajor) + "." + str(vminor) + "." + str(vedit) + "." + str(vwho)  ! [103] 
            endif
        endif
        close #ch
    endif
EndFunction
!-------------------------------------------------------------------------
!Function: 
!   Compare program versions
!Parameters:
!   vernew$ [s30,in] - version of new file (M.m.p.eee) or M.m{s}(eee){-p}
!   verold$ [s30,in] - version of old file (M.m.p.eee) or M.m{s}(eee){-p}
!
!Returns:
!   > 0 if new file has newer version than old file
!   < 0 if new file has older version
!   0 if same
!Notes:
!   Lifted from Fn'Fn'CompareFileVers in fnfilever.bsi
!-------------------------------------------------------------------------
Function Fn'Prog'Compare'Versions(fvernew$ as s30,fvercur$ as s30) as i2
    map1 locals
        map2 fdelim,s,5,".()-"
        map2 rdelim,s,2
        map2 vers(2)
            map3 vmajor,s,3
            map3 vminor,s,4
            map3 vedit,s,5
            map3 vpatch,s,3
    ! split the two version strings into component parts
    ! note that if there is a "sub", it will be appended to vminor
    xcall STRTOK, 0, fvernew$, fdelim, vmajor(1), rdelim, &
        vminor(1), vedit(1), vpatch(1)
    rdelim = ""
    xcall STRTOK, 0, fvercur$, fdelim, vmajor(2), rdelim, &
        vminor(2), vedit(2), vpatch(2)
    .fn = val(vmajor(1)) - val(vmajor(2))
    if .fn = 0 then
        .fn = val(vminor(1)) - val(vminor(2))
        if .fn = 0 then
            if vminor(1) > vminor(2) then
                .fn = 1
            elseif vminor(1) < vminor(2) then
                .fn = -1
            else
                .fn = val(vedit(1)) - val(vedit(2))
                if .fn = 0 then
                    .fn = val(vpatch(1)) - val(vpatch(2))
                endif
            endif
        endif
    endif
End Function
++endif
