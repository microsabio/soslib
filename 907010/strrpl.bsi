!strrpl.bsi [106] - Replace all occurences of the search needle with the replacement needle
!
!PROGRAM strrpl.bsi,1.0(106)
!VEDIT = 106

! Name: strrpl.bsi
! Description: Replace all occurences of the search needle with the replacement needle
! Author: Stephen Funkhouser
! Date: 04-26-2007
!
!----------------------------------------------------------------------------------------------------------------
!{history}
! 10-22-2010 --- 106 --- Stephen Funkhouser
! 1. edit$(string$,40) was causing a needle like "=" replaced w/ " = " to be an infinite loop.
!
! 06-15-2010 --- 105 --- Stephen Funkhouser
! 1. Fix another glitch related to the issue in 104 where replacing " " was failing.
!
! 03-01-2010 --- 104 --- Stephen Funkhouser
! 1. Fix glitch where replacing a string like "( " w/ "(" was failing because A-Shell strips spaces in the comparison.
!	So, we added an additional search v. replace needle string length test to fix this.
!
! 09-22-2009 --- 103 --- Stephen Funkhouser
! 1. Fix glitch which caused an infinite loop.
!	a.  this happened when the replace needle contained the search needle.
! 2. Only preform replace if the search needle and replace needle don't match
!
! 07-23-2009 --- 102 --- Stephen Funkhouser
! Improve Effiecency
!
! 12-18-2008 --- 101 --- Stephen Funkhouser
! Increase argument and return string sizes to 4096 bytes
!
! 04-26-2007 --- 100 --- Stephen Funkhouse
! Original write
!
!----------------------------------------------------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------

++ifndef STRPL_ARYSIZ

  DEFINE STRPL_ARYSIZ	= 10
  DEFINE STRPL_STRSIZ = 4096

++IFNDEF str'replace_struct
DEFSTRUCT str'replace_struct
	MAP2 needle$	,s,512
ENDSTRUCT
++ENDIF


FUNCTION fn'str'replace$(hsearch'needle$,hreplace'needle$,string$ as x STRPL_STRSIZ) as x STRPL_STRSIZ

MAP1 locals
	MAP2 x	,f,8
	MAP2 p	,f,8,1
	MAP2 search'array
		MAP3 search'rec(STRPL_ARYSIZ),str'replace_struct
	MAP2 replace'array
		MAP3 replace'rec(STRPL_ARYSIZ),str'replace_struct
	MAP2 ct	,f,8

	XGETARG 1,search'array
	XGETARG 2,replace'array

	string$ = edit$(string$,40)
!trace.print "fn'str'replace$: string$=["+string$+"] LEN(string$)=["+LEN(string$)+"] "
	FOR x = 1 TO STRPL_ARYSIZ
!trace.print "fn'str'replace$: search'rec.needle$("+x+")=["+search'rec.needle$(x)+"] "

		![102] if search'rec.needle$(x) array pos is null
		! string then assume all the rest are as well
		![105] add len() test because a-shell strips spaces in string comparisons
		IF (search'rec.needle$(x) = "") AND (LEN(search'rec.needle$(x)) = 0) THEN
			EXIT
		ENDIF

		![104]
!trace.print "fn'str'replace$: x=["+x+"] len'srch=["+LEN(search'rec.needle$(x))+"] len'rpl=["+LEN(replace'rec.needle$(x))+"] search'rec.needle$=["+search'rec.needle$(x)+"] replace'rec.needle$=["+replace'rec.needle$(x)+"]"
		IF (search'rec.needle$(x) # replace'rec.needle$(x)) OR ((LEN(search'rec.needle$(x)) # LEN(replace'rec.needle$(x)))) THEN
			IF (p = 0) THEN p = 1
!trace.print "fn'str'replace$: 1: p=["+p+"] LEN(string$)=["+LEN(string$)+"] string$=["+string$+"] "

			p = INSTR(p, string$, search'rec.needle$(x))
!trace.print "fn'str'replace$: 2: p=["+p+"] "
			IF p > 0 THEN
				string$ = string$[1;(p - 1)] + replace'rec.needle$(x) + string$[(p+(LEN(search'rec.needle$(x)))),-1]

				IF p < LEN(string$) THEN
!					p = p + LEN(edit$(replace'rec.needle$(x),40))         ! [102]
					p = p + LEN(replace'rec.needle$(x))                   ! [106]
					x = x - 1
				ELSE
					p = 1
				ENDIF
			ENDIF
		ENDIF

		ct = ct + 1
		IF (ct > 100) THEN
			EXIT
		ENDIF

	NEXT x

	fn'str'replace$ = edit$(string$,40)

ENDFUNCTION

++endif