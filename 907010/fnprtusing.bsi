!fnprtusing.bsi [101] - function version of PRINT USING MASK,VAR1,...VARN
!-----------------------------------------------------------------------
! [101] 06-Sep-23 / jdm / add keywords (for funcidx)
! [100] 19-Jan-23 / jdm / created
!-----------------------------------------------------------------------
!Keywords: formatting presentation masks
!
!Functions:
!   Fn'PrtUsing$(mask$,var1,...varN) - equiv to PRINT USING mask$,var1...varN
!				except returns the string
!------------------------------------------------------------------------
++ifndef INC_PRTUSING_BSI     ! skip if already included
define INC_PRTUSING_BSI = 1
++include'once ashinc:ashell.def
++include'once ashinc:regex.def
++include'once sosfunc:fnistype.bsi
define MAX_PRTUSING_FIELDS  = 28	! max # fields in the mask
define MAX_PRTUSING_MASKLEN = 260	! max overall length of mask
define MAX_PRTUSING_FLDLEN  = 80	! max length of individual field 
!---------------------------------------------------------------------
!Function:
!   simulate PRINT USING MASK, returning result instead of printing it
!Params:
!   mask$  (str) [in] - mask
!	f1$...f28$ (str) [in] - variables to insert into mask
!Returns:
!	formatted string
!Globals:
!Notes:
!	We do not support the "n" character at all (it changes the width
!		of the field and thus requires fancier re-insertion, but 
!		could be added later)
!	Performance of this routine is a bit less than the built-in
!	PRINT USING but not bad - only about 25% slower in a typical case.
!---------------------------------------------------------------------
Function Fn'PrtUsing$(mask$ as s160:inputonly, &
					 f1$ as s MAX_PRTUSING_FLDLEN:inputonly, f2$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f3$ as s MAX_PRTUSING_FLDLEN:inputonly, f4$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f5$ as s MAX_PRTUSING_FLDLEN:inputonly, f6$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f7$ as s MAX_PRTUSING_FLDLEN:inputonly, f8$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f9$ as s MAX_PRTUSING_FLDLEN:inputonly, f10$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f11$ as s MAX_PRTUSING_FLDLEN:inputonly, f12$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f13$ as s MAX_PRTUSING_FLDLEN:inputonly, f14$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f15$ as s MAX_PRTUSING_FLDLEN:inputonly, f16$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f17$ as s MAX_PRTUSING_FLDLEN:inputonly, f18$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f19$ as s MAX_PRTUSING_FLDLEN:inputonly, f20$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f21$ as s MAX_PRTUSING_FLDLEN:inputonly, f22$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f23$ as s MAX_PRTUSING_FLDLEN:inputonly, f24$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f25$ as s MAX_PRTUSING_FLDLEN:inputonly, f26$ as s MAX_PRTUSING_FLDLEN:inputonly, &
					 f27$ as s MAX_PRTUSING_FLDLEN:inputonly, f28$ as s MAX_PRTUSING_FLDLEN:inputonly) as s MAX_PRTUSING_MASKLEN
						 
	map1 locals
		map2 f$(MAX_PRTUSING_FIELDS),s,MAX_PRTUSING_FLDLEN,@f1$		! convenient overlay of arguments
		map2 x,i,2
		map2 x2,i,2
		map2 f,f
		map2 length,b,2
		map2 masklen,i,2
		map2 maskchar$,s,1
		map2 fno,i,2
	
	! example masks
	!"###### \--PRODUCT DEC\######, ########,.# \--------\\-CUSTOME-\ \\/\\/\\ \---\"
	! scan data mask locating the starting position and length of each column
	.fn = mask$				! start with the raw mask
	masklen = len(mask$)
	x = 1
	do while x <= masklen
		
		! find the start of the next field
		x = instr(x,mask$,"[!#.\\$N]",0)			! regex search for first ! # . \ N or $
		if x = 0 exit
		maskchar$ = mask$[x;1]
	
		fno += 1
		!debug.print "fno="+fno+", x="+ x + ", maskchar$= "+ maskchar$
		if fno > MAX_PRTUSING_FIELDS exit
		
		switch maskchar$
			
			case "!"				! single char field
				.fn[x;1] = f$(fno)
				exit
				
			case "\"				! string up to next \
				x2 = instr(x+1,mask$,"\")
				if x2 = 0 then					! if no trailing \, assume one at end of mask
					x2 = -1
				endif
				.fn[x,x2] = pad(f$(fno),x2-x+1)
				x = x2 max x
				exit
				
			case "."			! ok as leading char if followed by # or N
				maskchar$ = mask$[x+1;1]
				if maskchar$ # "#" and maskchar$ # "N" then		! ignore stranded dot
					fno -= 1
					exit
				endif
				! else fall thru to handle as numeric mask
			case "N"						! like # provided preded by non alpha and not followed by alpha
				if maskchar$ = "N" then		! (ignore if falling thru from .)
					if x > 1 then
						maskchar$ = mask$[x-1;1]
					else
						maskchar$ = ""
					endif
					if fn'isalpha(maskchar$) then			! if preceding char is alpha
						fno -= 1							! then treat this N as a literal
						exit
					elseif instr(x+1,mask$,"[ABCDEFGHIJKLMOPQRSTUVWXYZ]",PCRE_CASELESS) then ! or followed by alpha (except N)
						fno -= 1
						exit
					endif
				endif
			case "#"
			case "$"
				x2 = instr(x+1,mask$,"[^#.\-$,N]",0) 	! regex search for first char not in the clase of #.-$,Nn
				f = f$(fno)
				.fn[x,x2-1] = f using mask$[x,x2-1]
				x = (x2 - 1) max x
				exit
				
			default					! not sure what happened here but don't count it as a field
				fno -= 1
				exit
		endswitch
		x += 1
	loop
	
EndFunction
++endif
