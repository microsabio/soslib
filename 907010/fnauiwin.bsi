! fnauiwin - Various wrappers around AUI_WINDOW 
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 12-Dec-18 / jdm / created
!------------------------------------------------------------------------
!ROUTINES:
! Fn'AuiWin'Ctl'Size'Get(ctlid,width,height,flags) - get ctl height,width 
!------------------------------------------------------------------------
!REQUIREMENTS:
!------------------------------------------------------------------------
 
++ifndef INC_FNAUIWIN_BSI_

define INC_FNAUIWIN_BSI_ = 1

++include'once ashinc:ashell.def
++include'once ashinc:types.def
++include'once ashinc:dynlib.def
++include'once ashinc:dynlib.bsi

! Flags 
define AWF_PIX          = &h0000        ! coordinates in pixels
define AWF_ALTPOS_MILLI = &h0001        ! coordinates in altpos milli-units
define AWF_GRID_MILLI   = &h0002        ! coordinates in main window grid milli-units
define AWF_UNIVERSAL    = &h0004        ! coordinates in universal units
define AWF_CLIENT       = &h0008        ! get client area (rather than entire window)
!---------------------------------------------------------------------
!Function:
!   Return size of specified control
!Params:
!   ctlid (str or num) [in] - ID of target window or control
!   width (num) [out] - width of control
!   height (num) [out] - height of control
!   flags (num) [in] - see AWF_xxx
!Returns:
!   1 for success
!   0 if target window not located
!   <0 for errors (invalid platform, ...)
!Globals:
!   none
!Notes:
!---------------------------------------------------------------------
Function Fn'AuiWin'Ctl'Size'Get(ctlid as s24:inputonly, width as b4:outputonly, &
                       height as b4:outputonly, flags as b4:inputonly) as i4
            
    map1 locals
        map2 wl,i,4
        map2 wt,i,4
        map2 wr,i,4
        map2 wb,i,4
        map2 wrows,i,4
        map2 wcols,i,4
        map2 resx,i,4
        map2 resy,i,4
        map2 op,b,4

    if flags and AWF_CLIENT
        op = SW_QRYCTLPIXCLI
    else
        op = SW_QRYCTLPIX
    endif
    
    xcall AUI, AUI_WINDOW, op, wl,wt,wr,wb, wrows,wcols, -1,-1,  &
        ctlid, resx, resy

    ! how do we check for errors?
    height = wb - wt + 1
    width = wr - wl + 1
    
    ! convert from pixels to something else ...
    if flags and AWF_ALTPOS_MILLI then
        xcall AUI,AUI_WINDOW,SW_QUERY,HALTGRID,VALTGRID,HALTRES,VALTRES, &
            HDLGOVERHEAD,VDLGOVERHEAD,S'TOPSTS,S'BOTSTS,CID_MON1,HRES,VRES, &
            MONITORS
        
!AUIWIN sample...
!>!    ! [102]  First, lets get the screen res...(CID=-1, i.e. CID_MON1)
!>!    xcall AUI,AUI_WINDOW,SW_QUERY,HALTGRID,VALTGRID,HALTRES,VALTRES, &
!>!        HDLGOVERHEAD,VDLGOVERHEAD,S'TOPSTS,S'BOTSTS,CID_MON1,HRES,VRES, &
!>!        MONITORS  ! [102][105][107]
!>!
!>!    ![105] ? "Ignoring task bar..."
!>!
!>!    ? "# of monitors: ";MONITORS          ! [105]
!>!    if MONITORS = 1 then                    ! [105]
!>!        ? "Current Screen Res:  ";HRES;"x";VRES;" (pixels)"
!>!    else
!>!        ? "Primary Monitor Res: ";HRES;"x";VRES;" (pixels)"
!>!    endif
!>!    ? "                     ";HALTRES;"x";VALTRES;" (dialog altpos units)" ![103]
!>!    ? "Dialog altpos unit:  ";HALTGRID;"x";VALTGRID;" (pixels)" ![103]
!>!    ? "Dialog overhead (caption,mgn,bdr): ";HDLGOVERHEAD;"x"; &
!>!        VDLGOVERHEAD;" (pixels) "
    
    
    
    xputarg @height
    xputarg @width
    
EndFunction


!---------------------------------------------------------------------
!Function:
!   Reposition/size an external app window (SetWindowPos)
!Params:
!   whdl (b4) [in/out] - window handle of window to move (* see notes)
!   wtitle$ (str) [in/out] - title of window to move (* see notes)
!   wclass$ (str) [in/out] - class of window to move (* see notes)
!   x,y (num) [in] - upper left,top coordinate (in pixels); ignore if SWP_NOMOVE set
!   width,height [in] - if either 0, or SWP_NOSIZE set, don't resize
!   flags (num) [in] - SWP_xxx flags for SetWindowPos()
!   insertafter (i4) [in] - HWND_xxx flags for SetWindowPos(); ignnore if SWP_NOOWNERZORDER
!Returns:
!   1 for success
!   0 if target window not located
!   <0 for DYNLIB errors
!   >1 for Windows system errors - use MX_ERRNOMSG to translate
!Globals:
!   none
!Notes:
!   * Target window must be identified by either its handle (whdl),
!       in which case the wtitle$ and wclass$ can be left blank, 
!       or by the wtitle$ and/or wclass$. If window found, all three
!       are updated/returned.
!   See Fn'FindWindow() for notes on popular and special window classes
!---------------------------------------------------------------------
Function Fn'SetWindowPos(whdl=0 as b4, wtitle$="" as s100, wclass$="" as s100, &
                       x=0 as b4:inputonly, y=0 as b4:inputonly, &
                       width=0 as b4:inputonly, height=0 as b4:inputonly, &
                       flags=0 as b4:inputonly, insertafter=-3 as i4:inputonly) as i4
             
    map1 locals
        map2 bOk,i,2

    whdl = Fn'FindWindow(wtitle$, wclass$, whdl)
    debug.print "Fn'SetWindowPos whdl = "+Fn'Dec2Hex$(whdl)
    
    if whdl = 0 then
        exitfunction            ! window not found
    endif

    if width=0 or height=0 then
        flags = flags or SWP_NOSIZE
    endif
    
    if insertafter = -3 then
        flags = flags or SWP_NOZORDER
    endif
    
    Fn'SetWindowPos = fn'dyn'load'dll("USER32.DLL")
    
    if Fn'SetWindowPos = 0 then           ! if load succeeded

        if flags and SWP_MINIMIZE then      ! special case - uses ShowWindow instead of SetWindowPos
            flags = SW_SHOWMINNOACTIVE      ! minimize and don't activate
        elseif flags and SWP_MAXIMIZE then  ! special case - uses ShowWindow instead of SetWindowPos
            flags = SW_SHOWMAXIMIZED        ! maximize
        elseif flags and SWP_RESTORE then   ! special case - uses ShowWindow instead of SetWindowPos
            flags = SW_RESTORE              ! restore to normal
        endif
        
        if (flags = SW_SHOWMINNOACTIVE or flags = SW_SHOWMAXIMIZED or flags = SW_RESTORE) then
            !    BOOL WINAPI ShowWindow(
            !	_In_  HWND whdl,
            ! 	_In_  int  nCmdShow) 

            xcall DYNLIB, dlctl, "ShowWindow(ii)B", whdl, flags, bOk
            
        else        ! normal SetWindowPos()

            ! (WIN32 API from USER32.DLL...)
            ! BOOL WINAPI SetWindowPos(
            !	_In_  HWND whdl,
            !	_In_  HWND insertafter,
            !	_In_  int  x,
            !	_In_  int  y, 
            ! 	_In_  int  width, 
            ! 	_In_  int  height, 
            ! 	_In_  UINT flags)

            xcall DYNLIB, dlctl, "SetWindowPos(iiiiiii)B", whdl, insertafter, x, y, width, height, flags, bOk
            debug.print "SetWindowPos("+Fn'Dec2Hex$(whdl)+","+insertafter+","+x+","+y+","+width+","+height+","+flags+") = "+bOk
            debug.print "dlctl.status = "+dlctl.status+", syserr="+dlctl.syserr
        endif
        
        if bOk then
            Fn'SetWindowPos = 1
        else
            Fn'SetWindowPos = -100        ! SetWindowPos returned error - may be overridden below
        endif
        
        if dlctl.status then
            Fn'SetWindowPos = dlctl.status
        elseif dlctl.syserr then
            Fn'SetWindowPos = dlctl.syserr
        endif

        call fn'dyn'unload'dll()

        xputarg @whdl
        xputarg @wtitle$
        xputarg @wclass$

    endif
    debug.print "Fn'SetWindowPos returns "+Fn'SetWindowPos
EndFunction

!---------------------------------------------------------------------
!Function:
!   Locate a window by its title, class, and/or handle
!Params:
!   wtitle$  (str) [in/out] - window title
!   wclass$  (str) [in/out] - window class 
!                             Known and Special classes:
!                               Internet Explorer 11 : IEFrame
!                               Explorer (Win10) : CabinetWClass
!                               Firefox : <unknown>
!                               Chrome : Chrome_WidgetWin_1 (note: if title="", use parent which is same class)
!                               A-Shell : MIAMEWClass
!                               Excel 97 - 2016 : XLMAIN
!                               LibreOffice : SALFRAME
!                               $BROWSER - special pseudo-class which tries to locate any of the above browsers
!                                   (Currently only works for IE and Chrome)
!                               $XLS - special pseudo-class which tries to locate any of the above spreadsheet handlers
!                                   (Currently only works for Excel 2016 and LibreOffice)
!
!   whdl (b4) [in/out] - window handle
!Returns:
!   window handle if found, else 0
!Module vars:
!   may load USER32 library in which case dlctl will be updated
!Notes:
!   If whdl is specified, wtitle$ and wclass$ are ignored on input.
!   Otherwise searches for first window matching the wtitle$ and/or wclass$
!   If found, all params updated based on found window
!---------------------------------------------------------------------
Function Fn'FindWindow(wtitle$ as s100, wclass$ as s100, whdl as b4) as b4

    map1 locals
        map2 bOk,i,2
        
    if wclass$ = "$BROWSER" then        ! check for any known browser
        wclass$ = "IEFrame"
        xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
        if whdl = 0 then
            wclass$ = "Chrome_WidgetWin_1"
            xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
        endif
    elseif wclass$ = "$XLS" then        ! check for spreadsheet handlers
        wclass$ = "XLMAIN"              ! Excel 97 - 2016
        xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
        if whdl = 0 then
            wclass$ = "SALFRAME"        ! LibreOffice
            xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
        endif
    else
        xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
    endif

    debug.print "Fn'FindWindow: wclass$ = "+wclass$ +", whdl = "+Fn'Dec2Hex$(whdl)
    if whdl then
        ! handle special case of Chrome, where found window may not be the
        ! top level and we need to get its parent
        if wclass$ = "Chrome_WidgetWin_1" and wtitle$ = "" then  ! if no title, assume not top window
            if fn'dyn'load'dll("USER32.DLL") = 0 then
                ! HWND WINAPI GetParent(
                !    _In_ HWND hWnd);
                xcall DYNLIB, dlctl, "GetParent(i)i", whdl, whdl
                debug.print "Fn'FindWindow: GetParent = "+Fn'Dec2Hex$(whdl)
                if whdl = 0 then                ! if no parent, go back to prev win
                    xcall MIAMEX, MX_FINDWINDOW, whdl, wtitle$, wclass$
                endif
            endif
        endif
    endif
    
    if whdl then
        Fn'FindWindow = whdl
        xputarg @wtitle$
        xputarg @wclass$
        xputarg @whdl
    endif
EndFunction


!---------------------------------------------------------------------
!Function:
!   Call the FileOpenModeless routine 
!Params:
!Returns:
!   1 for success
!   0 if target window not located
!   <0 for DYNLIB errors
!   -100 MoveWindow returned an error
!Globals:
!   none
!Notes:
!---------------------------------------------------------------------
Function Fn'FileOpenModeless(folder$ as s0:inputonly, flags as b4:inputonly) as i2
             
    map1 locals
        map2 bOk,i,2

    if fn'dyn'load'dll("ashwin") = 0 then
        ? "ashwin.dll loaded"
    
        dlctl.opcode = DCOP_CALL
        xcall DYNLIB, dlctl, "ASHWIN_FileOpenModeless(Wi)i", folder$, flags, Fn'FileOpenModeless
        ? "ASHWIN_FileOpenModeless returns ";Fn'FileOpenModeless
        ? "status,syserr = ";dlctl.status;dlctl.syserr
        stop
        call fn'dyn'unload'dll()
    endif

EndFunction


!---------------------------------------------------------------------
!Function:
!   Load a Windows DLL via DYNLIB
!Params:
!   libname$  (str) [in] - name of lib to load (e.g. USER32.DLL)
!Returns:
!   0 on success, else error code
!Module vars:
!   dlctl
!Notes:
!   dlctl.opcode gets set to DCOP_CALL on exit
!   dlctl.handle set non-zero on success
!   if libname$ same as last load and dlctl.handle # 0 then assume
!   already loaded and do nothing.
!---------------------------------------------------------------------
Function fn'dyn'load'dll(libname$ as s64:inputonly) as i4

    if libname$ # last'lib'loaded$ or dlctl.handle = 0 then
        
        dlctl.opcode = DCOP_LOAD
        xcall DYNLIB, dlctl, libname$, DLCC_C_X86_WIN32_STD

        if dlctl.syserr then
            fn'dyn'load'dll = dlctl.syserr
        elseif dlctl.status then
            fn'dyn'load'dll = dlctl.status
        else
            dlctl.opcode = DCOP_CALL
            last'lib'loaded$ = libname$
        endif
    endif
    if fn'dyn'load'dll = 0 then
        dlctl.opcode = DCOP_CALL
    endif
    debug.print "fn'dyn'load'dll("+libname$+") = "+fn'dyn'load'dll+", dcop = "+dlctl.opcode
EndFunction

!---------------------------------------------------------------------
!Function:
!   Unload the currently loaded DLL
!Params:
!   none
!Returns:
!   0 on success, else error code
!Module vars:
!   dlctl
!Notes:
!   if dlctl.handle # 0, then unload it and set it to zero
!---------------------------------------------------------------------
Function fn'dyn'unload'dll(libname$ as s64:inputonly) as i4

    if dlctl.handle then
        
        dlctl.opcode = DCOP_UNLOAD
        xcall DYNLIB, dlctl
        dlctl.handle = 0
        
        if dlctl.syserr then
            fn'dyn'unload'dll = dlctl.syserr
        elseif dlctl.status then
            fn'dyn'unload'dll = dlctl.status
        else
            dlctl.opcode = DCOP_LOAD
        endif
    endif
    
EndFunction


++endif
