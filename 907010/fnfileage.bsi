!fnfileage [105] - functions related to file age 
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [105] 13-Jan-25 / jdm / Add Fn'FileAgeDate$(file$,flags) - display file date
! [104] 22-Jun-19 / jdm / Adjust debug.print statements 
! [103] 26-Feb-18 / jdm / Add Fn'FileAgeDiff(file1,file2,flags) - age diff in secs;
!                           ++include fnextch.bsi
! [102] 23-Sep-15 / jdm / Add :inputonly, minor modernization
! [101] 23-Oct-14 / jdm / Add ++ifndef
! [100] 10-Sep-07 / jdm / Create Fn'FileAge(file,flags) - age in sec
!----------------------------------------------------------------------
!ROUTINES:
! Fn'FileAge(file$, flags) - return age of file
! Fn'FileAgeDiff(file1$,file2$,flags) - file1 age minus file2 age (secs)
! Fn'FileAgeDate$(file$,flags) - display file date 
!---------------------------------------------------------------------
!Keywords: date, time, file
!---------------------------------------------------------------------
!Also see:
!   fndatetime.bsi (general date/time routines)
!   fndtoffset.bsi (date offset by days/hours)
!---------------------------------------------------------------------

++ifndef FILEAGE_MTIME

++include'once sosfunc:fnextch.bsi

  ! Fn'FileAge flags
  define FILEAGE_MTIME  = &h0000        ! use last mod time
  define FILEAGE_CTIME  = &h0001        ! use create time
  define FILEAGE_REMPC  = &h0002        ! file(s) reside(s) on remote ATE PC

!---------------------------------------------------------------------
!Function:
!   Return age of file (in secs)
!Params:
!   Filespec [string, in] - AMOS or native spec of file
!   Flags [num, in, optional] : which date to use
!        FILEAGE_MTIME = last modify time (default)
!        FILEAGE_CTIME = create time
!        FILEAGE_REMPC = file resides on remote (ATE) PC
!Returns:
!   # secs since file last modified (or created)
!   (-1 if file not found)
!   (-2 if file on remote pc and you don't have ATE)
!Globals:
!   No global vars
!Locals:
!   No other local vars
!Error handling:
!   Traps errors, returns err(0) value as status code
!Notes:
!   Theoretically we could figure this out without creating a
!   reference file, but the code shown below to do so is off
!   by what appears to be a GMT offset.  Since I'm not sure
!   how to determine that for the local filesystem, we take the
!   easy way out and create a reference file to get the "now"
!   time.  To avoid having to do this every time (especially
!   in progs like DIR which might do it repetitively, we use
!   a pair of static vars to keep track of our reference time.)
!---------------------------------------------------------------------
Function Fn'FileAge(fspec$ as s260:inputonly, flags as b4:inputonly) as i4

    Static map1 s_midnight,b,4        ! reference time of prev midnight
    Static map1 s_today,b,4           ! reference day

    map1 locals
        map2 locrem$,s,1
        map2 bytes,f
        map2 mtime,b,4
        map2 ctime,b,4
        map2 now,b,4
        map2 ch,b,2
        map2 nowfile$,s,16,"fnfileage.tmp"

    if (flags and FILEAGE_REMPC)
        locrem$ = "R"
    else
        locrem$ = "L"
    endif

    ! due to some confusion about how to calibrate the local file times,
    ! just take the easy way out and create a file now in order to get
    ! the "now" time
    if (s_midnight=0 or s_today#DATE) then
        ch = Fn'NextCh(62000)
        open #ch,nowfile$,output
        close #ch
        xcall MIAMEX, MX_FILESTATS, "L", nowfile$, bytes, now
        kill nowfile$
        s_midnight = now - TIME
        s_today = DATE
        debug.print (99, "fnfileage") "Reference time: now=["+now+"], midnight=["+s_midnight+"], today=["+s_today+"]"
    else
        now = s_midnight + TIME ! update now value based on seconds clock
    endif

    xcall MIAMEX, MX_FILESTATS, locrem$, fspec$, bytes, mtime, ctime
    debug.print (99, "fnfileage") "stats on "+fspec$+" - bytes: "+bytes+", mtime: "+mtime+", ctime: "+ctime

    if (flags and FILEAGE_CTIME) then
        mtime = ctime
    endif

! Alternate method to calculate now
!    if now = 0 then
!        ! get 'now' time
!        now = DATE
!        DEBUG.PRINT "today (sep): now=["+now+"] "
!        xcall DSTOI, now    ! convert from sep to days since 1/1/00
!        DEBUG.PRINT "dstoi: now=["+now+"] "
!        now = now - 25567   ! subtract 1/1/1970 value
!        DEBUG.PRINT "since epoch: now=["+now+"] "
!        now = now * 86400   ! convert days to secs
!        DEBUG.PRINT "secs: now=["+now+"] "
!        now = now + TIME    ! add secs since midnight
!        DEBUG.PRINT "time=["+time+"], +time: now=["+now+"] "
!
        ! add GMT factor? (7 hours for me???)
        !    now = now + 25200
!    endif

    if bytes >= 0 then
        Fn'FileAge = now - mtime
    else
        Fn'FileAge = bytes
    endif

EndFunction


!---------------------------------------------------------------------
!Function:
!   Fn'FileAgeDiff - return age diff between two files (in secs)
!Params:
!   file1$  (str) [in] - first file
!   file2$  (str) [in] - second file
!   flags [num, in, optional] : which date to use
!        FILEAGE_MTIME = last modify time
!        FILEAGE_CTIME = create time
!        FILEAGE_REMPC = files reside on remote (ATE) PC
!   filecount (num) [out] - # of files found (0, 1 or 2)
!                           -1 indicates no ATE support for FILEAGE_REMPC
!Returns:
!   file1 date minus file2 date (if > 0, file1 newer than file2)
!   Returns 0 if both files missing (see filecount arg)
!Globals:
!Notes:
!   A missing file is treated as having date zero, so if file1 exists
!   and file2 is missing, return value will be the mtime (or ctime)
!   of file1. In the reverse case, it will be negative mtime or ctime.
!   If both are missing, return value is 0. Use filecount arg to 
!   check for that condition if you care.
!---------------------------------------------------------------------
Function Fn'FileAgeDiff(fspec1$ as s260:inputonly, fspec2$ as s260:inputonly, &
                        flags as b4:inputonly, filecount as i2:outputonly) as i4
    map1 locals
        map2 locrem$,s,1
        map2 bytes1,f
        map2 bytes2,f
        map2 mtime1,b,4
        map2 ctime1,b,4
        map2 mtime2,b,4
        map2 ctime2,b,4

    if (flags and FILEAGE_REMPC)
        locrem$ = "R"
    else
        locrem$ = "L"
    endif

    xcall MIAMEX, MX_FILESTATS, locrem$, fspec1$, bytes1, mtime1, ctime1
    debug.print (99, "fnfileage") "stats on "+fspec1$+" - bytes: "+bytes1+", mtime: "+mtime1+", ctime: "+ctime1

    if (bytes1 = -2) then           ! no ATE support for FILEAGE_REMPC
        filecount = -1
        Fn'FileAgeDiff = 0
        exitfunction
    endif

    xcall MIAMEX, MX_FILESTATS, locrem$, fspec2$, bytes2, mtime2, ctime2
    debug.print (99, "fnfileage") "stats on "+fspec2$+" - bytes: "+bytes2+", mtime: "+mtime2+", ctime: "+ctime2

    filecount = 2
    if (flags and FILEAGE_CTIME) then
        mtime1 = ctime1
        mtime2 = ctime2
    endif
    if (bytes1 < 0)
        mtime1 = 0
        filecount -= 1
    endif
    if (bytes2 < 0)
        mtime2 = 0
        filecount -= 1
    endif

    Fn'FileAgeDiff = mtime1 - mtime2
    xputarg @filecount
    debug.print (99, "fnfileage") "Fn'FileAgeDiff("+fspec1$+","+fspec2$+") ="+Fn'FileAgeDiff
EndFunction

!---------------------------------------------------------------------
!Function:
!   Return formatted file date
!Params:
!   Filespec [string, in] - AMOS or native spec of file
!   Flags [num, in, optional] : which date to use
!        FILEAGE_MTIME = last modify time (default)
!        FILEAGE_CTIME = create time
!Returns:
!   "Sun Jul 31 16:02:41 2011"
!Globals:
!   No global vars
!Locals:
!   No other local vars
!Error handling:
!   Traps errors, returns err(0) value as status code
!Notes:
!---------------------------------------------------------------------
Function Fn'FileAgeDate$(fspec$ as s260:inputonly, flags as b4:inputonly) as s24

    map1 locals
        map2 bytes,i,4
        map2 mtime,b,6
        map2 ctime,b,6
        
    xcall MIAMEX, MX_FILESTATS, "L", fspec$, bytes, mtime, ctime
    if flags and FILEAGE_CTIME then
        xcall MIAMEX, MX_FTFORMAT, ctime, .fn
    else
        xcall MIAMEX, MX_FTFORMAT, mtime, .fn
    endif
endfunction

++endif
