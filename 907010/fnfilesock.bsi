!fnefilesock.bsi [100] - open a file socket
!-------------------------------------------------------------------------
!Edit History
!VEDIT=100
![100] 01-Sep-16 01:38       Edited by Jack
!    Created   
!-----------------------------------------------------------------------------
!Functions:
! Fn'FileSock'In(channel) - open an input file socket
! Fn'FileSock'Out(channel) - open an output file socket
!-----------------------------------------------------------------------------
!Notes:
!   "File Sockets" (6.3.1524+) allow TCP socket input/output using sequential
!   file I/O syntax. (Syntactic simplification over TCPX.SBR calls, although
!   you do have to use TCPX.SBR to open the socket.)  To open a file socket,
!   the steps are:
!
!   1. Open a dummy file channel (using the special .NULL file spec)
!   2. Open the TCP socket.
!   3. Use MX_FILESOCK to bind the socket to the channel
!   4. Use PRINT #ch or INPUT #ch to perform I/O over the socket
!   5. Close the file channel (which also closes the socket)
!
!For simple test/demo (see routine at end of moudule) ...
!   .COMPIL FNFILESOCK.BSI/X:2/C:FNFILESOCK_BSI_TEST=1 
!   .RUN FNFILESOCK
!-----------------------------------------------------------------------------

++ifndef INC_FNFILESOCK_
  define INC_FNFILESOCK_ = 1    

++include'once ashinc:ashell.def
++include'once ashinc:types.def    
++include'once sosfunc:fnextch.bsi

! flags
define FSF_INPUT  = &h0001      ! (else output)

!---------------------------------------------------------------------
!Function:
!   Open a file socket for output or output
!Params:
!   ch  (num) [in] - Output channel to use. If non-zero and file is 
!                    already open on that channel, we skip the open
!                    just do the socket open / bind. If zero, we 
!                    assign a file channel > 50000 and open it.
!
!   socket (T_SOCKET) [in/out] - Optional socket.  If specified and non-zero,
!                    must be an open TCP socket, which we then just 
!                    bind to the channel. Otherwise we create the 
!                    socket first.
!
!   server$ (T_HOSTNAME) [in] - hostname:port of server 
!                   (needed if socket needs to be opened)
!
!   flags (T_BITFLAGS16) [in] -
!                   FSF_INPUT indicates input channel (else output)
!Returns:
!   if > 0 : open file channel, else error code
!Globals:
!Notes:
!   wrapper/front-end for Fn'File'Sock'In'Out which does both
!   input and output file sockets
!---------------------------------------------------------------------

!---------------------------------------------------------------------
!Function:
!   Open a file socket for input or output
!Params:
!   ch  (num) [in] - Output channel to use. If non-zero and file is 
!                    already open on that channel, we skip the open
!                    just do the socket open / bind. If zero, we 
!                    assign a file channel > 50000 and open it.
!
!   socket (T_SOCKET) [in/out] - Optional socket.  If specified and non-zero,
!                    must be an open TCP socket, which we then just 
!                    bind to the channel. Otherwise we create the 
!                    socket first.
!
!   server$ (T_HOSTNAME) [in] - hostname:port of server (needed if socket
!                    needs to be opened; only port matters for input mode)
!
!Returns:
!   if > 0 : open file channel, else error code
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'FileSock(ch as i4:inputonly, socket as T_SOCKET, &
                     server$ as T_HOSTNAME:inputonly, &
                     flags as T_BITFLAGS16:inputonly) as i4
    
    map1 locals
        map2 status,i,4
        map2 port,T_PORT
        map2 x,i,2
        map2 sock'opened,BOOLEAN
        
    if ch = 0 then                      ! if no channel specified
        ch = Fn'NextCh(50000)           ! use first unused ch >= 50000
    endif
    
    if eof(ch) = -1                     ! if ch not open
        if (flags and FSF_INPUT) then     
            open #ch, .NULL, input      ! open it for input
        else
            open #ch, .NULL, output     ! open it for output
        endif
    endif
        
    if socket = 0 then                  ! if no socket specified, open one
        x = instr(1,server$,":")
        if x then
            socket = server$[x+1,-1]
            server$ = server$[1,x-1]
        endif
        debug.print "server$ ["+server$+"], x="+x+", socket="+socket
        if (flags and FSF_INPUT) then
            xcall TCPX, TCPOP_ACCEPT, status, "", socket, TCPXFLG_BLOCK, 0
        else
            xcall TCPX, TCPOP_CONNECT, status, "", socket, TCPXFLG_BLOCK, 0, server$
        endif
        if status >= 0 then                 ! success 
            sock'opened = .TRUE
        else                                ! failed socket open
            Fn'FileSock = -abs(status)      ! return error code <= 0
            close #ch                       ! close file
            exitfunction
        endif
    endif
    
    if Fn'FileSock = 0 then                 ! no error so far
        status = -99                        ! detect if MX_FILESOCK supported
        xcall MIAMEX, MX_FILESOCK, ch, socket, status   ! bind channel to socket
        if status = 0 then              ! on success
            Fn'FileSock = ch            ! return ch
        else                            ! else
            ? "Error ";status;" binding ch ";ch;" to socket ";socket
            Fn'FileSock = -abs(status)  ! return status (make sure <= 0)
            close #ch                   ! close the channel
            if sock'opened then         ! if we opened the socket, close it
                xcall TCPX, TCPOP_CLOSE, status, "", socket, 0
                socket = 0
            endif
        endif
    endif
    
    xputarg 2, socket       ! output updated socket

EndFunction    
    
!---------------------------------------------------------------------
!Function:
!   Input- and output- specific wrappers 
!   Same calling parameters as Fn'FileSock, minus flags
!Notes:
!   Saves have to remember the flags 
!---------------------------------------------------------------------
Function Fn'FileSock'Out(ch as i4:inputonly, socket as T_SOCKET, &
                         server$ as T_HOSTNAME:inputonly) as i4

    Fn'FileSock'Out = Fn'FileSock(ch, socket, server$)
    xputarg 2, socket
EndFunction    

Function Fn'FileSock'In(ch as i4:inputonly, socket as T_SOCKET, &
                         server$ as T_HOSTNAME:inputonly) as i4

    Fn'FileSock'In = Fn'FileSock(ch, socket, server$, FSF_INPUT)
    xputarg 2, socket
EndFunction    
    
    
!======================================================================
! Simple test code: 
!   .COMPIL FNFILESOCK.BSI/X:2/C:FNFILESOCK_BSI_TEST=1 
!   .RUN FNFILESOCK
!======================================================================
++ifdef FNFILESOCK_BSI_TEST

    ++message Compiling FNFILESOCK.BSI test routine...
    
    map1 testvars
        map2 chin,i,4
        map2 chout,i,4
        map2 in$,s,100
        map2 i,i,2
        map2 socket, T_SOCKET
        map2 test,b,1
        
    ? "Testing Fn'FileSock ..."
    ? "This test needs to be run on two jobs - "
    ? "  One in input mode (must be started first)"
    ? "  One in output mode (started second)"
    input "Enter 1) input test, 2) output test, 0) quit: ",test

    if test = 1 then        ! input test

        chin = Fn'FileSock'In(chin, socket, "localhost:44444")
        ? "chin: ";chin;", socket: ";socket
        if chin <= 0 then
            ? "Error opening file channel / socket for input"
            close #chout
            end
        endif

        ? "Inputting 15 lines from file socket..."
        for i = 1 to 15
            input #chin, in$
            ? "input: [";in$;"]"
        next i
        close #chin
        ? "Test complete"
        
    else                    ! output test
        chout = Fn'FileSock'Out(chout, socket, "localhost:44444")
        ? "chout: ";chout;", socket: ";socket
        if chout <= 0 then
            ? "Error opening file channel / socket for output"
            end
        endif

        for i = 1 to 15
            ? #chout, "This is line # " + i + " sent to output file socket"
            ? ".";tab(-1,254);
        next i
        ? "Output complete"
        input "Verify input on other terminal and hit ENTER: ",test
        close #chout
    endif

    end

++endif     ! end of test code
    
++endif     ! end of fnfilesock.bsi
    