! fninstr.bsi [103] - Enhanced wrapper for INSTR()
!---------------------------------------------------------------------
!EDIT HISTORY
! [103] 06-Sep-23 / jdm / Add keywords, notes
! [102] 08-Oct-18 / jdm / Minor format modernization / cleanup
! [101] 23-Oct-14 / jdm / Add ++ifndef, switch to asbfunc
! [100] 17-Sep-11 / jdm / Created
!---------------------------------------------------------------------
!NOTES
!   Largely superseded by .INSTRR() added in 6.3.1527
!
!KEYWORDS: instr
!
!PUBLIC FUNCTIONS
!   Fn'Instr(spos,string$,pattern$,flags) - combined instr, .instrr wrapper
!---------------------------------------------------------------------

++ifnlbl Fn'Instr()
  
!-------------------------------------------------------------------------
!Function:
!   Similar to the INSTR() function, but if the spos < 0, then works
!   right to left (i.e. finds last match rather than first match)
!Parameters:
!   spos,i,2     [in] starting position (if < 0, count from end of string)
!   string$,s,0  [in] string to search
!   pattern$,s,0 [in] substring to look for
!   flags,b,4    [in] optional regex flags (any arg, even 0, activates regex)
!Returns:
!   position (0 for not found, >0 for pos from left, <0 for pos from right)
!Globals:
!   none
!Error handling:
!   none
!Comments:
!  - There is no particular advantage to using this as a wrapper for 
!    the built-in INSTR() except for the find-last-match (spos < 0) feature.
!
!  - Last-match logic is implemented by creating an enhanced regular 
!    expresson from the original pattern which effectively finds the last
!    match. 
!
!  - If flags specified, the pattern$ is assumed to be a valid regular 
!    expression as is.  If not specified and spos < 0, any special 
!    characters will be assumed to be literal and will be escaped.
!
!Examples:
!    Fn'Instr(1,"abcdabcg","abc")      -->  1
!    Fn'Instr(2,"abcdabcg","abc")      -->  5
!    Fn'Instr(-1,"abcdabcg","abc")     --> -4
!    Fn'Instr(-5,"abcdabcg","abc")     --> -8
!-------------------------------------------------------------------------

FUNCTION Fn'Instr(spos as i2:inputonly, string$ as s0:inputonly, pattern$ as s0:inputonly,  &
                    flags as b4:inputonly) as i2

    map1 locals
        map2 regex,b,1
        map2 i,i,2
        map2 np,i,2
        map2 ns,i,2
        map2 x,i,2

    ! if empty pattern, no match
    np = len(pattern$)
    if np then

        ! if flags arg passed set regex flag 
        if (.argcnt >= 4) then
            regex = 1    
        endif

        if spos = 0 then
            spos = 1
        endif

        ! if spos < 0 but no flags, 'escape' pattern (else assume already escaped)
        if spos < 0 and regex = 0 then
            for i = 1 to np
                if instr(1,"[\^$.|?+(){}",pattern$[i;1]) then
                    pattern$ = pattern$[1,i-1] + "\" + pattern$[i+1,-1]
                    i += 1  ! skip the escaped char
                endif
            next i
        endif
    
        ! for spos > 0, perform standard instr()
        if spos > 0 then
            if regex
                .fn = instr(spos,string$,pattern$,flags)
            else
                .fn = instr(spos,string$,pattern$)
            endif
        else
            ! to get effect of starting n chars from right, trim string 
            ns = len(string$)
            if (spos < -1) then
                string$ = string$[1,ns + spos + np]
            endif
    
            ! and create a regex pattern 
            pattern$ = pattern$ + "(?!(.*" + pattern$ + ".*))"
            .fn = instr(1,string$,pattern$,flags)
    
            ! now convert result to offset from right edge
            if Fn'Instr then
                .fn = -1 - ns + .fn
            endif
        endif
    endif
    
End FUNCTION
 
++endif
