!fnbool.bsi [102] - Boolean utility functions
!------------------------------------------------------------------------
!EDIT HISTORY
! [102] 30-Dec-24 / jdm / Add "Y" 
! [101] 06-Sep-23 / jdm / add keywords (for FUNCIDX)
! [100] 06-Apr-18 / jdm / created
!------------------------------------------------------------------------
!NOTES
!  Created mainly to aid in standardizing various other methods of 
!  representing true/value (e.g. 1/0, "T/F", etc.)
!
!KEYWORDS: boolean
!------------------------------------------------------------------------
!FUNCTIONS
!  Fn'Val2Bool(value$) as BOOLEAN - convert argument to .TRUE or .FALSE
!------------------------------------------------------------------------

++ifnlbl Fn'Val2Bool()

++include'once ashinc:types.def

!---------------------------------------------------------------------
!Function:
!   convert various forms of boolean representation to .TRUE or .FALSE
!Params:
!   value$  (str) [in] - 1/0, T/F, TRUE/FALSE, YES/NO, Y/N, ON/OFF, etc.
!Returns:
!	.TRUE or .FALSE
!Globals:
!Notes:
!	.TRUE is the only "real" way to store the boolean value TRUE
!	that supports logical negation!
!---------------------------------------------------------------------
Function Fn'Val2Bool(value$ as s20:inputonly) as BOOLEAN

	if val(value$) then			! treat any numeric value > 0 as .TRUE
		Fn'Val2Bool = .TRUE
	else
		switch ucs(value$)		! else look for various synonyms
			case "T"
			case "TRUE"
			case "ON"
			case "YES"
            case "Y"            ! [102]
			case "SI"
			case "SIM"
			case "OUI"
				Fn'Val2Bool = .TRUE
				exit
			default						! anything else treat as .FALSE
				Fn'Val2Bool = .FALSE	! (actually redundant but makes it clear)
		endswitch
	endif

EndFunction


++endif
