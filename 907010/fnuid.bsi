!fnuid.bsi [100] - functions related to UID's
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 07-Jul-19 / jdm / created
! [101] 09-Jul-21 / jdm / added Fn'UID'To'Login$()
!------------------------------------------------------------------------
!REQUIREMENTS
!
!NOTES
!   This is mostly of interest under UNIX. For Windows, the UID will
!   (for now) be 0.
!------------------------------------------------------------------------
!PUBLIC FUNCTIONS
!  Fn'UID(gid) - return UID for current user (+ optional gid)
!  Fn'UID'To'Login$(uid) - return login name for specified UID 
!------------------------------------------------------------------------

++include'once ashinc:ashell.def
++include'once sosfunc:fnextch.bsi
++include'once sosfunc:fnexplode.bsi

!---------------------------------------------------------------------
!Function:
!   Return the UID
!Params:
!   gid (num) [out] - optional gid to return
!Returns:
!   real UID
!Globals:
!Notes:
!   Lacking an internal XCALL to do this, we instead start with the
!   username and then try to match it up to a UID in the /etc/passwd
!---------------------------------------------------------------------
Function Fn'UID(gid as b2:outputonly) as i2

    map1 locals
        map2 usn$,s,32
        map2 ch,b,2
        map2 pline$,s,100
        map2 fields,i,4
        map2 pwname$,s,32
        map2 pwx$,s,1
        map2 pwuid,b,2
        map2 pwgid,b,2
        
    usn$ = .USERNAME
    
    if lookup("/etc/passwd") then
        ch = Fn'NextCh(60000)
        open #ch, "/etc/passwd", input
        do while eof(ch)=0
            input line #ch, pline$
            fields = Fn'ExplodeEx(pline$, ":", FXF_TRIM, pwname$,pwx$,pwuid,pwgid)
            if fields >= 4 then
                if pwname$ = usn$ then
                    .fn = pwuid
                    gid = pwgid
                    exit
                endif
            endif
        loop
        close #ch
    endif
    
    xputarg @gid
EndFunction


!---------------------------------------------------------------------
!Function:
!   Convert UID to display format
!Params:
!   uid  (num) [in] - user id
!Returns:
!   display name$
!Globals:
!Notes:
!   [101] now returns the login name
!---------------------------------------------------------------------
Function Fn'UID'2'Name$(uid as b2:inputonly) as s32
    
    .fn = Fn'UID'To'Login$(uid) ! [101]
    
EndFunction

!---------------------------------------------------------------------
!Function:
!   Convert linux UID to login name (from /etc/passwd)
!Params:
!   uid (num) [in] - uid
!Returns:
!   login name (of uid if no match)
!Globals:
!Notes:
!   On first call, builds a static map $login(uid) -> login and
!   then uses the map directly on subsequent calls
!---------------------------------------------------------------------
Function Fn'UID'To'Login$(uid as b2:inputonly) as s20

    map1 locals
        map2 ch,b,2
        map2 pline$,s,100
        map2 fields,i,2
        map2 login$,s,20
        map2 x$,s,2
        map2 uid$,s,8
        
    static dimx $login, ordmap(varstr;varstr)   ! uid -> login

    if .extent($login()) < 1 then   ! if map not yet created, create it
        ch = Fn'NextCh(61000)
        open #ch, "/etc/passwd", input
        do while eof(ch) = 0
            input line #ch, pline$       ! login:x:uid:gid::homedir:shell
            fields = Fn'ExplodeEx(pline$,":",0,login$,x$,uid$)
            if fields >= 3 then
                $login(uid$) = login$
            endif
        loop
        close #ch
    endif
    
    ! now just retrieve it from the map
    .fn = $login(uid)
    if .isnull(.fn) then        ! if no match
        .fn = uid               ! just return uid as login
    endif
EndFunction
