! fnhostexl.bsi [102] - wrapper for HOSTEX.SBR
!-------------------------------------------------------------------------
!Edit History
! [102] 08-Oct-18 / jdm / minor format cleanup
! [101] 21-May-07 / jdm / Add HXF_RES to return first of output in lstfil$ argument (instead of fspec)
!-------------------------------------------------------------------------

++ifndef HXF_TRM 

++include'once sosfunc:fnextch.bsi
++include'once sosproc:syslog.bsi

define HXF_TRM    = &h0001     ! send output to screen
define HXF_WIN    = &h0002     ! if outputting to screen and GUI, use debug win
define HXF_TRMWIN = &h0003     ! HXF_TRM + HXF_WIN
define HXF_LOG    = &h0004     ! equivalent to HXF_LOG (output to SysLog())
define HXF_TRC    = &h0008     ! equivalent to HXF_TRC (output to SysLog())
define HXF_ECHO   = &h0020     ! echo command line
define HXF_RES    = &h0040     ! [101] return 1st line of output in lstfil$ arg

define HXF_TRMWINLOGTRC = &h000f  !

!-------------------------------------------------------------------------
!Function:
!   wrapper for HOSTEX.SBR
!Parameters:
!   cmd$ [string,in] : is the command line to execute
!   flags [b1, in]   : optional flags:
!               HXF_TRM  &h01 - always output to screen
!               HXF_WIN  &h02 - output to debug window instead of main window
!                        (if debug window available)
!               HXF_ECHO &h20 - echo command line
!                        (otherwise we only output to screen if error)
!               if DEBUG, all above flags set automatically
!               HXF_LOG  &h04 - (output response via SysLog() to log file)
!               HXF_TRC  &h08 - (output response via SysLog() to trace file)
!                       (note: by themselves, th _LOG and _TRAC, combine SLF_xxx with other HXF_x flags to log
!                           the corresponding messages)
!
!   lstfil$ [string,in/out] : optional output file name
!Returns:
!   status code (0=success)
!Globals:
!   none
!
!Notes:
!   The value added by this wrapper is that it redirects the output
!   of the command to <jobnam>.tmp, and then displays that only if
!   there was an error, the DEBUG flag is set, or one of the HXF_xxx
!   bits is set.
!
!   If outputting to screen and GUI available, use debug window instead.
!
!   HXF_LOG and HXF_TRC cause messages to be sent via the SysLog() routine
!   to the current log and/or trace files.  If the other HXF_xxx flags are
!   not set, then we SysLog just the output of the HOSTEX.  If HXF_ECHO is
!   set, we SysLog the command line itself.  See syslog.bsi for info on
!   setting the syslog filespecs.
!
!   Note that 2008, HOSTEX.SBR (UNIX) was enhanced to allow a 3rd 
!   param that captures and returns the stdout.  That might elminate
!   some of the motivation for this wrapper, although not necessarily all.
!-------------------------------------------------------------------------
Function Fn'Hostex(cmd$ as s512:inputonly, flags as b1:inputonly,lstfil$ as s120) as f6

    Static map1 guiflags,b,4
    map1 locals
        map2 argcnt,f
        map2 ch,f
        map2 pline$,s,300
        map2 sflags,b,4
        map2 resflag,b,1
        
    argcnt = .ARGCNT    ! preserve in case we call another function

    if guiflags = 0 then
        if flags and HXF_WIN then
            xcall AUI, AUI_ENVIRONMENT, MXOP_GET, guiflags
        endif
    endif

    if lstfil$ = "" or ((flags and HXF_RES)#0) then     ! [101]
        xcall GETJOB,lstfil$
        lstfil$ = lcs(strip(lstfil$))+".tmp"
    endif

    ! turn on all screen flags if DEBUG
    if DEBUG then
        flags = flags or HXF_TRM or HXF_WIN or HXF_ECHO
    endif

    ! set up SysLog() flags...
    sflags = 0
    if flags and HXF_TRC    sflags = sflags or SLF_TRC
    if flags and HXF_LOG    sflags = sflags or SLF_LOG

    ! if echoing, output a preliminary message showing the cmd line
    if flags and HXF_ECHO then
        ! if echoing to window and GUI available, use trace.print
        if ((flags and HXF_WIN)#0) and ((guiflags and AGF_GUIEXT)#0) then
            trace.print "Hostex: "+cmd$+" >"+lstfil$+" 2>&1"
        else
            if flags and HXF_TRMWIN sflags = sflags or SLF_TRM
        endif

        ! if we have any syslog flags, then call it now
        ! (not mutually exclusive with trace.print output)
        if (sflags) then
            call SysLog(sflags,"Hostex: %s >%s 2>&1",cmd$,lstfil$)
        endif

    endif

    xcall HOSTEX, cmd$ + " >"+lstfil$+ " 2>&1", Fn'Hostex

    ! output results if HXF_TRMWIN or error or using SysLog
    if (Fn'Hostex # 0) or ((flags and (HXF_TRMWINLOGTRC+HXF_RES))#0)    ! [101]
        sflags = sflags and not SLF_TRM

        ! read output back from file
        ch = Fn'NextCh(60000)
        open #ch, lstfil$, input
        do while pline$#"" or eof(ch)#1
            pline$=""
            input line #ch, pline$

            ! [101] return first line of output to caller if HXF_RES
            if (flags and HXF_RES) and (argcnt >= 3) and (resflag=0) then
                xputarg 3,pline$
                resflag = 1
                ! [101] and if this is the only reason we're here, exit
                if (Fn'Hostex = 0) and ((flags and HXF_TRMWINLOGTRC)=0) then
                    close #ch
                    Exitfunction
                endif
            endif

            if ((flags and HXF_WIN)#0) and ((guiflags and AGF_GUIEXT)#0) then
                trace.print pline$
            else
                if flags and HXF_TRMWIN sflags = sflags or SLF_TRM
            endif

            if sflags # 0 and pline$ # "" then
                call SysLog(sflags,pline$)
            endif
        loop
        close #ch
    endif

    ! return tmpfil spec if arg passed [101] (and not HXF_RES)
    if (argcnt >= 3) and ((flags and HXF_RES)=0) then   ! [101]
        xputarg 3,lstfil$
    endif
EndFunction

++endif
