! fntrmchr.bsi [101]  - functions related to TRMCHR
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 03-Mar-22 / jdm / created
! [101] 23-Jul-24 / jdm / add Fn'TRMCHR'Fgc(), Fn'TRMCHR'Bgc()

!----------------------------------------------------------------------
!REQUIREMENTS
!
!NOTES
!------------------------------------------------------------------------
!FUNCTIONS:
! Fn'TRMCHR'Rows() - returns current # rows in terminal
! Fn'TRMCHR'Cols() - returns current # cols in terminal
! Fn'TRMCHR'Flags() - returns current flags (TD_xxx)
! Fn'TRMCHR'Fgc() - returns current foreground color # [101]
! Fn'TRMCHR'Bgc() - returns current background color # [101] 
!------------------------------------------------------------------------

++ifnlbl Fn'TRMCHR'Rows()

++include'once ashinc:trmchr.def

!---------------------------------------------------------------------
!Function:
!   Return # rows terminal currently configured for
!Params:
!Returns:
!   rows (not including status lines)
!Globals:
!Notes:
!   Set rows with TAB(-5,x)
!---------------------------------------------------------------------
Function Fn'TRMCHR'Rows() as i2

    map1 locals
        map2 trmchr,ST_TRMCHR
        map2 status,f
        
    xcall TRMCHR,status,trmchr
    
    call fn'trmchr'fix'scale(trmchr)    ! adjust for scale if nec.
    .fn = trmchr.ROWS
    trace.print (99,"log") status,trmchr.ROWS, trmchr.COLS, .fn
EndFunction
 
!---------------------------------------------------------------------
!Function:
!   Return # cols terminal currently configured for
!Params:
!Returns:
!   columns
!Globals:
!Notes:
!   Set rows with TAB(-6,x)
!---------------------------------------------------------------------
Function Fn'TRMCHR'Cols() as i2

    map1 locals
        map2 trmchr,ST_TRMCHR
        map2 status,f
        
    xcall TRMCHR,status,trmchr
    call fn'trmchr'fix'scale(trmchr)    ! adjust for scale if nec.
    .fn = trmchr.COLS

EndFunction

!---------------------------------------------------------------------
!Function:
!   Return current foreground color #
!Params:
!Returns:
!   fgc - foreground color number
!Globals:
!Notes:
!   Set fgc with TAB(-2,x)
!---------------------------------------------------------------------
Function Fn'TRMCHR'Fgc() as i2

    map1 locals
        map2 trmchr,ST_TRMCHR
        map2 status,f
        
    xcall TRMCHR,status,trmchr
    
    call fn'trmchr'fix'scale(trmchr)    ! adjust for scale if nec.
    .fn = trmchr.FORE
    trace.print (99,"log") status,trmchr.FORE, trmchr.BACK, .fn
EndFunction

!---------------------------------------------------------------------
!Function:
!   Return current background color #
!Params:
!Returns:
!   fgc - foreground color number
!Globals:
!Notes:
!   Set bgc with TAB(-3,x)
!---------------------------------------------------------------------
Function Fn'TRMCHR'Bgc() as i2

    map1 locals
        map2 trmchr,ST_TRMCHR
        map2 status,f
        
    xcall TRMCHR,status,trmchr
    
    call fn'trmchr'fix'scale(trmchr)    ! adjust for scale if nec.
    .fn = trmchr.BACK
    trace.print (99,"log") status,trmchr.FORE, trmchr.BACK, .fn
EndFunction

!---------------------------------------------------------------------
!Function:
!   Return terminal characteristic flags (TD_xxx)
!Params:
!Returns:
!   flags 
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'TRMCHR'Flags() as b4

    map1 locals
        map2 trmchr,ST_TRMCHR
        map2 status,f
        
    xcall TRMCHR,status,trmchr
    call fn'trmchr'fix'scale(trmchr)    ! adjust for scale if nec.
    .fn = trmchr.FLAGS

EndFunction


!---------------------------------------------------------------------
!Function:
!   adjust for SCALE (since fields embedded in a structure may not
!   be corrected for)
!Params:
!   trmchr  (ST_TRMCHR) [in/out] -
!Returns:
!Globals:
!Notes:
!---------------------------------------------------------------------
Private Function fn'trmchr'fix'scale(trmchr as ST_TRMCHR)
    trmchr.ROWS = ifelse(trmchr.ROWS>1,trmchr.ROWS,trmchr.ROWS*100)
    trmchr.COLS = ifelse(trmchr.COLS>2,trmchr.COLS,trmchr.COLS*100)
    trmchr.FLAGS = ifelse(trmchr.FLAGS>2,trmchr.FLAGS,trmchr.FLAGS*100)
    trmchr.COLORS = ifelse(trmchr.COLORS>2,trmchr.COLORS,trmchr.COLORS*100)
    trmchr.FORE = ifelse(trmchr.FORE>2,trmchr.FORE,trmchr.FORE*100)
    trmchr.BACK = ifelse(trmchr.BACK>2,trmchr.BACK,trmchr.BACK*100)
    trmchr.WINROW = ifelse(trmchr.WINROW>2,trmchr.WINROW,trmchr.WINROW*100)
    trmchr.WINCOL = ifelse(trmchr.WINCOL>2,trmchr.WINCOL,trmchr.WINCOL*100)
    trmchr.TSLSIZ = ifelse(trmchr.TSLSIZ>2,trmchr.TSLSIZ,trmchr.TSLSIZ*100)
    trmchr.BSLSIZ = ifelse(trmchr.BSLSIZ>2,trmchr.BSLSIZ,trmchr.BSLSIZ*100)
    trmchr.BSSSIZ = ifelse(trmchr.BSSSIZ>2,trmchr.BSSSIZ,trmchr.BSSSIZ*100)
    xputarg @trmchr
EndFunction

++endif
