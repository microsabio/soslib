!fnselrange.bsi [103] - routines related to selecting recs by ranges, lists, etc.
!------------------------------------------------------------------------
!EDIT HISTORY
! [103] 06-Sep-23 / jdm / add keywords for funcidx
! [102] 19-May-19 / jdm / add Fn'StrRangeHelp(), Fn'NumRangeHelp()
! [101] 09-Jun-16 / jdm / add Fn'StrInRangeList$
! [100] 22-Sep-14 / jdm / created
!------------------------------------------------------------------------
!NOTES
!KEYWORDS: filtering
!------------------------------------------------------------------------
!Function List
!Function Fn'NumInRangeList(rangelist$ as s MAX_FN_RANGELIST:inputonly, &
!                           testval as f6:inputonly) as i2
!Function Fn'StrInRangeList(rangelist$ as s MAX_FN_RANGELIST:inputonly, &
!                           testval$ as s100:inputonly) as i2
!Function Fn'StrRangeHelp() - display string range help [102]
!Function Fn'NumRangeHelp() - display num range help    [102]
!------------------------------------------------------------------------
 
++ifnlbl Fn'NumInRangeList()
   
++include'once ashinc:types.def   
   
define MAX_FN_RANGELIST = 256          ! max length of list of ranges
define MAX_FN_RANGESTR  = 100          ! [101] max length of individual element
 
defstruct ST_RANGE_FROM_TO_NUM
    map2 from,f
    map2 to,f
endstruct

defstruct ST_RANGE_FROM_TO_STR          ! [101]
    map2 from$,s,MAX_FN_RANGESTR
    map2 to$,s,MAX_FN_RANGESTR
endstruct


!---------------------------------------------------------------------
!Function:
!   Select by a list of values and ranges (numeric)
!Params:
!   rangelist$  (str) [in] - comma delimited list of values and begin-end ranges:
!                              n1,n2,n3-n4,n5,n6-n7  etc. 
!   testval (f) [in]       - value to test (see Returns, Notes)
!Returns:
!  If testval not specified, then function checks syntax of rangelist$ 
!  and returns >= 0 for the number of elements, or -n for syntax error at position n
!  Otherwise, function checks if testval is in rangelist and returns
!      true (-1) if testval in rangelist$, else false (0)
!Globals:
!Notes:
!  Empty range selects all.
!
!  Range accepts positive and negative values, in decimal, octal or hex, e.g.
!       -99--88,-55,-22-11,33,44-55,&hF9,&o177
!
!  List of ranges does not have to be in order.
!
!  Each time rangelist$ changes, we re-parse it and convert it to an
!  array of to-from values. It's probably best to do the syntax check
!  at that time. Otherwise invalid syntax will simply mess up the selection
!  results without being obvious. Typical usage:
!
!       rangelist$ = <input string>
!       if Fn'NumInRangeList(rangelist$) < 0 then 
!           ? "syntax error" 
!           goto <input again>
!       endif
!
!       do  
!           testval = <some value>
!           if Fn'NumInRangeList(rangelist$,testval) then
!               ? testval;" is IN range"
!           else
!               ? testval;" NOT in range"
!           endif
!       loop
!
!---------------------------------------------------------------------
Function Fn'NumInRangeList(rangelist$ as s MAX_FN_RANGELIST:inputonly, &
                            testval as f6:inputonly) as BOOLEAN
 
    map1 localwork
        map2 rx,i,2          ! index into rangelist$
        map2 j,i,2           ! 
        map2 k,i,2           ! 
        map2 ftidx,i,2       ! index into fromto
        map2 lenrange,i,2
        map2 onerange$,s,30  ! one from-to range 
        map2 errpos,i,2      ! pos of syntax error
        map2 i,i,2
        map2 n,i,2
        
    ! local static values, used to avoid overhead of parsing the range each time
    static map1 s_lastrange$,s,MAX_FN_RANGELIST
    static map1 s_ftcount,i,2       ! # tlements in s_fromto()
    static dimx s_fromto(10),ST_RANGE_FROM_TO_NUM,auto_extend

    ! if range has changed, or we are doing a syntax check, then parse it and
    ! translate into array of from-to values for each selection processing
    if s_lastrange$ # rangelist$ or .argcnt <= 1 then
        xcall TRIM,rangelist$
        lenrange = len(rangelist$)
        do while rx < lenrange
            rx += 1
            j = instr(rx,rangelist$,",")
            onerange$ = rangelist$[rx,j-1]
            xcall TRIM,onerange$
            
            if .argcnt <= 1 then     ! syntax check mode
                n = len(onerange$)
                for i = 1 to n
                    if not fn'is'valid'range'char(onerange$,i) then
                        debug.print "Invalid char at pos "+i
                        if errpos = 0
                            errpos = rx + i - 1
                            exit
                        endif
                    endif
                next i
            endif

            if onerange$ # "" then
                ftidx += 1
                k = instr(2,onerange$,"-")  ! skip over leading - (neg)
                if k then
                    s_fromto(ftidx).from = val(onerange$[1,k-1])
                    s_fromto(ftidx).to = val(onerange$[k+1,-1])
                else
                    s_fromto(ftidx).from = val(onerange$)
                    s_fromto(ftidx).to = val(onerange$)
                endif
            endif
            
            if j then
                rx = j
            else
                exit
            endif
        loop
        s_ftcount = ftidx           ! new # elements in s_fromto()
        s_lastrange$ = rangelist$   ! memoize
    endif
 
    if .argcnt <= 1 then            ! if syntax check mode, return valid/invalid
        if errpos then
            Fn'NumInRangeList = -errpos
        endif
        exitfunction
    endif
    
    ! now do the selection
    debug.print s_ftcount
    if s_ftcount then
        for ftidx = 1 to s_ftcount
            debug.print testval,ftidx,s_fromto(ftidx).from,s_fromto(ftidx).to
            if testval >= s_fromto(ftidx).from and testval <= s_fromto(ftidx).to then
                Fn'NumInRangeList = -1      ! true (match)
                exit
            endif
        next ftidx
    else
        Fn'NumInRangeList = .TRUE      ! empty list matches all
    endif
debug.print Fn'NumInRangeList,rangelist$,testval    
 EndFunction


!---------------------------------------------------------------------
!Function:
!   Select by a list of strings
!Params:
!   rangelist$  (str) [in] - comma delimited list of values and begin-end ranges:
!                              n1,n2,n3-n4,n5,n6-n7  etc. 
!   teststr$ (str) [in]       - string to test (see Returns, Notes)
!Returns:
!  Function checks if teststr$ is in rangelist and returns
!      true (-1) if teststr$ in rangelist$, else false (0)
!Globals:
!Notes:
!  Empty range selects all.
!
!  Range accepts strings delimited by commas and optional ranges using dash. (Comma
!  and dash are special delimiters, unless the string is quoted. Trailing * activates
!  left-anchored substring match. Example:
!       aaa,bbb*,ccc-ddd,"ee,ff-gg"
!
!  Each time rangelist$ changes, we re-parse it and convert it to an
!  array of to-from values. (See Fn'NumInRangeList notes)
!---------------------------------------------------------------------
Function Fn'StrInRangeList(rangelist$ as s MAX_FN_RANGELIST:inputonly, &
                            teststr$ as s MAX_FN_RANGESTR:inputonly) as BOOLEAN
 
    map1 localwork
        map2 rx,i,2          ! index into rangelist$
        map2 j,i,2           ! 
        map2 k,i,2           ! 
        map2 ftidx,i,2       ! index into fromto
        map2 lenrange,i,2
        map2 onerange$,s,MAX_FN_RANGELIST  ! one from-to range 
        map2 errpos,i,2      ! pos of syntax error
        map2 i,i,2
        map2 n,i,2
        map2 op,b,2,4        ! quoted/csv mode
        map2 fdelim,s,2,","
        
    ! local static values, used to avoid overhead of parsing the range each time
    static map1 s_lastrange$,s,MAX_FN_RANGELIST
    static map1 s_ftcount,i,2       ! # tlements in s_fromto()
    static dimx s_fromto(10),ST_RANGE_FROM_TO_STR,auto_extend

    ! if range has changed, or we are doing a syntax check, then parse it and
    ! translate into array of from-to values for each selection processing
    if s_lastrange$ # rangelist$ or .argcnt <= 1 then
        do 
            xcall STRTOK, op, rangelist$, fdelim, onerange$
            if onerange$ = "" exit
                
            ftidx += 1
            k = instr(1,onerange$,"-") 
            if k then       ! onerange$ contains from-to
                s_fromto(ftidx).from$ = onerange$[1,k-1]
                s_fromto(ftidx).to$ = onerange$[k+1,-1]
                
            else            ! onerange contains just one value
                if onerange$[-1,-1] = "*" then
                    s_fromto(ftidx).from$ = onerange$[1,-2]
                    s_fromto(ftidx).to$ = onerange$[1,-2]+chr(255)
                else
                    s_fromto(ftidx).from$ = onerange$
                    s_fromto(ftidx).to$ = onerange$
                endif
            endif
           
        loop
        s_ftcount = ftidx           ! new # elements in s_fromto()
        s_lastrange$ = rangelist$   ! memoize
    endif
 
    ! now do the selection
    if s_ftcount then
        for ftidx = 1 to s_ftcount
            if teststr$ >= s_fromto(ftidx).from$ and teststr$ <= s_fromto(ftidx).to$ then
                Fn'StrInRangeList = -1      ! true (match)
                exit
            endif
        next ftidx
    else
        Fn'StrInRangeList = .TRUE      ! empty list matches all
    endif
    
 EndFunction


 !---------------------------------------------------------------------
 !Function:
 !   check position in range element for validity
 !Params:
 !  s$ (str) [in]  - input string being tested (a numeric value string or from-to range)
 !  pos (num) [in,out] - position in string
 !Returns:
 !  true (-1) if valid
 !Globals:
 !Notes:
 !  pos will get updated if we process a multi-char element (&hf1a)
 !---------------------------------------------------------------------
 Function fn'is'valid'range'char(s$ as s MAX_FN_RANGELIST:inputonly,pos as b2) as i2
    map1 locals
        map2 c$,s,1
        map2 lens,i,2
        map2 x,i,2
        map2 hex,b,1
        map2 match,i,2
        
    c$ = s$[pos;1]
    
    if instr(1,"0123456789.-",c$) then
        if c$ # "-" then
            fn'is'valid'range'char = -1
        elseif pos < len(s$) then               ! "-" in last position invalid
            fn'is'valid'range'char = -1
        endif
    elseif c$ = "&" then
        lens = len(s$)
        if (s$[pos+1;1] = "h") then
            hex = 1
        endif
        if (hex # 0) or (s$[pos+1;1] = "o") then  ! &hxxx  or &o### (hex or octal value)
            x = instr(pos+1,s$,"-")               ! search for end of range item
            if x = 0 then
                x = lens
            endif
            fn'is'valid'range'char = -1         ! here start by assuming true
            pos += 2                            ! start on char after &h or &o
            do while pos < x
                pos += 1
                if hex then
                    match = instr(1,"0123456789ABCDEFabcdef",s$[pos;1])  ! match against hex chars
                else    
                    match = instr(1,"01234567",s$[pos;1])                ! match against octal chars
                endif
                if match < 1 then                                        ! not valid 
                    fn'is'valid'range'char = 0                           ! fail
                    exit
                endif
            loop
        endif
        xputarg 2,pos               ! update pos
    endif
debug.print "fn'is'valid'range'char("+s$+","+pos+") = "+fn'is'valid'range'char
EndFunction


!---------------------------------------------------------------------
!Function:
!   display str range help
!Params:
!   none
!Returns:
!   0
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'StrRangeHelp() as i2
    xcall MSGBOX, "Range syntax consists of a list of one or comma-delimited " &
        + " individual case-sensitive string values with optional trailing '*', " &
        + " and/or from-to value pairs. For example..." + chr(13)+chr(13) &
        + "DAISY,da*,Mc-Mz,Y-Z" + chr(13)+chr(13) &
        + "... selects 'DAISY' (but not 'DAISY-MAY' or 'Daisy'), 'dan', 'dagwood' " &
        + "(but not Dagwood), 'McGregor' and 'Mr. Z' (but not 'Mack' or 'MCTEAGUE' " &
        + "or 'MR. X'), and 'YGREC' and 'Z' (but not 'yGrec', 'z', or 'Z1')", & 
        "String Range Selection", MBTN_OK, MBICON_ICON, MBMISC_TASKMODAL+MBMISC_TOPMOST
EndFunction
 
!---------------------------------------------------------------------
!Function:
!   display num range help
!Params:
!   none
!Returns:
!   0
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'NumRangeHelp() as i2
    xcall MSGBOX, "Range syntax consists of a list of one or comma-delimited " &
        + "individual numeric values (positive or negative, integer or floating point, " &
        + "decimal, octal or hex) and/or from-to value pairs. For example..." + chr(13)+chr(13) &
        + "-99--88,-55,-22-11,3.14-3.15,33,44-55,&hF9,&o177" + chr(13)+chr(13) &
        + "... selects values from negative 99 thru negative 88, negative 55, ", &
        + "negative 22 thru positive 11, 3.14 thru 3.15, 33, 44 thru 55, hex F9 (248) " &
        + "and octal 177 (127)", "Numeric Range Selection", &
        MBTN_OK, MBICON_ICON, MBMISC_TASKMODAL+MBMISC_TOPMOST
EndFunction

 
 ++endif
 
 