!uzstrings.fnp - string manipulation functions by UmZero (originally strings.fnp)
!   
! 2009.05.21 - FN'if'else'value$(value'if$ as s300, value'else$ as s300) as s300
!					returns value'else$ if value'if$ is empty, else returns value'if$
!			Example: xcall miamex, 95, path$, FN'if'else'value$(file'type$, "File | *.*", FN'if'value$(lable$, "Select", ...
!				(the same in fmwfnp:masks.fnp, but handling numbers)
! 2009.05.21 - FN'left'str$(searched'text$ as s3000, text2search$ as s100, incr'decr as f6, nr'of'occurrences as b2) as s3000
!	      - FN'right'str$(searched'text$ as s3000, text2search$ as s100, incr'decr as f6, nr'of'occurrences as b2) as s3000
!	      - FN'mid'str$(searched'text$ as s3000, left'delimiter$ as s100, right'delimiter$ as s100, left'incr'decr as f6, right'incr'decr as f6) as s3000
!			. returns the substring on the left, to the right of the prefix, or between prefix and suffix
!			. the result can be adjusted using incr'decr with positive or negative values
!			. in the left/right, the number of ocurrencies for "text" can be specified
!		Examples using file$ = "%temp%\folder\filename.pdf"
!				? FN'left'str$(file$, "\", 2, -1) = "%temp%\folder"
!				? FN'right'str$(file$, "\") = "\folder\filename.pdf"
!				? FN'mid'str$(file$, "\", "\", 1, 0) = "folder\"
! 2010.05.01 - FN'concatenate'if'value$(value$ as s0, prefix$ as s0, suffix$ as s0, zero'as'blank as b1) as s0
!			if ZERO_AS_BLANK in the 4th argument, value$ is considered to be a number and 0 is evaluated as a space
! 2010.06.10 - FN'if'mid'str$(searched'text$ as s0, left'delimiter$ as s0, right'delimiter$ as s0) as s0
!			- simplification of FN'mid'str$ where, the substring is returned only if prefix and suffix are found and, those are 
!			  automatically removed
! 2010.08.09 - FN'if'then'else'value$()
! 2010.08.26 - ZERO_AS_BLANK added to FN'if'else'value$() and FN'if'then'else'value$()
! 2012.05.05 - FN'count'str() was not executing the loop
! 2012.06.11 - FN'alfa'num'sign$() same as FN'alfa'num$() but doesn't remove puntuation marks and similar
! 2012.11.24 - FN'left'str$() if incr'decr<0 and not found, returns empty
! 2014.09.28 - FN'left'str$() if incr'decr<0 and not found, returns empty but, if
!										ZERO_OCCURRENCES is received in the last argument, returns the entire searched text
! 2015.03.25 - FN'right'str$ was wrong when asked the n occurence and it doesn't occur; now returns empty
! 2015.03.26 - FN'right'str$ and FN'left'str$ was wrong for nr'ocorrencias=0 porque -1 because of the binary >65000
! 2015.05.08 - Fix FN'excel'column$() - wasn't handling col properly when col = 26*N+1 (N>1) /jdm
! 2015.05.10 - Rename to uzstrings.fnp
!---------------------------------------------------------------------------------------------
!Function List
! FN'1char'ucs$(pos) 
!       returns A-Z for 1-26
! FN'1char'lcs$(pos) 
!       returns a-z for 1-26
! FN'abcd'pos(letra$) 
!       returns ordinal position of char in alphabet
! FN'alfa'num$(variavel$,keep'spaces) 
!       removes punct, converts accented to non-accented, ucs
! FN'alfa'num'sign$(variavel$,keep'spaces) 
!       like FN'alfa'num$ but allows some numeric punct chars
! FN'excel'column$(col) 
!       convert col # 1-702 to Excel column code A-ZZ
! FN'excel'col'letter2num(col$,col) 
!       convert excel col code A-ZZ back to number 1-702
! FN'concatenate'if'value$(value$,prefix$,suffix$,zero'as'blank) 
!       conditional concatenation
! FN'count'str(searched'text$,text2search$) 
!       count occurrences of substring
! FN'if'else'value$(value'if$,value'else$,zero'as'blank) 
!       return value'if$ or value'else$ depending on blank/zero value'if$
! FN'if'then'else'value$(value'if$,value'then$,value'else$,zero'as'blank) 
!       return value'then$ or value'else$ depending on blank/zero value'if$
! FN'mid'str$(searched'text$,left'delimiter$,right'delimiter$,left'incr'decr,right'incr'decr) 
!       return middle section of string between two delimters, else left or right part, with offsets
! FN'if'mid'str$(searched'text$,left'delimiter$,right'delimiter$) 
!       return middle section of string between two delimiters if found; else ""
! FN'soma'string(val1,val2) 
!       val1 + val2 (mainly useful for strings with numeric values)
! FN'prepare'search(text$)
!       prepare for multi-substring search
! FN'search'string(text2search$,adjust'text) 
!       carry out multi-substring search
! FN'left'str$(searched'text$,text2search$,incr'decr,nr'of'occurrences) 
!       return left part of string up to nth substring match
! FN'right'str$(searched'text$,text2search$,incr'decr,nr'of'occurrences)
!       return right part of string up to nth substring match
!-----------------------------------------------------------------------------------------------
define KEEP_SPACES = 1
define ZERO_AS_BLANK = 1
define ZERO_OCCURRENCES = -2

DEFSTRUCT search'string'structure
    map2 buffer
        map3 nr'words,b,1
        map3 word$(4),s,20
ENDSTRUCT

PRIVATE map1 search, search'string'structure
PRIVATE map1 abcd$,s,26,"ABCDEFGHIJKLMNOPQRSTUVWXYZ"

!---------------------------------------------------------------------
!Function:
!   return upper case A-Z for input 1-26
!Params:
!   pos  (num) [in] - 1-26
!Returns:
!   corresponding A-Z
!Module private vars:
!   abcd$
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'1char'ucs$(pos as f6:inputonly) as s1

	FN'1char'ucs$ = ""

	if (pos>0 AND pos<27) then
	   FN'1char'ucs$ = abcd$[pos;1]
	endif

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   return lower case a-z for input 1-26
!Params:
!   pos  (num) [in] - 1-26
!Returns:
!   corresponding a-z
!Module private vars:
!   abcd$
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'1char'lcs$(pos as f6:inputonly) as s1

	FN'1char'lcs$ = ""

	if (pos>0 AND pos<27) then
	   FN'1char'lcs$ = LCS(abcd$[pos;1])
	endif

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   return ordinal position of input char in alphabet
!Params:
!   letra$  (s,1) [in] - upper or lower case char A-Z,a-z
!Returns:
!   corresponding ordinal position 1-26
!Module private vars:
!   abcd$
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'abcd'pos(letra$ as s1:inputonly) as b1
	FN'abcd'pos = instr(1, abcd$, ucs(letra$))
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   convert integer 1-702 to Excel-style column identifier A-ZZ
!Params:
!   col  (num) [in] - 1-702
!Returns:
!   "A" - "ZZ" 
!Notes:
!   1 -> A
!   26 -> Z
!   27 -> AA
!   52 -> AZ
!   702 -> ZZ
!---------------------------------------------------------------------
FUNCTION FN'excel'column$(col as f6:inputonly) as s3

map1 resto,f,6
map1 inteiro,f,6

	FN'excel'column$ = ""

	inteiro = int((col - 1) / 26)              ! 2015.05.08 was / 27
	resto = col - (inteiro * 26)

	if inteiro>0 then
	   FN'excel'column$ = abcd$[inteiro;1]
	endif
	FN'excel'column$ = FN'excel'column$ + abcd$[resto;1]

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   convert excel-style column "A" - "ZZ" to equivalent number
!Params:
!   col$  (s,2) [in] - A thru ZZ
!   col   (b,2) [out] - equivalent col # (same as return value of function)
!Returns:
!   1 - 702
!Notes:
!   "A" -> 1
!   "Z" -> 26
!   "AA" -> 27
!   "AZ" -> 52
!   "ZZ" -> 702
!---------------------------------------------------------------------
FUNCTION FN'excel'col'letter2num(col$ as s2:inputonly, col as b2:outputonly) as b2

map1 resto,f,6
map1 inteiro,f,6

	FN'excel'col'letter2num = 0

	if (col$#"" AND val(col$)=0) then
	   col$ = strip(ucs(col$))

	   FN'excel'col'letter2num = FN'abcd'pos(col$[1; 1])
	   if col$[2; 1]#"" then
	      FN'excel'col'letter2num = (26 * FN'excel'col'letter2num) + FN'abcd'pos(col$[2; 1])
	   endif
	endif

	xputarg 2, FN'excel'col'letter2num
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   conditional concatenation - depending on whether specified value$
!   is blank (or optionally 0 valued), we concatenate it with a specified
!   prefix and suffix
!Params:
!   value$  (s0) [in] - source string 
!   prefix$ (s0) [in] - prefix to conditionally add
!   suffix$ (s0) [in] - suffix to conditionall add
!   zero'as'blank (b1) [in] - if set, then test based on val(value$) # 0
!                           - else test based on value$ # ""
!Returns:
!   original value$, with or without the prefix$ and suffix$ depending
!   on the outcome of the conditional test
!Notes:
!   FN'concatenate'if'value$("v","p","s") -> "pvs"        (value$ not blank)
!   FN'concatenate'if'value$("","p","s") -> ""            (value blank)
!   FN'concatenate'if'value$("v","p","s",1) -> "v"        (value$ = 0)
!   FN'concatenate'if'value$("99","p","s",1) -> "p99s"    (value$ # 0)
!---------------------------------------------------------------------
FUNCTION FN'concatenate'if'value$(value$ as s0:inputonly, &
                                  prefix$ as s0:inputonly, &
                                  suffix$ as s0:inputonly, &
                                  zero'as'blank as b1:inputonly) as s0

	FN'concatenate'if'value$ = ""

	if zero'as'blank then
	   if val(value$)#0 then
	      FN'concatenate'if'value$ = prefix$ + value$ + suffix$
	   endif
	elseif value$#"" then
	      FN'concatenate'if'value$ = prefix$ + value$ + suffix$
	endif

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   depending on whether argument is blank (or possibly 0), return it
!   or an alternate string.
!Params:
!   value$  (s0) [in] - source string 
!   value'else$ (s0) [in] - value to return if value$ blank (or zero, zero'as'blank)
!   zero'as'blank (b1) [in] - if set, then test based on val(value$) # 0
!                           - else test based on value$ # ""
!Returns:
!   Either original value$, or the value'else$, depending on whether value$
!   is zero (zero'as'blank) or blank 
!Notes:
!   FN'if'else'value$("a","b") -> "a"        (value$ not blank)
!   FN'if'else'value$("","b") -> "b"         (value$ is blank)
!   FN'if'else'value$("0","b") -> "0"        (value$ not blank)
!   FN'if'else'value$("a","b",1) -> "b"      (value$ is zero)
!   FN'if'else'value$("2","b",1) -> "2"      (value$ not blank/zero)
!---------------------------------------------------------------------
FUNCTION FN'if'else'value$(value'if$ as s0, value'else$ as s0, zero'as'blank as b1) as s0

	if value'if$="" then
	   FN'if'else'value$ = value'else$
	elseif zero'as'blank then
		if val(value'if$)=0 then
		   FN'if'else'value$ = value'else$
		else
		   FN'if'else'value$ = value'if$
		endif
	else
	   FN'if'else'value$ = value'if$
	endif
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Return one of two strings, depending on whether a third string is
!   zero/blank.
!Params:
!   value$  (s0) [in] - source string 
!   value'then$  (s0) [in] - result if value$ is not blank or not zero (zero'as'blank)
!   value'else$ (s0) [in] - result if value$ is blank or zero (zero'as'blank)
!   zero'as'blank (b1) [in] - if set, then test based on val(value$) # 0
!                           - else test based on value$ # ""
!Returns:
!   Either value'then$ or value'else$ depending whether the value$ is blank
!   (or zero if zero'as'blank), or not zero/blank.
!Notes:
!   FN'if'then'else'value$("a","b","c") -> "b"    (value$ not blank)
!   FN'if'then'else'value$("","b","c") -> "c"     (value$ is blank)
!   FN'if'then'else'value$("0","b","c") -> "b"    (value$ not blank)
!   FN'if'then'else'value$("a","b","c",1) -> "c"  (value$ is zero)
!   FN'if'then'else'value$("2","b","c",1) -> "c"  (value$ not blank/zero)
!---------------------------------------------------------------------

FUNCTION FN'if'then'else'value$(value'if$ as s0, value'then$ as s0, value'else$ as s0, zero'as'blank as b1) as s0

	if value'if$#"" then
	   FN'if'then'else'value$ = value'then$

	   if zero'as'blank then
	      if val(value'if$)=0 then
	         FN'if'then'else'value$ = value'else$
	      endif
	   endif
	else
	   FN'if'then'else'value$ = value'else$
	endif
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   add two values
!Params:
!   val1  (num) [in] - addend #1
!   val2  (num) [in] - addend #2
!Returns:
!   val1 + val2
!Notes:
!   Mainly useful as a shorthand to add two values that are instr
!   string format (taking advantage of the auto-conversion of params.
!   Example:
!       a$ = "1" : b$ = "2" 
!       FN'soma'string(a$,b$) -> 3
!   Equivalent to val(arg1) + val(arg2)
!---------------------------------------------------------------------
FUNCTION FN'soma'string(val1 as f6:inputonly,val2 as f6:inputonly) as f6
	FN'soma'string = val1 + val2
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   remove non-alphanum chars from string (convert accented chars
!   to non-accented equivalents; remove punctuation)
!Params:
!   variavel$  (str) [in] - source string
!   keep'spaces (num) [in] - if set, keep spaces, else remove
!Returns:
!   converted variavel$ 
!Notes:
!   "a;Z��" -> "AZE"
!   Uses repstr.sbx
!---------------------------------------------------------------------
FUNCTION FN'alfa'num$(variavel$ as s0:inputonly, keep'spaces as b1:inputonly) as s0

	FN'alfa'num$ = ""

	if variavel$="" then
	   EXITFUNCTION
	endif

	variavel$ = ucs(strip(variavel$))
	variavel$ = xfunc$("repstr", variavel$, "������������", "AAAAOOOEECIU", 1)
	if keep'spaces then
	   variavel$ = xfunc$("repstr", variavel$, ";,:._-><��~^+*�`\|!#$%&/({[)]}=?'��", "")
	else
	   variavel$ = xfunc$("repstr", variavel$, " ;,:._-><��~^+*�`\|!#$%&/({[)]}=?'��", "")
	endif

	FN'alfa'num$ = variavel$
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Same as FN'alpha'num$ but leaves characters ;,:._->< 
!Params:
!   variavel$  (str) [in] - source string
!   keep'spaces (num) [in] - if set, keep spaces, else remove
!Returns:
!   converted variavel$ 
!Notes:
!   "a;Z��" -> "A;ZE"
!   Uses repstr.sbx
!---------------------------------------------------------------------

FUNCTION FN'alfa'num'sign$(variavel$ as s0:inputonly, keep'spaces as b1:inputonly) as s0

	FN'alfa'num'sign$ = ""

	if variavel$="" then
	   EXITFUNCTION
	endif

	variavel$ = ucs(strip(variavel$))
	variavel$ = xfunc$("repstr", variavel$, "������������", "AAAAOOOEECIU", 1)
	if keep'spaces then
	   variavel$ = xfunc$("repstr", variavel$, "��~^*�`?", "")
	else
	   variavel$ = xfunc$("repstr", variavel$, " ��~^*�`?", "")
	endif

	FN'alfa'num'sign$ = variavel$
ENDFUNCTION


!---------------------------------------------------------------------
!Function:
!   Accepts a search text containing up to 4 space-separated tokens
!   and loads up the private search structure in preparation for
!   using the FN'search'string() function
!Params:
!   text$ (str) [in] - string to search for (up to 4 space-delimited tokens)
!Returns:
!   0 if the search text is null, else 1
!Module private vars:
!   search
!Notes:
!   non alpha chars are removed; accented chars converted
!---------------------------------------------------------------------
FUNCTION FN'prepare'search(text$ as s0:inputonly) as b1

map1 pos1,b,2
map1 pos2,b,2

	FN'prepare'search = 0

	if text$="" then
	   EXITFUNCTION
	endif

       search.nr'words = 0
       pos2 = 0
       do
          pos1 = pos2 + 1
          pos2 = instr(pos1, text$, " ")

          search.nr'words = search.nr'words + 1
          search.word$(search.nr'words) = FN'alfa'num$(text$[pos1, pos2-1])

          FN'prepare'search = 1

          if search.nr'words=4  then
             exit
          elseif text$[pos2+1; 1]=" " then
          	   exit
          elseif pos2=0 then
                 exit
          endif
	loop

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Search specified string for words previously specified instr
!   FN'prepare'search.
!Params:
!   text2search$ (str) [in] - text to search
!   adjust'text (num) [in] - if set, convert text2search$ to all 
!                            alphanumeric first (using FN'alpha'num$)
!Returns:
!   1 if all the search words are found; else 0
!Module private vars:
!   abcd$
!Notes:
!   Must use FN'prepare'search first
!---------------------------------------------------------------------
FUNCTION FN'search'string(text2search$ as s0:inputonly, adjust'text as b1:inputonly) as b1

map1 i,f

	if text2search$="" then
	   FN'search'string = 0
	   EXITFUNCTION
	endif

	if adjust'text then
	   text2search$ = FN'alfa'num$(text2search$)
	endif

	FN'search'string = 1

       for i=1 to search.nr'words
           if instr(1, text2search$, search.word$(i))=0 then
              FN'search'string = 0
              exit
           endif
       next i

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Return search'text$[1,n] based on n'th occurrence of substring
!Params:
!   searched'text$ (str) [in] - string to search in
!   text2search$ (str) [in] - substring to search for
!   incr'decr (num) [in] - additional (+/-) characters to return 
!   nr'of'occurrences (num) [in] - nth substring to match thru 
!                                  -2 (special value?)
!                          
!Returns:
!   search'text$[1,n] where n depends on the matches, incr'decr
!Globals:
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'left'str$(searched'text$ as s0:inputonly, text2search$ as s0:inputonly, &
                      incr'decr as f6:inputonly, nr'of'occurrences as f6:inputonly) as s0
map1 found,b,2
map1 last'found,b,2
map1 pos,b,2,1
map1 counter,b,2

	FN'left'str$ = ""

	if searched'text$="" then
	   EXITFUNCTION
	endif

	if nr'of'occurrences=0 then
	   nr'of'occurrences = -1
	endif

	do
	    found = instr(pos, searched'text$, text2search$)

	    if (found=0 OR counter=nr'of'occurrences) then
	        if last'found=0 then
	            if (incr'decr>=0 OR nr'of'occurrences=ZERO_OCCURRENCES) then
	                FN'left'str$ = searched'text$
	            endif
	        elseif (last'found+incr'decr)#0 then
	            FN'left'str$ = searched'text$[1, last'found+incr'decr]
	        endif
	        exit
	    endif

	    counter = counter + 1
	    last'found = found
	    pos = found + 1
	loop
ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Return searched'text$[n,-1] where n based on substring matches
!Params:
!   searched'text$ (str) [in] - string to search in
!   text2search$ (str) [in] - substring to search for
!   incr'decr (num) [in] - additional (+/-) characters to return 
!   nr'of'occurrences (num) [in] - nth substring to match thru 
!                                  -2 (special value?)
!                          
!Returns:
!   search'text$[1,n] where n depends on the matches, incr'decr
!Globals:
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'right'str$(searched'text$ as s0:inputonly, text2search$ as s0:inputonly, &
                       incr'decr as f6:inputonly, nr'of'occurrences as f6:inputonly) as s0
map1 found,b,2
map1 last'found,b,2
map1 pos,b,2,1
map1 counter,b,2

	FN'right'str$ = ""

	if searched'text$="" then
	   EXITFUNCTION
	endif

	if nr'of'occurrences=0 then
	   nr'of'occurrences = -1
	endif

	do
	    found = instr(pos, searched'text$, text2search$)

	    if (found=0 OR counter=nr'of'occurrences) then
	       if (last'found>0 AND counter>=nr'of'occurrences) then
	          FN'right'str$ = searched'text$[last'found + incr'decr, -1]
	       endif
	       exit
	    endif

	    counter = counter + 1
	    last'found = found
	    pos = found + 1
	loop

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Return middle section of string between two matched subscrings
!Params:
!   searched'text$ (str) [in] - string to search in
!   left'delimiter$ (str) [in] - substring to match on the left
!   right'delimiter$ (num) [in] - substring to match on the right
!   left'incr'decr (num) [in] - offset from left'delimiter match to start
!                                   of substring to return
!   right'incr'decr (num) [in] - offset from right'delimiter match to start
!                                   of substring to return
!                          
!Returns:
!   The substring between the two found delimiters. If either delimiter is
!   not found, we use the corresponding edge of the string, i.e. if just
!   the left delimiter found, the return string is from there to the right edge.
!   If no delimiter matches, we return entire string.
!
!Globals:
!Notes:
!   Fn'mid'str$("Hey, <bold>ATTENTION</bold> folks!","<bold>","</bold>",6,-1)
!       returns "ATTENTION"
!---------------------------------------------------------------------
FUNCTION FN'mid'str$(searched'text$ as s0:inputonly, left'delimiter$ as s0:inputonly, &
                     right'delimiter$ as s0:inputonly, left'incr'decr as f6:inputonly, &
                     right'incr'decr as f6:inputonly) as s0

map1 left'pos,b,2
map1 right'pos,f

	FN'mid'str$ = ""

	if searched'text$="" then
	   EXITFUNCTION
	endif

	left'pos = instr(1, searched'text$, left'delimiter$)
	right'pos = instr(left'pos+1, searched'text$, right'delimiter$)

	if left'pos>0 then
	   left'pos = left'pos + left'incr'decr
	else
	   left'pos = 1
	   left'incr'decr = 0
	endif

	if right'pos>0 then
	   right'pos = right'pos + right'incr'decr
	else
	   right'pos = -1
	   right'incr'decr = 0
	endif

	FN'mid'str$ = searched'text$[left'pos, right'pos]

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Search string for left and right delimiter patterns, returning the
!   middle section between them if found, else ""
!Params:
!   searched'text$ (str) [in] - string to search in
!   left'delimiter$ (str) [in] - substring to match on the left
!   right'delimiter$ (num) [in] - substring to match on the right
!                          
!Returns:
!   the substring betweent the two found delimiters, or "" if both
!   left and right delimiters not found
!Notes:
!   Similar as Fn'mid'str$ except we return "" if either delimiter
!   fails to match, and the offset options are set to return from
!   just past the left delimiter match to just before the right.
!
!   FN'if'mid'str$("blue red green","blue","green") -> " red "
!   FN'if'mid'str$("blue red green","yellow","green") -> ""
!---------------------------------------------------------------------
FUNCTION FN'if'mid'str$(searched'text$ as s0, left'delimiter$ as s0, right'delimiter$ as s0) as s0
map1 left'pos,f
map1 right'pos,f

	FN'if'mid'str$ = ""

	if searched'text$="" then
	   EXITFUNCTION
	endif

	left'pos = instr(1, searched'text$, left'delimiter$)
	if left'pos=0 then
	   EXITFUNCTION
	endif

	searched'text$ = searched'text$[left'pos+len(left'delimiter$), -1]

	right'pos = instr(1, searched'text$, right'delimiter$)
	if right'pos=0 then
	   EXITFUNCTION
	endif

	FN'if'mid'str$ = searched'text$[1, right'pos-1]

ENDFUNCTION

!---------------------------------------------------------------------
!Function:
!   Count # of occurrences of pattern in a string
!Params:
!   searched'text$ (str) [in] - string to search in
!   text2search$ (str) [in] - substring to search for
!Returns:
!   # of matches
!Notes:
!---------------------------------------------------------------------
FUNCTION FN'count'str(searched'text$ as s0, text2search$ as s0) as b2
map1 pos,b,2,1
map1 l,b,2,len(text2search$)
map1 l2,b,2,len(searched'text$)

	FN'count'str = 0

	if (searched'text$="" OR text2search$="") then
	   EXITFUNCTION
	endif

	do
	   pos = instr(pos, searched'text$, text2search$)
	   if pos=0 then
	      EXITFUNCTION
	   endif

	   FN'count'str = FN'count'str + 1
	   pos = pos + l
	loop while pos<l2
ENDFUNCTION


