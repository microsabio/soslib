:<A-Shell Utility Functions and Procedures

These get overwritten frequently so if you want to make your own 
personal modifications, it is advised that you move your copy elsewhere. 

Or, to customize a module and still share it, you can create a sidecar
file and conditionally include it (using ++include'if'exists) to define
a symbol for conditional compilation to isolate your customizations.

See [907,11] for Test programs for these functions 

Use .EZTYP SOSFUNC.TXT to display the function directory
Use .RUN SOSFUNCIDX for interactive index
>
SET LONGDIR
:R

