! fndectl [101] - decode strings with embedded ^x chars
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![101] 06-Sep-23 / jdm / Add keywords (for FUNCIDX)
![100] 06-Sep-23 / jdm / Created 
!------------------------------------------------------------------------
!REQUIREMENTS
!
!NOTES
!
!KEYWORDS: decode control-chars
!
!PUBLIC FUNCTIONS:
!   Fn'Decode'Ctl$(string$ as s0) as s0
!------------------------------------------------------------------------
++ifnlbl Fn'Decode'Ctl$()
++include'once ashinc:ashell.def
!---------------------------------------------------------------------
!Function:
!   Convert ^x sequences within the specified string to the corresponding
!   control codes.
!Params:
!   string (str) [in] - string possibly containing ^x sequences
!Returns:
!   equivalent string with the ^x sequences converted
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'Decode'Ctl$(string$ as s0) as s0
    map1 x,f
    ! convert ^x notation to control char
    do 
        x = instr(1,string$,"^")
        if x > 0 then
            ! handle special case of ^ in first position (due to a 
            ! historical quirk, a$[1,0] is same as a$[1,1])
            if x = 1 then
                string$ = chr(asc(ucs(string$[x+1;1]))-asc("A")+1) &
                    + string$[x+2,-1]
            elseif x > 1 and x < len(string$)
                string$ = string$[1,x-1] &
                    + chr(asc(ucs(string$[x+1;1]))-asc("A")+1) &
                    + string$[x+2,-1]
            endif
        endif
    loop until x = 0
    Fn'Decode'Ctl$ = string$
EndFunction
++endif
