
! Program Name: test
! Description:
! Author: Jorge Tavares
! Date: 10-11-2006
!----------------------------------------------------------------------------------------------------------------------------
! History
!----------------------------------------------------------------------------------------------------------------------------
!
!----------------------------------------------------------------------------------------------------------------------------
!{end history}

!----------------------------------
++include bpi:geral.bpi
!----------------------------------

                                   PROGRAM test,25.0(000)

!----------------------------------
!{INCLUDES}
!----------------------------------
define NR_FIELDS = 11
!----------------------------------

!----------------------------------
!{MAP}
!----------------------------------






start:
	xcall sbxgui,1,4,"Testing",id'obj,1,5000,5,15000,60
	xcall sbxgui,2,BT_OK,"",0,1,9000,0,0,44
	xcall sbxgui,2,BT_CANCEL,"",0,1,9000,0,0,54


	op'code = 2
	for i=1 to NR_FIELDS
	    call fields
	next i

	op'code = 1


	do
	   xcall aui,AUI_EVENTWAIT,id'obj,set'focus,exitcode,EVW_EXCDFOCUS+EVW_EDIT
!TRACE.PRINT "start: id'obj=["+id'obj+"] set'focus=["+set'focus+"] exitcode=["+exitcode+"] "

	   if (exitcode>=(-100-NR_FIELDS) and exitcode<-100) then
	      i = abs(exitcode) - 100
	      call fields
	   endif

!	   trace.print "EXITCODE="+exitcode
	loop until (exitcode=EX_CANCEL or exitcode=1)
end



fields:
	op'mouse = 100 + i
	on i call field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11
return

field1:
	xcall sbxinp,"|Name",op'code,2000,10,10,"","","",50,op'mouse,"",0,exitcode
return

field2:
	xcall sbxinp,"^Address",op'code,4000,10,10,"","","",50,op'mouse,"address",0,exitcode
return
field3:
	xcall sbxinp,"%200%&Phone",op'code,5000,10,10,"","","",0,op'mouse,"phone",0,exitcode
return
field4:
	xcall sbxinp,"<22>&Fax",op'code,5000,26,10,"","","",0,op'mouse,"fax",0,exitcode
return
field5:
	xcall sbxinp,"<37<&e-mail",op'code,5000,42,10,"","","*@",40,op'mouse,"e-mail address",0,exitcode
return
field6:
	xcall sbxinp,"||Sports",op'code,6000,10,10,"","","",0,op'mouse,"",0,exitcode
return
field7:
	xcall sbxinp,">|Art",op'code,6000,22,10,"","","",0,op'mouse,"",0,exitcode
return
field8:
	xcall sbxinp,"|>Politics",op'code,6000,34,10,"","","",0,op'mouse,"",0,exitcode
return
field9:
	xcall sbxinp,">>Computers",op'code,6000,46,10,"","","",0,op'mouse,"",0,exitcode
return
field10:
	xcall sbxinp,"Birthday",op'code,2000,30,6,"","D>",">8",8,op'mouse,"",0,exitcode
return
field11:
	xcall sbxinp,"Call hour",op'code,3000,30,6,"","t",">5",5,op'mouse,"",0,exitcode
return

