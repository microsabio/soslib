
! Name: MODEL2
! Description:
! Author:
! Date:
!
!----------------------------------------------------------------------------------------------------------------
!{History}
! 23-12-2009 --- 101 ---
! Start customizing
!
!----------------------------------------------------------------------------------------------------------------
! Notes: See the following for examples of typical coding tasks:
! [a] preloading a field at display and/or edit time, based on some runtime criteria
! [b] post-processing a field
! [c] forcing a field (or range of fields) to be refreshed
! [d] forcing input focus to a field (based on post-processing)
! [e] processing command buttons
!----------------------------------------------------------------------------------------------------------------


	PROGRAM MODEL2,1.0(101)

!{INCLUDES}
!--------------------------
!++include fmwbpi:geral.bpi
++include ashinc:ashell.def
++include ashinc:xtext.def
!--------------------------
!{DEFINITIONS}
![101] define symbols of interest
define d1_OK        = -190
define d1_CANCEL    = -62
define d1_ADDCUS    = -192


++include MODEL2.gen
!--------------------------
map1 set'focus,b,4
map1 exitcode,f,6
!--------------------------
!{MAP}
map1 fxid,b,2           ! [101] misc field id (exitcode)


!{EVENTWAIT}
!--------------------------


       call d1_load'dialog(d1'dlgid)

       do
          xcall AUI,AUI_EVENTWAIT,d1'dlgid,set'focus,exitcode,EVW_DESCEND+EVW_EDIT+EVW_EXCDFOCUS+EVW_EXCDINOUT

          if (abs(exitcode)>d1_EXITCODE_BASE and abs(exitcode)<=d1_LAST_EXITCODE) then
             exitcode = fn'd1_handle'controls(EDIT_MODE,abs(exitcode))
          endif

           ![101] special handling
           switch exitcode
           case d1_OK               ! [e] user clicked OK
                xcall MSGBOX,"Bye now","Debug",MBTN_OK,MBICON_ICON,MBMISC_TASKMODAL
                exitcode = 1
                exit
           case d1_ADDCUS               ! [e] user clicked OK
                xcall MSGBOX,"What does this button do? ","Debug",MBTN_OK,MBICON_ICON,MBMISC_TASKMODAL
                exitcode = d1_CANCEL
                exit
           case d1_CANCEL
                xcall MSGBOX,"Sorry to see you cancel ","Debug",MBTN_OK,MBICON_ICON,MBMISC_TASKMODAL
                exit

           endswitch
 
       loop until exitcode=1


       xcall AUI,AUI_CONTROL,CTLOP_DEL,d1'dlgid
end


!{GEN PROCEDURES}
!--------------------------

PROCEDURE d1_fld'pre(op as b1,fname$ as s24,do'nothing as b1)
DEBUG.PRINT "d1_fld'pre: op=["+op+"] fname$=["+fname$+"] do'nothing=["+do'nothing+"] "
++pragma auto_extern

STRSIZ 24                                                    ! needed for the SWITCH evaluation

       ![a] Following line would limit preload processing to only on display
       ![a] if op=DISPLAY_MODE then		       ! only on display
          switch fname$
             case "model2.city"                         ! [a] example of preloading a default value
                if d1'city = "" and d1'address # "" then
                    d1'city = "city preload"
                endif
                exit

          default
                 exit
          endswitch
       ![101] endif

       xputarg 1,op
       xputarg 2,fname$
       xputarg 3,do'nothing
ENDPROCEDURE


FUNCTION fn'd1_fld'post(op as b1,exitcode as f6,fname$ as s24) as f6
++pragma auto_extern

map1 fnext$,s,24

       fnext$ = fname$
DEBUG.PRINT "Post process, op="+op+", fname$ = "+fname$
STRSIZ 24                                                    ! needed for the SWITCH evaluation

       if (op<INFOP_DISPLAY and exitcode#1) then    ! only on edit
                                                    ! validate the field we exit
          switch fname$
             case "model2.city"                         ! [b] example of post-processing a field
                if d1'city = "" and d1'address # "" then
                    d1'city = "Must ENTER!"
                    ! force it to redisplay...
                    fxid = fn'd1_name2fxid(fname$)                      ! [c]
                    call d1_refresh'controls(fxid,fxid,DISPLAY_MODE)    ! [c]
                    exitcode = fxid                                     ! [d] force re-entry on this field
                endif
                exit
             default
                 exit
          endswitch
                                                    ! if clicked in one of our fields go there w/o loop in the EVENTWAIT
          if (exitcode#FIELD_INVALID and (abs(exitcode)>d1_EXITCODE_BASE and abs(exitcode)<=d1_LAST_EXITCODE)) then
             fnext$ = fn'd1_fxid2name$(exitcode)
             switch fnext$
             default
                    exit
             endswitch
          endif
       endif

       fn'd1_fld'post = exitcode
       xputarg 3,fnext$

ENDFUNCTION

