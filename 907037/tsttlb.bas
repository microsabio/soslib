program TSTTLB,1.0(002)     ! test TLBBAR.SBX
!--------------------------------------------------------------------------------
!Test program for TLBBAR.SBX
!--------------------------------------------------------------------------------
!{Edit History}
! 02-12-2006 --- 002 --- Jack McGregor
! Add some comments; delete dialog on exit
!
! 02-Dec-2006 --- 001 --- Jorge Tavares
! Created.
!--------------------------------------------------------------------------------

++include ashinc:ashell.def
++include fmwmap:tlbbar.map

map1 id,f,6
map1 i,f,6
map1 start'key,f,6
map1 exitcode,f,6
map1 set'focus,f,6

    on error goto TRAP                        ! [002]

    ! Display a message saying what we're going to do
	xcall sbxmsg,0,"This program uses TLBBAR.SBX to create several tool bar/panels"

    ! Create a dialog to put some toolbars in
	xcall aui,AUI_CONTROL,CTLOP_ADD,id,"",0,MBF_DIALOG+MBF_ALTPOS, &
	   "","",0,1000,1,23000,80,-2,-2,0,0,"","",0,"","",""

!	control'toolbar'id,b,4                    ! updated by TLBBAR but, if an ID is assigned in the program
                                              ! no toolbar will be created inside TLBBAR
!	control'toolbar'type,f,6                  ! if not defined, default to MBF_TAB, can also be passed in
                                              !   arg#7 of TLBBAR
!	control'toolbar'orientation,b,1           ! 0; ORIENT_VERTICAL=1; ORIENT_HORIZONTAL=2 and, defines
                                              !   the objects sequence orientation on creating
!	control'arrows,b,1                        ! 0 - if controls exceed area, do not display Prev/Next
                                              ! 1 - if controls exceed area, display Prev/Next arrow buttons
!	control'prev'id,b,4                       ! updated by TLBBAR
!	control'next'id,b,4                       ! updated by TLBBAR
!	control'first'item,b,1                    ! first control to display, can be 0 and TLBBAR willconvert to 1
!	control'last'item,b,1                     ! last control to display, must be set
	                                          ! both first and last items are updated by TLBBAR
!	control'nr'columns,b,1                    ! define nr. of item columns to display
!	control'nr'rows,b,1                       ! define nr. of item rows to display
!	control'horz'gap,b,4,1                    ! gap (in columns) between each control, default = 1
!	control'vert'gap,b,4,500                  ! gap (in millirows) between each control, default = 500
!	control'top'gap,b,4,1500                  ! start row for first control, default = 1500
!	control'left'gap,b,4,2                    ! start column for first control, default = 2
!	control'item(30)                          ! array for 30 control items
!		ctl'id,f,6                            ! updated by TLBBAR
!		ctl'name,s,30                         ! ctext
!		ctl'file,s,100                        ! ctext for icons (icon filename or icon from library)
!		ctl'tooltip,s,50                      ! tooltip
!		ctl'state,b,1                         ! cstate
!		ctl'key,s,8                           ! exitcode
!		ctl'type,f,6                          ! ctype
!		ctl'srow,f,6                          ! starting row, not necessarily defined
!		ctl'scol,f,6                          ! starting column, not necessarily defined
!		ctl'height,f,6                        ! height
!		ctl'width,f,6                         ! width

! TLBBAR arguments:
! action:
!	TLB_CREATE(1)-necessarily the first action where all the structure is defined and created
!	TLB_CHANGE_CTL(2)-to change something in an existing control
! 	TLB_SCROLL(3)-to scroll the content of a toolbar (left/up/right/down)
!	TLB_NO_RETURN(100)-must be added to one of the above and, informs TLBBAR to NOT return (xputarg) the updated control'structure
! control'structure:
!	defined in TLBBAR.MAP and has settings for the toolbar and the 30 possible items, some variables are updated inside TLBBAR
!	- If TLB_NO_RETURN passed in "action", it is NOR returned to the program
! tlb'srow,tlb'scol,tlb'erow,tlb'ecol:
!	are the coordinates for the toolbar but, tlb'erow and tlb'ecol can be ZERO, in this case, the toolbar will be expanded
!	automaticaly accordingly to its content
! control'toolbar'type (it's the same defined here or in the control'structure):
!	if ZERO, it will be MBF_TAB
! tlb'parent:
!	it's the parent ID for the toolbar
! control'toolbar'orientation (it's the same defined here or in the control'structure):
!	defines the sequence to create the controls inside the toolbar (ORIENT_VERTICAL-1 or ORIENT_HORIZONTAL-2 or 0-TLBBAR will decide)
! control'arrows (it's the same defined here or in the control'structure):
!	if 1 (one) and, if the content of the toolbar doesn't fit inside the toolbar area, left/up/right/down arrows will be displayed
!	to scroll the content. If 0 (zero) and the content doesn't fit inside, the controls outside the boundaries will just not be displayed
! item'default'type:
!	all the items w/o declared ctl'type() will assume this (if not defined at all, it will be MBF_BUTTON but, if ctl'file() defined
!	will be MBF_BUTTON+MBF_ICON
! start'key:
!	the starting exitcode to assign to each control w/o ctl'key() defined
! item'default'height:
!	assigned to all items w/o ctl'height(), if also not defined here, will be default height is 8
! item'default'width:
!	assigned to all items w/o ctl'width(), if also not defined here, will be default width is 1000
! item'icon'path:
!	when ctl'file() defined, if no path or library defined in ctl'file(), this will be considered to all



    ! Define 10 controls about Label and Tooltip
	control'first'item = 1
	control'last'item = 10
	for i=control'first'item to control'last'item
	    ctl'name(i) = "Option " + str(i)
	    ctl'tooltip(i) = "Tooltip for Option " + str(i)
	next i

	! Define exitcode -601 for the first control, following controls
	! will be sequential after the first one
	start'key = 601

! First panel:
	! Toolbar (default MBF_TAB) with 10 buttons in vertical sequence

	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   2000,2,10000,32,id,0,ORIENT_VERTICAL,0,0,start'key,0,0

	! I don't want to return the control'structure updated into the program so, TLB_NO_RETURN was set

! Second panel:
	! Toolbar (GOUPBOX created in the program and ID passed in control'toolbar'id) with 10 static buttons (default MBF_STATIC)
	! in horizontal sequence
	! The coordinates of the toolbar are not necessary since it's created outside the TLBBAR
	! The buttons will be STATIC FRAMES and resized to 7
	! The horizontal gap between the buttons will be 2 (default = 1)
	xcall aui,AUI_CONTROL,CTLOP_ADD,control'toolbar'id, &
	   "Second Panel",0,MBF_GROUPBOX+MBF_ALTPOS,"","",0,14000,2,22000,32

	control'horz'gap = 2
	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   0,0,0,0,0,0,ORIENT_HORIZONTAL,0,MBF_STATIC+MBF_FRAME+MBF_KBD,start'key,0,7

	control'toolbar'id = 0      ! reset to not interfere with the next TLBBAR call
	control'horz'gap = 1        ! assign the default value

! Third panel:
	! An auto-expanded toolbar (erow=0 and ecol=0) with checkboxes but, only considering items from 4 to 9
	control'first'item = 4
	control'last'item = 9
	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   2000,35,0,0,id,0,ORIENT_VERTICAL,0,MBF_CHKBOX+MBF_TABSTOP+MBF_KBD,start'key,0,0

! Fourth panel:
	! The toolbar will be the main Dialog itself so, the top'gap must be adjusted (12000)
	! Let's try with Radio Buttons for the items 4 through 8
	control'toolbar'id = id
	control'top'gap = 12000
	control'first'item = 4
	control'last'item = 8
	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   0,0,0,0,0,0,ORIENT_HORIZONTAL,0,MBF_RADIOBTN+MBF_TABSTOP+MBF_KBD,start'key,0,0

! Fifth panel:
	! Let's mix a lot of things in this one; the Toolbar will be a Modeless Dialog with STATIC ICONS on the 1st row;
	! BUTTON ICONS on the 2nd row; STATIC SUNKEN ICONS on the 3rd row; a PROGRESS bar (10th item) and a GROUPBOX (11th item)
	! I want 3 columns (control'nr'columns)
	control'toolbar'id = 0
	control'first'item = 1
	control'last'item = 11

	ctl'file(1) = "copy::ashico1"
	ctl'type(1) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'file(2) = "cut::ashico1"
	ctl'type(2) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'file(3) = "paste::ashico1"
	ctl'type(3) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD

	ctl'file(4) = "undo::ashico1"
	ctl'type(4) = MBF_BUTTON+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'file(5) = "redo::ashico1"
	ctl'type(5) = MBF_BUTTON+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'file(6) = "refresh::ashico1"
	ctl'type(6) = MBF_BUTTON+MBF_ICON+MBF_NODISTORT+MBF_KBD

	ctl'file(7) = "information::ashico1"
	ctl'type(7) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'winstyle(7) = SS_SUNKEN
	ctl'file(8) = "forbidden::ashico1"
	ctl'type(8) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'winstyle(8) = SS_SUNKEN
	ctl'file(9) = "warning::ashico1"
	ctl'type(9) = MBF_STATIC+MBF_ICON+MBF_NODISTORT+MBF_KBD
	ctl'winstyle(9) = SS_SUNKEN

	ctl'name(10) = "85"
	ctl'type(10) = MBF_PROGRESS+MBF_KBD
	ctl'width(10) = 22

	ctl'type(11) = MBF_GROUPBOX
	ctl'srow(11) = 1000
	ctl'scol(11) = 2
	ctl'height(11) = 6500
	ctl'width(11) = 27

	control'top'gap = 1500
	control'left'gap = 4
	control'horz'gap = 2
	control'nr'columns = 3

	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   2500,48,10000,78,id,MBF_DIALOG+MBF_MODELESS,ORIENT_HORIZONTAL,0,0,start'key,0,6

! Sixth panel:
	! Taking the previous example, just changing the orientation from VERTICAL to HORIZONTAL
	! Adjust the vertical and horizontal gap between buttons, define 3 rows and, define width buttons to 3 (last argument)
	! the PROGRESS width reseted to zero that, will assume the global 3 width
	ctl'width(10) = 0

	control'vert'gap = 900
	control'horz'gap = 4
	control'nr'columns = 0
	control'nr'rows = 3
	xcall tlbbar,TLB_CREATE+TLB_NO_RETURN,control'structure, &
	   11000,48,18500,78,id,MBF_DIALOG+MBF_MODELESS,ORIENT_VERTICAL,0,0,start'key,0,3

                                                                     ! reset everything changed in the previous panel
	control'top'gap = 1500
	control'left'gap = 2
	control'horz'gap = 1
	control'vert'gap = 500
	control'nr'columns = 0
	control'nr'rows = 0

! Last Panel:
	! To finish, a scrollable panel for 9 items with a fix positioned [Exit] button that will also exit the program
	! The scroll is activated by the 1 (after ORIENT_HORIZONTAL) and, because the buttons doesn't fit inside the toolbar area
	control'last'item = 10			                    ! adding another button
	ctl'name(control'last'item) = "Exit"
	ctl'key(control'last'item) = "VK_ESC"                         ! has a specific exitcode instead of the incremental exitcode
	ctl'type(control'last'item) = MBF_BUTTON+MBF_ALTPOS+MBF_KBD   ! will be a standard button...
	ctl'srow(control'last'item) = 3000                            ! with a specific position; row...
	ctl'scol(control'last'item) = 25                              ! col...
	ctl'width(control'last'item) = 6                              ! width...

	control'horz'gap = 2                                          ! defining a different horizontal gap between each button
	xcall tlbbar,TLB_CREATE,control'structure,18500,48,22000,79, &
	   id,0,ORIENT_HORIZONTAL,1,0,start'key,0,3

	! In this case, because it's scrollable, TLB_NO_RETURN can't be set because control'structure is needed in the
	! main program to get back into TLBBAR to execute scroll when the prev/next buttons will be pressed

! Here is the EVENTWAIT that will take care of all the exitcodes
	do
	   xcall aui,AUI_EVENTWAIT,id,set'focus,exitcode,EVW_EXCDFOCUS+EVW_DESCEND+EVW_SIBLINGS

	   if exitcode=99 then
	      exitcode = 0
	      repeat
	   endif
                                                                     ! Scrolling
                                                                     ! The exitcodes for scroll need to be customized, by now,
                                                                     ! are defined with 998 and 999
	   if (exitcode=-998 or exitcode=-999) then
	      xcall tlbbar,TLB_SCROLL,control'structure,abs(exitcode)
	      exitcode = 0
	      repeat
	   endif
                                                                     ! click on any button will display a message
	   if (abs(exitcode)>600 and abs(exitcode)<=630) then
	      xcall sbxmsg,0,"Message for"+ctl'name((abs(exitcode)-600))
	   endif
	loop until exitcode=1

    xcall aui,AUI_CONTROL,CTLOP_DEL,id          ! [002] delete the dialog
end

TRAP:                                           ! [002] add a simple error trap
    ? "Error # ";err(0)
    xcall aui,AUI_CONTROL,CTLOP_DEL,id          ! [002] delete the dialog
    end
