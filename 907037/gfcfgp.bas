
! Name: GFCFGP
! Description:
! Author:
! Date:
!
!----------------------------------------------------------------------------------------------------------------
! History:
!----------------------------------------------------------------------------------------------------------------
! 2008.07.03 - 1.0(001) - delete from memory
! 2008.10.08 - 1.1(000) - modelos na pasta PATH_MODELOS (%MIAME%\modelos)
! ATEN��O: tem que ser instalado com a vers�o 5.1.1126.2c (por causa do MIAMEX 20 aceitar o %MIAME% no path
! 2008.10.29 - 1.2(000) - papel branco (n�o imprime se estiver seleccionada a op��o pr�-impresso)
!----------------------------------------------------------------------------------------------------------------


						PROGRAM GFCFGP,1.2(000)

!{PRAGMAS}
!--------------------------
++pragma sbx
!--------------------------

!define MX_SETMEMFLAGS = 163
!xcall MIAMEX,MX_SETMEMFLAGS,0,1		! delete from memory


!{INCLUDES}
!--------------------------
++include fmwbpi:geral.bpi
!--------------------------
!{MAP}
map1 lista'modelos$,s,2000
map1 lista'formatos$,s,50,"~A4~A5~~"

map1 cfg'pagina$
	map2 gofor'formato$,s,8
	map2 gofor'orientacao$,s,1
	map2 gofor'modelo$,s,30
	map2 gofor'assinatura,b,1


map1 signatures'list$,s,2000
map1 signatures(99)
	map2 signature'name$,s,50
	map2 signature'path'filename$,s,256
	map2 signature'row,b,2
	map2 signature'col,b,2
	map2 signature'width,b,2
	map2 signature'height,b,2
	map2 signature'all'pages,b,1

map1 file'name$,s,50

!--------------------------
++include cfgp.gen

++include gflayout.bpi
!--------------------------

!{EVENTWAIT}
!--------------------------


	xgetarg 1,cfg'pagina$
	xgetarg 2,exitcode
	xgetarg 3,cfg'page'label'enviar$

	cfg'page'formato$ = strip(gofor'formato$)
	SWITCH gofor'orientacao$
	CASE "H"
		cfg'page'ori2 = 1
		exit
	DEFAULT                                                       ! "V"
		cfg'page'ori1 = 1
		exit
	ENDSWITCH

	call load'signatures()
	call cria'lista'modelos()

       call cfg'page_load'dialog(cfg'page'dlgid)

       do
          xcall AUI,AUI_EVENTWAIT,cfg'page'dlgid,set'focus,exitcode,EVW_DESCEND+EVW_EDIT+EVW_EXCDFOCUS+EVW_EXCDINOUT

          if (abs(exitcode)>=cfg'page_TABCTL_LOWER_EXIT AND abs(exitcode)<=cfg'page_TABCTL_HIGHER_EXIT) then
             call FN'cfg'page_handle'tabctl(exitcode)
             SWITCH cfg'page'tabctl'current'panel(1)
             CASE 2
             		call xtree'layout(0)
             		exit
             ENDSWITCH
             exitcode = 0
          endif

          if (abs(exitcode)>cfg'page_EXITCODE_BASE AND abs(exitcode)<=cfg'page_LAST_EXITCODE) then
             exitcode = FN'cfg'page_handle'controls(EDIT_MODE,abs(exitcode))
          endif

                                                                     ! Layout panel
	   if cfg'page'tabctl'current'panel(1)=2 then
	      if exitcode=-900 then
	         call xtree'layout(4,1,exitcode)
	      endif

	      if exitcode=-5600 then
	         xcall MIAMEX,95,(PATH_MODELOS+"\"),"Modelos (*.pmo)|*.pmo","Guardar como", &
	   		           OFN_HIDEREADONLY + OFN_DONTADDTORECENT,"pmo",cfg'page'modelo$,1

	         if cfg'page'modelo$<>"" then
	            cfg'page'modelo$ = cfg'page'modelo$[1,instr(1,cfg'page'modelo$,".pmo")-1]
	            call grava'modelo(cfg'page'modelo$)
	            lista'modelos$ = "~" + strip(cfg'page'modelo$) + lista'modelos$
	            exitcode = 0
	         endif
	      endif

	      if exitcode=-5500 then
	         call grava'modelo(cfg'page'modelo$)
	         exitcode = 0
	      endif
	   endif


          if exitcode=-600 then                                      ! Anular
             call remove'signature()
             exitcode = -101
             REPEAT
          endif

          if exitcode=-650 then                                      ! Search new signature file...
             call add'signature()
             exitcode = 0
             REPEAT
          endif

          if exitcode=-6000 then                                     ! Edit image
             if lookup(cfg'page'sign'path'file$)#0 then
                xcall miamex,96,0,cfg'page'sign'path'file$
             endif
             exitcode = 0
             REPEAT
          endif

	   if (exitcode=-1000 OR exitcode=-5000 OR exitcode=-9001 OR exitcode=-9000) then
	      gofor'formato$ = cfg'page'formato$
	      gofor'orientacao$ = FN'evaluate'orientacao$()
	      gofor'modelo$ = FN'concatenate'if'value$(cfg'page'modelo$,PATH_MODELOS + "\",".pmo")
	      gofor'assinatura = val(cfg'page'assinatura'seq$)
	      call save'signatures(exitcode,cfg'page'assinatura'seq$)
	      if exitcode=-5000 then
	         exitcode = 0
	      else
	         exit
	      endif
	   endif

       loop until exitcode=1

       xcall AUI,AUI_CONTROL,CTLOP_DEL,cfg'page'dlgid

       if exitcode#1 then
          xputarg 1,cfg'pagina$
          xputarg 2,exitcode
       endif
end


!{GEN PROCEDURES}
!--------------------------

PROCEDURE cfg'page_fld'pre(op as b1,fname$ as s24,do'nothing as b1)
++pragma auto_extern

STRSIZ 24                                                    ! needed for the SWITCH evaluation

       if op=DISPLAY_MODE then		       ! only on display
          SWITCH fname$
          DEFAULT
                 exit
          ENDSWITCH
       endif

       xputarg 1,op
       xputarg 2,fname$
       xputarg 3,do'nothing
ENDPROCEDURE


FUNCTION FN'cfg'page_fld'post(op as b1,exitcode as f6,fname$ as s24)
++pragma auto_extern

map1 fnext$,s,24

       fnext$ = fname$

STRSIZ 24                                                    ! needed for the SWITCH evaluation

       if (op<INFOP_DISPLAY AND exitcode#1) then    ! only on edit
                                                    ! validate the field we exit
          SWITCH fname$
          CASE "cfgp.assina'list"
                 call update'signature'settings(cfg'page'assinatura'seq$)
          	   exit
          CASE "cfgp.modelo"
                 call FN'carrega'layout()
                 call xtree'layout(1)
                 exit
          DEFAULT
                 exit
          ENDSWITCH
                                                    ! if clicked in one of our fields go there w/o loop in the EVENTWAIT
          if (exitcode#FIELD_INVALID AND (abs(exitcode)>cfg'page_EXITCODE_BASE AND abs(exitcode)<=cfg'page_LAST_EXITCODE)) then
             fnext$ = FN'cfg'page_fxid2name$(exitcode)
             SWITCH fnext$
             DEFAULT
                    exit
             ENDSWITCH
          endif
       endif

       FN'cfg'page_fld'post = exitcode
       xputarg 3,fnext$

ENDFUNCTION



PROCEDURE update'signature'settings(indice as b1)
++pragma auto_extern

	if indice=0 then
	   cfg'page'sign'path'file$ = ""
	   cfg'page'assina'top = 0
	   cfg'page'assina'left = 0
	   cfg'page'assina'width = 0
	   cfg'page'assina'height = 0
	   cfg'page'all'pages = 0
	else
	   cfg'page'sign'path'file$ = signature'path'filename$(indice)
	   cfg'page'assina'top = signature'row(indice)
	   cfg'page'assina'left = signature'col(indice)
	   cfg'page'assina'width = signature'width(indice)
	   cfg'page'assina'height = signature'height(indice)
	   cfg'page'all'pages = signature'all'pages(indice)
	endif

	call cfg'page_refresh'controls()
	call cfg'page_aui_assina'img(CTLOP_CHG,cfg'page'sign'path'file$,MBST_CHANGE)
	call cfg'page_aui_all'pages(CTLOP_CHG,cfg'page'all'pages,MBST_CHANGE)
ENDPROCEDURE


FUNCTION FN'evaluate'orientacao$() as s8

map1 value,b,1

	xcall AUI,AUI_CONTROL,CTLOP_QRYCB,"cfgp.ori1","",0,0,"",0,value
	if value then
	   FN'evaluate'orientacao$ = "P"
	else
	   FN'evaluate'orientacao$ = "H"
	endif

ENDFUNCTION

FUNCTION FN'evaluate'all'pages() as b1
++pragma auto_extern

	xcall AUI,AUI_CONTROL,CTLOP_QRYCB,"cfgp.all'pages","",0,0,"",0,cfg'page'all'pages

	FN'evaluate'all'pages = cfg'page'all'pages
ENDFUNCTION


PROCEDURE load'signatures()
++pragma auto_extern

map1 seq,b,1

	if lookup(PATH_USERDEF+"\signatures.cfg")#0 then
	   open #FN'free'channel(channel),PATH_USERDEF+"\signatures.cfg",input

	   input line #channel,signatures'list$

	   do
	      input csv #channel,seq,file'name$, 					&
	      						cfg'page'sign'path'file$,	&
	      						cfg'page'assina'top, 	&
	      						cfg'page'assina'left, 	&
	      						cfg'page'assina'width, 	&
	      						cfg'page'assina'height, 	&
	      						cfg'page'all'pages

	      if eof(channel)#0 then
	         exit
	      endif

	      if file'name$="" then
	         REPEAT
	      endif

	      if (seq>0 AND seq<100)then
	         signature'name$(seq) = file'name$
	         signature'path'filename$(seq) = cfg'page'sign'path'file$
	         signature'row(seq) = cfg'page'assina'top
	         signature'col(seq) = cfg'page'assina'left
	         signature'width(seq) = cfg'page'assina'width
	         signature'height(seq) = cfg'page'assina'height
	         signature'all'pages(seq) = cfg'page'all'pages
	      endif
	   loop

	   close #channel
	endif

	if signatures'list$="" then
	   signatures'list$ = "~000~ ~~"
	endif

	cfg'page'assinatura'seq$ = "000"
	cfg'page'sign'path'file$ = ""
	cfg'page'assina'top = 0
	cfg'page'assina'left = 0
	cfg'page'assina'width = 0
	cfg'page'assina'height = 0
	cfg'page'all'pages = 0

	if (gofor'assinatura>0 AND gofor'assinatura<100) then
	   if instr(1,signatures'list$,"~"+(gofor'assinatura using "#ZZ")+"~")>0 then
	      cfg'page'assinatura'seq$ = gofor'assinatura using "#ZZ"
	      cfg'page'sign'path'file$ = signature'path'filename$(gofor'assinatura)
	      cfg'page'assina'top = signature'row(gofor'assinatura)
	      cfg'page'assina'left = signature'col(gofor'assinatura)
	      cfg'page'assina'width = signature'width(gofor'assinatura)
	      cfg'page'assina'height = signature'height(gofor'assinatura)
	      cfg'page'all'pages = signature'all'pages(gofor'assinatura)
	   endif
	endif
ENDPROCEDURE



PROCEDURE save'signatures(op as f6,indice as b1)
++pragma auto_extern

	if (indice>0 AND indice<100) then
	   signature'row(indice) = cfg'page'assina'top
	   signature'col(indice) = cfg'page'assina'left
	   signature'width(indice) = cfg'page'assina'width
	   signature'height(indice) = cfg'page'assina'height

	   signature'all'pages(indice) = FN'evaluate'all'pages()
	endif

	open #FN'free'channel(channel),PATH_USERDEF+"\signatures.cfg",output
	? #channel,signatures'list$
	for i=1 to 99
	    if signature'name$(i)#"" then
	       writecd #channel,i,signature'name$(i),signature'path'filename$(i),	&
	       					   		signature'row(i),	&
	       					   		signature'col(i),	&
	       					   		signature'width(i),	&
	       					   		signature'height(i),	&
	       					   		signature'all'pages(i)
	    endif
	next i
	close #channel

ENDPROCEDURE



PROCEDURE add'signature()
++pragma auto_extern

	xcall MIAMEX,95,cfg'page'sign'path'file$,"Imagens |*.jpg;*.bmp;*.pcx*","Assinatura digital",4,"jpg",file'name$

	if file'name$="" then
	   EXITPROCEDURE
	endif

	xcall strip,file'name$
	for i=len(file'name$) to 2 step -1
	    if file'name$[i;1]="." then
	       file'name$ = file'name$[1,i-1]
	       exit
	    endif
	next i

	for i=1 to 99
	    if signature'name$(i)="" then
	       cfg'page'assinatura'seq$ = i using "#ZZ"

	       signature'name$(i) = file'name$
	       signature'path'filename$(i) = cfg'page'sign'path'file$
	       signature'row(i) = cfg'page'assina'top
	       signature'col(i) = cfg'page'assina'left
	       signature'width(i) = cfg'page'assina'width
	       signature'height(i) = cfg'page'assina'height

	       signature'all'pages(i) = FN'evaluate'all'pages()

	       signatures'list$ = signatures'list$[1,-2] + cfg'page'assinatura'seq$ + "~" + strip(file'name$) + "~~"

	       exit
	    endif
	next i

	call cfg'page_refresh'controls()
	call cfg'page_aui_assina'img(CTLOP_CHG,cfg'page'sign'path'file$,MBST_CHANGE)

ENDPROCEDURE


PROCEDURE remove'signature()
++pragma auto_extern

	if val(cfg'page'assinatura'seq$)=0 then
	   EXITPROCEDURE
	endif

	xcall sbxmsg,MSG'EXIT,"Assinatura: "+signature'name$(val(cfg'page'assinatura'seq$))+chr(13)+chr(10)+ &
	             		 "Ficheiro: "+strip(cfg'page'sign'path'file$)+chr(13)+chr(10)+ &
	             		 "Confirma ?","Remover da lista",YES_NO,INTERROGADO
	if MSG'EXIT=27 then
	   EXITPROCEDURE
	endif

	signature'name$(val(cfg'page'assinatura'seq$)) = SPACE(50)
	signature'path'filename$(val(cfg'page'assinatura'seq$)) = SPACE(256)
	signature'row(val(cfg'page'assinatura'seq$)) = 0
	signature'col(val(cfg'page'assinatura'seq$)) = 0
	signature'width(val(cfg'page'assinatura'seq$)) = 0
	signature'height(val(cfg'page'assinatura'seq$)) = 0
	signature'all'pages(val(cfg'page'assinatura'seq$)) = 0

	call FN'list'remove'by'code(signatures'list$,cfg'page'assinatura'seq$)

	cfg'page'sign'path'file$ = ""
	cfg'page'assinatura'seq$ = "000"
	cfg'page'assina'top = 0
	cfg'page'assina'left = 0
	cfg'page'assina'width = 0
	cfg'page'assina'height = 0
	cfg'page'all'pages = 0

	call cfg'page_refresh'controls()
	call cfg'page_aui_assina'img(CTLOP_CHG,cfg'page'sign'path'file$,MBST_CHANGE)
	call cfg'page_aui_all'pages(CTLOP_CHG,cfg'page'all'pages,MBST_CHANGE)

ENDPROCEDURE


