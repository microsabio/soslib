
! Name: MODEL3
! Description: 
! Author: 
! Date: 
!
!----------------------------------------------------------------------------------------------------------------
! History:
!----------------------------------------------------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------


	PROGRAM MODEL3,1.0(100)

!{INCLUDES}
!--------------------------
++include ashinc:ashell.def
!--------------------------
++include model3.gen
!--------------------------
map1 set'focus,b,4
map1 exitcode,f,6
!--------------------------
!{MAP}


!{EVENTWAIT}
!--------------------------


       call mod3_load'dialog(mod3'dlgid)

       do
          xcall AUI,AUI_EVENTWAIT,mod3'dlgid,set'focus,exitcode,EVW_DESCEND+EVW_EDIT+EVW_EXCDFOCUS+EVW_EXCDINOUT

          if (abs(exitcode)>mod3_EXITCODE_BASE AND abs(exitcode)<=mod3_LAST_EXITCODE) then
             exitcode = fn'mod3_handle'controls(EDIT_MODE,abs(exitcode))
          endif

       loop until exitcode=1

       xcall AUI,AUI_CONTROL,CTLOP_DEL,mod3'dlgid
end


!{GEN PROCEDURES}
!--------------------------

procedure mod3_fld'pre(op as b1,fname$ as s24,do'nothing as b1)
++pragma auto_extern

STRSIZ 24                                                    ! needed for the SWITCH evaluation

       if op=DISPLAY_MODE then		       ! only on display
          SWITCH fname$
          DEFAULT
                 exit
          ENDSWITCH
       endif

       xputarg 1,op
       xputarg 2,fname$
       xputarg 3,do'nothing
endprocedure


function fn'mod3_fld'post(op as b1,exitcode as f6,fname$ as s24)
++pragma auto_extern

map1 fnext$,s,24

       fnext$ = fname$

STRSIZ 24                                                    ! needed for the SWITCH evaluation

       if (op<INFOP_DISPLAY AND exitcode#1) then    ! only on edit
                                                    ! validate the field we exit
          SWITCH fname$
          DEFAULT
                 exit
          ENDSWITCH
                                                    ! if clicked in one of our fields go there w/o loop in the EVENTWAIT
          if (exitcode#FIELD_INVALID AND (abs(exitcode)>mod3_EXITCODE_BASE AND abs(exitcode)<=mod3_LAST_EXITCODE)) then
             fnext$ = fn'mod3_fxid2name$(exitcode)
             SWITCH fnext$
             DEFAULT
                    exit
             ENDSWITCH
          endif
       endif

       fn'mod3_fld'post = exitcode
       xputarg 3,fnext$

endfunction

