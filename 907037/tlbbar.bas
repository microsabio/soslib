program TLBBAR,1.0(003)     ! TLBBAR.SBX
!--------------------------------------------------------------------------------
!SBX to create various types of toolbars and tool panels
!--------------------------------------------------------------------------------
!{Edit History}
! 03-12-2006 --- 003 --- Jorge Tavares
! - TLB_CHANGE_CTL (2) accepting range of controls to update
! 
! 03-12-2006 --- 002 --- Jorge Tavares
! - Some refinements about positioning and limit control for the Prev/Next buttons
! - New icons from ashico1 (arrow_up...right_blue) for Prev/Next buttons
! - Controls size redefined: SROW+WIDTH-1 (previously was SROW+WIDTH)
!
! 02-Dec-2006 --- 001 --- Jorge Tavares
! Created.
!--------------------------------------------------------------------------------

++pragma sbx

++include ashinc:ashell.def

++include fmwmap:tlbbar.map

map1 i,f,6
map1 ini'row,f,6
map1 end'row,f,6
map1 ini'col,f,6
map1 end'col,f,6
map1 parent,f,6
map1 srow,f,6
map1 erow,f,6
map1 scol,f,6
map1 ecol,f,6
map1 aui'action,b,2
map1 aui'text,s,100
map1 option,b,1
map1 item'indice,b,1
map1 item'indice2,b,1
map1 exitcode,f,6
map1 current'row,b,2
map1 current'column,b,2
map1 aui'state,b,4
map1 aui'winstyle,b,4
map1 auto'expand,b,1
map1 expand'row,f,6
map1 expand'col,f,6
map1 dummy'getarg,b,1
map1 item'default'type,b,4
map1 item'start'key,b,4
map1 item'default'height,b,2
map1 item'default'width,b,1
map1 item'icon'path,s,100
MAP1 limit,B,2
map1 what'changes,b,1
map1 action,b,1
map1 j,f,6
!{MAP}



	xgetarg 1,action
	if action>100 then
	   option = action - 100
	else
	   option = action
	endif
	xgetarg 2,control'structure

	switch option
	case 1
	   xgetarg 3,ini'row
	   xgetarg 4,ini'col
	   xgetarg 5,end'row
	   xgetarg 6,end'col
	   xgetarg 7,parent

	   if control'toolbar'type=0 then
	      xgetarg 8,control'toolbar'type
	   else
	      xgetarg 8,dummy'getarg
	   endif

	   if control'toolbar'orientation=0 then
	      xgetarg 9,control'toolbar'orientation

	      if control'toolbar'orientation=0 then
	         if control'horz'gap>0 then
	            control'toolbar'orientation = ORIENT_HORIZONTAL
	         else
	            control'toolbar'orientation = ORIENT_VERTICAL
	         endif
	      endif
	   else
	      xgetarg 9,dummy'getarg
	   endif

	   xgetarg 10,dummy'getarg
	   if dummy'getarg=1 then
	      control'arrows = 998
	   else
	      if dummy'getarg>1 then
	         control'arrows = dummy'getarg
	      endif
	   endif

	   xgetarg 11,item'default'type
	   xgetarg 12,item'start'key
	   xgetarg 13,item'default'height
	   xgetarg 14,item'default'width
	   xgetarg 15,item'icon'path

	   call create'toolbar

	   xputarg 3,ini'row
	   xputarg 4,ini'col
	   xputarg 5,end'row
	   xputarg 6,end'col

	   exit
	case 2
	   xgetarg 3,item'indice
	   xgetarg 4,item'indice2
	   if (item'indice>=control'first'item and item'indice<=control'last'item) then
	      xgetarg 5,what'changes
	      xgetarg 6,aui'state
	      xgetarg 7,aui'winstyle

	      call change'item
	   endif

	   exit
	case 3
	   xgetarg 3,exitcode

	   call update'toolbar

	   exit
	endswitch

	if action<100 then
	   xputarg 2,control'structure
	endif
end


create'toolbar:
	if control'toolbar'id=0 then
	   if (end'row=0 or end'col=0) then
	      aui'state = MBST_HIDE

	      auto'expand = 1
	   endif

	   if control'toolbar'type=0 then
	      control'toolbar'type = MBF_TAB
	   endif

	   aui'action = CTLOP_ADD
	else
	   aui'action = CTLOP_INFO
	endif

	xcall aui,AUI_CONTROL,aui'action,control'toolbar'id,"",aui'state,control'toolbar'type,"","",0,ini'row,ini'col,end'row,end'col,-2,-2,0,0,"","",parent,"","",""

	for i=1 to control'last'item
	    call adjust'item'settings
	next i

	call loop'itens

	if auto'expand then
	   aui'state = MBST_POS

	   if end'row=0 then
	      end'row = ini'row + expand'row
	   endif

	   if end'col=0 then
	      end'col = ini'col + expand'col
	   endif

	   xcall aui,AUI_CONTROL,CTLOP_CHG,control'toolbar'id,"",aui'state,control'toolbar'type,"","",0,ini'row,ini'col,end'row,end'col,-2,-2,0,0,"","",parent,"","",""
	   xcall aui,AUI_CONTROL,CTLOP_CHG,control'toolbar'id,"",MBST_SHOW,control'toolbar'type,"","",0,ini'row,ini'col,end'row,end'col,-2,-2,0,0,"","",parent,"","",""
	endif
return


change'item:
	if item'indice2<item'indice then
	   item'indice2 = item'indice
	endif

	for i=item'indice to item'indice2
	    xcall aui,AUI_CONTROL,CTLOP_INFO,ctl'id(i),"",0,0,"","",0,srow,scol,erow,ecol
	    xcall aui,AUI_CONTROL,CTLOP_DEL,ctl'id(i)

	    if (what'changes and CHG_STATE) then
	       ctl'state(i) = aui'state
	    endif
	    if (what'changes and CHG_STYLE) then
	       ctl'winstyle(i) = aui'winstyle
	    endif

	    aui'action = CTLOP_ADD
	    call toolbar'item
	next i
return

update'toolbar:
	xcall aui,AUI_CONTROL,CTLOP_INFO,control'toolbar'id,"",0,0,"","",0,ini'row,ini'col,end'row,end'col

	for i=control'first'item to control'last'item
	    if ctl'id(i)<>0 then
	       xcall aui,AUI_CONTROL,CTLOP_DEL,ctl'id(i)
	       ctl'id(i) = 0
	    endif
	next i

	if control'next'id>0 then
	   xcall aui,AUI_CONTROL,CTLOP_DEL,control'next'id
	   control'next'id = 0
	endif

	if control'prev'id>0 then
	   xcall aui,AUI_CONTROL,CTLOP_DEL,control'prev'id
	   control'prev'id = 0
	endif

	if exitcode=998 then
	   control'first'item = control'first'item - 1
	else
	   control'first'item = control'first'item + 1
	endif

	call loop'itens
return

loop'itens:
	limit = FN'limit()

	srow = control'top'gap
	scol = control'left'gap

	if (control'arrows>0 and control'first'item>1) then
	   switch control'toolbar'orientation
	   case ORIENT_VERTICAL
	      control'prev'id = FN'arrows(1)
	      if control'top'gap<1500 then
	         erow = 1500
	      else
	         erow = control'top'gap
	      endif
	      current'row = current'row + 1
	      exit
	   DEFAULT
	      control'prev'id = FN'arrows(3)
	      if control'left'gap<2 then
	         ecol = 2
	      else
	         ecol = control'left'gap
	      endif
	      current'column = current'column + 1
	      exit
	   endswitch
	else
	   current'row = 0
	   current'column = 0
	   erow = control'top'gap - control'vert'gap
	   ecol = control'left'gap - control'horz'gap
	endif

	for i=control'first'item to control'last'item
	    if FN'check'limits()=1 then
	       exit
	    endif

	    if ctl'id(i)=0 then
	       aui'action = CTLOP_ADD
	    else
		aui'action = CTLOP_CHG
		ctl'state(i) = ctl'state(i) AND MBST_CHGALL
	    endif

	    call toolbar'item
	next i
                                                                     ! create control with fixed srow,scol
	if i<=control'last'item then
	   j = i
	   aui'action = CTLOP_ADD
	   for i=j to control'last'item
		if (ctl'srow(i)>0 and ctl'scol(i)>0) then
	          call toolbar'item
	       endif
	   next i
	endif
return

toolbar'item:
	if ctl'srow(i)<>0 then
	   srow = ctl'srow(i)
	endif
	if ctl'scol(i)<>0 then
	   scol = ctl'scol(i)
	endif
	erow = srow + ctl'height(i)
	ecol = scol + ctl'width(i) - 1

	if (ctl'type(i) AND MBF_ICON)<>0 then
	   aui'text = ctl'file(i)
	else
	   aui'text = ctl'name(i)
	endif

	xcall aui,AUI_CONTROL,aui'action,ctl'id(i),aui'text,ctl'state(i),ctl'type(i),ctl'key(i),"",0, &
					   srow,scol,erow,ecol,-2,-2,0,0,"", &
					   ctl'tooltip(i),control'toolbar'id,"",ctl'winstyle(i)

	if auto'expand then                                           ! this is only possibly TRUE on create
	   if erow>expand'row then
	      expand'row = erow
	   endif
	   if ecol>expand'col then
	      expand'col = ecol
	   endif
	endif
return


adjust'item'settings:
	if (ctl'file(i)<>"" and item'icon'path<>"") then
	   if instr(1,ctl'file(i),"::")=0 then
	      if ctl'file(i)[1,2]<>"\\" then
	         if ctl'file(i)[3,4]<>":\" then
		     if item'icon'path[1,2]="::" then
		        ctl'file(i) = ctl'file(i) + item'icon'path
		     else
		        ctl'file(i) = strip(item'icon'path) + ctl'file(i)
		     endif
		  endif
  	      endif
	   endif
	endif

	if ctl'type(i)=0 then
	   if item'default'type=0 then
	      ctl'type(i) = MBF_BUTTON + MBF_KBD

	      if ctl'file(i)<>"" then
	         ctl'type(i) = ctl'type(i) + MBF_ICON + MBF_NODISTORT
	      endif
	   else
	      ctl'type(i) = item'default'type
	   endif
	endif

	if ctl'height(i)=0 then
	   if item'default'height=0 then
	      ctl'height(i) = 1000
	   else
	      ctl'height(i) = item'default'height
	   endif
	endif

	if ctl'width(i)=0 then
	   if item'default'width=0 then
	      ctl'width(i) = 8
	   else
	      ctl'width(i) = item'default'width
	   endif
	endif

	if ctl'key(i)="" then
	   if item'start'key>0 then
	      ctl'key(i) = "VK_xF" + str(item'start'key)
	      item'start'key = item'start'key + 1
	   endif
	endif
return

FUNCTION FN'limit()
++pragma auto_extern

	if control'arrows>0 then
	   if control'toolbar'orientation=ORIENT_VERTICAL then
	      FN'limit = end'row - ini'row
	   else
	      FN'limit = end'col - ini'col
	   endif
	else
	   if control'toolbar'orientation=ORIENT_VERTICAL then
	      FN'limit = end'row - ini'row + 1000
	   else
	      FN'limit = end'col - ini'col + 1
	   endif
	endif

endfunction


function FN'check'limits()
++pragma auto_extern

	if control'toolbar'orientation=ORIENT_VERTICAL then
	   srow = erow + control'vert'gap

	   FN'check'limits = &
	   FN'adjust'limits((srow + ctl'height(i)),srow,scol,current'row,control'nr'rows,limit,(ctl'width(i)+control'horz'gap),control'top'gap,control'arrows,ORIENT_VERTICAL,control'next'id)
	else
	   scol = ecol + control'horz'gap

	   FN'check'limits = &
	   FN'adjust'limits((scol + ctl'width(i) - 1),scol,srow,current'column,control'nr'columns,limit,(ctl'height(i)+control'vert'gap),control'left'gap,control'arrows,ORIENT_HORIZONTAL,control'next'id)
	endif

endfunction

function FN'adjust'limits(test'limit,current'coord,other'coord,current'col'row,nr'col'row,limit,increm'coord,start'position,control'arrows,orientation,arrow'id)

	FN'adjust'limits = 0

	if (nr'col'row>0 and current'col'row=nr'col'row) then
	   other'coord = other'coord + increm'coord
	   current'coord = start'position
	   current'col'row = 1
	else
	   if test'limit>limit then
	      if control'arrows=0 then
		  other'coord = other'coord + increm'coord
		  current'coord = start'position
		  current'col'row = 1
	      else
		  arrow'id = FN'arrows(orientation * 2)

		  FN'adjust'limits = 1
	      endif
	   else
	      current'col'row = current'col'row + 1
	   endif
	endif

	xputarg 2,current'coord
	xputarg 3,other'coord
	xputarg 4,current'col'row
	xputarg 11,arrow'id
endfunction


function FN'arrows(type)
++pragma auto_extern

map1 label$,s,100
map1 key$,s,10
map1 sr,b,4
map1 sc,b,4
map1 er,b,4
map1 ec,b,4
map1 cor,s,10,"_blue"

	switch type
	case 1
	   label$ = "arrow_up"+cor+"::ashico1"
	   key$ = "VK_xF" + str(control'arrows)
	   sr = 1000
	   sc = end'col - 1
	   er = 1500
	   ec = end'col
	   exit
	case 2
	   label$ = "arrow_down"+cor+"::ashico1"
	   key$ = "VK_xF" + str(control'arrows+1)
	   sr = end'row - ini'row - 1000
	   sc = end'col - 1
	   er = end'row - ini'row
	   ec = end'col
	   exit
	case 3
	   label$ = "arrow_left"+cor+"::ashico1"
	   key$ = "VK_xF" + str(control'arrows)
	   sr = control'top'gap
	   sc = 1
	   er = control'top'gap + 1000
	   ec = 2
	   exit
	case 4
	   label$ = "arrow_right"+cor+"::ashico1"
	   key$ = "VK_xF" + str(control'arrows+1)
	   sr = control'top'gap
	   sc = end'col - 1                                           !- ini'col - 1
	   er = control'top'gap + 1000
	   ec = end'col                                               ! - ini'col
	   exit
	endswitch

	xcall aui,AUI_CONTROL,CTLOP_ADD,FN'arrows,label$,0,MBF_STATIC + MBF_ICON + MBF_NODISTORT + MBF_KBD,key$, &
					    "",0,sr,sc,er,ec,-2,-2,0,0,"","",control'toolbar'id,"",0,0,1
endFUNCTION
