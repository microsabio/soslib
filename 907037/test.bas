
! Program Name: test
! Description:
! Author: Jorge Tavares
! Date: 10-11-2006
!----------------------------------------------------------------------------------------------------------------------------
!!{History}
! 21-11-2006 --- 001 --- Jack McGregor
! Refinements to XTREEs to test clickcode issues

!----------------------------------------------------------------------------------------------------------------------------
!{end history}
!

!----------------------------------
++include bpi:geral.bpi
!++include map:xtree.map
!++include def:xtree.def
!----------------------------------
![001] Second copy of XTRCTL...
MAP1 XTRCTL2         ! extended XTREE parameters
        MAP2 XTR2'OPCODE,B,1             ! 0=normal (create), 1=replace data,
                                        ! 2=append, 3=delete, 4=select,
                                        ! 5=delete one row
        MAP2 XTR2'CTLNO,B,1              ! 0=1st pcklst ctl, 1=2nd, 2=3rd
        MAP2 XTR2'ITEMLINES,B,1          ! max # dsp lines of text per item
        MAP2 XTR2'TREELINESTYLE,B,1      ! 0=none,1=solid,2=dotted
        MAP2 XTR2'SHOWBUTTONS0,B,1       ! Show level 0 tree btns (0=no, 1=yes)
        MAP2 XTR2'SHOWBUTTONS,B,1        ! Show level 1+ tree btns
        MAP2 XTR2'SHOWGRID,B,1           ! Show grid lines (0=no,1=yes)
        MAP2 XTR2'GRIDSTYLE,B,1          ! 0=vert, 1=horz, 2=both
                                        ! (0-2 = solid, 3-5 = dotted)
        MAP2 XTR2'TRUNCATED,B,1          ! Show truncation indicator
        MAP2 XTR2'SELECTAREA,B,1         ! Set to 1
        MAP2 XTR2'FLYBY,B,1              ! Fly by highlighting (0=no, 1=yes)
        MAP2 XTR2'SCROLLTIPS,B,1         ! Show scroll tips (0=no, 1=yes)
        MAP2 XTR2'COLUMNACTIVE,B,1       ! Active column (on entrance & exit)
        MAP2 XTR2'COLUMNSORT(3),B,1      ! Columns sorted by (1=1st col)
        MAP2 XTR2'SORTORDER(3),B,1       ! Sort order
                                        ! 0=none,1=ascending,2=descending
        MAP2 XTR2'KBDSTR,S,10            ! kbd click string
        MAP2 XTR2'USETHEMES,B,1          ! 1=use XP themes (if available)
        MAP2 XTR2'PARENTID,B,2           ! ID of parent control (e.g. TAB)
        MAP2 XTR2'SHOW3D,B,1             ! 1=use 3D style [105]
        MAP2 XTR2'HIDEHEADER,B,1         ! 1=hide header [106]
        MAP2 XTR2'XNAVCOD,B,1            ! [109] cell editing navigation code
        MAP2 XTR2'XNAVMASK,B,1           ! [109] internal use
        MAP2 XTR2'XROW,B,4               ! [108] current row
        MAP2 XTR2'XCOL,B,1               ! [108] current col
        MAP2 XTR2'EXPANDLEVEL,B,1        ! [111] 0=none, N=thru level N
        MAP2 XTR2'SKEY,S,10              ! [112] single-sel srch key (overrides answer)
        MAP2 XTR2'NEXTROW,B,4            ! [113] used internally
        MAP2 XTR2'DELCTLID,B,4           ! [113] ID of ctl to delete (progress)
        MAP2 XTR2'USECOLORDER,B,1        ! [114] use COLORDER() to reorder?
        MAP2 XTR2'COLORDER(31),B,1       ! [114] a(x) = real col # for disp col x
        MAP2 XTR2'TIMEOUT,B,4            ! [115] timeout (ms) (exitcode=11)
        MAP2 XTR2'LEFTPANECOLS,B,1       ! [116] (XTF_SPLIT) # cols in left pane
        MAP2 XTR2'LEFTPANEWIDTH,B,1      ! [116] (XTF_SPLIT) width of left pane
                                        !   0=optimize, -1=50/50, else col units
        MAP2 XTR2'CLOSEDENDED,B,1        ! [117] 1=last col not open ended
        map2 XTR2'XVALIDATE,B,1          ! [119] 1=must validate xrow,xrol
        MAP2 XTR2'CLICKROW,B,4           ! [118] last clicked on row
        MAP2 XTR2'CLICKCOL,B,1           ! [118] last clicked on col
        MAP2 XTR2'UNUSED2,X,19           ! [118] (was 25) [119] was 20


map1 XTREE2'PARAMS
    map2 XTREE2'COLDEF,S,400


                                   PROGRAM test,25.0(001)

!----------------------------------
!{INCLUDES}
!----------------------------------
define NR_FIELDS = 6
!----------------------------------

!----------------------------------
map1 list$,s,50
map1 array1
	map2 arr1(100)
		map3 col11,s,10
		map3 col12,s,10
		map3 col13,s,10

map1 arr1'sel(100)
	map2 cs11,s,10
	map2 cs12,s,10
	map2 cs13,s,10

! 1st
XTREE'COLDEF = "1~10~Coluna1~SEX~~11~10~Coluna2~SEX~~21~10~Coluna3~SEX~~"
col11(1) = "col1=1"
col12(1) = "col2=1"
col13(1) = "col3=1"
col11(2) = "col1=2"
col12(2) = "col2=2"
col13(2) = "col3=2"
col11(3) = "col1=3"
col12(3) = "col2=3"
col13(3) = "col3=3"
col11(4) = "col1=4"
col12(4) = "col2=4"
col13(4) = "col3=4"
col11(5) = "col1=5"
col12(5) = "col2=5"
col13(5) = "col3=5"
col11(6) = "col1=6"
col12(6) = "col2=6"
col13(6) = "col3=6"

map1 i1,f,6,6
map1 xtr1'id,f,6
map1 array2
	map2 arr2(100)
		map3 col21,s,10
		map3 col22,s,10
		map3 col23,s,10
map1 arr2'sel(100)
	map2 cs21,s,10
	map2 cs22,s,10
	map2 cs23,s,10
col21(1) = "col2=1"
col22(1) = "col2=1"
col23(1) = "col3=1"
col21(2) = "col2=2"
col22(2) = "col2=2"
col23(2) = "col3=2"
col21(3) = "col2=3"
col22(3) = "col2=3"
col23(3) = "col3=3"
col21(4) = "col2=4"
col22(4) = "col2=4"
col23(4) = "col3=4"
col21(5) = "col2=5"
col22(5) = "col2=5"
col23(5) = "col3=5"
col21(6) = "col2=6"
col22(6) = "col2=6"
col23(6) = "col3=6"
map1 i2,f,6,6
map1 xtr2'id,f,6

XTRCTL2 = XTRCTL    ! start 2nd xtree with same defaults as first
XTREE2'COLDEF = "1~10~Coluna1~SEX~~11~10~Coluna2~SEX~~21~10~Coluna3~SEX~~"

map1 XTREE1'EXITCODE,F
map1 XTREE2'EXITCODE,F

!{MAP}
!----------------------------------



start:
	xcall sbxgui,1,4,"Testing",id'obj,1,5000,5,15000,80
	xcall sbxgui,2,BT_OK,"",0,1,9000,0,0,44
	xcall sbxgui,2,BT_CANCEL,"",0,1,9000,0,0,54

list$ = "~1~zzzzz~3~ffffff~2~bbbbbbbb~~"
list$ = xfunc$("sbxcmb",list$,1)                                     ! by code
!list$ = xfunc$("sbxcmb",list$,1,1)                                  ! by descr
!TRACE.PRINT "start: list$=["+list$+"] "


	op'code = 2
	XTREE'FLAGS = XTF'DEF'ARRAY + XTF_EDITABLE
	for i=1 to NR_FIELDS
	    call fields
	next i

	op'code = 1
	XTREE'FLAGS = XTF'DEF'ARRAY + XTF_EDITABLE - XTF_NOSEL
	do
	   xcall aui,AUI_EVENTWAIT,id'obj,set'focus,exitcode,EVW_EXCDFOCUS+EVW_EDIT

	   if (exitcode>=(-100-NR_FIELDS) and exitcode<-100) then
	      i = abs(exitcode) - 100
	      call fields
	   endif

	   call handle'commands
	loop until (exitcode=EX_CANCEL or exitcode=1)
	? tab(-1,0);

end


handle'commands:
	   if exitcode=-301 then
	      xcall sbxmsg,0,"You clicked on the first field"
	   endif
return

fields:
	op'mouse = 100 + i
	on i call field1,field2,field3,field4,xtree1,xtree2
return

field1:
!	xcall sbxinp,"|&Name",op'code,2000,10,20,"","","",50,op'mouse,"name",0,exitcode
	xcall sbxinp,"|&Name",op'code,2000,10,10,"","||L",list$,50,op'mouse,list$,0,exitcode
return

field2:
	xcall sbxinp,"&Address",op'code,3000,10,10,"","","",50,op'mouse,"address",0,exitcode
return
field4:
	xcall sbxinp,"&Phone",op'code,4000,10,10,"","","",50,op'mouse,"phone",0,exitcode
return
field3:
	xcall sbxinp,"&Fax",op'code,5000,10,10,"","","",50,op'mouse,"fax",0,exitcode
return

xtree1:
	if op'code=2 then
	   XTR'OPCODE = 0
	   XTR'CTLNO = -1
	else
	   XTR'CTLNO = xtr1'id
	endif
	XTR'KBDSTR = op'mouse using "#ZZ"
	XTR'KBDSTR = CHR(7) + CHR(250) + XTR'KBDSTR + "."
    XTREE1'EXITCODE = op'mouse      ! [001]
trace.print "$# $T XTREE1 entrance: xnavcod,xnavmask=["+XTR'XNAVCOD+","+XTR'XNAVMASK &
        +"] clickrow,col,validate=["+XTR'CLICKROW+","+XTR'CLICKCOL+","+XTR'XVALIDATE+"]"

	xcall xtree,2000,25,arr1'sel(1),arr1(1),i1,XTREE'COLDEF,exitcode, &
		     8000,45,XTREE'FLAGS,"",MMOCLR,XTRCTL

    trace.print "$# $T XTREE1 exitcode=["+exitcode+"], xrow,xcol=["+XTR'XROW+"," &
        +XTR'XCOL+"] clickrow,col,validate=["+XTR'CLICKROW+","+XTR'CLICKCOL+","+XTR'XVALIDATE+"]"

	if op'code=2 then
	   xtr1'id = XTR'CTLNO
	endif

	! if we clicked to the other xtree, transfer the
	! clicked on info to its copy of XTRCTL
	if abs(exitcode)=XTREE2'EXITCODE then
	   XTR2'CLICKCOL = XTR'CLICKCOL
	   XTR2'CLICKROW = XTR'CLICKROW
	   XTR2'XNAVCOD = XTR'XNAVCOD
        endif
        if exitcode = -48 goto xtree1       ! [002]

return

xtree2:
	if op'code=2 then
	   XTR2'OPCODE = 0
	   XTR2'CTLNO = -1
	else
	   XTR2'CTLNO = xtr2'id
	endif
	XTR2'KBDSTR = op'mouse using "#ZZ"
	XTR2'KBDSTR = CHR(7) + CHR(250) + XTR2'KBDSTR + "."
    XTREE2'EXITCODE = op'mouse      ! [001]
trace.print "$# $T XTREE2 entrance: xnavcod,xnavmask=["+XTR2'XNAVCOD+","+XTR2'XNAVMASK &
        +"] clickrow,col,validate=["+XTR2'CLICKROW+","+XTR2'CLICKCOL+","+XTR2'XVALIDATE+"]"

	xcall xtree,2000,50,arr2'sel(1),arr2(1),i2,XTREE2'COLDEF,exitcode, &
		     8000,70,XTREE'FLAGS,"",MMOCLR,XTRCTL2

trace.print "$# $T XTREE2 exitcode=["+exitcode+"], xrow,xcol=["+XTR2'XROW+"," &
        +XTR2'XCOL+"] clickrow,col,validate=["+XTR2'CLICKROW+","+XTR2'CLICKCOL+","+XTR2'XVALIDATE+"]"

	if op'code=2 then
	   xtr2'id = XTR2'CTLNO
	endif

	! if we clicked to the other xtree, transfer the
	! clicked on info to its copy of XTRCTL
	if abs(exitcode)=XTREE1'EXITCODE then
	   XTR'CLICKCOL = XTR2'CLICKCOL
	   XTR'CLICKROW = XTR2'CLICKROW
	   XTR'XNAVCOD = XTR2'XNAVCOD
        endif

        if exitcode = -48 goto xtree2       ! [002]


return




