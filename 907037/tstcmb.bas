program tstcmb,1.0(100)      ! test sbxcmb.sbx
!------------------------------------------------------------------
! More detailed description
! 
!------------------------------------------------------------------
!{Edit History}
! 16-08-2006 --- 100 --- Jack McGregor
! Created
!{End Edit History}
!------------------------------------------------------------------

map1 code'and'descr,b,1
map1 sort'descr,b,1
map1 listsize,f
!------------------------------------------------------------------

    ? tab(-1,0);"Simple test of sbxcmb.sbx"
    input "Enter size of list in bytes: ", listsize
    dimx list(1),s,listsize
    input "Enter code'and'descr, sort'descr: ",code'and'descr,sort'descr
    ? "Enter your list (~ as the delimiter) "
    input line "? ",list(1)
    
    xcall sbxcmb,list(1),code'and'descr,sort'descr
    
    ? "Sorted list:"
    ? list(1)
    end
    
    