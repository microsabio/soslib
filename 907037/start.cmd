:<(FIRMAWARE) GUI routines from Jorge

HELP.SBX       - Helper routine used by SBXINP
SBXCMB.SBX/BAS - Fancy version of a combo box, with sorting options
SBXINP.SBX/BAS - Wrapper for INFLD
SBXINF.SBX     - Helper SBX (copy to BAS:)
SBXMSG.SBX/BAS - Message box wrapper
TLBBAR.BAS/SBX - Create "tool bar" 
TSTTLB.BAS     - Sample/test for TLBBAR.SBX
VALIDA.SBX     - Helper SBX
MODEL1.BAS     - A simple model program illustrating SBXINP, EVENTWAIT.

Note: many of the programs here reference the ersatzes FMWBMP:, FMWMAP:,
and FMWDEF:, all of which have been consolidated in [907,15] for the
purposes of SOSLIB.  (Add them to your ersatz table).
>

 
