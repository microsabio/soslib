! Name: SBXCMB.SBX
! Author: Jorge Tavares
! Date: 16-Aug-2006
! Description: sorts and return a received string structured for combo lists
! Parameters:
!    list'wk - [string,in/out]
!         The original list; can be in two formats (code+descr or simple)
!         The list separator is defined in the 5th parameter (if empty "~" is considered)
!         Returned sorted based on next two params
!    code'and'descr - [numeric,in]
!         If <>0 indicates list'wk is in code+descr format
!    sort'descr - [string,input]
!         Only used for code+descr format and if <>0 means that sort
!         is by descr, otherwise will be sorted by code
!    path'file'sort - [string,in,optional]
!         path and filename for temporary sort file, killed at the end
!         if not specified, a temporary filename will be generated in
!         the %temp% directory.
!    sep$ - [string,in,optional]
!	   receives the list separator, if empty, it will be considered the "~"
!--------------------------------------------------------------------------------------------------------------
! Updates:
!{Edit History}
! 16-08-2006 --- 001 --- Jack McGregor
! Adjust first parameter to be auto-dimensioned based on incoming param
! 17-08-2006 --- 002 --- Jorge Tavares
! . Remove possible first empty element from sort process adding it at the end (avoids the need that the initial empty
!   element "code", for code+descr structured list, match the size "code" of the other elements)
! . With the above, no need to add a trailling space at the end of each element to guarantee its not empty
! . Message for incorrect list passed along from the main program
! 15-04-2007 --- 003 --- Jorge Tavares
! . SEP$ was added as 5th parameter allowing to receive the list separator (default is "~")
! 05-02-2009 --- 004 --- Jorge Tavares
! .use of "input line" instead of "input" on reading the sort temporary file
!  this was causing that, items from the list with embedded commas become splitted and build a bad structured list
! 03-10-2018 --- 005 --- Jack McGregor
!   Minor modernization (eliminate xcall.bpi) and recompilation
!{End Edit History}
!--------------------------------------------------------------------------------------------------------------
!
!
!--------------------------------------------------------------------------------------------------------------

++pragma sbx

	program sbxcmb,1.0(005)

![005]++include xcall.bpi     ! [001] Needed for XCB'PSIZE()

![001] map1 list'wk,s,1000     ! should be dimensioned accordingly the received list size
                        ! to review
map1 count,f,6
map1 max'size'element,f,6
map1 i'start,f,6
map1 i'end,f,6
map1 i,f,6
map1 key'size,f,6
map1 key'pos,f,6
map1 code'and'descr,b,1
map1 even'count,b,1
map1 sort'descr,b,1
map1 start'of'list,s,20
map1 end'of'list,s,6
map1 separator'pos,f,6
map1 path'file'sort,s,100
map1 sep$,s,1

    ! [001] verify that we have at least one param; (if not, abort)
    if .argcnt < 1 then     ! [005] XCBCNT < 1 then
        return("")          ! (might be nice to display a warning message too)
        end
    endif

    ![001] dimension an array to hold the incoming list'wk
    ![001] note: dimx requires an array, so we create an array of (1)
    dimx list'wk(1),s,.argsiz(1)                                    ! [005] XCB'PSIZE(1)  ! [001]
    DEBUG.PRINT "Dimensioning array of " + .argsiz(1) + " bytes"    ! [005] XCB'PSIZE(1)+" bytes"

	xgetargs list'wk(1),code'and'descr,sort'descr,path'file'sort,sep$

	xcall strip,list'wk(1)

	if sep$="" then
	   sep$ = "~"
	endif

	if (list'wk(1)<>"" AND (sep$#"~" OR (sep$="~" AND list'wk(1)[1,1]="~" AND list'wk(1)[-2,-1]="~~"))) then		! trying to verify basics for valid list

	   if path'file'sort="" then
                                                                     ! add date+time to avoid concurrent sorts
	      xcall ODTIM,path'file'sort,DATE,TIME,(8+32+256)
	      path'file'sort = "%temp%\combo_srt_" + path'file'sort[7;4] &
	            + path'file'sort[4;2] + path'file'sort[1;2] &
	       		+ path'file'sort[12;2] + path'file'sort[15;2] &
	       		+ path'file'sort[18;2] + ".tmp"
	   endif

	   call sort'list

	   xputarg 1,list'wk(1)                                          ! xcall
	   return(list'wk(1))                                            ! xfunc$
	else
	   xcall SBXMSG,0,"Incorrect list structure","SBXCMB: Error",0,16        ! [002]
	endif
end


sort'list:
	open #35,path'file'sort,output
	                                                                 ! [002] remove, if exists, the initial blank element
	start'of'list = ""
	i'start = 2
	if code'and'descr then
	   i'end = instr(2,list'wk(1),sep$) - 1
	   if strip(list'wk(1)[2,i'end])="" then
	      i'start = i'end + 2
	      i'end = instr(i'start,list'wk(1),sep$) - 1
	      if strip(list'wk(1)[i'start,i'end])="" then
	         start'of'list = list'wk(1)[1,i'end]
	         list'wk(1) = list'wk(1)[i'end+1,-1]
	      endif
	   endif
	else
	   i'end = instr(2,list'wk(1),sep$) - 1
	   if strip(list'wk(1)[2,i'end])="" then
	      start'of'list = list'wk(1)[1,i'end]
	      list'wk(1) = list'wk(1)[i'end+1,-1]
	   endif
	endif

	list'wk(1) = list'wk(1)[1,-2]
	end'of'list = sep$ + sep$

	if code'and'descr then
	   if list'wk(1)[-4,-1]=("*"+sep$+"*"+sep$) then
	      end'of'list = sep$ + "*" + sep$ + "*" + sep$ + sep$
	      list'wk(1) = list'wk(1)[1,-5]
	   endif
	else
	   if list'wk(1)[-2,-1]=("*"+sep$) then
	      end'of'list = sep$ + "*" + sep$ + sep$
	      list'wk(1) = list'wk(1)[1,-3]
	   endif
	endif

	count = 0
	max'size'element = 0
	i'end = 1
	i'start = 2
	even'count = 0

	do
	   i'end = instr(i'start,list'wk(1),sep$)

	   if i'end=0 then
	      exit
	   endif

	   if code'and'descr then
	      if even'count then
	         even'count = 0
	      else
	         even'count = 1
	         list'wk(1)[i'end;1] = "�"
	         REPEAT
	      endif
	   endif

	   count = count + 1

	   print #35,list'wk(1)[i'start-1,i'end-1]

	   if (i'end-i'start+1)>max'size'element then
	      max'size'element = i'end - i'start + 1
	   endif

	   i'start = i'end + 1
	loop

	close #35

DIMX element(1),s,max'size'element

	if code'and'descr then
	   separator'pos = instr(2,list'wk(1),"�")

	   if sort'descr then
	      key'pos = separator'pos + 1
	      key'size = max'size'element - separator'pos
	   else
	      key'size = separator'pos - 1
	      key'pos = 2
	   endif
	else
	   key'size = max'size'element
	   key'pos = 2
	endif


	open #35,path'file'sort,input
	xcall basort,35,35,max'size'element,key'size,key'pos,0
	close #35

	open #35,path'file'sort,input

	list'wk(1) = ""
	for i=1 to count
	    input line #35,element(1)
DEBUG.PRINT "sort'list: ["+element(1)+"]"

	    if code'and'descr then
	       element(1)[separator'pos;1] = sep$
	    endif

	    list'wk(1) = list'wk(1) + strip(element(1))               ! [002] not adding a final space because, possible
	                                                              !       first blank element was removed from sort
	next i

	close #35
	kill path'file'sort

	list'wk(1) = start'of'list + list'wk(1) + end'of'list         ! [002]
return

