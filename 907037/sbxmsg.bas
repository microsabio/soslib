
! SBXMSG.SBX
!
!	XCALL SBXMSG,EXITCODE,MSG,TITMSG,MBUTTON,MICON,MOPTIONS
!
!
!
!	EXITCODE
!
!
!
!
! 

++pragma sbx


MAP1 YES'NO'CANCEL,F,6,3
map1 a$,s,1

MAP1 mflagslo,F,6
MAP1 mflagshi,F,6

map1 exitcode,F,6
map1 msg,s,512
map1 titmsg,s,80
map1 mbutton,F,6
map1 micon,F,6
MAP1 moptions,F,6,256

	xgetargs exitcode,msg,titmsg,mbutton,micon,moptions


	on error goto erro'sbx



	call trata'msg


	xputarg 1,exitcode

END


trata'msg:


	mflagslo = (mbutton + micon + moptions) AND 255
	mflagshi = INT((mbutton + micon + moptions) / 256)

	? TAB(-10,17);CHR(mflagslo + 32);CHR(mflagshi + 32);msg; &
		     ;CHR(126);titmsg;CHR(127);

	XCALL GET,a$
	if (a$=CHR(33) OR a$=CHR(36) OR a$=CHR(38))	then
	   exitcode = 0
	else 
		if (a$=CHR(37) OR (a$=CHR(39) AND mbutton=YES'NO'CANCEL)) then
		 	exitcode = 11	
		else
			exitcode = 27
		endif
	endif

return	


!====================================
! Erro SBX
!====================================
erro'sbx:
	? "Erro SBXMSG ";ERR(0);",";ERR(1);",";ERR(2)
end
