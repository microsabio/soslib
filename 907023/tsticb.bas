program TSTICB,1.0(105)		! test ICOBAR.SBX
!-----------------------------------------------------------------------
!EDIT HISTORY
![100] September 12, 2004 12:05 PM      Edited by Jack
!   Created
![101] February 08, 2005 04:40 PM       Edited by joaquin
!   Minor aesthetic improvements
![102] October 17, 2006 03:24 PM        Edited by joaquin
!   Use ashell.def; test MBF_STATIC+MBF_ICON by using it for
!   first icon (rest are MBF_BUTTON+MBF_STATIC)
![103] October 18, 2006 01:47 PM        Edited by joaquin
!   Make some of the filespecs long and fully qualified to test
!   968.7 fix to allow retrieval of specs > 40; don't prepend
!   ICODIR if spec is fully qualified
![104] October 19, 2006 04:45 PM        Edited by joaquin
!   Add variations for BUTTON+BMP, BUTTON+JPG, gear::ashico1
![105] October 30, 2006 05:23 PM        Edited by joaquin
!   Minor adjustment to not try to add ".\" if spec starts with %
!-----------------------------------------------------------------------

++include ashinc:ashell.def

MAP1 PARAMS
	MAP2 OPCODE,F
	MAP2 POSCOD,S,3
	MAP2 COUNT,F

MAP1 MAX'COUNT,F,6,40
MAP1 ICON'ARYX
        MAP2 ICON'ARY(40)
            MAP3 ICON'FSPEC,S,80
            MAP3 ICON'TYPE,F,6
            MAP3 ICON'CMD,S,64
	    MAP3 ICON'TIP,S,80

MAP1 ICODIR,S,80,".\"	! location of icon files (native directory)
MAP1 STATUS,F
MAP1 I,F

	! load up the ICON'ARY with icons
! 328292 = MBF_KBD + MBF_STATIC + MBF_ICON
!  66048 = MBF_KBD + MBF_BUTTON + MBF_ICON
! 327936 = MBF_KBD + MBF_STATIC + MBF_BITMAP
!  65792 = MBF_KBD + MBF_BUTTON + MBF_BITMAP
!

![102] try one with MBF_STATIC (should work as of 968.6)
![103] try to test all permutions of static/button + bitmap/icon
DATA "%miame%\dsk0\907023\Control_Panel.ico",328292,"1","Control Panel Tip #1 (static)"

DATA "Control_Panel3.ico",66048,"2","Control Panel Tip #3 (button)"
!DATA "Documents_Settings.ico",66048,"3","Tip #3 (button)"
DATA "arrow.ico",66048,"3","Tip #3 (button)"

DATA "My_Pictures3.ico",66048,"4^M","My Pictures"
DATA "email.ico",66048,"5","Send Email (button)"
DATA "#winptr",328292,"6","#winptr (static)"
DATA "gear::ashico1",66048,"7","button icon from dll"
DATA "exit::ashico1",328292,"8","static icon from dll (no scaling)"

DATA "dice1.bmp",65792,"9","button bitmap"
DATA "dice2.bmp",327936,"10","static bitmap"

!DATA "sample.jpg",65792,"11","button jpg"
DATA "arrow.png",65792,"11","static png"
DATA "coffee.jpg",327936,"12","static jpg"
DATA "",0,"",""

!DATA "c:\vm\miame\dsk0\907023\Control_Panel2.ico",66048,"2^M","Control Panel Tip #2 (button)"
!DATA "Open_Folder.ico",66048,"9^M","Open Folder"
!DATA "Recent_Documents.ico",66048,"10^M","Recent Documents"
!DATA "Search.ico",66048,"11^M","Search"
!DATA "Run2.ico",66048,"12^M","Run"
!DATA "My_Downloads.ico",66048,"7^M","My Downloads"
!DATA "My_Documents.ico",66048,"6^M","My Documents"
!DATA "Documents_Settings.ico",66048,"4^M","Tip #4 (button)"
!DATA "#info",66048,"6","#info (button)"


	COUNT = 0
LOAD'LOOP:
	COUNT = COUNT + 1
	if COUNT > MAX'COUNT goto DO'IT
	read ICON'FSPEC(COUNT),ICON'TYPE(COUNT),ICON'CMD(COUNT),ICON'TIP(COUNT)
	if ICON'FSPEC(COUNT)="" then COUNT = COUNT - 1 : goto DO'IT
        ![103] prepend ICODIR only if spec not fully qualified
        if ICON'FSPEC(COUNT)[1,1] # "\" &
        and ICON'FSPEC(COUNT)[1,1] # "#" &
        and ICON'FSPEC(COUNT)[1,1] # "%" &
        and instr(1,ICON'FSPEC(COUNT),":")<1 &
        and instr(1,ICON'FSPEC(COUNT),"[")<1 then &
	    ICON'FSPEC(COUNT) = ICODIR + ICON'FSPEC(COUNT)
	GOTO LOAD'LOOP
DO'IT:
	? tab(-1,0);
	OPCODE = 1
	!POSCOD = "T35"	! Top, 3x5
    POSCOD = "R24"  ! Right side, 2X4

	? tab(5,1);"Loading following icons into icon bar..."
	for I = 1 to 12
            ? tab(5+I,2);str(I);" ";ICON'FSPEC(I);tab(5+I,48);
            if ICON'TYPE(I) and MBF_STATIC then
                ? "MBF_STATIC ";
            else
                ? "MBF_BUTTON ";
            endif
            if ICON'TYPE(I) and MBF_ICON then ? "MBF_ICON ";
            if ICON'TYPE(I) and MBF_BITMAP then ? "MBF_BITMAP ";
            ?
        next I

	? tab(21,1);"Calling ICOBAR.SBX,";OPCODE;",";POSCOD;",..."
	STATUS = XFUNC("ICOBAR",OPCODE,POSCOD,ICON'ARYX,COUNT)
	? tab(22,1);"STATUS = ";STATUS
	? "(Each icon has a tooltip and transmits a number when clicked"

	end

