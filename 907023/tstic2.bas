program TSTIC2,1.0(102)		! Variation of TSTICB but with DLL icons
!-----------------------------------------------------------------------
!EDIT HISTORY
![100] September 12, 2004 12:05 PM      Edited by Jack
!   Created
![101] February 08, 2005 04:40 PM       Edited by joaquin
!   Minor aesthetic improvements	
![102] February 22, 2005 11:17 AM       Edited by joaquin
!   Modified from TSTICB to reference icons within a DLL and to 
!   test the MBF'NODISTORT option; use SCRSTS for cleaner screen
!-----------------------------------------------------------------------

MAP1 PARAMS
	MAP2 OPCODE,F
	MAP2 POSCOD,S,3
	MAP2 COUNT,F
	
MAP1 MAX'COUNT,F,6,40
MAP1 ICON'ARYX
        MAP2 ICON'ARY(40)
            MAP3 ICON'FSPEC,S,80
            MAP3 ICON'TYPE,F,6      
            MAP3 ICON'CMD,S,64    
	    MAP3 ICON'TIP,S,80

MAP1 ICODIR,S,80,""	! DLL Resources don't use directory specifiers
MAP1 STATUS,F
MAP1 I,F
	! load up the ICON'ARY with icons (from ashico1.dll) [102]
        ! Note: 4259840 = MBF'KBD (65536) + MBF'NODISTORT (4194304)
DATA "navigate_beginning::ashico1.dll",4259840,"1 ","Go to beginning"
DATA "navigate_end::ashico1.dll",4259840,"2 ","Go to end"
DATA "navigate_left::ashico1.dll",4259840,"3 ","Go back 1"
DATA "navigate_left2::ashico1.dll",4259840,"4 ","Go back several"
DATA "navigate_right::ashico1.dll",4259840,"5 ","Go forward 1"
DATA "navigate_right2::ashico1.dll",4259840,"6 ","Go forward several"
DATA "navigate_up::ashico1.dll",4259840,"7 ","Go up 1"
DATA "navigate_up2::ashico1.dll",4259840,"8 ","Go up several"
DATA "navigate_down::ashico1.dll",4259840,"9 ","Go down 1"
DATA "navigate_down2::ashico1.dll",4259840,"10 ","Go down several"
DATA "stop::ashico1.dll",4259840,"11 ","Stop"
DATA "unknown::ashico1.dll",65536,"12 ","Mystery"  ! (allow stretch for comparison)
DATA "",0,"",""

	COUNT = 0
LOAD'LOOP:
	COUNT = COUNT + 1
	if COUNT > MAX'COUNT goto DO'IT
	read ICON'FSPEC(COUNT),ICON'TYPE(COUNT),ICON'CMD(COUNT),ICON'TIP(COUNT)
	if ICON'FSPEC(COUNT)="" then COUNT = COUNT - 1 : goto DO'IT
	ICON'FSPEC(COUNT) = ICODIR + ICON'FSPEC(COUNT)
	GOTO LOAD'LOOP
DO'IT:
        ? "Button size & position code (T35=top 3x5; R24=right 2x4, etc.) ";
        input "",POSCOD
	? tab(-1,0);
	OPCODE = 1
!	POSCOD = "T44"	! Top, 2,2


![102]	? tab(6,1);"Loading following icons into icon bar..."
![102]	for I = 1 to 11 : ? ICON'FSPEC(I) : NEXT I

        ! [102] use SCRSTS.SBX to provide a nice place for info...
        xcall SCRSTS,0,8,15,12,55       ! create box
        xcall SCRSTS,1,"Loading following icons into icon bar..."
        for I = 1 to COUNT
            xcall SCRSTS,1,ICON'FSPEC(I)
        next I
        xcall SCRSTS,1,"Note that the last button does not specify the " &
        + "MBF'NODISTORT option, so its image will fill the rectangle " &
        + "even if the buttons are not square."

![102]	? tab(20,1);"Calling ICOBAR.SBX,";OPCODE;",";POSCOD;",..."
        xcall SCRSTS,1,"Calling ICOBAR.SBX,"+OPCODE+","+POSCOD+"..."
	STATUS = XFUNC("ICOBAR",OPCODE,POSCOD,ICON'ARYX,COUNT)
![102]	? tab(21,1);"STATUS = ";STATUS
        xcall SCRSTS,1,"STATUS = "+STATUS
        
      	? tab(16,15);"(Each icon has a tooltip and transmits a number when clicked)"
        ? tab(17,15);"Try clicking >";
	end

