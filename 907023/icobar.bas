program ICOBAR,1.0(102)     ! SBX to draw a "toolbar" of icons
!-----------------------------------------------------------------------
!EDIT HISTORY
![100] September 12, 2004 12:05 PM      Edited by Jack
!   Created
![101] February 08, 2005 04:32 PM       Edited by joaquin
!   Remove debug messages, switch to XGETARGS	
![102] February 22, 2005 11:47 AM       Edited by joaquin
!   Now actually works for R, B, and L (not just T); also, use 
!   TRMCHR to retrieve actual screen (or dialog) size
!-----------------------------------------------------------------------
!USAGE:
!   xcall ICOBAR,OPCODE,POSCOD,ICOARYX,COUNT
!WHERE:
!   OPCODE=1 to create, 2 to modify, 3 to delete
!   POSCOD (S,2)=<T|L|R|B><height><width>
!       example: "T23" Top, 2x3 (horiz line in row 3)
!                "R34" Right 3x4 (vert line in col scrwid-5)
!
!   MAP1 ICOARYX
!     MAP3 ICOARY(COUNT)
!       MAP3 ICON'FSPEC,S,80
!       MAP3 ICON'TYPE,F,6      ! MX'WINCTL type codes (MBF_xxxx)
!       MAP3 ICON'CMD,S,64    
!
!  COUNT = # of items in array (max is 40)
!------------------------------------------------------------------------
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"


MAP1 PARAMS
        MAP2 OPCODE,F
        MAP2 POSCOD,S,3
        MAP2 COUNT,F

MAP1 ICON'ARYX
        MAP2 ICON'ARY(40)
            MAP3 ICON'FSPEC,S,80
            MAP3 ICON'TYPE,F,6      
            MAP3 ICON'CMD,S,64    
            MAP3 ICON'TIP,S,80

MAP1 MISC
        MAP2 SROW,B,1
        MAP2 SCOL,B,1
        MAP2 EROW,B,1
        MAP2 ECOL,B,1
        MAP2 STATUS,F
        MAP2 HI,B,1                        ! icon height
        MAP2 WI,B,1                        ! icon width
        MAP2 I,F

++include ashinc:ASHELL.DEF
++include ashinc:MSBOXX.BSI                ! MSBOXX symbols
++include ashinc:XCALL.BSI                 ! picks up param list
++include ashinc:TRMCHR.MAP                ! [102]

BEGIN:
        if XCBCNT < 4 then
            xcall MESAG,"Insufficient parameters in ICOBAR.SBX",1
            return(-1)
        endif
        
![101]  xcall MIAMEX,MX'XCBDATA,XCBADR,XCBGET,1,OPCODE
![101]  xcall MIAMEX,MX'XCBDATA,XCBADR,XCBGET,2,POSCOD
![101]  xcall MIAMEX,MX'XCBDATA,XCBADR,XCBGET,3,ICON'ARYX
![101]  xcall MIAMEX,MX'XCBDATA,XCBADR,XCBGET,4,COUNT
	xgetargs OPCODE,POSCOD,ICON'ARYX,COUNT		! [101]

        HI = POSCOD[2,2]
        WI = POSCOD[3,3]        
        if (instr(1,"TLRB",POSCOD[1,1]) < 1) or HI=0 or WI=0 then
            xcall MESAG,"Invalid POSCOD ("+POSCOD+")",1
            return(-1)
        endif

        ! [102] get screen dimensions and force our button group in bounds
        TRMCHR'WINROW = -1    ![103] (this needed to query ATE client)
        xcall TRMCHR,TRMCHR'STATUS,TRMCHR'MAP
        if TRMCHR'WINROW > 0 then         ![102] if in a dlg, 
            TRMCHR'ROWS = TRMCHR'WINROW   ![102] use dlg dimensions instead of scrn
            TRMCHR'COLS = TRMCHR'WINCOL
        endif

        if POSCOD[1,1]="T" then
                SROW = HI + 1
                EROW = SROW
                SCOL = 1
                ECOL = TRMCHR'COLS  ! [102] was 80
                BOXCOD = BOX'HLI
                call DRAW'LINE
                SROW = 1            ! [102] reset starting button pos
        endif
        if POSCOD[1,1]="B" then
                SROW = TRMCHR'ROWS - HI - 1     ! [102] was 24 - HI - 1
                EROW = SROW
                SCOL = 1
                ECOL = TRMCHR'COLS              ! [102] was 80
                BOXCOD = BOX'HLI                
                call DRAW'LINE
                SROW = SROW + 1     ! [102] reset starting button pos
        endif
        if POSCOD[1,1]="L" then
                SROW = 1
                SCOL = WI + 1
                ECOL = SCOL
                EROW = TRMCHR'ROWS      ! [102] was 24
                BOXCOD = BOX'VLI
                call DRAW'LINE
                SCOL = 1            ! [102] reset starting button pos
        endif
        if POSCOD[1,1]="R" then
                SROW = 1
                SCOL = TRMCHR'COLS - WI - 1     ! [102] was 24 - WI - 1
                ECOL = SCOL
                EROW = TRMCHR'ROWS              ! [102] was 24
                BOXCOD = BOX'VLI
                call DRAW'LINE
                SCOL = SCOL + 1     ! [102] reset starting button pos
        endif

        ! draw the icons (assume top for now)
![102]  SROW = 1
![102]  SCOL = 1
        for I = 1 to COUNT
            EROW = SROW + HI - 1
            ECOL = SCOL + WI - 1
            ICON'TYPE(I) = ICON'TYPE(I) or MBF_ICON

!? TAB(6+I,40);ICON'FSPEC(I)

![102]      print TAB(-10,20);"1,0,";ICON'FSPEC(I); &
!               ",0,";ICON'TYPE(I);",";ICON'CMD(I);",,"; &
!               SROW;",";SCOL;",";EROW;",";ECOL;",,,,,,"; &
!               ICON'TIP(I);chr(127);

            ! [102] use AUI equivalent of above (no functional difference)
            xcall AUI,"CONTROL",1,0,ICON'FSPEC(I),MBST_ENABLE, &
                ICON'TYPE(I),ICON'CMD(I),"","",SROW,SCOL,EROW,ECOL, &
                -2,-2,0,0,"",ICON'TIP(I)

            ! [102] adjust next row or column based on our orientation
            if POSCOD[1,1]="T" or POSCOD[1,1]="B" then 
                SCOL = ECOL + 1     ! horizontal row
            else
                SROW = EROW + 1     ! vertical column
            endif
        next I

        return(STATUS)

!------------------------------------------------------------------------
NOTYET:
        xcall MESAG,"Sorry, ICOBAR.SBX not yet available in text mode!",1
        return

!------------------------------------------------------------------------

DRAW'LINE:
        BOXCOD = BOXCOD + BOX'BDR
! ? TAB(22,1);SROW;SCOL;EROW;ECOL;BOXCOD
        xcall MSBOXX,SROW,SCOL,EROW,ECOL,BOXCOD,BOXSTS
        return
