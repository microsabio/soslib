:<(DART+DVERTO) (Madics' Data Analysis And Reporting Tool interface routines)

NOTE: DART itself must be licensed from Madics, and must be downloaded
and installed before using the DART.SBX.  An eval version, docs, videos,
etc., can be downloaded from:

    http://www.da-rt.com/

First install DART and manually launch it one time to get it initialized.

    DART.BAS/SBX  - Routine takes a CSV & DEF file and produces charts
    DARTX.BAS     - Sample program
    HISSAL.CSV    - Sample CSV
    HISSAL.CSE    - European version (DD/MM/YY dates; rename to CSV if appl)
    HISSAL.DEF    - Sample DEF
    GETREG.SBX    - Utility routine used by DART
    FULPTH.SBX    - Utility routine used by DART
    EXISTL.SBX    - Utility routine used by DART
    DELMEM.SBX    - Utility routine used by DART
    WINATE.SBX    - Utility routine used by DART
    DARTHIST.HTM  - History

Hit ENTER to display history file, or START2 to display additional notes
>
:K
SHLEXC .\darthist.htm
