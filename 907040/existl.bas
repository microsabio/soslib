!========================================================================
! EXISTL - Local File Lookup
!========================================================================
! Supported under ATE and Ashell Windows.
! ZTerm/dumb terminals etc return -1.
!
!Usage:
!	XCALL EXISTL,filename,size
!
!	Size returns: -1   - not support ZTerm Etc.
!		       0   - not found
!	              >0   - found, and byte size.
!
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps}
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 MISC
 MAP2 GUI'AVAIL,F,6
 MAP2 ATE'USED,F,6
 MAP2 NULL'PARAM,F,6
 MAP2 LOCAL'USED,F,6

MAP1 PARMS
 MAP2 X'FILE,S,510
 MAP2 X'SIZE,F,6

MAP1 LOOKUP'LOCAL
	MAP2 LOOKUP'LOCATION,S,1
	MAP2 LOOKUP'FILE,S,510
	MAP2 LOOKUP'UTIME,S,12
	MAP2 LOOKUP'FMTIME,B,4
	MAP2 LOOKUP'FCTIME,B,4
	MAP2 LOOKUP'FMODE,B,2
	MAP2 LOOKUP'BYTES,F
	MAP2 LOOKUP'MOD'DATE,S,100
	MAP2 LOOKUP'CREATE'DATE,S,100
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
      	XGETARGS X'FILE,X'SIZE
	XCALL WINATE,GUI'AVAIL,ATE'USED,NULL'PARAM,LOCAL'USED
	X'SIZE=0
	IF GUI'AVAIL=0 THEN
	   X'SIZE=-1
	  ELSE
	   CALL CHECK'EXISTS
	ENDIF
	XPUTARG 2,X'SIZE
	END
!
!==============================================================================
!{Does it exist?}
!==============================================================================
CHECK'EXISTS:
	IF ATE'USED=1 THEN LOOKUP'LOCATION="R"
	IF LOCAL'USED=1 THEN LOOKUP'LOCATION="L"

	LOOKUP'FILE=X'FILE
	XCALL MIAMEX,131,LOOKUP'LOCATION,LOOKUP'FILE,LOOKUP'BYTES,LOOKUP'FMTIME,LOOKUP'FCTIME,LOOKUP'FMODE
	IF LOOKUP'BYTES=<0 THEN
	   X'SIZE=0
	  ELSE
	   X'SIZE=LOOKUP'BYTES
	  ENDIF
 	RETURN
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
	
