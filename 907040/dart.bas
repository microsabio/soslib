!==============================================================================
! DART.SBX - {Create a chart in DART with a CSV file from Ashell}
!==============================================================================
! Also requires: WINATE.SBX, INIX.SBX, FULPTH.SBX, EXISTL.SBX, GETREG.SBX
!==============================================================================
!
!Usage:
!	XCALL DART,{opcode},{rtncde},{Csv File},{method},{value field},
!		   {group field},{chart type},{display type},&
!		   {transfer files},{chart title},{username},&
!                  {Cross Chart Label Field}
!
!	If the {Cross Chart Label Field} is set and not left blank then
!       DART will create a CrossTab Table instead of a normal Table.
!
! 	See PARMS MAP below for parameters.
!
!	* The CSV does not required a header line start the start.
!
!	* A .DEF file with the same prefix as the CSV file is also required
!	  this defines the CSV file layout and field types.
!	  For Example:
!
!		[COL1]
!		NAME=CUSTNO
!		TYPE=STRING
!		SIZE=6
!
!		[COL2]
!		NAME=INVOICE_DATE
!		TYPE=DATE
!
!		[COL3]
!		NAME=SALES_PRICE
!		TYPE=CURRENCY
!
!		[COL4]
!		NAME=QTY
!		TYPE=NUMBER
!
!	The TYPE's available are: STRING (plus size),DATE, CURRENCY, NUMBER
!
!==============================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++INCLUDE xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!------------------------------------------------------------------------------
!{Maps Zone}
!------------------------------------------------------------------------------
MAP1 PARMS
 MAP2 X'OPCODE,F,6		! 1 - Launch DART import CSV and display.
 			        ! 2 - Check DART is found on PC.
				! 3 - Launch DART (as if you clicked on the icon)
				!
 MAP2 X'RTNCDE,F,6		! 1=OK, 0=Failed.
 MAP2 X'CSV'FILE,S,255		! CSV File
 MAP2 X'METHOD,B,1		! 0=Sum, 1=Count
 MAP2 X'VALUE'FIELD,S,255	! Value Field
 MAP2 X'GROUP'FIELD,S,255	! Group Field
 MAP2 X'CHART'TYPE,F,6		! 0=Pie, 1=Bar, 2=Line, 3=Area
 MAP2 X'DISPLAY'TYPE,F,6	! 0=Display Chart Only (read-only) then exit.
 			        ! 1=Display Chart and enter DART mode.
 MAP2 X'TRANSFER'FILES,F,6	! 0=Yes (FTP or Window copy), 1=No
 MAP2 X'CHART'TITLE,S,255	! Title of the Chart
 MAP2 X'USER'NAME,S,64          ! DART user name (for Multi user mode)
 MAP2 X'CROSS'LABEL'FIELD,S,255	! Cross Chart Label Field	![504]

MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6
MAP1 WHAT'JOB,S,6
MAP1 DARTCSV'INI,S,25
MAP1 FILE'EXISTS,F,6
MAP1 EXE'SIZE,F,6
MAP1 DEF'FILE,S,255
MAP1 WORK'X,F,6
MAP1 OK,S,1
MAP1 LAUNCH'STRING,S,2048
MAP1 DART'EXE'FILE,S,2048
MAP1 NATIVE'FILE,S,2048
MAP1 A,X,1

MAP1 FROM'FILE,S,25
MAP1 TO'FILE,S,2048
MAP1 LOCAL'FILE,S,2048
MAP1 COPY'STATUS,F,6
MAP1 COPY'FLAGS,F,6

MAP1 DART'INI
 MAP2 DART'INI'FILE,S,25,"DSK0:DART.INI[1,2]"
 MAP2 DART'EXE'FOLDER,S,1024
 MAP2 DART'DATA'FOLDER,S,1024

MAP1 WINATE   !0-Not Available, 1-Avaiable
 MAP2 WINATE'GUI,B,1
 MAP2 WINATE'ATE,B,1
 MAP2 WINATE'ATSD,B,1
 MAP2 WINATE'LOCAL,B,1

DEFINE CPYF_REPL = &h0002

DEFINE MX_GETREG = 99                                              ! 4.2(502)
DEFINE HKEY 	 = &h80000001       !HKEY_CURRENT_USER             ! 4.2(502)
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error GOTO TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
	XCALL GETJOB,WHAT'JOB : XCALL STRIP,WHAT'JOB

    	XGETARGS X'OPCODE,X'RTNCDE,X'CSV'FILE,X'METHOD,X'VALUE'FIELD,&
    	         X'GROUP'FIELD,X'CHART'TYPE,X'DISPLAY'TYPE,&
    	         X'TRANSFER'FILES,X'CHART'TITLE,X'USER'NAME,&
                 X'CROSS'LABEL'FIELD

	X'RTNCDE=0

        ! We only support ATE and Windows Ashell, sorry ZTerm.
	XCALL WINATE,WINATE'GUI,WINATE'ATE,WINATE'ATSD,WINATE'LOCAL
	IF WINATE'GUI=0 THEN
	   XCALL MESAG,"YOUR TERMINAL IS NOT SUPPORTED, ATE/WINDOWS REQUIRED",2
	   GOTO EXIT'SBX
	ENDIF

	! Launch, import CSV and show chart.
	IF X'OPCODE=1 THEN
	   CALL GET'DART'FOLDER  : IF OK="N" THEN GOTO EXIT'SBX
	   CALL CREATE'DARTCSV'INI
	   CALL TRANSFER'FILES   : IF OK="N" THEN GOTO EXIT'SBX
	   CALL LAUNCH'DART
	ENDIF

        ! RTNCDE return's 1 if DART.EXE is found else returns 0.
	IF X'OPCODE=2 THEN
	   CALL GET'DART'FOLDER
	   IF OK="Y" THEN X'RTNCDE=1
	ENDIF

	 ! Just Launch DART thats it, nothing else.
	IF X'OPCODE=3 THEN
	   CALL GET'DART'FOLDER  : IF OK="N" THEN GOTO EXIT'SBX
	   CALL LAUNCH'DART
	   X'RTNCDE=1
	ENDIF

EXIT'SBX:
  	XPUTARG 2,X'RTNCDE
	END
!
!==============================================================================
!{Get DART Folder, look in registry first then DART.INI}
!==============================================================================
GET'DART'FOLDER:
	OK="N"

        ! Get the DART folder from registry...by default.
	XCALL MIAMEX,MX_GETREG,HKEY,"Software\Madics Systems Ltd\DART",&
	             "DART EXE FOLDER",DART'EXE'FOLDER                 ! 4.2(502)

        ! Get the DART Data folder from registry.
	XCALL MIAMEX,MX_GETREG,HKEY,"Software\Madics Systems Ltd\DART",&
	             "DART DATA FOLDER",DART'DATA'FOLDER                 ! 4.2(503)

	! But if its set in DART.INI this can be used instead.
	XCALL INIX,DART'INI'FILE,0,"DART","DART FOLDER",DART'EXE'FOLDER,DART'EXE'FOLDER
	XCALL INIX,DART'INI'FILE,0,"DART","DART DATA FOLDER",DART'DATA'FOLDER,DART'DATA'FOLDER

	IF DART'EXE'FOLDER="" THEN
	   IF X'OPCODE<>2 THEN
	     XCALL MESAG,"DART.EXE NOT FOUND, PLEASE RUN 'DART' TO TO VERIFY.",2
	   ENDIF
	ENDIF

       ! Can we find DART.EXE on the local PC..?
       IF DART'EXE'FOLDER<>"" THEN
	  DART'EXE'FILE=DART'EXE'FOLDER+"\DART.EXE"
	  XCALL EXISTL,DART'EXE'FILE,EXE'SIZE
	  IF EXE'SIZE=>0 THEN
	     OK="Y"
	    ELSE
	     IF X'OPCODE<>2 THEN
	        XCALL MESAG,DART'EXE'FILE+" NOT FOUND.",2
	     ENDIF
	  ENDIF
	ENDIF

	RETURN
!
!==============================================================================
!{Create DARTCSV.INI first its based in your job number}
! This is then transfered to the PCas a DARTCSV.INI
!==============================================================================
CREATE'DARTCSV'INI:
	DARTCSV'INI="DSK0:DART"+WHAT'JOB+".INI[1,2]"
	LOOKUP DARTCSV'INI,FILE'EXISTS
	IF FILE'EXISTS<>0 THEN KILL DARTCSV'INI
	XCALL INIX,DARTCSV'INI,1,"PARMS","CSV FILE",X'CSV'FILE
	XCALL INIX,DARTCSV'INI,1,"PARMS","CHART METHOD",X'METHOD
	XCALL INIX,DARTCSV'INI,1,"PARMS","VALUE FIELD",X'VALUE'FIELD
	XCALL INIX,DARTCSV'INI,1,"PARMS","GROUP FIELD",X'GROUP'FIELD
	XCALL INIX,DARTCSV'INI,1,"PARMS","CHART TYPE",X'CHART'TYPE
	XCALL INIX,DARTCSV'INI,1,"PARMS","DISPLAY TYPE",X'DISPLAY'TYPE
	XCALL INIX,DARTCSV'INI,1,"PARMS","CHART TITLE",X'CHART'TITLE
	XCALL INIX,DARTCSV'INI,1,"PARMS","USER NAME",X'USER'NAME
	XCALL INIX,DARTCSV'INI,1,"PARMS","CROSSTAB LABEL",X'CROSS'LABEL'FIELD
 	RETURN
!
!==============================================================================
!{FTP or Copy files to DART CSV Folder}
!==============================================================================
TRANSFER'FILES:
	OK="N"

	IF X'TRANSFER'FILES=0 THEN

	  LOOKUP X'CSV'FILE,FILE'EXISTS
	  IF FILE'EXISTS=0 THEN
	    XCALL MESAG,"CHART "+X'CSV'FILE+" DOES NOT EXIST, ABORTING..",2
	    GOTO RTN'TRANSFER'FILES
	  ENDIF

	  DEF'FILE=X'CSV'FILE
	  WORK'X=INSTR(1,UCS(DEF'FILE),".CSV")
	  DEF'FILE[WORK'X;4]=".DEF"
	  LOOKUP DEF'FILE,FILE'EXISTS
	  IF FILE'EXISTS=0 THEN
	     XCALL MESAG,"CHART "+DEF'FILE+" DOES NOT EXIST, ABORTING..",2
	     GOTO RTN'TRANSFER'FILES
	  ENDIF

	  FROM'FILE=X'CSV'FILE  : TO'FILE=FROM'FILE     : CALL TRANSFER'IT
	  FROM'FILE=DEF'FILE    : TO'FILE=FROM'FILE     : CALL TRANSFER'IT
	  FROM'FILE=DARTCSV'INI : TO'FILE="dartcsv.ini" : CALL TRANSFER'IT
	ENDIF
	OK="Y"

RTN'TRANSFER'FILES:
	RETURN
!
!------------------------------------------------
! Transfer a file..
!------------------------------------------------
TRANSFER'IT:
	XCALL FULPTH,FROM'FILE,NATIVE'FILE
	LOCAL'FILE=DART'DATA'FOLDER+"\CSV\"+TO'FILE

	IF WINATE'LOCAL=1 THEN
	    ! Windows Copy
	    COPY'FLAGS=CPYF_REPL
	    XCALL MIAMEX,27, NATIVE'FILE, LOCAL'FILE,COPY'FLAGS,COPY'STATUS
	    IF COPY'STATUS<>0 THEN
	       XCALL MESAG,"COPY FAILED WITH ERROR "+STR(COPY'STATUS),2
	    ENDIF
	  ELSE
	    ! FTP File (username/password need to be set)
	    PRINT TAB(-10,22);"2";NATIVE'FILE;"~";LOCAL'FILE;chr(127);
	    XCALL ACCEPN,A
	ENDIF

	RETURN
!
!==============================================================================
!{Launch DART}
!==============================================================================
LAUNCH'DART:
	X'RTNCDE=1
	LAUNCH'STRING=DART'EXE'FILE+" "+X'CSV'FILE
	PRINT TAB(-10,23);LAUNCH'STRING+" $";CHR(127);
	RETURN
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
