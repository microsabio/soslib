!========================================================================
!FULPTH - Returns full native file and path of a AMOS file spec.
!
!Usage:
!	XCALL FULPTH,{amos format},{native format},{path},{filename}
!
!	Set AMOS format, eg DSK0:MYFILE.DAT[1,2]
!
!	native format is return:
!	Eg.  /madics/miame/dsk0/001002/myfile.dat
!
!	filename - file name only
!	path     - path only
!
!========================================================================
!
! VERSION HISTORY
! 31/May/2005 - 4.2(100) - Steve - New SBX
! 25/Nov/2005 - 4.2(500) - Steve - Madics 4.2a 
!
program FULPTH,4.2(500)

++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! NOTE: MUST COMPILE WITH COMPIL FULPTH/X:2 
!
++include xcall.bsi
!--------------------------------------------------------------------

MAP1 PARAMS
	MAP2 AMOS'FILE,S,132
	MAP2 NATIVE'FILE,S,256
	MAP2 NPATH,S,256
	MAP2 NFILE,S,64

MAP1 SPEC,S,100
MAP1 LOCAL,S,100
MAP1 CODE,F,6
MAP1 MX_FSPEC,F,6,3
MAP1 STATUS,F,6
MAP1 LENX,F,6
MAP1 AMOS'SPEC,F,6
MAP1 FOUNDX,F,6

map1 DDB
        map2 DEV,s,6        ! 6 characters AMOS device 
        map2 FILNAM,s,32    ! 6 characters AMOS or 32 host
        map2 EXT,s,8        ! 3 characters AMOS or 8 host
        map2 PRJ,s,3        ! 3 character justified P
        map2 PRG,s,3        ! 3 character justified PN

     	on error goto TRAP

	! pick up all args; any not passed will just be set to null
      	XGETARGS AMOS'FILE,NATIVE'FILE,NPATH,NFILE
      	   
      	SPEC=AMOS'FILE : CODE=6
      	xcall MIAMEX,MX_FSPEC,SPEC,LOCAL,EXT,CODE,DDB,STATUS
	NATIVE'FILE=LOCAL
	
	LENX=LEN(NATIVE'FILE)
       	FOUNDX=INSTR(1,LCS(NATIVE'FILE),LCS(FILNAM))
       	IF FOUNDX<>0 THEN
       	  NPATH=NATIVE'FILE[1;FOUNDX-2]
       	  NFILE=NATIVE'FILE[FOUNDX,LENX]
       	ENDIF
     		
	XPUTARG 2,NATIVE'FILE
	XPUTARG 3,NPATH
	XPUTARG 4,NFILE
	END

TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in FULPTH.SBX!!",2
	END			! return to caller

