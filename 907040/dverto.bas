!**************************************************************************
! Little Utility to edit the DVerto Files		Steve	17/Mar/2008
!**************************************************************************
!PROBAS=X
++PRAGMA ERROR_IF_NOT_MAPPED "ON"

++INCLUDE MADINC:ASHELL.DEF
++INCLUDE MADINC:XTREE.DEF
++INCLUDE MADINC:XTREE.MAP
!
!==============================================================================
!{Map Zone}
!==============================================================================
! Buttons
MAP1 DSROW,F,6
MAP1 DSCOL,F,6
MAP1 DEROW,F,6
MAP1 DECOL,F,6
MAP1 BID,F,6
MAP1 STATUS,F,6
! Event Wait
MAP1 EVENT'PARENTID,F,6
MAP1 EVENT'CTLID,F,6
MAP1 EVENT'EXITCODE,F,6
MAP1 EVENT'OPFLAGS,F,6
! Dialog
MAP1 DLGID,F,6
MAP1 DLGID_GLOBAL,F,6
! Misc
MAP1 GUI'AVAIL,F,6
MAP1 DEF'FILE'COUNT,F,6
MAP1 DEF'FILE'NAME,S,25
MAP1 XX,F,6
MAP1 FF,F,6
MAP1 REPLACE'FROM'STR,S,255
MAP1 REPLACE'TO'STR,S,255
MAP1 REPLACE'SIZ'LENGTH,F,6
MAP1 PARTA,S,255
MAP1 PARTB,S,255
MAP1 LENX,F,6
MAP1 INSTR'XX,F,6
MAP1 ABORT'OUT,S,1,"N"
MAP1 FF'EXISTS,F,6
MAP1 FILE'POINTER'ERR,F,6
MAP1 TMP'FILE,S,512
!
! INFLD
MAP1 INFLD'XMIN,F,6
MAP1 INFLD'SETDEF,S,510
MAP1 INFLD'TYPE,S,80
MAP1 INFLD'ENTRY,S,132
MAP1 INFLD'INXCTL,F,6,0
MAP1 INFLD'EXITCODE,F,6
MAP1 INFLD'TIMER,F,6
MAP1 INFLD'DEFPT,F,6,-1
MAP1 INFLD'MAXPT,F,6,-1
MAP1 INFLD'FUNMAP,F,6,-1
MAP1 INFLD'HLPIDX,X,80
MAP1 INFLD'MAXWID,F,6
MAP1 INFLD'V,F,6,1
MAP1 INFLD'OPCODE,F,6,1
MAP1 INFLD'CMDFLG,F,6,0
MAP1 INFLD'INFCLR                     ! INFLD colors
       MAP2 DFCLR,B,1,0                ! display fg (black)
       MAP2 DBCLR,B,1,0                ! display bg (gray)
       MAP2 EFCLR,B,1,0                ! editing fg (black)
       MAP2 EBCLR,B,1,1                ! editing bg (white)
       MAP2 NFCLR,B,1,4                ! negatives fg (red)
       MAP2 NBCLR,B,1,1                ! negatives bg (white)
       MAP2 UFCLR,B,1,6                ! update fg (green)
       MAP2 UBCLR,B,1,0                ! update bg (gray)
       MAP2 MFCLR,B,1,2                ! messages fg (blu)
       MAP2 MBCLR,B,1,0                ! messages bg
       MAP2 OFCLR,B,1,2                ! original messages fg
       MAP2 OBCLR,B,1,0                ! original messages bg
       MAP2 FFCLR,B,1,2                ! forms fg
       MAP2 FBCLR,B,1,0                ! forms bg
!
! MADXTX - XText
MAP1 MADXTX'EXITCODE,F,6
MAP1 EDIT'FILE,S,255
MAP1 LAY'FILE,S,255

!
!------------------------------------------------
! Edit DEF XTREE Map
!------------------------------------------------

MAP1 XTREE'PARAMS
    MAP2 XT'COLDEF$,S,2048
    MAP2 XT'FLAGS,B,4

MAP1 DATA'ARY(250)
  MAP2 XD'FILE,S,15
  MAP2 XD'DESCR,S,30
  MAP2 XD'PATH,S,256

MAP1 ANSWER'ARY(250)
  MAP2 XA'PATH,S,256

MAP1 MAX'ARRAY,F,6,250
MAP1 ARRAY'COUNT,F,6
MAP1 XTREE'EXITCODE,F,6
!
!==============================================================================
!{Lets start here with a small button menu..}
!==============================================================================
	?TAB(-1,0);
	XCALL WINATE,GUI'AVAIL
	IF GUI'AVAIL=0 THEN
	   XCALL MESAG,"SORRY ATE/WINDOWS IS REQUIRED FOR THIS PROGRAM.",2
	  ELSE
	   XCALL FMTTXT,1,20,"Edit DVerto Settings"

	   DSROW=3 : DSCOL=5 : DEROW=4 : DECOL=55
	   XCALL AUI,AUI_CONTROL,1,BID,"Edit Definitions and file locations",0, &
	             MBF_KBD,"%VK_xF401%","",STATUS,DSROW,DSCOL,DEROW,DECOL

	   DSROW=6 : DSCOL=5 : DEROW=7 : DECOL=55
	   XCALL AUI,AUI_CONTROL,1,BID,"Exit",0, &
	             MBF_KBD,"%VK_xF402%","",STATUS,DSROW,DSCOL,DEROW,DECOL
	   DO
	     XCALL AUI,"EVENTWAIT",EVENT'PARENTID,EVENT'CTLID,EVENT'EXITCODE,EVENT'OPFLAGS
	     IF EVENT'EXITCODE=-401 THEN CALL EDIT'DEF'FILE'LOCATIONS
	   LOOP UNTIL EVENT'EXITCODE=-402
	ENDIF
	?TAB(-1,0);
	END

!==============================================================================
!{Edit all the DEF files and edit the data file locations}
!==============================================================================
EDIT'DEF'FILE'LOCATIONS:
	XCALL FNDFIL,"",".def",DEF'FILE'COUNT,"DVERTO.SEQ"
	IF DEF'FILE'COUNT=0 THEN
	   XCALL MESAG,"NO .DEF FILES FOUND IN CURRENT PPN",2
 	  ELSE
	   CALL READ'DEF'LIST
	   CALL SHOW'EDIT'DEF'XTREE
	   IF ABORT'OUT<>"Y" THEN
	      CALL SAVE'DEF'LIST
	      CALL VERIFY'FILE'POINTERS
	   ENDIF
 	ENDIF
	RETURN

!------------------------------------------------
! Read Def List
!------------------------------------------------
READ'DEF'LIST:
	ARRAY'COUNT=0
	OPEN #1,"DVERTO.SEQ",INPUT
	DO WHILE EOF(1)=0
	   INPUT LINE #1,DEF'FILE'NAME
	   ARRAY'COUNT=ARRAY'COUNT+1
	   XD'FILE(ARRAY'COUNT)=DEF'FILE'NAME
	   XCALL INIX,DEF'FILE'NAME,0,"SETTINGS","DESCR",XD'DESCR(ARRAY'COUNT)
	   XCALL INIX,DEF'FILE'NAME,0,"SETTINGS","DATAFILE",XD'PATH(ARRAY'COUNT)
           !!TRACE.PRINT XD'PATH(ARRAY'COUNT)
	LOOP
	CLOSE #1
	ARRAY'COUNT=ARRAY'COUNT-1
	RETURN

!------------------------------------------------
! Save Def List
!------------------------------------------------
SAVE'DEF'LIST:
	FOR XX=1 TO ARRAY'COUNT
	   DEF'FILE'NAME=XD'FILE(XX)
	   XCALL STRIP,XA'PATH(XX)
	   XCALL INIX,DEF'FILE'NAME,1,"SETTINGS","DATAFILE",XA'PATH(XX)
	NEXT XX
	RETURN
!
!------------------------------------------------
!{Show XTREE and edit DEFdata file paths.}
!------------------------------------------------
SHOW'EDIT'DEF'XTREE:
        ! Create Dialog Box.
        DSROW=3 : DSCOL=2 : DEROW=25 : DECOL=79
	XCALL AUI,AUI_CONTROL,1,DLGID,"Edit Definitions and file locations",0,&
 	          MBF_DIALOG+MBF_SYSMENU,"","",STATUS,DSROW,DSCOL,DEROW,DECOL

      !  Save and Exit Button
      DSROW=21 : DEROW=DSROW+1 : DSCOL=3 : DECOL=13
      XCALL AUI,AUI_CONTROL,1,BID,"Save and e&xit",0, &
            MBF_KBD,"%VK_xF410%","",STATUS,DSROW,DSCOL,DEROW,DECOL

     !  Abort
      DSROW=21 : DEROW=DSROW+1 : DSCOL=15 : DECOL=25
      XCALL AUI,AUI_CONTROL,1,BID,"Abort (No Save)",0, &
            MBF_KBD,"%VK_xF414%","",STATUS,DSROW,DSCOL,DEROW,DECOL

      !  Edit DEF File
      DSROW=21 : DEROW=DSROW+1 : DSCOL=27 : DECOL=37
      XCALL AUI,AUI_CONTROL,1,BID,"Edit &DEF File",0, &
            MBF_KBD,"%VK_xF411%","",STATUS,DSROW,DSCOL,DEROW,DECOL

      !  Edit LAY File
      DSROW=21 : DEROW=DSROW+1 : DSCOL=39 : DECOL=49
      XCALL AUI,AUI_CONTROL,1,BID,"Edit &LAY File",0, &
            MBF_KBD,"%VK_xF412%","",STATUS,DSROW,DSCOL,DEROW,DECOL

      !  Global File Locations
      DSROW=21 : DEROW=DSROW+1 : DSCOL=51 : DECOL=61
      XCALL AUI,AUI_CONTROL,1,BID,"&Global Path",0, &
           MBF_KBD,"%VK_xF413%","",STATUS,DSROW,DSCOL,DEROW,DECOL

     ! Verify File Locations
      DSROW=21 : DEROW=DSROW+1 : DSCOL=63 : DECOL=73
      XCALL AUI,AUI_CONTROL,1,BID,"&Verify Files",0, &
           MBF_KBD,"%VK_xF415%","",STATUS,DSROW,DSCOL,DEROW,DECOL

	XT'FLAGS = 0
	XT'FLAGS = XT'FLAGS OR XTF_XYXY           ! alt coords (srow,scol,erow,ecol)
	XT'FLAGS = XT'FLAGS OR XTF_COLDFX         ! complex syntax in COLDEF
	XT'FLAGS = XT'FLAGS OR XTF_EDITABLE       ! needed for editable cb or text
	XT'FLAGS = XT'FLAGS OR XTF_FKEY           ! Allow Fx codes!!
	XT'FLAGS = XT'FLAGS OR XTF_SORT           ! Allow column sorting
	XT'FLAGS = XT'FLAGS OR XTF_ENTESC         ! ESC within an editable cell updates the cell
	XT'FLAGS = XT'FLAGS OR XTF_MODELESS	  ! LEAVE BOX ON SCREEN AFTER EXIT

	XTR'CTLNO = -1                      ! auto-select xtree #
	XTR'ITEMLINES = 1
	XTR'TREELINESTYLE = 0               ! n/a
	XTR'SHOWBUTTONS0 = 0                ! n/a
	XTR'SHOWBUTTONS = 0                 ! n/a
	XTR'SHOWGRID = 1                    ! grid lines (yes)
	XTR'GRIDSTYLE = 2                   ! solid horz & vert
	XTR'TRUNCATED = 1                   ! show dots if truncated
	!XTR'SELECTAREA = XTRSEL_AREA_CELL1 + XTRSEL_STY_CELL1
	XTR'FLYBY = 1                       ! Fly by highlighting (0=no, 1=yes)
	XTR'SCROLLTIPS = 1                  ! Show scroll tips (0=no, 1=yes)
	XTR'KBDSTR = "VK_xF3"               ! kbd click string
	XTR'USETHEMES = 1                   ! 1=use XP themes (if available)
	XTR'PARENTID = DLGID                ! ID of parent control
	XTR'SHOW3D = 0                      ! 1=use 3D style
	XTR'HIDEHEADER = 0                  ! 1=hide header
	XTR'EXPANDLEVEL = 0                 ! n/a
	XTR'SKEY = ""                       ! n/a
	XTR'TIMEOUT = 0                     ! timeout (ms) (exitcode=11)
	XTR'LEFTPANECOLS = 0                ! (XTF_SPLIT) # cols in left pane
	XTR'LEFTPANEWIDTH = 0               ! (XTF_SPLIT) width of left pane
	                                    !   0=optimize, -1=50/50, else col units
	XTR'CLOSEDENDED = 0                 ! last col open ended

	XT'COLDEF$ = ""
	XT'COLDEF$ = XT'COLDEF$ + "0~0~ ~H~HdrScale=125~~"
	XT'COLDEF$ = XT'COLDEF$ + "1~15~File~S~Dspmin=10~~"
	XT'COLDEF$ = XT'COLDEF$ + "16~30~Description~S~Dspmin=15~~"
	XT'COLDEF$ = XT'COLDEF$ + "46~256~Data file name and full path~SE\~~"

        ! Sort Order.
	XTR'COLUMNSORT(1)=1	! Sort Column
	XTR'SORTORDER(1)=0	! 0=ascending, 1=descending

	XTR'COLUMNACTIVE=3

	! Build Preset Answer Array
	FOR XX=1 TO MAX'ARRAY
	  XA'PATH(XX)=XD'PATH(XX)
	NEXT XX

        ! Display XTree
	XTR'OPCODE = XTROP_CREATE
	DO
	   CALL XTREE
	   IF XTREE'EXITCODE=-411 THEN CALL EDIT'DEF'FILE
	   IF XTREE'EXITCODE=-412 THEN CALL EDIT'LAY'FILE
	   IF XTREE'EXITCODE=-413 THEN CALL GLOBAL'REPLACE'FILE'LOCATION
	   IF XTREE'EXITCODE=-414 THEN ABORT'OUT="Y"
	   IF XTREE'EXITCODE=-415 THEN CALL VERIFY'FILE'POINTERS
	   XTR'OPCODE=XTROP_REPLACE
	LOOP UNTIL XTREE'EXITCODE=1 OR XTREE'EXITCODE=-410 OR XTREE'EXITCODE=-414  ! Escape

	XTR'OPCODE=XTROP_DELETE : CALL XTREE
	! Delete Dialog.
	XCALL AUI,AUI_CONTROL,3,DLGID
 	RETURN
!
!------------------------------------------------
! Display XTREE
!------------------------------------------------
XTREE:
	XCALL XTREE,1,2,ANSWER'ARY(1),DATA'ARY(1),ARRAY'COUNT,XT'COLDEF$, &
	                XTREE'EXITCODE,20,77,XT'FLAGS,"",0,XTRCTL
	RETURN
!
!==============================================================================
!{Edit DEF File}
!==============================================================================
EDIT'DEF'FILE:
	EDIT'FILE=XD'FILE(XTR'XROW)
	LOOKUP EDIT'FILE,FF
	IF FF=0 THEN
	   XCALL MESAG,EDIT'FILE+" NOT FOUND TO EDIT.",2
	  ELSE
	   XCALL STRIP,XA'PATH(XTR'XROW)
	   XCALL INIX,EDIT'FILE,1,"SETTINGS","DATAFILE",XA'PATH(XTR'XROW)
	   XCALL MADXTX,2,1,1,23,75,EDIT'FILE,MADXTX'EXITCODE,0,EDIT'FILE,2,0,0
	   XCALL INIX,EDIT'FILE,0,"SETTINGS","DATAFILE",XA'PATH(XTR'XROW)
	   XD'PATH(XTR'XROW)=XA'PATH(XTR'XROW)
	   XCALL INIX,EDIT'FILE,0,"SETTINGS","DESCR",XD'DESCR(XTR'XROW)
	ENDIF
	RETURN
!
!==============================================================================
!{Edit LAY File}
!==============================================================================
EDIT'LAY'FILE:
	XCALL INIX,XD'FILE(XTR'XROW),0,"SETTINGS","MAPFILE",LAY'FILE
	LOOKUP LAY'FILE,FF
	IF FF=0 THEN
	   XCALL MESAG,LAY'FILE+" NOT FOUND TO EDIT.",2
	  ELSE
	   XCALL MADXTX,2,1,1,23,75,LAY'FILE,MADXTX'EXITCODE,0,LAY'FILE,2,0,0
	ENDIF
	RETURN
!
!==============================================================================
!{Global Replace File locations}
!==============================================================================
GLOBAL'REPLACE'FILE'LOCATION:
       ! Create Dialog Box.
        DSROW=7 : DSCOL=5 : DEROW=15 : DECOL=75
	XCALL AUI,AUI_CONTROL,1,DLGID_GLOBAL,"Global Replace File Location",0,&
 	          MBF_DIALOG+MBF_SYSMENU,"","",STATUS,DSROW,DSCOL,DEROW,DECOL

	REPLACE'FROM'STR=XA'PATH(XTR'XROW)
	REPLACE'TO'STR=REPLACE'FROM'STR

	TPRINT TAB(2,2);"REPLACE STRING";
	TPRINT TAB(4,2);"WITH STRING";
	REPLACE'FROM'STR=FN'INFLD$(3,2,65,"")
	REPLACE'TO'STR=FN'INFLD$(5,2,65,REPLACE'FROM'STR)

	XCALL STRIP,REPLACE'FROM'STR
	XCALL STRIP,REPLACE'TO'STR
	REPLACE'SIZ'LENGTH=LEN(REPLACE'FROM'STR)

	IF REPLACE'FROM'STR<>REPLACE'TO'STR AND &
	   REPLACE'FROM'STR<>"" AND &
	   REPLACE'TO'STR<>"" THEN
	   FOR XX=1 TO ARRAY'COUNT
	     INSTR'XX=INSTR(1,XA'PATH(XX),REPLACE'FROM'STR)
!XCALL WINTRC,XA'PATH(XX),"XA'PATH(XX)",0
!XCALL WINTRC,REPLACE'FROM'STR,"look for",0
!XCALL WINTRC,INSTR'XX,"start found at",0
!XCALL WINTRC,REPLACE'SIZ'LENGTH,"Replace size length",0
!XCALL WINTRC,XX,"row",0

	     IF INSTR'XX<>0 THEN
 	        LENX=LEN(XA'PATH(XX))
!XCALL WINTRC,LENX,"LENX",0
   	        PARTA=XA'PATH(XX)[1;INSTR'XX-1]
	   	PARTB=XA'PATH(XX)[INSTR'XX+REPLACE'SIZ'LENGTH,LENX]
!XCALL WINTRC,"","=====================================",0
!XCALL WINTRC,PARTA,"PARTA",0
!XCALL WINTRC,REPLACE'TO'STR,"REPLACE'TO'STR",0
!XCALL WINTRC,PARTB,"PARTB",0
!XCALL WINTRC,PARTA+REPLACE'TO'STR+PARTB,"New"
	   	XA'PATH(XX)=PARTA+REPLACE'TO'STR+PARTB
!XCALL WINTRC,XA'PATH(XX),"New XA'PATH"
!XCALL WINTRC,"","=====================================",0
	     ENDIF
	   NEXT XX
	ENDIF

	! Delete Dialog.
	XCALL AUI,AUI_CONTROL,3,DLGID_GLOBAL

	RETURN
!
!==============================================================================
!{DVerto INFLD} - String Only.
!==============================================================================
FUNCTION FN'INFLD$(INFLD'ROW AS F6,INFLD'COL AS F6,INFLD'XMAX AS F6,INFLD'ENTRY AS S255)
ON ERROR GOTO LOCAL'TRAP
++PRAGMA AUTO_EXTERN "TRUE"
STATIC MAP1 I,F,6
        INFLD'TYPE="VW4[]|B|C|Q|a|p"    ! Defaults (Madics)
        INFLD'TYPE=INFLD'TYPE+"||Fg"    ! force fixed pitch
        INFLD'TYPE=INFLD'TYPE+"|G"      ! Always use the Windows Edit Box.
        INFLD'TYPE=INFLD'TYPE+"e"       ! non-destructive combo
        INFLD'TYPE=INFLD'TYPE+"*"	! String.

	  XCALL INFLD, INFLD'ROW, INFLD'COL, INFLD'XMAX, INFLD'XMIN, INFLD'TYPE,&
	        INFLD'ENTRY, INFLD'INXCTL ,INFLD'V, INFLD'OPCODE, INFLD'EXITCODE,&
	        INFLD'TIMER, INFLD'CMDFLG, INFLD'DEFPT, INFLD'MAXPT, INFLD'FUNMAP,&
	        INFLD'SETDEF, INFLD'INFCLR, INFLD'HLPIDX, INFLD'MAXWID

	FN'INFLD$=INFLD'ENTRY

	EXITFUNCTION
LOCAL'TRAP:
 	XCALL MESAG,"Warning: Error "+STR(ERR(0))+" in FN'INFLD$",2
	RESUME
ENDFUNCTION
!
!==============================================================================
!{Verify Disk Pointers}				4.2(503)
!==============================================================================
VERIFY'FILE'POINTERS:
	FILE'POINTER'ERR=0
	FOR XX=1 TO ARRAY'COUNT
	    TMP'FILE=XA'PATH(XX)
	    XCALL STRIP,TMP'FILE
	    LOOKUP TMP'FILE,FF'EXISTS
	    IF FF'EXISTS=0 THEN
	       XCALL MESAG,XD'DESCR(XX)+CHR(13)+"The following file was not found:"+CHR(13)+XA'PATH(XX),2
               FILE'POINTER'ERR=FILE'POINTER'ERR+1
	    ENDIF
	NEXT XX
	IF FILE'POINTER'ERR=0 THEN
	   XCALL MESAG,"All File Pointers have been verified and set correct",2
	  ELSE
	   XCALL MESAG,"Warning "+STR(FILE'POINTER'ERR)+" incorrect file pointer(s).",2
	ENDIF
	RETURN
