!==============================================================================
! Example launching DART with a CSV file via DART.SBX
!==============================================================================
! HISSAL.CSV and HISSAL.DEF also required.
! HISSL.DEf defines the field type and size of the data in HISSAL.CSV
!
!PROBAS=X
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! 
! History
!----------
! DART Value and Gruop by fields can now be left blank and selected 
! by the user in DART.
!
! 20/Dec/2006 - You are able to pass across the DART user name, only used
!               if DART is in multi user mode.
!
! 24/Jan/2008 - Added CrossTab Chart Option.
!	        Please update to DART Version 4.2.0.76
!		DART.SBX - 4.2(504)
!
!==============================================================================
!{Map Zone}
!==============================================================================
MAP1 DART
 MAP2 DART'OPCODE,F,6
 MAP2 DART'RTNCDE,F,6
 MAP2 DART'CSV,S,25
 MAP2 DART'METHOD,S,1
 MAP2 DART'VALUE'FIELD,S,255
 MAP2 DART'GROUP'FIELD,S,255
 MAP2 DART'CHART'TYPE,F,6
 MAP2 DART'DISPLAY'TYPE,F,6
 MAP2 DART'TRANSFER'FILES,F,6
 MAP2 DART'CHART'TITLE,S,255
 MAP2 DART'USER'NAME,S,64
 MAP2 DART'CROSSTAB'LABEL,S,255
!
!==============================================================================
!{Here we go...}
!==============================================================================
	?TAB(-1,0);
	XCALL DELMEM,"*.SBX"
	
	! First can we find the DART.EXE on this pc?
	! This used OPCODE 2 to just check if it exists.
	XCALL DART,2,DART'RTNCDE
	IF DART'RTNCDE=0 THEN 
	   XCALL MESAG,"DART IS NOT FOUND/INSTALLED ON YOUR PC",2
	   GOTO EXITOUT
	ENDIF

        ! OK now we know the DART.EXE exists, we are ready to set the
        ! parameters, remember we need the CSV file and a DEF file 

	DART'OPCODE=1		! Create Chart.

	DART'CSV="HISSAL.CSV"	! The DEF file will use the same prefix.

	DART'METHOD=0		! 0=Sum, 1=Count

!!!!	DART'VALUE'FIELD="PRICE"	! Same field name as in the DEF file
    	DART'VALUE'FIELD=""      	! Dont set if you want to select
                                        ! a field in DART.

  	DART'GROUP'FIELD="PART_NO"	! Same field name as in the DEF file

!!!!    DART'CROSSTAB'LABEL="CUSTNO"    ! Set if you want CrossTab Chart.

	DART'CHART'TYPE=0	! 0=Pie, 1=Bar, 2=Line

	DART'DISPLAY'TYPE=0	! 0=Display Chart Only then exit.
				! 1=Display Chart and enter normal DART mode.

	DART'TRANSFER'FILES=0	! 0=Yes, 1=No (manual placement)

	DART'CHART'TITLE="DART History Test"

	DART'USER'NAME="admin"

	! Now lets go for it..
	XCALL DART,DART'OPCODE,DART'RTNCDE,DART'CSV,DART'METHOD,DART'VALUE'FIELD,&
		   DART'GROUP'FIELD,DART'CHART'TYPE,DART'DISPLAY'TYPE,&
		   DART'TRANSFER'FILES,DART'CHART'TITLE,DART'USER'NAME,&
	           DART'CROSSTAB'LABEL

EXITOUT:
	! RTNCDE=0 Failed, 1=OK
	PRINT TAB(3,1);"DART'RTNCDE=";DART'RTNCDE
	END

