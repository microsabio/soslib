!========================================================================
! GETREG - {Simple SBX to read PC's Registry}
!========================================================================
! Currently always uses HKEY'LOCAL'MACHINE.
!
!Usage:
!	XCALL GETREG,{subkey},{name},{value}
!
! For example:
!
! XCALL GETREG,"Software\MicroSoft\Windows\CurrentVersion\Explorer\Shell Folders",&
!             "Common Desktop",DESKTOP'PATH
!
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps Zone} 
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARMS
 MAP2 X'SUBKEY,S,1024
 MAP2 X'NAME,S,1024
 MAP2 X'VALUE,S,1024

MAP1 HKEY,B,4,2147483650 !HKEY'LOCAL'MACHINE
MAP1 MX'GETREG ,F,6,99
MAP1 SUBKEY,S,510
MAP1 DESKTOP'PATH,S,510
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
      	XGETARGS X'SUBKEY,X'NAME,X'VALUE
	XCALL MIAMEX,MX'GETREG,HKEY,X'SUBKEY,X'NAME,X'VALUE
	XPUTARG 3,X'VALUE
	END	
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END
