:<(CSV) Routines for converting reports to CSV format (from Steve Evans)

    MSLCSV.PDF - Documentation
    MSLCSV.BAS - Source to MSLCSV.SBX (main routine)
    USER.TXT/RDE/CSV -   Sample report (TXT), def file (RDE), output (CSV)
    ORDER2.TXT/RDE/CSV - Sample report (TXT), def file (RDE), output (CSV)
    HIST.TXT/RDE/CSV -   Sample report (TXT), def file (RDE), output (CSV)
    USER.BAS,ORDER2.BAS,HIST.BAS - Source to initiate the report conversion
>
