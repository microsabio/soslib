!========================================================================
!MSLCSV - Parse a Report to a CSV Format.
!
!Usage:
!	XCALL MSLCSV,
!
!========================================================================
!
! VERSION HISTORY
! 22/Aug/2005 - 4.2(100) - Steve - New SBX
!
program MSLCSV,4.2(100)

++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps} 
!--------------------------------------------------------------------
MAP1 MISC
 MAP2 OK,S,1
 MAP2 FF,F,6
 MAP2 I,F,6
 MAP2 X,F,6
 MAP2 X1,F,6
 MAP2 S,F,6
 MAP2 L,F,6
 MAP2 MX_FILEPOS,F,6,118
 MAP2 BYTE'POS,F,6
 MAP2 CURRENT'LINE,F,6
 MAP2 INC'LINE,S,1
 MAP2 FIELD'NO,F,6
 MAP2 FIELD'DATA,S,255
 MAP2 LENX,F,6
 MAP2 AVI'ID,B,2
 MAP2 OUTPUT'LINE,S,5000
 MAP2 BLOCK'CSV,S,5000
 MAP2 HEADER'CSV,S,5000
 MAP2 TEMP'FIELD,S,255
 MAP2 CSV'COMPLETE,S,1,"N"
 MAP2 PAGE'NO,F,6
 MAP2 QUOTE,S,1
 MAP2 CURRENT'FIELD,S,128

MAP1 PREVIOUS'FIELD(99),S,128
   
MAP1 REPORT'MAPS
 MAP2 DEF'CHAN,F,6,63004
 MAP2 CSV'FILE'CHAN,F,6,63003
 MAP2 REPORT'CHAN,F,6,63002
 MAP2 REPORT'LINE,S,510
 MAP2 REPORT'LINES'COUNT,F,6
 MAP2 REPORT'TITLE,S,255
 
MAP1 REPORT'DEFS
 MAP2 R'PAGE'LENGTH,F,6
 MAP2 R'IGNORE'LINE,S,512
 MAP2 R'TITLE'LINE,F,6

MAP1 R'FIELD'COUNT,F,6
MAP1 R'FIELD'MAX,F,6,99
MAP1 R'FIELD'LIST(99,2),B,2
 
MAP1 R'IGNORE'COUNT,F,6
MAP1 R'IGNORE'MAX,F,6,100
MAP1 R'IGNORE'LIST(100),B,2

MAP1 R'BLANK'REPEAT'COUNT,F,6
MAP1 R'BLANK'REPEAT'MAX,F,6,99
MAP1 R'BLANK'REPEAT'LIST(99),B,2
MAP1 R'BLANK'REPEAT'LINE,S,255

MAP1 R'FIRST'IGNORE'COUNT,F,6
MAP1 R'FIRST'IGNORE'MAX,F,6,20
MAP1 R'FIRST'IGNORE'LIST(20),B,2
MAP1 R'FIRST'IGNORE'LINE,S,255

MAP1 R'BLOCK'FIELD'COUNT,F,6
MAP1 R'BLOCK'FIELDS'MAX,F,6,99
MAP1 R'BLOCK'LINES,F,6
MAP1 R'BLOCK(99,3),B,2
MAP1 R'BLOCK'LINES'MAX,F,6,10
MAP1 R'BLOCK'TEXT(10),S,255
MAP1 R'BLOCKING,S,1,"N"
MAP1 R'START'TEXT,S,255

MAP1 R'BLOCK'LABEL(99,3),B,2
MAP1 R'BLOCK'LABEL'FIELD'COUNT,F,6
MAP1 R'BLOCK'LABEL'FIELDS'MAX,F,6,99

MAP1 PARAM
 MAP2 X'REPORT'FILE,S,255
 MAP2 X'CSV'FILE,S,255
 MAP2 X'DEF'FILE,S,255
 MAP2 X'DELIMITOR,S,1
 
map1 DSPERC'PROGRESS'BAR
 MAP2 DSPERC'BAR'ID,B,2
 MAP2 DSPERC'BAR'STS,F,6
 MAP2 DSPERC'BAR'CAPTION,S,300,""
 MAP2 DSPERC'BAR'SROW,B,1,24
 MAP2 DSPERC'BAR'SCOL,B,1,40
 MAP2 DSPERC'BAR'EROW,B,1,24 
 MAP2 DSPERC'BAR'ECOL,B,1,79
 MAP2 DSPERC'BAR'CPOS,F,6,0
 MAP2 DSPERC'BAR'EPOS,F,6,100
 MAP2 DSPERC'BAR'FLAGS,F,6,0
 MAP2 DSPERC'BAR'INC,F,6,1
 map2 DSPERC'PERC'VALUE,F,6
 MAP2 DSPERC'WORK,F,6
 MAP2 DSPERC'COUNT,F,6
 MAP2 PROGRESS'COMMAND,B,1
   
MAP1 GUI'AVAIL
 MAP2 AVAIL'GUI,B,1
 MAP2 AVAIL'ATE,B,1
 MAP2 AVAIL'ZTERM,B,1
 MAP2 AVAIL'LOCAL,B,1
 
MAP1 IGNORE'TEXT'COUNT,F,6
MAP1 IGNORE'TEXT'MAX,F,6,100
MAP1 IGNORE'TEXT(100),S,255

MAP1 EXIT'TEXT'COUNT,F,6
MAP1 EXIT'TEXT'MAX,F,6,20
MAP1 EXIT'TEXT(20),S,255
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL WINATE,AVAIL'GUI,AVAIL'ATE,AVAIL'ZTERM,AVAIL'LOCAL

	! pick up all args; any not passed will just be set to null
      	XGETARGS X'REPORT'FILE,X'CSV'FILE,X'DEF'FILE,X'DELIMITOR
      	IF X'DELIMITOR="" THEN X'DELIMITOR=","
	?TAB(-1,202);
	CALL READ'DEF'FILE		: IF OK="N" THEN GOTO EXITOUT
	CALL OPEN'REPORT 		: IF OK="N" THEN GOTO EXITOUT
	CALL COUNT'LINES		: IF OK="N" THEN GOTO EXITOUT
	CALL BLOCKING'HEADER		: IF OK="N" THEN GOTO EXITOUT
	CALL PROCESS'REPORT		: IF OK="N" THEN GOTO EXITOUT
	CLOSE #REPORT'CHAN
EXITOUT:
	?TAB(-1,203);
	END
!
!==============================================================================
!{Open Report}
!==============================================================================
OPEN'REPORT:
	LOOKUP X'REPORT'FILE,FF
	IF FF<>0 THEN 
	  OPEN #REPORT'CHAN,X'REPORT'FILE,INPUT
	  OK="Y"
	 else 
	  XCALL MESAG,"SORRY "+X'REPORT'FILE+" WAS NOT FOUND.",2
	  OK="N"
	ENDIF
	RETURN
!
!==============================================================================
!{Process Report}
!==============================================================================
PROCESS'REPORT:
	! No CSV set.
       	IF X'CSV'FILE=""THEN
       	   XCALL MESAG,"NO CSV FILE SET, ABORTING",2
       	   OK="N"
       	   GOTO RTN'PROCESS'REPORT
	ENDIF

	! No fields definded
	IF R'FIELD'COUNT=0 THEN
       	   XCALL MESAG,"NO FIELDS DEFINED, ABORTING",2
       	   OK="N"
       	   GOTO RTN'PROCESS'REPORT
	ENDIF

	! OK, now open and process report
	CURRENT'LINE=0
	OPEN #CSV'FILE'CHAN,X'CSV'FILE,OUTPUT

        !Output Title if set
        IF REPORT'TITLE<>"" OR HEADER'CSV<>"" THEN
           BLOCK'CSV=HEADER'CSV
           REPORT'LINE=REPORT'TITLE
           CALL OUTPUT'CSV'LINE
           BLOCK'CSV=""
        ENDIF

        ! Output The Rest of the report..
	PRINT TAB(-1,29);
	CALL PROGRESS'BAR'SHOW
	BYTE'POS=1 : CALL MOVE'POSTION
	CURRENT'LINE=0 : PAGE'NO=0
	FOR I=1 TO REPORT'LINES'COUNT
	   CALL READ'LINE
	   IF CSV'COMPLETE<>"Y" THEN CALL PROCESS'LINE
	NEXT I
	CALL PROGRESS'BAR'DELETE

	! Close CSV
	CLOSE #CSV'FILE'CHAN
	
RTN'PROCESS'REPORT:
	PRINT TAB(-1,28);
	RETURN
!
!------------------------------
! Read in line from Report
!------------------------------
READ'LINE:
	CURRENT'LINE=CURRENT'LINE+1
	IF CURRENT'LINE>R'PAGE'LENGTH THEN CURRENT'LINE=1
	CALL PROGRESS'BAR'UPDATE
	INPUT LINE #REPORT'CHAN,REPORT'LINE 
	IF REPORT'LINE[1;1]=CHR(12) THEN CURRENT'LINE=1              !Form Feed
	IF CURRENT'LINE=1 THEN PAGE'NO=PAGE'NO+1
	RETURN
!
!==============================================================================
!{Process Line}
!==============================================================================
PROCESS'LINE:
	INC'LINE="Y"

	! Check FIRST PAGE Ignore line number list.
	if PAGE'NO=1 THEN 
	   IF R'FIRST'IGNORE'COUNT>0 THEN
	      FOR X=1 TO R'FIRST'IGNORE'COUNT
	        IF CURRENT'LINE=R'FIRST'IGNORE'LIST(X) THEN INC'LINE="N" : X=R'FIRST'IGNORE'COUNT
	      NEXT X
	   ENDIF
	   IF INC'LINE="N" THEN GOTO RTN'PROCESS'LINE
	ENDIF

	! Check Ignore line number list.
	IF R'IGNORE'COUNT>0 THEN
	   FOR X=1 TO R'IGNORE'COUNT
	     IF CURRENT'LINE=R'IGNORE'LIST(X) THEN INC'LINE="N" : X=R'IGNORE'COUNT
	   NEXT X
	ENDIF
	IF INC'LINE="N" THEN GOTO RTN'PROCESS'LINE
	
	! Check Ignore matching TEXT list. 
	IF IGNORE'TEXT'COUNT>0 THEN
	   FOR X=1 TO IGNORE'TEXT'COUNT
	     X1=INSTR(1,REPORT'LINE,IGNORE'TEXT(X))   
	     IF X1<>0 THEN INC'LINE="N" : X=IGNORE'TEXT'COUNT
	   NEXT X
	ENDIF
	IF INC'LINE="N" THEN GOTO RTN'PROCESS'LINE

	! Skip Blank lines. 
	IF REPORT'LINE="" THEN GOTO RTN'PROCESS'LINE
	IF REPORT'LINE[1;1]=CHR(12) GOTO RTN'PROCESS'LINE

        !Ignore end of report text.
	FF=INSTR(1,REPORT'LINE,"END OF REPORT")
	IF FF<>0 THEN GOTO RTN'PROCESS'LINE

	!Check Exit List
	IF EXIT'TEXT'COUNT>0 THEN
	   FOR X=1 TO EXIT'TEXT'COUNT
	     X1=INSTR(1,REPORT'LINE,EXIT'TEXT(X))   
	     IF X1<>0 THEN CSV'COMPLETE="Y" : X=EXIT'TEXT'COUNT
	   NEXT X
	ENDIF
	IF CSV'COMPLETE="Y" THEN GOTO RTN'PROCESS'LINE

        !Nearly there....
	FF=INSTR(1,REPORT'LINE,R'START'TEXT)
	IF R'BLOCKING="Y" AND FF<>0 THEN
	   CALL BLOCK'TEXT
	  ELSE              
	   CALL OUTPUT'CSV'LINE
	endif
	
RTN'PROCESS'LINE:
	RETURN	
!
!==============================================================================
!{Output to CSV}
!==============================================================================
OUTPUT'CSV'LINE:
	OUTPUT'LINE=""
	XCALL STRRPL,REPORT'LINE,CHR(34),"'"  !Replace any quotes.
	FOR X=1 TO R'FIELD'COUNT
	  CURRENT'FIELD=REPORT'LINE[R'FIELD'LIST(X,1);R'FIELD'LIST(X,2)]
	  IF R'BLANK'REPEAT'COUNT>0 THEN
             FOR L=1 TO R'BLANK'REPEAT'COUNT
               IF R'BLANK'REPEAT'LIST(L)=X AND CURRENT'FIELD="" THEN &
                  CURRENT'FIELD=PREVIOUS'FIELD(X) 
             NEXT L 
	  ENDIF 
	  OUTPUT'LINE=OUTPUT'LINE+QUOTE+CURRENT'FIELD+QUOTE
	  XCALL STRIP,OUTPUT'LINE
	  XCALL STRIPL,OUTPUT'LINE
	  IF X<>R'FIELD'COUNT THEN OUTPUT'LINE=OUTPUT'LINE+X'DELIMITOR
	  PREVIOUS'FIELD(X)=CURRENT'FIELD
	NEXT X
	IF R'BLOCKING="Y" THEN
	   OUTPUT'LINE=BLOCK'CSV+X'DELIMITOR+OUTPUT'LINE
	ENDIF
	PRINT #CSV'FILE'CHAN,OUTPUT'LINE
	RETURN

!==============================================================================
!{Block Text}
!==============================================================================	
BLOCK'TEXT:
	FOR L=1 TO R'BLOCK'LINES
	  R'BLOCK'TEXT(L)=REPORT'LINE
	  CALL READ'LINE  
	NEXT L
	BLOCK'CSV=""
	FOR X=1 TO R'BLOCK'FIELD'COUNT
	  TEMP'FIELD=R'BLOCK'TEXT(R'BLOCK(X,3))[R'BLOCK(X,1);R'BLOCK(X,2)]
	  XCALL STRIP,TEMP'FIELD
	  XCALL STRIPL,TEMP'FIELD
	  BLOCK'CSV=BLOCK'CSV+QUOTE+TEMP'FIELD+QUOTE
	  IF X<>R'BLOCK'FIELD'COUNT THEN BLOCK'CSV=BLOCK'CSV+X'DELIMITOR
	NEXT X
	RETURN
!
!==============================================================================
!{Blocking Label Header}
!==============================================================================	
BLOCKING'HEADER:
	HEADER'CSV=""
	IF R'BLOCKING<>"Y" then GOTO RTN'BLOCKING'HEADER
	BYTE'POS=1 : CALL MOVE'POSTION	!Check we are at the start.
	IF BYTE'POS<0 THEN OK="N" : GOTO RTN'BLOCKING'HEADER 
NEXT'BLOCK'LABEL'LINE:
	INPUT LINE #REPORT'CHAN,REPORT'LINE
	IF EOF(REPORT'CHAN) GOTO DONE'BLOCK'LABEL'LINE
	FF=INSTR(1,REPORT'LINE,R'START'TEXT)
	IF FF<>0 THEN CALL BUILD'BLOCKING'HEADER : GOTO DONE'BLOCK'LABEL'LINE
	GOTO NEXT'BLOCK'LABEL'LINE
DONE'BLOCK'LABEL'LINE:
	BYTE'POS=1 : CALL MOVE'POSTION	!Go back to start of report
	IF BYTE'POS<0 THEN OK="N" : GOTO RTN'BLOCKING'HEADER 
RTN'BLOCKING'HEADER:
	RETURN	
!
! Build CSV header from Report block.
!	
BUILD'BLOCKING'HEADER:
	FOR L=1 TO R'BLOCK'LINES
	  R'BLOCK'TEXT(L)=REPORT'LINE
	  CALL READ'LINE  
	NEXT L
	HEADER'CSV=""
	FOR X=1 TO R'BLOCK'LABEL'FIELD'COUNT
	  TEMP'FIELD=R'BLOCK'TEXT(R'BLOCK'LABEL(X,3))[R'BLOCK'LABEL(X,1);R'BLOCK'LABEL(X,2)]
	  XCALL STRIP,TEMP'FIELD
	  XCALL STRIPL,TEMP'FIELD
	  HEADER'CSV=HEADER'CSV+QUOTE+TEMP'FIELD+QUOTE
	  IF X<>R'BLOCK'LABEL'FIELD'COUNT THEN HEADER'CSV=HEADER'CSV+X'DELIMITOR
	NEXT X
	RETURN
!
!==============================================================================
!{Read Global File Defs}
!==============================================================================	
READ'DEF'FILE:
      	QUOTE=CHR(34)
 	LOOKUP X'DEF'FILE,FF
       	IF FF=0 THEN
       	   OK="N"
       	   XCALL MESAG,"Sorry no report definition found for "+X'DEF'FILE,2
       	   GOTO RTN'OPEN'DEF'FILE
	ENDIF
	! Get Page Length
	XCALL INIX,X'DEF'FILE,0,"REPORT","PAGE LENGTH",R'PAGE'LENGTH,0
	IF R'PAGE'LENGTH=0 THEN &
	   XCALL MESAG,"WARNING, NO PAGE LENGTH DEFINDED",2

	! Get Title Line
	XCALL INIX,X'DEF'FILE,0,"REPORT","TITLE LINE",R'TITLE'LINE,0

	CALL BUILD'PARSE'IGNORE'LIST	! Build Ignore line list
	CALL BUILD'PARSE'FIELD'LIST	! Build Field List
	CALL DEF'TEXT'IGNORE'LIST	! Build Text Ignore Line List
	CALL DEF'EXIT'LIST		! Build Exit on list
	CALL BUILD'PARSE'FIRST'PAGE'IGNORE'LIST	!Build 1st ignore list.
	CALL BUILD'BLANK'REPEAT'LIST    ! Build blank field rpeat list.
	
	! Report Blocking 
	XCALL INIX,X'DEF'FILE,0,"BLOCK","START TEXT",R'START'TEXT,""
 	XCALL INIX,X'DEF'FILE,0,"BLOCK","LINES",R'BLOCK'LINES,0
   	IF R'BLOCK'LINES>R'BLOCK'LINES'MAX THEN R'BLOCK'LINES=R'BLOCK'LINES'MAX
      	IF R'START'TEXT<>"" AND R'BLOCK'LINES>0 THEN R'BLOCKING="Y" 
     	IF R'BLOCKING="Y" THEN
	   CALL BUILD'BLOCK'DATA
	   CALL BUILD'BLOCK'LABELS
	ENDIF
	
RTN'OPEN'DEF'FILE:
	RETURN
!
!==============================================================================
!{Build Text Ignore List}  
!==============================================================================
DEF'TEXT'IGNORE'LIST:
	X1=0
	OPEN #DEF'CHAN,X'DEF'FILE,INPUT
	DO WHILE EOF(DEF'CHAN)=0
	   INPUT LINE #DEF'CHAN,TEMP'FIELD
	   IF X1=1 AND TEMP'FIELD[1;1]="[" THEN X1=0
	   IF X1=1 THEN 
	      IF IGNORE'TEXT'COUNT=<IGNORE'TEXT'MAX AND TEMP'FIELD<>"" THEN  
	         IGNORE'TEXT'COUNT=IGNORE'TEXT'COUNT+1
	         IGNORE'TEXT(IGNORE'TEXT'COUNT)=TEMP'FIELD
	      ENDIF    
	   ENDIF
	   IF TEMP'FIELD="[IGNORE LIST]" THEN X1=1
	LOOP
	CLOSE #DEF'CHAN
	RETURN
!
!==============================================================================
!{Build Exit on List}  
!==============================================================================
DEF'EXIT'LIST:
	X1=0
	OPEN #DEF'CHAN,X'DEF'FILE,INPUT
	DO WHILE EOF(DEF'CHAN)=0
	   INPUT LINE #DEF'CHAN,TEMP'FIELD
	   IF X1=1 AND TEMP'FIELD[1;1]="[" THEN X1=0
	   IF X1=1 THEN 
	      IF EXIT'TEXT'COUNT=<EXIT'TEXT'MAX AND TEMP'FIELD<>"" THEN  
	         EXIT'TEXT'COUNT=EXIT'TEXT'COUNT+1
	         EXIT'TEXT(EXIT'TEXT'COUNT)=TEMP'FIELD
	      ENDIF    
	   ENDIF
	   IF TEMP'FIELD="[EXIT ON]" THEN X1=1
	LOOP
	CLOSE #DEF'CHAN
	RETURN
!
!==============================================================================
!{Build Block Data}  
!==============================================================================
BUILD'BLOCK'DATA:
	R'BLOCK'FIELD'COUNT=0
	FOR I=1 TO 99
	 FIELD'NO=I : CALL PARSE'BLOCK'FIELD
	NEXT I
	RETURN

PARSE'BLOCK'FIELD:
	XCALL INIX,X'DEF'FILE,0,"BLOCK DATA",FIELD'NO,FIELD'DATA
	IF FIELD'DATA="" THEN GOTO RTN'PARSE'FIELD
	IF R'BLOCK'FIELD'COUNT+1>R'BLOCK'FIELDS'MAX THEN GOTO RTN'PARSE'BLOCK'FIELD
	X=INSTR(1,FIELD'DATA,",")
	X1=INSTR(X+1,FIELD'DATA,",")
	R'BLOCK'FIELD'COUNT=R'BLOCK'FIELD'COUNT+1
	R'BLOCK(R'BLOCK'FIELD'COUNT,1)=FIELD'DATA[1;X-1]
	R'BLOCK(R'BLOCK'FIELD'COUNT,2)=FIELD'DATA[X+1,X1-1]
	R'BLOCK(R'BLOCK'FIELD'COUNT,3)=FIELD'DATA[X1+1;2]
	IF R'BLOCK(R'BLOCK'FIELD'COUNT,3)=0 THEN R'BLOCK(R'BLOCK'FIELD'COUNT,3)=1
RTN'PARSE'BLOCK'FIELD:
	RETURN
!	
!==============================================================================
!{Build Block Labels}  
!==============================================================================
BUILD'BLOCK'LABELS:
	R'BLOCK'LABEL'FIELD'COUNT=0
	FOR I=1 TO 99
	 FIELD'NO=I : CALL PARSE'BLOCK'LABELS
	NEXT I
	RETURN

PARSE'BLOCK'LABELS:
	XCALL INIX,X'DEF'FILE,0,"BLOCK LABELS",FIELD'NO,FIELD'DATA
	IF FIELD'DATA="" THEN GOTO RTN'PARSE'FIELD
	IF R'BLOCK'LABEL'FIELD'COUNT+1>R'BLOCK'LABEL'FIELDS'MAX THEN GOTO RTN'PARSE'BLOCK'LABELS
	X=INSTR(1,FIELD'DATA,",")      
	X1=INSTR(X+1,FIELD'DATA,",")
	R'BLOCK'LABEL'FIELD'COUNT=R'BLOCK'LABEL'FIELD'COUNT+1
	R'BLOCK'LABEL(R'BLOCK'LABEL'FIELD'COUNT,1)=FIELD'DATA[1;X-1]
	R'BLOCK'LABEL(R'BLOCK'LABEL'FIELD'COUNT,2)=FIELD'DATA[X+1,X1-1]
	R'BLOCK'LABEL(R'BLOCK'LABEL'FIELD'COUNT,3)=FIELD'DATA[X1+1;2]
	IF R'BLOCK(R'BLOCK'LABEL'FIELD'COUNT,3)=0 THEN R'BLOCK(R'BLOCK'LABEL'FIELD'COUNT,3)=1
RTN'PARSE'BLOCK'LABELS:
	RETURN	
!
!
!==============================================================================
!{Build Field List}  
!==============================================================================
BUILD'PARSE'FIELD'LIST:
	R'FIELD'COUNT=0
	FOR I=1 TO 99
	 FIELD'NO=I : CALL PARSE'FIELD
	NEXT I
	RETURN

PARSE'FIELD:
	XCALL INIX,X'DEF'FILE,0,"FIELDS",FIELD'NO,FIELD'DATA
	IF FIELD'DATA="" THEN GOTO RTN'PARSE'FIELD
	IF R'FIELD'COUNT+1>R'FIELD'MAX THEN GOTO RTN'PARSE'FIELD
	X=INSTR(1,FIELD'DATA,",") : LENX=LEN(FIELD'DATA)
	R'FIELD'COUNT=R'FIELD'COUNT+1
	R'FIELD'LIST(R'FIELD'COUNT,1)=FIELD'DATA[1;X-1]
	R'FIELD'LIST(R'FIELD'COUNT,2)=FIELD'DATA[X+1,LENX]
RTN'PARSE'FIELD:
	RETURN	
!
!==============================================================================
!{Parse Ignore List}  
!==============================================================================
BUILD'PARSE'IGNORE'LIST:
	R'IGNORE'COUNT=0
	XCALL INIX,X'DEF'FILE,0,"REPORT","IGNORE LINE",R'IGNORE'LINE
       	IF R'IGNORE'LINE="" THEN GOTO RTN'PARSE'IGNORE'LIST
	S=1
	FOR I=1 TO LEN(R'IGNORE'LINE)
	 IF R'IGNORE'LINE[I;1]="," THEN 
	    CALL ADD'TO'IGNORE'ARRAY
	    S=I+1
	 ENDIF	 	  
	NEXT I
	CALL ADD'TO'IGNORE'ARRAY
RTN'PARSE'IGNORE'LIST:
	RETURN
!
! Add to Array.
!
ADD'TO'IGNORE'ARRAY:
	R'IGNORE'COUNT=R'IGNORE'COUNT+1
	IF R'IGNORE'COUNT=<R'IGNORE'MAX THEN
	   R'IGNORE'LIST(R'IGNORE'COUNT)=R'IGNORE'LINE[S,I-1]
	ENDIF
	RETURN
!
!==============================================================================
!{Parse First Page Ignore List}  
!==============================================================================
BUILD'PARSE'FIRST'PAGE'IGNORE'LIST:
	R'FIRST'IGNORE'COUNT=0
	XCALL INIX,X'DEF'FILE,0,"REPORT","IGNORE FIRST PAGE",R'FIRST'IGNORE'LINE
       	IF R'FIRST'IGNORE'LINE="" THEN GOTO RTN'BUILD'PARSE'FIRST'PAGE'IGNORE'LIST
	S=1
	FOR I=1 TO LEN(R'FIRST'IGNORE'LINE)
	 IF R'FIRST'IGNORE'LINE[I;1]="," THEN
	    CALL ADD'TO'FIRST'PAGE'IGNORE'ARRAY
	    S=I+1
	 ENDIF	 	  
	NEXT I
	CALL ADD'TO'FIRST'PAGE'IGNORE'ARRAY
RTN'BUILD'PARSE'FIRST'PAGE'IGNORE'LIST:
	RETURN
!
! Add to Array.
!
ADD'TO'FIRST'PAGE'IGNORE'ARRAY:
	R'FIRST'IGNORE'COUNT=R'FIRST'IGNORE'COUNT+1
	IF R'FIRST'IGNORE'COUNT=<R'FIRST'IGNORE'MAX THEN
	   R'FIRST'IGNORE'LIST(R'FIRST'IGNORE'COUNT)=R'FIRST'IGNORE'LINE[S,I-1]
	ENDIF                    
	RETURN
!
!==============================================================================
!{Parse Blank Field repeat List}  
!==============================================================================
BUILD'BLANK'REPEAT'LIST:
	R'BLANK'REPEAT'COUNT=0
	XCALL INIX,X'DEF'FILE,0,"REPORT","BLANK REPEAT FIELDS",R'BLANK'REPEAT'LINE
       	IF R'BLANK'REPEAT'LINE="" THEN GOTO RTN'BUILD'BLANK'REPEAT'LIST
	S=1
	FOR I=1 TO LEN(R'BLANK'REPEAT'LINE)
	 IF R'BLANK'REPEAT'LINE[I;1]="," THEN
	    CALL ADD'TO'BLANK'REPEAT'ARRAY
	    S=I+1
	 ENDIF	 	  
	NEXT I
	CALL ADD'TO'BLANK'REPEAT'ARRAY
RTN'BUILD'BLANK'REPEAT'LIST:
	RETURN
!
! Add to Array.
!
ADD'TO'BLANK'REPEAT'ARRAY:
	R'BLANK'REPEAT'COUNT=R'BLANK'REPEAT'COUNT+1
	IF R'BLANK'REPEAT'COUNT=<R'BLANK'REPEAT'MAX THEN
	   R'BLANK'REPEAT'LIST(R'BLANK'REPEAT'COUNT)=R'BLANK'REPEAT'LINE[S,I-1]
	ENDIF                    
	RETURN
!
!==============================================================================
!{Count Lines}
!==============================================================================
COUNT'LINES:
	REPORT'LINES'COUNT=0
	DO WHILE EOF(REPORT'CHAN)=0
	   INPUT LINE #REPORT'CHAN,REPORT'LINE
	   REPORT'LINES'COUNT=REPORT'LINES'COUNT+1
	   IF R'TITLE'LINE=REPORT'LINES'COUNT THEN REPORT'TITLE=REPORT'LINE
	LOOP
	BYTE'POS=1 : CALL MOVE'POSTION	!Back to start of report.
	IF BYTE'POS<0 THEN OK="N" : GOTO RTN'COUNT'LINES 
RTN'COUNT'LINES:
	RETURN
!
!==============================================================================
!{Move File Postion}
!==============================================================================
MOVE'POSTION:
	! Set BYTE'POS
	xcall MIAMEX, MX_FILEPOS,REPORT'CHAN,1,BYTE'POS
	IF BYTE'POS<0 THEN &
	  XCALL MESAG,"MIAMEX MX_FILEPOS FAILED TO GOTO LINE.",2
	RETURN
!
!==============================================================================
!{Progress Bars}
!==============================================================================
PROGRESS'BAR'SHOW:
	DSPERC'COUNT=0
	PRINT TAB(24,1);"CREATING CSV - PLEASE WAIT...";TAB(-1,9);
	if REPORT'LINES'COUNT>0 then 
	 DSPERC'PERC'VALUE=100/REPORT'LINES'COUNT
	 IF AVAIL'GUI=1 THEN
	    XCALL MSLAVI,1,AVI'ID,8,22,23,50,60   
	    PROGRESS'COMMAND=1 : CALL PROGRESS'BAR
	 ENDIF
	endif
	RETURN

PROGRESS'BAR'UPDATE:
	DSPERC'COUNT=DSPERC'COUNT+1
	DSPERC'WORK=INT(DSPERC'COUNT*DSPERC'PERC'VALUE)
	DSPERC'BAR'CPOS=DSPERC'WORK
	IF AVAIL'GUI=1 THEN
 	   PROGRESS'COMMAND=2 : CALL PROGRESS'BAR
 	 ELSE
  	   PRINT TAB(24,72);DSPERC'WORK;"%";
	ENDIF
	RETURN

PROGRESS'BAR'DELETE:
	IF AVAIL'GUI=1 THEN
	   XCALL MSLAVI,3,AVI'ID
	   PROGRESS'COMMAND=3 : CALL PROGRESS'BAR
 	ENDIF
	RETURN
	
PROGRESS'BAR:
	    XCALL PROGRS,PROGRESS'COMMAND,DSPERC'BAR'STS,DSPERC'BAR'ID,DSPERC'BAR'FLAGS,DSPERC'BAR'CAPTION,&
	                 DSPERC'BAR'CPOS,DSPERC'BAR'EPOS,DSPERC'BAR'SROW,DSPERC'BAR'SCOL,&
	                 DSPERC'BAR'EROW,DSPERC'BAR'ECOL
	RETURN	
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,"MSLCSV.SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
	
