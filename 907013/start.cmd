:<A-Shell Source Procedure Test Programs
(These get overwritten frequently so if you want to make your own 
personal modifications, it is advised that you move your copy elsewhere.)

See SOSPROC: for the procedure bsi files

Program         Description
STSMSGBOX       Tester for StsMsgBox()
SYSLOG          Tester for SysLog()
XTR32BITS       Tester for Xtree'32Bits()

>
SET LONGDIR
