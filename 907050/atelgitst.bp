program ATELGITST, 1.0(102)  ! simple test of ATELGI.SBX
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![100] 06/04/16 11:15:21      Edited by Jack
!	Created 
![101] 13-Sep-16 14:01       Edited by Jack
!   Add command line option to specify SBX name (RUN ATELGITST/TEST)
!   or even the full http spec to get it (ATELGITST/http://www.microsabio.net/temphold/test.sbx)
![102] 13-Dec-16 10:09       Edited by Jack
!   Test new query interface   
!------------------------------------------------------------------------
!NOTES
!   Use for simple testing of ATELGI outside of ATE.
!   (Use ATE for real testing)
!------------------------------------------------------------------------

++include ashinc:ashell.def
++include sosfunc:fnhttpget.bsi         ! [101]
++include sosfunc:fninstr.bsi           ! [101]

MAP1 PARAMS
    MAP2 LOGIN$,S,64,""
    MAP2 PW$,S,128,""
    MAP2 PKPW$,S,260
    MAP2 STS,I,2
    MAP2 HOST$,S,80,"" ! "127.0.0.1:23123"
    MAP2 CFG$,S,64,"testcfg"
    MAP2 SBXNAME$,S,16,"ATELGI"
    MAP2 X,I,2
    MAP2 CLINE$,S,200
    MAP2 URL$,S,200
    MAP2 RESPONSE$,S,200        ! [102] query response
    MAP2 TIMEOUT,B,2,5000       ! [102] 5 second timeout
    
    ! [101] check for /SBX or /HTTP://...
    if instr(1,CMDLIN,"/") then
        if CMDLIN$ = "/H" or CMDLIN$ = "/?" goto HELP
            
        xcall LSTLIN, CLINE$
        X = instr(1,CLINE$,"/")
        CLINE$ = CLINE$[X+1,-1]
        X = instr(1,CLINE$,"/")
        if X > 1 then               ! check for http://...
            if ucs(CLINE$[1,7]) = "HTTP://" then
                URL$ = CLINE$[8,-1]                 ! remove "HTTP://"
                X = Fn'Instr(-1,URL$,"/")   ! get last "/"
                if X then 
                    SBXNAME$ = URL$[X+1,-1]
                    if ucs(SBXNAME$[-4,-1]) # ".SBX" then
                        ? "SBXNAME: [";SBXNAME$;"]"
                        ? "URL must specify a .SBX file!"
                        goto HELP
                    endif
                    
                    X = Fn'HttpGet(URL$,SBXNAME$)
                    if X <= 0 then
                        ? "Unable to download ";URL$;" - error= ";X
                        goto HELP
                    endif
                else
                    ? "Invalid URL spec"
                    goto HELP
                endif
            else
                ? "Invalid argument syntax!"
                goto HELP
            endif
        else                        ! assume just SBX name
            SBXNAME$ = CLINE$
            X = instr(1,SBXNAME$,".")       ! remove any .SBX
            if X > 0 then
                SBXNAME$  = SBXNAME$[1,X-1]
            endif
        endif
    endif
    
    input "Enter incoming STS: ",STS
    vxcall SBXNAME$,  LOGIN$, PW$, PKPW$, STS, HOST$, CFG$
    
    ? "LOGIN$: ";LOGIN$
    ? "PW$   : ";PW$
    ? "PKPW$ : ";PKPW$
    ? "STS   :"; STS
    ? "HOST$ :"; HOST$
    
    ! [102] test query interface...
    if STS = 1 then
        ?
        ? "Testing query interface..."
        xcall MIAMEX, MX_AGWRAPPER, AG_XFUNCS, SBXNAME$+",host", RESPONSE$, TIMEOUT
        ? "host: ";RESPONSE$
        xcall MIAMEX, MX_AGWRAPPER, AG_XFUNCS, SBXNAME$+",dbse", RESPONSE$, TIMEOUT
        ? "dbse: ";RESPONSE$
        xcall MIAMEX, MX_AGWRAPPER, AG_XFUNCS, SBXNAME$+",all", RESPONSE$, TIMEOUT
        ? "all: ";RESPONSE$
    else
        ? "(user aborted)"
    endif
    END
    
HELP:
    ? "Syntax: "
    ? "   .RUN ATELGITST             ! test ATELGI.SBX"
    ? "   .RUN ATELGITST/xxx         ! test XXX.SBX" 
    ? "   .RUN ATELGITST/http://url  ! retrieve and test SBX in url"
    end
