:<(ATECUSTOM) ATE Customization Dialogs
    ATE will use BAS:ATELGI.SBX for login (if present)
      ATELGIX.BP - Sample version
      ATELGITST.BP - Simple test of ATELGI.SBX

    ATE will use %MIAME%\custom\atesplash.lit for splash page 
    (if present and not -n)
      ATESPLASH.BP - Sample splash dialog
      ATESPLASH.PNG - Sample ATE splash image
>
