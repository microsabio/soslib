program fndynst, 1.0(102)  ! test fndynst.bsi runctions
!------------------------------------------------------------------------
!EDIT HISTORY
! [102] 31-Aug-18 / jdm / replace ST_VARDEF with expanded ST_FLDDEF 
! [101] 30-Aug-18 / jdm / add optin for file-based DYNOP_DEF
! [100] 29-Aug-18 / jdm / created
!------------------------------------------------------------------------
!REQUIREMENTS
!   A-Shell 6.5.1645.3+, compiler 876+  [103]
!NOTES
!   See sosfunc:fndynst.bsi for notes
!------------------------------------------------------------------------

++include'once ashinc:ashell.def
++include'once sosfunc:fndynst.bsi

map1 misc
    map2 file$,s,260
    map2 dsname$,s,64
    map2 dsdef$,s,0
    map2 a,b,4
    map2 b,b,4
    map2 i,i,2
    map2 j,i,2
    map2 fields,i,4
    map2 stsize,b,4
    map2 errmsg$,s,0
    map2 offset,b,4
    map2 status,i,4
    map2 op,b,1
    map2 flddef'hdr$,s, 80,"F#  Struct or Field Name       Type  Size  Subs     Pos  Deftype"
    map2 flddef'mask$,s,80,"##. \----------Name------------\ \\  ####  \-----\ ####  \--------------------\"
    
map1 ds,dynstruct
dimx flddefs(0), ST_FLDDEF, auto_extend     ! array for retrieving/displaying dynstruct field info [103]

    ? .PGMNAME;" version ";.PGMVERSION;" - test fndynst.bsi routines"
    
    input line "Enter file$ to read DEFSTRUCT from [sosfunc:fndynst.bsi] : ",file$
    if file$ = "" then
        file$ = "sosfunc:fndynst.bsi"
    endif
    input line "Enter DEFSTRUCT name to load [ST_FLDDEF] : ",dsname$
    if dsname$ = "" then
        dsname$ = "ST_FLDDEF"
    endif
    op = 1
    input "1) for file-based DYNOP_DEF, 0) to extract and define as string [1]: ",op
    
    if op = 0 then
        dsdef$ = Fn'Dynst'Load'Defstruct$(file$,dsname$)
        if dsdef$ = "" then
            ? "Nothing loaded (file or defstruct not found)"
            end
        endif
    
        ? "Found and loaded."
        ? "Size of loaded defstruct: ";len(dsdef$)
        input "Enter 1) to display the structure into the string:  ",a
        if a then
            open #1, "fndynst.lst", output
            ? #1, dsdef$
            close #1
            xcall EZTYP, "fndynst.lst"
        endif
        a = 0
        input "Enter 1) to define the dynamic structure: ",a
    else
        dsdef$ = "@" + file$        ! [101]
        a = 1
    endif
    
    if a then
        fields = Fn'Dynst'Define(dsdef$,stsize,dsname$,errmsg$)
        ? "fields: ";fields; ifelse$(fields >= 0, " [ok]"," Error: "+errmsg$)
        if fields < 0 then
            offset = instr(1,errmsg$,"offset:")
            if offset > 0 then
                offset = val(errmsg$[offset+7,-1])
                ? "Approximate location of error (assuming no macros) : "
                a = .instrr(offset,dsdef$,chr(10))
                b = instr(offset,dsdef$,chr(10))
                ? dsdef$[a+1,b-1]
            endif
        else
            input "Enter 1) to get and display defined structure: ",a
            if a then
                fields = Fn'Dynst'Get'Def'By'Name(dsname$,flddefs())
                ? "fields: ";fields; ifelse$(fields >= 0, " [ok]"," [Error]")
                if fields > 0 then
                    ? flddef'hdr$
                    for i = 1 to fields
                        ? using flddef'mask$, i, flddefs(i).name, Fn'Dec2Hex$(flddefs(i).vartyp and &h1f), &
                                    flddefs(i).varsiz, Fn'Dynst'Format'Subs$(flddefs(i)), &
                                    flddefs(i).pos, flddefs(i).deftyp
                    next i
                endif
            endif
            input "Enter 1) to test binding it to the dynstruct var ds: ",a
            if a then
                status = Fn'Dynst'Bind(dsname$,ds)
                ? "status: ";ifelse$(status = 0, " [ok]"," [Error: "+status+"]")
                if status = 0 then
                    input "Enter 1) to retrieve dyndef name from ds instance: ",a
                    if a then
                        ? "Structure name bound to ds instance: ";
                        ? Fn'Dynst'Name'From'Instance$(ds) 
                    endif
                endif
            endif
        endif
    endif
    end
       
