program uzstrings, 1.0(100)  ! examples of functions in uzstrings.fnp
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![100] 05/08/15 08:52:31      Edited by Jack
!	Created 
!------------------------------------------------------------------------
!NOTES 
!   strings.fnp contains a collection of string-manipulation functions
!   contributed to the SOSLIB by Jorge Tavares
!------------------------------------------------------------------------

++include ashinc:ashell.def
++include sosfunc:jtstrings.fnp

map1 misc
    map2 fno,b,1
    map2 col$,s,3
    map2 col,b,2
    map2 val1$,s,20
    map2 val2$,s,20
    map2 val1,f
    map2 val2,f
    map2 zero'as'blank,b,1
    map2 left'offset,i,2
    map2 right'offset,i,2
    
map1 value$,s,0
map1 prefix$,s,0
map1 suffix$,s,0
map1 then$,s,0
map1 else$,s,0
map1 left'delimiter$,s,0
map1 right'delimiter$,s,0
map1 sourcetext$,s,0
map1 text2search$,s,0

map1 text$,s,0,"plain\plain\f0\fs24\ql\plain\f0\fs24 these are test notes for clib,test2\par"
    
    ? tab(-1,0);"Test string manipulation functions in strings.fnp"
    do
        ?
        ? "1) FN'excel'column$(col)"
        ? "2) FN'excel'col'letter2num(col$, col)"
        ? "3) FN'concatenate'if'value$(value$,prefix$,suffix$,zero'as'blank)"
        ? "4) FN'if'else'value$(value$,else$,zero'as'blank)"        
        ? "5) FN'if'then'else'value$(value$,then$,else$,zero'as'blank)"        
        ? "6) FN'soma'string$(val1,val2)" 
        ? "7) FN'mid'str$(sourcetext$,left'delimiter$,right'delimiter$,left'offset,right'offset)"
        ? "8) FN'if'mid'str$(sourcetext$,left'delimiter$,right'delimiter$)"
        ? "9) FN'count'str(sourcetext$, text2search$)"
        ? "10) Jorge's examples of FN'if'mid'str$,FN'left'str$,FN'right'str$" 
        
        input "Function to test: ",fno
        
        if fno = 0 exit
        on fno call excel'column, excel'col'leter2num, concatenate'if'value, &
                if'else'value, if'then'else'value, soma'string, &
                mid'str, if'mid'str, count'str, mid'left'right
        ?
        stop
    loop
    end
    
excel'column:
    input "Enter column #: ",col
    ? "FN'excel'column$(";col;") = ";FN'excel'column$(col)
    return
    
excel'col'leter2num:
    input "Enter column$ A-ZZ: ",col$
    ? "FN'excel'col'letter2num(";col$;") = ";FN'excel'col'letter2num(col$, col)
    ? "Returned col = ";col
    return
    
concatenate'if'value:
    value$ = ""
    prefix$ = ""
    suffix$ = ""
    input "Enter value$: ",value$
    input "Enter prefix$: ",prefix$
    input "Enter suffix$: ",suffix$
    input "Enter zero'as'blank: ",zero'as'blank
    ? "FN'concatenate'if'value$(";value$;",";prefix$;",";suffix$;",";zero'as'blank;") = ";
    ? FN'concatenate'if'value$(value$,prefix$,suffix$,zero'as'blank)
    return
   
if'else'value:
    value$ = ""
    else$ = ""
    input "Enter value$: ",value$
    input "Enter else$: ",else$
    input "Enter zero'as'blank: ",zero'as'blank
    ? "FN'if'else'value$(";value$;",";else$;",";zero'as'blank;") = ";
    ? FN'if'else'value$(value$,else$,zero'as'blank)
    return

if'then'else'value:
    value$ = ""
    then$ = ""
    else$ = ""
    input "Enter value$: ",value$
    input "Enter then$: ",then$
    input "Enter else$: ",else$
    input "Enter zero'as'blank: ",zero'as'blank
    ? "FN'if'then'else'value$(";value$;",";then$;",";else$;",";zero'as'blank;") = ";
    ? FN'if'then'else'value$(value$,then$,else$,zero'as'blank)
    return
    
soma'string:
    input "val1: ",val1$
    input "val2: ",val2$
    ? "FN'soma'string(";val1$;",";val2$;") = "
    ? " using string args: ";FN'soma'string(val1$,val2$)
    val1 = val(val1$)
    val2 = val(val2$)
    ? " using num vars: ";FN'soma'string(val1,val2)
    ? " using val(val1) + val(val2): ";val(val1) + val(val2)
    return
    
mid'str:
    sourcetext$ = ""
    left'delimiter$ = ""
    right'delimiter$ = ""
    input "Enter sourcetext$: ",sourcetext$
    input "Enter left delimiter$: ",left'delimiter$
    input "Enter right delimiter$: ",right'delimiter$
    input "Enter left offset: ",left'offset
    input "Enter right offset: ",right'offset
    ? "FN'mid'str$(";sourcetext$;",";left'delimiter$;",";right'delimiter$;",";left'offset;",";right'offset") = ";
    ? FN'mid'str$(sourcetext$,left'delimiter$,right'delimiter$,left'offset,right'offset)
    return
    
if'mid'str:
    sourcetext$ = ""
    left'delimiter$ = ""
    right'delimiter$ = ""
    input "Enter sourcetext$: ",sourcetext$
    input "Enter left delimiter$: ",left'delimiter$
    input "Enter right delimiter$: ",right'delimiter$
    ? "FN'if'mid'str$(";sourcetext$;",";left'delimiter$;",";right'delimiter$;") = ";
    ? FN'if'mid'str$(sourcetext$,left'delimiter$,right'delimiter$)
    return    

count'str:
    sourcetext$ = ""
    text2search$ = ""
    input "Enter source text$: ",sourcetext$
    input "Enter text to search for$: ",text2search$
    ? "FN'count'str(";sourcetext$;",";text2search$;") = ";
    ? FN'count'str(sourcetext$,text2search$)
    return    
    

mid'left'right:

    ? "Jorge's examples using text:"
    ? "  ";text$
    ?
    ? "FN'left'str$(FN'right'str$(text$,""\fs24"",5,1), ""\par"", -1) = "
    ? tab(5);FN'left'str$(FN'right'str$(text$,"\fs24",5,1), "\par", -1)
    
    ? "FN'left'str$(FN'right'str$(text$,""\fs24"",5), ""\par"", -1) = "
    ? tab(5); FN'left'str$(FN'right'str$(text$,"\fs24",5), "\par", -1)
    
    ? "FN'left'str$(FN'right'str$(text$,""xxxxx"",5), ""\par"", -1) = "
    ? tab(5); FN'left'str$(FN'right'str$(text$,"xxxxx",5), "\par", -1)

    ? "FN'left'str$(FN'right'str$(text$,""\fs24"",5), ""xxxx"", -1) = "
    ? tab(5); FN'left'str$(FN'right'str$(text$,"\fs24",5), "xxxx", -1)
    
    ? "FN'left'str$(FN'right'str$(text$,""zzzzz"",5), ""xxxx"", -1) = "
    ? tab(5); FN'left'str$(FN'right'str$(text$,"zzzzz",5), "xxxx", -1)
    
    ? "FN'if'mid'str$(text$,""\fs24"", ""\par"") = "
    ? tab(5); FN'if'mid'str$(text$,"\fs24", "\par")
    
    ? "FN'if'mid'str$(text$,""\xxxx"", ""\par"") = "
    ? tab(5); FN'if'mid'str$(text$,"\xxxx", "\par")
    return
    