:<A-Shell Function Test programs
(These get overwritten frequently so if you want to make your own 
personal modifications, it is advised that you move your copy elsewhere.)

See [907,10] (SOSFUNC:) for the functions themselves

Program     Function       Description
----------  ----------     ---------------------------------------------------
fnalloc     Fn'Alloc       Wrapper for Basic allocate statement
fnatesbx    Fn'ATE'SBX     Execute SBX remotely on ATE client
fnameext    Fn'Name'Ext$   Extract just name.ext from filespec
fnbits      Fn'Bitxxxx$    Bit set, clear, toggle, test
fndec2hex   Fn'Dec2Hex$    Convert Decimal notation to hex
fnexplode   Fn'Explode     "Explode" string in to space-delimited tokens
fnfqfs      Fn'FQFS$       Convert partially qualified to fully qualified fspec
fngcd       Fn'GCD         Greatest Common Denominator (recursive)
fnhex2dec   Fn'Hex2Dec     Convert Hex notation to decimal
fnhttpget   Fn'HttpGet     Get a file from web using HTTP protocol
fnisalnum   Fn'IsAlNum     Check if a character is alphanumeric
fnhostex    Fn'Hostex      Wrapper for HOSTEX.SBR
fnminasver  Fn'MinAshVer   Check if A-Shell at least version x.xxx.xx.x
fnminmax    Fn'ValMinMax   Return a value limited by both a min and max
fnprogver   Fn'ProgVer$    Return version of RUN, LIT or SBX
fnprttok    Fn'PrtTok      Print string, using tab(x,y) for individual tokens
fnprtusing  Fn'PrtUsing$   Func equiv to PRINT USING MASK,VAR1,...VARN
fnsprintf   Fn'Sprintf$    Create a formatted string (ala C sprintf() function)
fnswitches  Fn'Get'Switches Parse a typical command line & return switches
fntohost    Fn'ToHost$     Convert AMOS spec to native path
fnfileage   Fn'FileAge     Get age of file in secs
fnverate    Fn'VerATE$     Get ATE version
fnztsupp    Fn'ZT'ESC'Supp Check if ZT Developer ESC Sequences supported
uzstrings   (many string functions in uzstrings.fnp)

(Use PAGE START.CMD to page above output if it scrolled) 
>
set longdir
:R
