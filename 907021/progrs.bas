 program PROGRS,1.0(103)		! progress bar subroutine (SBX)
!--------------------------------------------------------------------------
!NOTES
! This routine displays and updates a Windows-style progress bar.
! Under Windows or ATE, this uses the A-Shell 4.9 progress bar control.
! Otherwise, it should display an equivalent using non-GUI elements
! (although this has not yet been implemented.)
!
! Remember to rename the .RUN to .SBX after compiling!!!
!
!USAGE:
!	xcall PROGRS,OP,STS,ID,PFLAGS,MSG$,CPOS,EPOS,SROW,SCOL,EROW,ECOL
!
!WHERE:
!	OP (numeric) -  1) initialize control
!			2) update	
!			3) delete
!	
!	STS (F,6) returns status info: 0=OK, -1=CANCEL clicked
!                                       =2=Unable to create progress bar
!
!   ID (num) - ???
!
!	PFLAGS (numeric) sum of options:
!       1 Display progress bar within a larger frame
!           (in which case actual bar is on 2nd to last line)					       	
!       2 Cancel button
!       4 (GUI only) Get dialog box title from SYS:APPMSG 0,1
!       8 (GUI only) Use MSG$ for dialog box title
!       16 Update MSG$ when updating progress bar
!       32 (GUI only) Marquee [103]
!
!	MSG$ (string)  message to display above progress bar 
!			(assuming PFLAGS 1)
!	
!	CPOS (numeric) current position within process 
!	EPOS (numeric) ending position within process.  
!			(% complete = 100*CPOS/EPOS)
!
!	SROW,SCOL,EROW,ECOL (numeric) - position of bar (if not PFLAGS 1)
!		or of outer box (if PFLAGS 1).  Minimum height should be
!		1, 4, or 5 depending on which PFLAGS.
!
!
! Example layout:	
!
!	PFLAGS = 1 + 2 (Dialog box and cancel button)
!
!	SROW ->	+-------------------------------+
!		|  <----first line of msg --->  |
!		|  <- 2nd line (if space) --->  |
!		|                               |
!		|  [     Progress Bar        ]  |
!		|				|
!		|        [CANCEL BUTTON]        |
!		+-------------------------------+
!
!
!--------------------------------------------------------------------------
!EDIT HISTORY
! [103] 20-Mar-21 / jdm / ?
! [102] 12-Aug-17 / jdm / slight modernization
! [101] 14-Oct-04 / jdm / use xgetargs, add flags, support pure text
!                         version, use common, modernizaton
! [100] 28-Apr-04 / jdm / created (GUI version only)
!------------------------------------------------------------------------

++pragma SBX

MAP1 PARAMS
	MAP2 OP,F
	MAP2 ID,F
	MAP2 STS,F
	MAP2 PFLAGS,F
	MAP2 MSG$,S,200
	MAP2 CPOS,F
	MAP2 EPOS,F
	MAP2 SROW,B,1
	MAP2 SCOL,B,1
	MAP2 EROW,B,1
	MAP2 ECOL,B,1

MAP1 MISC
    MAP2 PCT,F		! % complete
    MAP2 GUIFLAGS,F		! GUI flags
    MAP2 CID,B,2		! Cancel button ID
    MAP2 DLGID,B,2          ! Dialog box ID  
    MAP2 TITLE$,S,80        ! Dialog box title
    MAP2 BLANKS,B,1		! Set if blank lines available within box
    MAP2 STATUS,F
    MAP2 A$,S,1
    MAP2 CANCEL'KBD,S,1,chr(27)	! CANCEL button kbd equivalent (ESC)
    MAP2 CENTER$,S,80
    MAP2 CSIZE,F    
    MAP2 ROFFSET,F
    MAP2 COFFSET,F
    MAP2 I,I,2
    MAP2 WINSTYLE,B,4   ! [103]
    
MAP1 SEND,B,1,0			! COMMON params
MAP1 RECV			!
    MAP2 F'RCV,B,1,1	!
    MAP2 RCVFLG,B,1,0	!
MAP1 MSGNAM,S,6,"PROGRS"	! packet name (con
MAP1 COMMON'PACKET
    MAP2 TID,B,2		! Text caption ID
    MAP2 PSROW,F		! start row of progress bar
    MAP2 PSCOL,F		! start col of progress bar
    MAP2 PEROW,F		! end row of progress bar
    MAP2 PECOL,F		! end col of progress bar
    MAP2 MSROW,F            ! start of text message (text mode)
    MAP2 MSCOL,F            ! "
    MAP2 MECOL,F            ! "


![102] switch to proper symbols
define PFLG_DLG      = &h0001   ! progress bar is part of a dialog box    
define PFLG_CANCEL   = &h0002   ! includes cancel button
define PFLG_APPTITLE = &h0004   ! [101] Use sys:appmsg 000,001 for title
define PFLG_MSGTITLE = &h0008   ! [101] Use MSG$ for dlg box title
define PFLG_UPDMSG   = &h0010   ! [101] Update MSG$ 
define PFLG_MARQUEE  = &h0020   ! [103] Marquee
   
++include'once ashinc:ashell.def        ! [102] ASHELL.BSI
++include'once ashinc:msboxx.def        ! [102] MSBOXX symbols
![102] ++include XCALL.BSI		! picks up param list

! We should have been called something like this...
!	xcall PROGRS,OP,STS,ID,PFLAGS,MSG$,CPOS,EPOS,SROW,SCOL,EROW,ECOL

BEGIN:
        ! [101] note that although we don't need the SROW,SCOL,EROW,ECOL
        ! [101] in GUI mode except on the first call, we require them
        ! [101] anyway because they are needed in the text mode

	if (.ARGCNT < 11) then      ! [102]
	    ? "Insufficient parameters (";.ARGCNT;") passed to PROGRS" 
	    stop 
	    ! end   ! (this would return to caller)
	    xcall MIAMEX, MX_EXITSBXX	! (this would abort to dot prompt) [102]
	endif

	! pick up the all the params in one shot
	xgetargs OP,STS,ID,PFLAGS,MSG$,CPOS,EPOS,SROW,SCOL,EROW,ECOL

	STS = 0				! param 2 is output only
	if OP=1 then ID = 0

	xcall AUI,AUI_ENVIRONMENT,2,GUIFLAGS	    ! [102]
	if (GUIFLAGS and AGF_GUIEXT) = 0 then
        on OP call TXT'INIT, TXT'UPDATE, TXT'DELETE
    else
	    on OP call GUI'INIT, GUI'UPDATE, GUI'DELETE
    endif

	if STS = 0 and OP = 1 then	! save params in common for 
	        			! use on subsequent calls
        xcall COMMON,SEND,MSGNAM,COMMON'PACKET
	endif

	! return parameters
	xputarg 2,STS	! return arg 2 from STS
	xputarg 3,ID	! return arg 3 from ID
	end		! return to caller

!------------------------------------------------------------------------
! Create the status bar and associated "dialog" box  (GUI version)
! Inputs: MSG$ (message appearing in dialog)
!         PFLAGS, SROW,EROW,SCOL,ECOL
!------------------------------------------------------------------------
GUI'INIT:
	if (PFLAGS and PFLG_DLG) then		! dialog box

 	    ! display Windows panel-style outer box

        ![101] Old method used static frame (aka panel) for pseudo dialog
        ![101]	    xcall MSBOXX,SROW,SCOL,EROW,ECOL,BOX_WIN+BOX_SVA

        ![101] New method uses real dialog...

        ! [101] First set up dialog box title, either from MSG$
        ! [101] or from message 000,001 of sys:appmsg[1,4], or blank

        if (PFLAGS and PFLG_APPTITLE) then 
            xcall MIAMEX,MX_LITMSG,0,1,-1,-1,TITLE$,"SYS:APPMSG","",""
            ! If we got a generic msg, just set it to blank
            if instr(1,TITLE$,"000,001") then 
                TITLE$ = ""
            endif
            if TITLE$ = "" then     ! [103] use program name if no APPMSG
                TITLE$ = .PGMNAME
            endif
        else
            if (PFLAGS and PFLG_MSGTITLE) then 
                TITLE$ = MSG$
            else
                TITLE$ = ""
            endif
        endif

        ! [101] create dialog box and get back DLGID
        xcall AUI,AUI_CONTROL,CTLOP_ADD,DLGID,TITLE$,MBST_ENABLE, &
                MBF_DIALOG+MBF_SYSMENU,"","",STATUS, &
                SROW-1,SCOL-1,EROW,ECOL

        if STATUS # 0 then 
            STS = -2 
            return    ! failed to create dialog
        endif
    
        ! [101] Since dialog children are positioned relative to 
        ! [101] dialog, but positional code below was originally written
        ! [101] for a non-dialog, just set up offsets that we can
        ! [101] apply as needed.
        ROFFSET = SROW-1
        COFFSET = SCOL-1

        ! put a cancel button on bottom row -1
        if (PFLAGS and PFLG_CANCEL) then

            ! Decide if we have room for blank lines
            if EROW-SROW > 3 then 
                BLANKS = 1 
            else 
                BLANKS = 0
            endif
            
            PSCOL = int((ECOL-SCOL)/2) + SCOL - 5

            ! [101] Create cancel button (but we don't care about its ID)
            xcall AUI,AUI_CONTROL,CTLOP_ADD,0,"Cancel",MBST_ENABLE, &
                MBF_BUTTON+MBF_KBD,"%VK_ESCAPE%","","", &
                EROW-BLANKS-ROFFSET,PSCOL-COFFSET, &
                EROW-BLANKS-ROFFSET,PSCOL+10-COFFSET
        endif
        
        ! progress bar goes above cancel button
        PSROW = EROW - BLANKS - 1
        ! If we have 2 extra rows, put one between bar and Cancel
        if EROW-SROW > 5 then
            PSROW = PSROW - 1
        else
            if EROW-SROW > 2 then 
                BLANKS = 1 
            else 
                BLANKS = 0
            endif
            PSROW = EROW - BLANKS  ! progress bar goes near bottom of box
        endif

        PEROW = PSROW
        PSCOL = SCOL + 2
        PECOL = ECOL - 2

        ! display caption in area above progress bar
        ![102] if (PFLAGS and PFLG_MSGTITLE) = 0 then
        xcall AUI,AUI_CONTROL,CTLOP_ADD,TID,MSG$,MBST_ENABLE, &
            MBF_STATIC,"","","", &
            SROW+BLANKS-ROFFSET,PSCOL-COFFSET, &
            PSROW-1-ROFFSET,PECOL-COFFSET
        ![102] endif           
        
	else		! progress bar occupies entire rectangle
	    PSROW = SROW	
	    PEROW = EROW
	    PSCOL = SCOL
	    PECOL = ECOL
	endif

	! init progress bar (we do need ID back in this case)
    if (PFLAGS and PFLG_MARQUEE) then       ! [103]
        WINSTYLE = PBS_MARQUEE
    else
        WINSTYLE = NUL_WINSTYLE
    endif
    
    xcall AUI,AUI_CONTROL,CTLOP_ADD,ID,MSG$,MBST_ENABLE, &
        MBF_PROGRESS,"","",STATUS, &
        PSROW-ROFFSET,PSCOL-COFFSET,PEROW-ROFFSET,PECOL-COFFSET, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "this is a test tooltip",NUL_PARENTID,NUL_WINCLASS$,WINSTYLE    ! [103]

    ! fall thru
!------------------------------------------------------------------------
! Update the status bar (GUI version)
! Inputs:
!	ID	Status bar control ID
!	CPOS	Current position
!	EPOS	Ending position
!	PFLAGS	
! Output:
!	STS = -1 if an ESC is ready to be input (and FLAG 2 bit set)
!------------------------------------------------------------------------
GUI'UPDATE:
	STS = 0
        
    ! if PFLG_UPDMSG flag set, update message 
    if (PFLAGS and PFLG_UPDMSG) then
       if ((PFLAGS and PFLG_MSGTITLE) = 0) then

            ! we need to get the text control id from common
            xcall COMMON,RECV,MSGNAM,COMMON'PACKET
            if RCVFLG then      ! if we got it, first resave it
                xcall COMMON,SEND,MSGNAM,COMMON'PACKET

               ! update message
               xcall AUI,AUI_CONTROL,CTLOP_CHG,TID,MSG$,MBST_CHANGE
            endif
       endif
    endif
        
    PCT = int(100 * CPOS / EPOS)
    xcall AUI,AUI_CONTROL,CTLOP_CHG,ID,str(PCT),MBST_CHANGE !,MBF_PROGRESS ! ,"","",STATUS

	if (PFLAGS and PFLG_CANCEL) then
	    xcall TINKEY,A$
	    if A$=CANCEL'KBD then STS = -1
	endif
	return

!------------------------------------------------------------------------
! Delete the status bar (GUI version)
! Inputs:
!	ID	Status bar control ID
!------------------------------------------------------------------------
GUI'DELETE:
        ! delete the progress bar (by its ID #)
        ! xcall MIAMEX,MX'WINBTN,3,ID,"",0,MBF_PROGRESS, &	
        !	"","",STATUS,0,0,0,0
	! writecd tab(-10,20);3,ID,"",0,MBF_PROGRESS,"","", &
	!	0,0,0,0;chr(127);
        
	if (PFLAGS and PFLG_DLG) then
        ! delete the box (deletes the controls too)
        ! [101] Use a trick to delete the current active dialog
        ! [101] without knowing its ID...
        xcall AUI,AUI_CONTROL,CTLOP_DEL,0,"",0,MBF_DIALOG
    
    else    ! [101] if no dialog, then just delete the PROGRESS bar
        xcall AUI,AUI_CONTROL,CTLOP_DEL,ID
    endif	

	return
!------------------------------------------------------------------------
! Create the status bar and associated "dialog" box  (TEXT version)
! Inputs: OP (if not 1, then go through motions of computing PSxxxx
!         coordinates but don't display anything (needed by TXT'UPDATE 
!         since it can't keep track of them) 
! Outputs: PSROW,PSCOL,PEROW,PECOL
!------------------------------------------------------------------------
TXT'INIT:
	if (PFLAGS and PFLG_DLG) then		! dialog box

        ! use MSBOXX to display outer "dialog" box

        if OP=1 then 
            xcall MSBOXX,SROW,SCOL,EROW,ECOL,BOX_BDR+BOX_ERA+BOX_SVA
        endif
        
        ! In the text version of the dialog, we don't need a title
        ! put a cancel button on bottom row -1
        if (PFLAGS and PFLG_CANCEL) then

            ! Decide if we have room for blank lines
            if EROW-SROW > 3 then 
                BLANKS = 1 
            else 
                BLANKS = 0
            endif
            
            PSCOL = int((ECOL-SCOL)/2) + SCOL - 7

            if OP = 1 then
                ! Create pseudo cancel "button"
                print tab(EROW-BLANKS,PSCOL);
                print "[";tab(-1,32);"CANCEL(ESC)";tab(-1,33);"]";
            endif

            ! progress bar goes above cancel button
            PSROW = EROW - BLANKS - 1
            ! If we have 2 extra rows, put one between bar and Cancel
            if EROW-SROW > 5 then
                PSROW = PSROW - 1
            endif
            
        else
            if EROW-SROW > 2 then 
                BLANKS = 1 
            else 
                BLANKS = 0
            endif
            
            PSROW = EROW - BLANKS  ! progress bar goes near bottom of box
        endif

	    PEROW = PSROW
	    PSCOL = SCOL + 2
 	    PECOL = ECOL - 2

        if OP = 1 then
        ! display caption in area above progress bar
            ![102] CENTER$ = MSG$
            ![102] CSIZE = PECOL-PSCOL+1
            CENTER$ = Fn'Center$(MSG$,PECOL-PSCOL+1)    ! [102]
            MSROW = SROW + BLANKS
            MSCOL = PSCOL
            MECOL = PECOL            
            print tab(MSROW,MSCOL);CENTER$[1,MECOL-MSCOL+1];
        endif

	else		! progress bar 1st or 2nd row of box
        PSROW = SROW	
        PEROW = EROW
        PSCOL = SCOL
        PECOL = ECOL
        if SROW < EROW then     ! text version only 1 row high
            PSROW = PSROW + 1
            PEROW = PSROW
        endif
	endif

    if OP = 1 then
        ! Init the progress bar as [                   ]
        print tab(PSROW,PSCOL);"[";
        print tab(PSROW,PECOL);"]";
    endif

    return

!------------------------------------------------------------------------
! Update the status bar (TXT version)
! Inputs:
!	CPOS	Current position
!	EPOS	Ending position
!	PFLAGS	
! Output:
!	STS = -1 if CANCEL (ESCAPE) hit
!------------------------------------------------------------------------
TXT'UPDATE:
	STS = 0

    ! we need to get the position info from common
    xcall COMMON,RECV,MSGNAM,COMMON'PACKET
    if RCVFLG then      ! if we got it, first resave it
        xcall COMMON,SEND,MSGNAM,COMMON'PACKET
    else
        call TXT'INIT  ! recalculate info from passed params  
    endif
    
    ! if PFLG_UPDMSG flag set, update message 
    if (PFLAGS and PFLG_UPDMSG) then
        if ((PFLAGS and PFLG_MSGTITLE) = 0) then

            ! redisplay caption in area above progress bar
            ![102] CENTER$ = MSG$
            ![102] CSIZE = MECOL-MSCOL+1
            CENTER$ = Fn'Center$(MSG$,MECOL-MSCOL+1) ! [102]
            print tab(MSROW,MSCOL);CENTER$[1,MECOL-MSCOL+1];
        endif
    endif

    PCT = int(100 * CPOS / EPOS)

    ! convert PCT to a number of X's
    PCT = int((PECOL - PSCOL - 1) * PCT / 100)

    print tab(PSROW,PSCOL+1);
    print tab(-1,23);       ! alt char set 
    for I = 1 to PCT
        print tab(-1,49);   ! solid block 
    next I
    print tab(-1,24);       ! back to normal 

    if (PFLAGS and PFLG_CANCEL) then
        xcall TINKEY,A$
        if A$=CANCEL'KBD then STS = -1
    endif
    return

!------------------------------------------------------------------------
! Delete the status bar (TXT version)
! Inputs: SROW,SCOL,EROW,ECOL
!	
!------------------------------------------------------------------------
TXT'DELETE:

    if (PFLAGS and PFLG_DLG) then
        ! delete the box and restore the area
        xcall MSBOXX,SROW,SCOL,EROW,ECOL,BOX_BDR+BOX_ERA+BOX_RSA
    else    ! if no dialog, then just clear the PROGRESS bar
        print tab(PSROW,PSCOL);space(PECOL-PSCOL+1);
    endif

	return
!------------------------------------------------------------------------
! Center a string 
! Input: CENTER$, CSIZE
! Output: CENTER$
!-----------------------------------------------------------------------
!>!CENTER:
!>!    xcall TRIM,CENTER$,1
!>!    I = len(CENTER$)
!>!    I = (CSIZE - I) / 2
!>!    if I > 1 then 
!>!        CENTER$ = SPACE(I) + CENTER$ + SPACE(I)
!>!    else 
!>!        CENTER$ = CENTER$[1,CSIZE]
!>!    endif
!>!    return
    
![102] 
!---------------------------------------------------------------------
!Function:
!   center a string within a specified width
!Params:
!   msg$  (str) [in] - string to center
!   width (num) [in] - width of background space
!Returns:
!   string with spaces added to either side
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'Center$(msg$ as s200:inputonly, width as b2:inputonly) as s200
    map1 n,i,2
    xcall TRIM,msg$,1
    n = len(msg$)
    n = (width - n) / 2
    if n > 1 then 
        Fn'Center$ = space(n) + msg$ + space(n)
    else 
        Fn'Center$ = msg$[1,width]
    endif

EndFunction    
