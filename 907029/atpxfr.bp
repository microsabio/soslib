program ATPXFR,1.0(104)     ! print filter to pre-send //IMAGE & //XTEXT files
!
!A-Shell/Server print filter to pre-scan the file for //IMAGE and //XTEXT
!(and //METAFILE) auxiliary file references that need to be pre-transferred
!from the server to the ATE client before the file can be printed successfully.
!Typically this would be used with DEVICE=AUXLOC: (since the AUXLOC: driver has
!no understanding of the file contents.)
!
!NOTES:
! To minimize dependencies, we use the same AUXLOC channel to transfer
! the files (via ATEAPX.SBX) to the %ATECACHE% directory.   We maintain
! a list of up to 10 filespecs being transferred, to avoid repetitively
! transferring the same files over and over again.  (We also check
! to see if the file is already in the ATECACHE directory, but only for
! files larger than 20K
!
! WARNING: we assume here that the all unique files have unique name.ext
! (because the path or PPN info is lost and we put everything in ATECACHE)
!
! To invoke the routine, add the following to your printer INI:
!
!	COMMAND=SBX:ATPXFR{,FLAGS}
!
! Possible FLAGS values (see APXF_xxx below)
!
! This routine is actuall called (by the spooler) with the following parameters:
!
!     XCALL ATPXFR,STS,FSPEC,PRINTER,SWITCHES,COPIES,FORM{,FLAGS}
!
! STS is mapped B,1 and must returned with a positive number if
! you want the print operation to proceed.  Else set it to 0 to
! cancel the print operation, or a negative number to signify an error.
! (It gets passed in as -1, so you don't need to set it to abort.)
!
! The remaining parameters are all string format.  You can convert
! SWITCHES and COPIES to numeric variables, but if you want to change
! them, convert them back to strings before passing the parameters back.
! You may change any of these parameters in this routine, and the
! changes will take effect before the print operation proceeds.
!
! NOTE: You may not XCALL SPOOL from within this routine!!!  (XCALL ATEPRT
! could be considered dangerous, but we're counting on it to invoke the
! spool operation on the ATE side, rather than calling SPOOL again on
! the server side).
!
! NOTE: The incoming FSPEC will be in native format; if you want to
! modify it, you can pass back an updated FSPEC in either native or
! AMOS format.
!
! Note that the XCALL.BSI module contains both map statements and
! Basic code that picks up the  parameter block address and param
! structure.  It doesn't matter if it gets executed before or after
! other BSI's, as long as it gets executed before the BEGIN: label.
!
! Remember to rename the .RUN to .SBX after compiling!!!
!------------------------------------------------------------------------
!EDIT HISTORY
![100] May 3, 2008 01:15 PM         Edited by Jack
!	Created
![101] May 5, 2008 09:38 AM         Edited by Jack
!	Add optional flags param to turn off post-verify in ATEAPX and
!   stifle tracing of transfer errors here, enable pre-verify (and
!   move pre-verify logic to ATEAPX).
![102] January 28, 2011 11:00 AM         Edited by Jack
!   Add support for //METAFILE;
![103] February 4, 2011 10:45 AM         Edited by Jack
!   Support filespecs in "dev#:[p,pn]file.exe" format; fix failure
!   to close file (prevented spool DELETE switch from working) /jdm
![104] November 06, 2012 07:34 AM       Edited by jack
!   Fix bug: attempting to transfer PC files to PC 
!------------------------------------------------------------------------

++pragma SBX                        ! compile to .SBX
++pragma ERROR_IF_NOT_MAPPED "ON"   ! force /M

++include ashinc:ashell.def
++include sosfunc:fnameext.bsi
++include sosfunc:fnextch.bsi
++include ashinc:XCALL.BSI		! picks up param list

![101] flags (shared by ATEAPX but some are set by default  here)
define APXF_BINARY       = &h00 ! binary (MIME) transfer (default)
define APXF_ASCII        = &h02 ! ascii
define APXF_NULL         = &h04 ! add to use "NULL" printer (default)
define APXF_FTP          = &h08 ! use FTP
define APXF_NOPOSTVERIFY = &h10 ! don't post-verify
define APXF_PREVERIFY    = &h20 ! pre-verify (default)
define APXF_NOERRTRACE   = &h40 ! [101] don't trace transfer errors (ATPXFR only)

map1 PARAMS
    map2 FSPEC,S,260
    map2 PRINTER,S,8
    map2 SWITCHES,S,15
    map2 COPIES,S,6
    map2 FORM,S,8
    map2 STS,B,1

map1 MISC
    map2 STATUS,F
    map2 X,F
    map2 X1,F
    map2 GUIFLAGS,B,4
    map2 PCFILE,S,80
    map2 FILSIZ,F
    map2 CH,B,2
    map2 PLINE,S,4096
    map2 ASH'VER$,S,30
    map2 VMAJOR,B,1
    map2 VMINOR,B,1
    map2 VEDIT,B,2
    map2 VPATCH,B,1
    map2 ATE'VMAJOR,B,1
    map2 ATE'VMINOR,B,1
    map2 ATE'VEDIT,B,2
    map2 ATE'VPATCH,B,1
    map2 FBYTES,F
    map2 MTIME,B,4
    map2 CTIME,B,4
    map2 FMODE,B,2
    map2 VERSTR,S,30            ! #####.#####.#####.#####.#####
    map2 XFRSPEC,S,160
    map2 COMMA,F
    map2 FLAGS,B,4              ! [101]

BEGIN:
    on error goto TRAP

    if XCBCNT < 6 then
    	? "Insufficient parameters (";XCBCNT;") passed to ATPXFR"
    	End   ! return to spool
    endif

    xgetarg 2,FSPEC
    DEBUG.PRINT "ATPXFR, FSPEC ="+FSPEC     ! [103]

    if XCBCNT > 6 then
        xgetarg 7, FLAGS
    endif

    ! make sure file exists
    xcall SIZE,FSPEC,FILSIZ
    if FILSIZ <= 0 then
        ? "Error in ATPXFR.SBX - print file (";FSPEC;") does not exist"
        sleep 5
        goto BACK'TO'SPOOLER
    endif


    ! otherwise, we'll handle the AUX port printing here

    ! make sure we are running ATE
    xcall AUI, AUI_ENVIRONMENT, 2, GUIFLAGS
    STS = 1         ! proceed with print no matter what happens here
    if (GUIFLAGS and AGF_ATE) = 0 then
        ? "ATPXFR printer filter requires ATE - reverting to normal AUXLOC: "
    else
        ! get ATE version
        ! xcall MIAMEX,MX_GETVER,ASH'VER$,VMAJOR,VMINOR,VEDIT,VPATCH, &
        !    ATE'VMAJOR,ATE'VMINOR,ATE'VEDIT,ATE'VPATCH

        ! scan the print file for //IMAGE and //METAFILE and //XTEXT commands
        CH = Fn'NextCh(65000)
        open #CH, FSPEC, input
        do while eof(CH) # 1
          input line #CH, PLINE
          if PLINE[1,2]="//" then
            DEBUG.PRINT PLINE
            if ucs(PLINE[1,8]) = "//IMAGE," then
                ! syntax: //IMAGE,fspec,left,top,right,bottom{,flags,border,spec2}
                X = instr(10,PLINE,"]")
                X = X max 10
                X = instr(X,PLINE,",")
                if X > 0 then
                    XFRSPEC = PLINE[9,X-1]
                    xcall TRIM, XFRSPEC

                    ![104] if fspec contains "\", assume visible to PC
                    ![104] (and skip transfer)
                    if instr(1,XFRSPEC,"\") = 0 then    ![104]
                        DEBUG.PRINT " Preprocessing IMAGE spec "+XFRSPEC
                        STATUS = Fn'Xfer(XFRSPEC,"%ATECACHE%",APXF_NULL or APXF_PREVERIFY or FLAGS)
                        if STATUS < 0 and ((FLAGS and APXF_NOERRTRACE) = 0) then  ![101]
                            TRACE.PRINT "Error transferring "+XFRSPEC+": "+STATUS
                        endif
                    endif
                endif

            elseif ucs(PLINE[1,11]) = "//METAFILE," then     ! [102]
                ! syntax: //METAFILE,fspec,left,top,right,bottom
                X = instr(13,PLINE,"]")
                X = X max 13
                X = instr(X,PLINE,",")
                if X > 0 then
                    XFRSPEC = PLINE[12,X-1]
                    xcall TRIM, XFRSPEC

                    ![104] if fspec contains "\", assume visible to PC
                    ![104] (and skip transfer)
                    if instr(1,XFRSPEC,"\") = 0 then    ![104]
                        DEBUG.PRINT " Preprocessing METAFILE spec "+XFRSPEC
                        STATUS = Fn'Xfer(XFRSPEC,"%ATECACHE%",APXF_NULL or APXF_PREVERIFY or FLAGS)
                        if STATUS < 0 and ((FLAGS and APXF_NOERRTRACE) = 0) then
                            TRACE.PRINT "Error transferring "+XFRSPEC+": "+STATUS
                        endif
                    endif
                endif

            elseif ucs(PLINE[1,8]) = "//XTEXT," then
                ! syntax: //XTEXT,left,top,right,bottom,fspec{,offset}
                X1 = 8
                for COMMA = 1 to 4  ! skip left,top,right,bottom,
                    X1 = instr(X1+1,PLINE,",")
                    if X1 < 1 exit
                next COMMA
                if (X1 > 0) then
                    X1 = X1 + 1
                    X = instr(X1,PLINE,"]")
                    X = X max X1
                    X = instr(X,PLINE,",")     ! x -> , after spec (or 0)

                    XFRSPEC = PLINE[X1,X-1]
                    xcall TRIM, XFRSPEC

                    ![104] if fspec contains "\", assume visible to PC
                    ![104] (and skip transfer)
                    if instr(1,XFRSPEC,"\") = 0 then    ![104]
                        DEBUG.PRINT " Preprocessing XTEXT spec "+XFRSPEC
                        STATUS = Fn'Xfer(XFRSPEC,"%ATECACHE%",APXF_NULL or APXF_PREVERIFY or FLAGS)  ! [101]
                        if STATUS < 0 and ((FLAGS and APXF_NOERRTRACE) = 0) then  ![101]
                            TRACE.PRINT "Error transferring "+XFRSPEC+": "+STATUS
                        endif
                    endif
                endif
            endif
          endif
        loop
    endif

BACK'TO'SPOOLER:
    if CH close #CH         ! [103] close file if open
    xputarg 1,STS           ! return STS
    ? tab(-10,AG_RELEASEKBD);chr(127);  ! (in all cases)

End

!-------------------------------------------------------------------------------------
! Function :    Fn'Xfer()
!   Wrapper for ATEAPX.SBX to transfer a file, unless we know we've already xferred it
! Params:
!   xfrspec$ [s, in]    host filespec
!   pcdir$ [s, in]      pc dir
!   flags [b, in]       see APXF_xxxx
! Returns:
!   status >0 = transferred
!           0 = didn't need to be transferred
!          <0 = errors (see ATEAPX.SBX)
! Notes:
!
!-------------------------------------------------------------------------------------

FUNCTION Fn'Xfer(xfrspec$ as s260, pcdir$ as s160, flags as b2) as i2

    define MAX_LOCAL_CACHE = 10
    map1 statics
        STATIC map1 LCACHE(MAX_LOCAL_CACHE),S,50       ! list of xferred files to avoid re-xmit
        STATIC map1 MAX'LCX,I,2                        ! current # of files in the list
        STATIC map1 LCX,I,2                            ! idx into aray

    map1 locals
        map2 pcfile$,s,100
        map2 svrbytes,f
        map2 pcbytes,f
        map2 mtime,b,4
        map2 ctime,b,4
        map2 mode,b,2
        map2 ver$,s,12
        map2 pchash$,s,16
        map2 svrhash$,s,16
        map2 pcpath$,s,260

    if xfrspec$[1,1] = """"  then   ! strip quotes if quoted
        xfrspec$ = xfrspec$[2,-2]
    endif

    pcfile$ = Fn'Name'Ext$(xfrspec$)

    ! check if file has already been transferred
    DEBUG.PRINT "$T [ATPXFR] Checking local xfer cache for "+pcfile$
    for LCX = 1 to MAX'LCX
        DEBUG.PRINT "$T [ATPXFR] Cache check: #"+LCX+", "+LCACHE(LCX)
        if LCACHE(LCX) = pcfile$ then
            DEBUG.PRINT "$T [ATPXFR] Match! (File already transferred)"
            EXITFUNCTION    ! file already xferred
        endif
    next LCX

    ! add the file to our local cache if there is room
    if MAX'LCX < MAX_LOCAL_CACHE then
        MAX'LCX = MAX'LCX + 1
        LCACHE(MAX'LCX) = pcfile$
    endif

++ifdef NEVER       ! [101] move pre-verify logic to ATEAPX
    ! if larger than 20K, see if it already exists on PC
    xcall SIZE,xfrspec$,svrbytes
    if svrbytes > 20000 then
        if pcdir$ # "" then
            pcpath$ = pcdir$ + "\" + pcfile$
        else
            pcpath$ = ".\" + pcfile$
        endif
        xcall MIAMEX, MX_FILESTATS, "R", pcpath$, pcbytes, mtime, ctime, mode, ver$, pchash$
        if pcbytes = svrbytes then
            if pchash$ # "" then    ! if hash supported, check if they match
                xcall MIAMEX, MX_FILESTATS, "L", xfrspec$, svrbytes, mtime, ctime, mode, ver$, svrhash$
            endif
            if svrhash$ = "" or svrhash$ = pchash$ then
                DEBUG.PRINT "Skipping xfer, file already current on PC - hash: "+svrhash$+" vs "+pchash$
                EXITFUNCTION
            endif
        endif
    endif
++endif         ! [101]

    DEBUG.PRINT "$T [ATEAPX] Calling ATEAPX to transfer file..."
    xcall ATEAPX,xfrspec$,pcdir$,pcfile$,flags,Fn'Xfer
End FUNCTION


!----------------------------------------------------------------------
TRAP:
!(without this trap, Basic errors would return immediately back to
! caller making it difficult to see what happened.)

	! only ^c trap takes causes immediate return to Basic
	if err(0)=1 then
            ? "File transfer of ";XFRSPEC;" failed; proceeding with print anyway"
            resume BACK'TO'SPOOLER
	endif

	? tab(24,1);tab(-1,9);"Basic error #";ERR(0);" Last file: ";ERR(2);
	if err(0)=17 then
	    xcall FILNAM,err(2),FSPEC
	    ? "(";FSPEC;")";	! display filename if file-not-found
	endif
	? tab(-1,254);

	xcall SLEEP,10

    resume BACK'TO'SPOOLER

