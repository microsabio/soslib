program PSUFFX,1.0(100)
!
!Simple A-Shell/Windows spooler SBX to rename the print file by adding
!a -# suffix to it
!
!NOTES:
! To invoke the routine, add the following to your printer INI:
!
!	COMMAND=SBX:PSUFFX
!
! This routine is called with the following parameters:
!
!	XCALL PSUFFX,STS,FSPEC,PRINTER,SWITCHES,COPIES,FORM,COLUMNS
!
! STS is mapped B,1 and must returned with a positive number if
! you want the print operation to proceed.  Else set it to 0 to 
! cancel the print operation, or a negative number to signify an error.
! (It gets passed in as -1, so you don't need to set it to abort.)
!
! The remaining parameters are all string format.  You can convert
! SWITCHES and COPIES to numeric variables, but if you want to change
! them, convert them back to strings before passing the parameters back.
! You may change any of these parameters in this routine, and the 
! changes will take effect before the print operation proceeds.
!
!
! NOTE: You may not XCALL SPOOL from within this routine!!!
!
! NOTE: The incoming FSPEC will be in native format; if you want to 
! modify it, you can pass back an updated FSPEC in either native or
! AMOS format.
!
! Note that the XCALL.BSI module contains both map statements and
! Basic code that picks up the  parameter block address and param 
! structure.  It doesn't matter if it gets executed before or after
! other BSI's, as long as it gets executed before the BEGIN: label.
!
! Remember to rename the .RUN to .SBX after compiling!!!
!------------------------------------------------------------------------
!EDIT HISTORY
![100] July 13, 2010 11:33 AM         Edited by Jack
!	Created as a quick workaround for ATE bug when temp dir contains -
!------------------------------------------------------------------------
++pragma SBX                        ! compile to .SBX
++pragma ERROR_IF_NOT_MAPPED "ON"   ! force /M

MAP1 SUBJECT,S,256
MAP1 INTRO,S,512
MAP1 FLAGS,F
MAP1 STATUS,F
MAP1 FSPEC,S,128
MAP1 PRINTER,S,8
MAP1 SWITCHES,S,15
MAP1 COPIES,S,6
MAP1 FORM,S,8
MAP1 STS,B,1
MAP1 X,F
MAP1 Y,F
MAP1 COLUMNS,F

MAP1 ERRNO(31),F
MAP1 ERMSG(31),S,40
MAP1 FSPEC2,S,128
MAP1 PLINE,S,512
MAP1 DIRSEP$,S,1


++include ashinc:XCALL.BSI		! picks up param list
++include ashinc:ASHELL.DEF

BEGIN:
	on error goto TRAP

	if XCBCNT < 6 then 
		? "Insufficient parameters (";XCBCNT;") passed to PSUFFX" 
		end   ! return to spool
	endif

	! pick up the individual params of interest
        xgetarg 2,FSPEC
!       xgetarg 7,COLUMNS

        X = instr(1,FSPEC,".")
        if X > 0 then
            FSPEC2 = FSPEC[1,X-1] + "-1" + FSPEC[X,-1]
        else
            FSPEC2 = FSPEC + "-1"
        endif

        xcall MIAMEX, MX_COPYFILE, FSPEC, FSPEC2, CPYF_REPL, STATUS
        if STATUS then
            ? "Unable to copy ";FSPEC;" to ";FSPEC2;" error: ";STATUS
        else
            xputarg 2,FSPEC2        ! change the filespec
        endif
    
	STS = 1		        ! we DO want to print after this
        xputarg 1,STS           ! return STS

end
 
!----------------------------------------------------------------------
DISPLAY'ERROR:
	? "Shell Execute Error! Status = ";str(STATUS);" Fspec = ";FSPEC2
	if STATUS = -1 then &
		? "Out of system resources" &
	else if STATUS = 2 then &
		? "File not found" &
	else if STATUS = 3 then &
		? "Path not found" &
	else if STATUS = 5 then &
		? "Access denied" &
	else if STATUS = 26 then &
		? "Sharing error" &
	else if STATUS = 27 or STATUS = 31 then &
		? "File association incomplete or undefined" &
	else if STATUS = 32 then &
		? "DLL not found" &
	else ? "Unknown error"

	xcall SLEEP,5
	return

!----------------------------------------------------------------------
TRAP:
!(without this trap, Basic errors would return immediately back to
! caller making it difficult to see what happened.)

	! only ^c trap takes causes immediate return to Basic
	if err(0)=1 then 

		END	
	endif

	? tab(24,1);tab(-1,9);"Basic error #";ERR(0);" Last file: ";ERR(2);
	if err(0)=17 then
	    xcall FILNAM,err(2),FSPEC
	    ? "(";FSPEC;")";	! display filename if file-not-found
	endif
	? tab(-1,254);
	xcall SLEEP,10

	END				! back to Basic

 
