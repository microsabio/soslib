:<(KBDDLG) Floating Customizable Keyboard Dialog
   KBDDLG.BP/SBX - Create modeless keyboard dialog
   TSTKBDDLG.BP - Test for KBDDLG.SBX
   TACDLG.BP/SBX - Popup dialog to input one field with tactile support
   TSTTACDLG.BP - Test for TACDLG.SBX
   BTNMATRIX.BP - SBX to display matrix of buttons
   TSTBTNMTX.BP - Test for BTNMATRIX.SBX
   TSTBTNMTX2.BP -Variation with static text panels for buttons

>
