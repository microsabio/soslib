!============================================================
! Example RSS Created
!============================================================
! This file is then FTP/uploaded to a HTTP Web Server where
! a RSS reader can access it.
!
?TAB(-1,0);

MAP1 PARMS
 MAP2 OPCODE,F,6,1
 MAP2 RSS'FILE,S,510,"TEST.XML"
 MAP2 X'TITLE,S,255,"New Customer ABCD01"
 MAP2 X'LINK,S,510,"http://100.100.100.187/newcust.html"
 MAP2 X'DESCR,S,510 ,"News customer ABCD01 created"
 MAP2 RTNCDE,F,6
 MAP2 RSS'TITLE,S,255,"EXAMPLE RSS"
 MAP2 RSS'LINK,S,510,"http://100.100.100.187/"
 MAP2 RSS'DESCR,S,510,"This is an example RSS file from Ashell"

!XCALL DELMEM,"*.SBX"

?TAB(-1,0);
XCALL RSSXML,OPCODE,RSS'FILE,X'TITLE,X'LINK,X'DESCR,RTNCDE,&
	     RSS'TITLE,RSS'LINK,RSS'DESCR
PRINT RTNCDE

RSS'FILE="TEST.XML"
X'TITLE="New order 056432"
X'LINK="http://100.100.100.187/neworder.html"
X'DESCR="News order has been aded"
XCALL RSSXML,2,RSS'FILE,X'TITLE,X'LINK,X'DESCR,RTNCDE
PRINT RTNCDE

RSS'FILE="TEST.XML"
X'TITLE="New order 023321"
X'LINK="http://100.100.100.187/neworder.html"
X'DESCR="News order has been aded"
XCALL RSSXML,2,RSS'FILE,X'TITLE,X'LINK,X'DESCR,RTNCDE
PRINT RTNCDE

PRINT RSS'FILE
