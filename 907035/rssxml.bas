!========================================================================
! RSSXML - {Create a RSS Feed}
!========================================================================
! 
!Usage:
!	XCALL RSSXML,opcode,file,title,link,descr,RTNCDE,&
!                                {RSS title},{RSS link},{RSS descr}
!
!	opcode:	1: News RSS XML File
!	        2: Update existing RSS XML File
!
!	file:	XML RSS File.
!	RTNCDE	Zero or less failed.
!		1=Added                                            
!		2=Appended
!
!	The first set of title,link,descr is for the RSS feed, the 2nd
!	are for the RSS file title itself and not needed if your appending
!	to an existing RSS file.
!
!========================================================================
!	title:	 The name of the channel. It's how people refer to your service.
!		 If you have an HTML website that contains the same information 
!		 as your RSS file, the title of your channel should be the same 
!		 as the title of your website.  
!
!	link:	 	The URL to the HTML website corresponding to the channel. 
!	description:	Phrase or sentence describing the channel. 
!
!Full RSS 2.0 Specification
!http://blogs.law.harvard.edu/tech/rss
!
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps Zone}
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARMS
 MAP2 OPCODE,F,6
 MAP2 RSS'FILE,S,510
 MAP2 X'TITLE,S,255
 MAP2 X'LINK,S,510
 MAP2 X'DESCR,S,510
 MAP2 RTNCDE,F,6
 MAP2 RSS'TITLE,S,255
 MAP2 RSS'LINK,S,510
 MAP2 RSS'DESCR,S,510

MAP1 MISC
 MAP2 RSS'CHAN,F,6,63013
 MAP2 CHANX,F,6,63014
 MAP2 TEMP'FILE,S,25,"RSSXML.SEQ"
 MAP2 LINE'IN,S,510
 MAP2 XXX,F,6
 MAP2 DOES'IT'EXIST,F,6

MAP1 MX_COPYFILE,B,1,27

MAP1 TIME'DATE
 MAP2 NOW'YYMMDD,S,6
 MAP2 NOW'HHMM,S,5
 MAP2 NOW'DDMMYY,S,10
 MAP2 MONTH'DESCR(12),S,3
 MAP2 FORMAT'DATE,S,40

 	MONTH'DESCR(1)="Jan"
 	MONTH'DESCR(2)="Feb"
 	MONTH'DESCR(3)="Mar"
 	MONTH'DESCR(4)="Apr"
 	MONTH'DESCR(5)="May"
 	MONTH'DESCR(6)="Jun"
 	MONTH'DESCR(7)="Jul"
 	MONTH'DESCR(8)="Aug"
 	MONTH'DESCR(9)="Sep"
 	MONTH'DESCR(10)="Oct"
 	MONTH'DESCR(11)="Nov"
 	MONTH'DESCR(12)="Dec"
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
100	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
200	XCALL GETPRG,PRGNAM,SBX'NAME
250    	XGETARGS OPCODE,RSS'FILE,X'TITLE,X'LINK,X'DESCR,RTNCDE,&
      	         RSS'TITLE,RSS'LINK,RSS'DESCR

280 	RTNCDE=0
	LOOKUP RSS'FILE,DOES'IT'EXIST
	IF OPCODE=1 OR DOES'IT'EXIST=0 THEN CALL NEW'RSS
	IF OPCODE=2 CALL APPEND'RSS

	XPUTARG 6,RTNCDE
	END
	
!
!==============================================================================
!{Create or append}
!==============================================================================
NEW'RSS:
300 	CALL OPEN'RSS'FILE
320 	CALL RSS'HEADER
	CALL ADD'RSS'HEADER
	CALL ADD'RSS'ITEM
	CALL RSS'FOOTER
340 	CALL CLOSE'RSS'FILE
	RTNCDE=1
	RETURN
!
!------------------------------------------------
! Append to existing.
!------------------------------------------------
APPEND'RSS:
360 	CALL COPY'EXISTING'RSS'FILE
380 	CALL OPEN'RSS'FILE
400 	CALL OPEN'TEMP'FILE
  	CALL RETRIEVE'CURRENT	
420 	CALL CLOSE'TEMP'FILE
	CALL ADD'RSS'ITEM
	CALL RSS'FOOTER
440 	CALL CLOSE'RSS'FILE
	RTNCDE=2
	RETURN	
!	
!==============================================================================
!{Retrieve Current RSS feeds}
!==============================================================================
RETRIEVE'CURRENT:
460 	INPUT LINE #CHANX,LINE'IN
480 	IF EOF(CHANX) THEN GOTO RTN'RETRIEVE'CURRENT
	XXX=INSTR(1,LINE'IN,"</channel>")	! look for the end.
	IF XXX<>0 THEN GOTO RTN'RETRIEVE'CURRENT
500 	PRINT #RSS'CHAN,LINE'IN
	GOTO RETRIEVE'CURRENT
RTN'RETRIEVE'CURRENT:
520 	RETURN
!	
!==============================================================================
!{Copy File}
!==============================================================================
COPY'EXISTING'RSS'FILE:
540  	OPEN #RSS'CHAN,RSS'FILE,INPUT
560  	OPEN #CHANX,TEMP'FILE,OUTPUT
580  	xcall MIAMEX, MX_COPYFILE, RSS'CHAN,CHANX
600  	CLOSE #RSS'CHAN
620  	CLOSE #CHANX
	RETURN
!	
!==============================================================================
!{Open/Close Temp File}
!==============================================================================
OPEN'TEMP'FILE:
640  	OPEN #CHANX,TEMP'FILE,INPUT
660  	RETURN

CLOSE'TEMP'FILE:
680  	CLOSE #CHANX
700  	RETURN		
!	
!------------------------------------------------
!Open/Close RSS File
!------------------------------------------------
OPEN'RSS'FILE:
720  	OPEN #RSS'CHAN,RSS'FILE,OUTPUT
740  	RETURN

CLOSE'RSS'FILE:
760  	CLOSE #RSS'CHAN
780  	RETURN	
!
!==============================================================================
!{RSS Header Information}
!==============================================================================
ADD'RSS'HEADER:
800    	PRINT #RSS'CHAN,"    <title>"+RSS'TITLE+"</title>"
820    	PRINT #RSS'CHAN,"    <link>"+RSS'LINK+"</link>"
840    	PRINT #RSS'CHAN,"    <description>"+RSS'DESCR+"</description>"
860 	RETURN
!
!==============================================================================
!{RSS Item}
!==============================================================================
ADD'RSS'ITEM:
880  	CALL FORMAT'DATE
900    	PRINT #RSS'CHAN,"    <item>"
920    	PRINT #RSS'CHAN,"      <title>"+X'TITLE+"</title>"
940    	PRINT #RSS'CHAN,"      <link>"+X'LINK+"</link>"
960    	PRINT #RSS'CHAN,"      <description>"+X'DESCR+"</description>"
980    	PRINT #RSS'CHAN,"      <pubDate>"+FORMAT'DATE+"</pubDate>"
1000   	PRINT #RSS'CHAN,"      <lastBuildDate>"+FORMAT'DATE+"</lastBuildDate>"
1020   	PRINT #RSS'CHAN,"    </item>"
	RETURN

!------------------------------------------------
!Format Date String.
!------------------------------------------------
FORMAT'DATE:
1040 	XCALL GETNOW,NOW'YYMMDD,NOW'HHMM,NOW'DDMMYY
	FORMAT'DATE=NOW'DDMMYY[1;2]+" "                            ! day
1060 	FORMAT'DATE=FORMAT'DATE+MONTH'DESCR(NOW'YYMMDD[3;2])+" "   ! month
	FORMAT'DATE=FORMAT'DATE+"20"+NOW'YYMMDD[1;2]+" "           ! year
	FORMAT'DATE=FORMAT'DATE+NOW'HHMM+":00 +0000"               ! time
	RETURN
!
!==============================================================================
!{RSS/XML Header/footer}
!==============================================================================
RSS'HEADER:
1080 	PRINT #RSS'CHAN,"<?xml version="+CHR(34)+"1.0"+CHR(34)+"?>"
1100 	PRINT #RSS'CHAN,"<rss version="+CHR(34)+"2.0"+CHR(34)+">"
1120   	PRINT #RSS'CHAN,"  <channel>"
1140  	RETURN

RSS'FOOTER:
1160 	PRINT #RSS'CHAN,"  </channel>"
1180   	PRINT #RSS'CHAN,"</rss>"
1200  	RETURN		
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
1220 	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	RTNCDE=-1
	XPUTARG 6,RTNCDE
	END			! return to caller
