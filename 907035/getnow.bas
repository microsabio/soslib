!========================================================================
! GETNOW - Returns Current Date/Time
!========================================================================
!
!Usage:
!	XCALL GETNOW,{yymmdd},[time formatted},{date formatted}
!
!========================================================================
!
! VERSION HISTORY
! 14/Jul/2005 - 4.2(100) - Steve - Madics 4.2
! 25/Nov/2005 - 4.2(500) - Steve - Madics 4.2a 
!
program GETNOW,4.2(500)

++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! NOTE: MUST COMPILE WITH COMPIL RUNLOG/X:2 
!
++include xcall.bsi
!--------------------------------------------------------------------

MAP1 SYSTEM'DATE,B,4
MAP1 O'DATE,@SYSTEM'DATE
  MAP2 MONTH,B,1
  MAP2 DAY,B,1
  MAP2 YEAR,B,1
MAP1 TODAY,S,6
MAP1 TODAYFMT,S,8
MAP1 TIME'NOW,S,5
MAP1 TM,F,6
MAP1 HH,F,6
MAP1 MM,F,6

	! pick up all args; any not passed will just be set to null
      	XGETARGS TODAY,TIME'NOW,TODAYFMT
	CALL SET''TODAYS''DATE	
	XPUTARG 1,TODAY
	XPUTARG 2,TIME'NOW
	XPUTARG 3,TODAYFMT
	END

TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in GETNOW.SBX!!",2
	END			! return to caller
!
!=========================================================================
! Set Date/Time
!=========================================================================
SET''TODAYS''DATE:
	SYSTEM'DATE=DATE
	IF YEAR>99 THEN YEAR=YEAR-100
	TODAYFMT[1;2]=DAY USING "#Z"
	TODAYFMT[3;1]="/"
	TODAYFMT[4;2]=MONTH USING "#Z"
	TODAYFMT[6;1]="/"
	TODAYFMT[7;2]=YEAR USING "#Z"

	TODAY[5;2]=DAY USING "#Z"
	TODAY[3;2]=MONTH USING "#Z"
	TODAY[1;2]=YEAR USING "#Z"

	TM = TIME	
	HH = INT(TM/3600)
	MM = INT((TM-(HH*3600))/60)
	TIME'NOW=SPACE(5)
	TIME'NOW[3;2]=":"
	TIME'NOW[1;2] = HH USING "#Z" : TIME'NOW[4;2] = MM USING "#Z"
	RETURN
