program ANYCN5,1.0(101)
!A-Shell implementation of ANYCN5.SBR written in Basic (SBX)
!------------------------------------------------------------------------
!USAGE:
! MAP1 CNGCTL,F
! MAP1 WHATNO,F
!
!        XCALL ANYCN5,CNGCTL,WHATNO
!
!On input...
! If CNCGTL = 3, display "Invalid Selection" message and exit
! If CNGCTL = 2, ask "Any Change" but not "What Number? "
! Else, ask "Any Change" and if Y, "What Number?"
!
!On exit...
! CNGCTL = -1 if user hit ESC to Any Change prompt
!           0 if user answered N to Any Change prompt
!            1 if user answered Y or entered a number or func key
!            
! WHATNO = # entered (or negative function key number)
!
!-----------------------------------------------------------------------
!NOTES
! Remember to rename the .RUN to .SBX after compiling!!!
!------------------------------------------------------------------------
!EDIT HISTORY
!VEDIT=101
![100] December 02, 2001 10:31 PM       Edited by jack
!        Created
![101] October 03, 2006 10:09 AM        Edited by joaquin
!        Minor updating
!------------------------------------------------------------------------

++pragma SBX

MAP1 CONFIGURABLE'PARMS
        MAP2 TIME1,F,6,90                ! # secs for "Any Change"
        MAP2 TIME2,F,6,10                ! # secs for What #?
        MAP2 TIME3,F,6,10                ! # secs for CR TO RECOVER

        MAP2 TYPE1,S,10,"AE^]/"          ! codes for "Any Change?"
        MAP2 TYPE2,S,10,"#123]/"         ! codes for What #?
        MAP2 TYPE3,S,10,"@/I"            ! codes for CR TO RECOVER
        
        MAP2 CNGABT,F,6,-1               ! value to set CNGCTL on abort
        MAP2 TIMOUT,F,6,-1               ! value to set CNTCTL on timeout


MAP1 INFLD'PARAMS
        MAP2 ENTRY,S,6
        MAP2 INXCTL,F
        MAP2 EXITCODE,F
        MAP2 CNGCTL,F
        MAP2 WHATNO,F
        MAP2 ROW,B,2
        MAP2 COL,B,2
        MAP2 XMAX,B,2
        MAP2 XMIN,B,2
        MAP2 TYPE,S,20
        MAP2 CMDFLG,F
        MAP2 TIMER,F
        MAP2 SETDEF,S,80
        MAP2 FUNMAP,B,4,-1                ! enable all function keys

MAP1 INFCLR                             ! INFLD colors
        MAP2 DFCLR,B,1,-1                ! display fg
        MAP2 DBCLR,B,1,-1                 ! display bg
        MAP2 EFCLR,B,1,-1                ! editing fg
        MAP2 EBCLR,B,1,-1                ! editing bg
        MAP2 NFCLR,B,1,-1                ! negatives fg
        MAP2 NBCLR,B,1,-1                ! negatives bg
        MAP2 UFCLR,B,1,-1                ! update fg
        MAP2 UBCLR,B,1,-1                ! update bg
        MAP2 MFCLR,B,1,-1                ! messages fg
        MAP2 MBCLR,B,1,-1                ! messages bg
        MAP2 OFCLR,B,1,-1                ! original messages fg
        MAP2 OBCLR,B,1,-1                ! original messages bg
        MAP2 FFCLR,B,1,-1                ! forms fg
        MAP2 FBCLR,B,1,-1                ! forms bg


++include ashinc:XCALL.BSI                ! picks up param list info (XCBCNT)
BEGIN:
        if XCBCNT < 2 then &
                ? "Insufficient parameters (";XCBCNT;") passed to ANYCN5" :&
                stop : &
                xcall MIAMEX,82                ! exit to dot, as opposed to END,
                                        ! which merely returns to caller

        ! receive the CNGCTL param
        xgetarg 1,CNGCTL                                ! [101]

        ! handle invalid selection         
        if CNGCTL > 2 then
                ? tab(24,1);tab(-1,9);tab(-1,12);"Invalid Selection";
                ? tab(24,55);"Hit any key to Continue";
                ? chr(7);
                ENTRY = ""
                ROW = 24 : COL = 78 : XMAX = 1 : XMIN = 0 : TYPE = TYPE3
                TIMER = TIME3
                call INFLD
                ? tab(24,1);tab(-1,9);
                goto RTN'TO'BASIC        
        endif

ANYCN:
        ? tab(24,1);tab(-1,9);tab(-1,11);"Any Change ? ";
        ENTRY = ""
        ROW = 24 : COL = 14 : XMAX = 3 : XMIN = 0 : TYPE = TYPE1
        TIMER = TIME1
        call INFLD
 
        WHATNO = 0
        if EXITCODE = 11 then                ! time out
                CNGCTL = CNGABT
                goto RTN'TO'BASIC
        endif

        if EXITCODE = 1 then                 ! user hit ESC
                CNGCTL = -1
                goto RTN'TO'BASIC
        endif

        if EXITCODE < 0 then                ! function key
                CNGCTL = 1
                WHATNO = EXITCODE
                goto RTN'TO'BASIC
        endif

        if val(ENTRY) > 0 then                ! direct entry of a number
                CNGCTL = 1
                WHATNO = val(ENTRY)
                goto RTN'TO'BASIC
        endif

        if ENTRY # "Y" then 
                CNGCTL = 0 
                goto RTN'TO'BASIC
        endif

        if ENTRY = "Y" and CNGCTL = 2 then
                CNGCTL = 1
                goto RTN'TO'BASIC
        endif


WHATNO:        
        ? tab(24,17);tab(-1,11);"What Number ? ";
        ROW = 24 : COL = 31 : XMAX = 3 : XMIN = 0 : TYPE = TYPE2
        TIMER = TIME2
        call INFLD
        
        if EXITCODE > 0 or val(ENTRY) = 0 then goto ANYCN

        CNGCTL = 1
        WHATNO = val(ENTRY)

        ! fall through...
        
RTN'TO'BASIC:
        ! send params back
        xputarg 1,CNGCTL
        xputarg 2,WHATNO

        END                ! this returns us to calling program

!--------------------------------------------------------------------------
INFLD:
        xcall INFLD,ROW,COL,XMAX,XMIN,TYPE,ENTRY,INXCTL,1,0,EXITCODE, &
                TIMER,CMDFLG,-1,-1,FUNMAP,SETDEF,INFCLR
        return
