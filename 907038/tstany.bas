program TSTANY,1.0(100)     ! simple test for ANYCN5.SBX

MAP1 CNGCTL,F
MAP1 WHATNO,F

	? tab(-1,0);"Text ANYCN5.SBX"
LOOP:	
	? tab(3,1);
	? tab(-1,9)
	? tab(-1,9)
	? tab(3,1);
	input "Input CNGCTL: ",CNGCTL
	? tab(3,15);CNGCTL
	input "Input WHATNO: ",WHATNO
	? tab(4,15);WHATNO

	? tab(5,1);tab(-1,10);
	xcall ANYCN5,CNGCTL,WHATNO

	? tab(10,1);
	? "CNGCTL: ",CNGCTL
	? "WHATNO: ",WHATNO
	?
	GOTO LOOP
