!xtext.sdf[109] 	! XTEXT TXTCTL structure definition
!-------------------------------------------------------------------------
!Variation of XTEXT.MAP - useful when you might end up needing 0 or >1
!independent copies
!-------------------------------------------------------------------------
!EDIT HISTORY
![107] October 5, 2008 04:35 PM       Edited by jacques
!       Created
![108] May 21, 2009 10:55 PM       Edited by jacques
!       Updated to match XTEXT.MAP [107]
![109] May 24, 2011 04:22 PM       Edited by jacques
!       Add MAXROWS
!-------------------------------------------------------------------------

++ifndef INC_XTEXT_SDF

  define INC_XTEXT_SDF = 1

map1 VERSYS_XTEXT_SDF,s,40,">>@VERSYS(1)->>xtext.sdf[109]"  ! [107]


DEFSTRUCT TXTCTL
    map2 CTLNO      ,B,1        !   0 [U] control # 0-3
    map2 KBDSTR     ,S,11       !   1 kbd string
    map2 PARENTID   ,I,2        !  12 parent ID
    map2 WRAPWIDTH  ,B,2        !  14 if set, wrap at this char width
    map2 DOCFMT'SRC ,B,2        !  16 input doc format (TXFF_xxx)
    map2 DOCFMT'DST ,B,2        !  18 output doc format (TXFF_xxx)
    map2 OUTBYTES   ,B,4        !  20 [U] # bytes output
    map2 OUTLINES   ,B,4        !  24 [U] # lines output
    map2 SAVEOFFSET ,B,4        !  28 [U] offset to next save
    map2 OVERFLOW   ,B,1        !  32 [U] output overflowed buffer (truncated)
    map2 MODIFIED   ,B,1        !  33 [U] text modified
    map2 FONTFACE   ,S,32       !  34
    map2 FONTSIZE   ,I,2        !  66 >0 for points, <0 for twips
    map2 FONTATTR   ,B,4        !  68
    map2 XROW       ,B,4	    !  72 [U] cursor row on exit
    map2 XCOL       ,B,2	    !  76 [U] cursor col on exit
    map2 XCUROFFSET ,B,4	    !  78 [U] abs cursor offset (from start)
    map2 SRCHKEY    ,S,30       !  82
    map2 SFLAGS     ,B,2        ! 112     Search flags (see TXSF_xxx)
    map2 CTLID      ,B,2        ! 114 [U] A-Shell control ID #
    map2 DELCTLID   ,B,4        ! 116
    map2 FLAGMASK   ,B,2        ! 120 1's indicate which flags to update
    map2 FLAGS1     ,B,4   	    ! 122 0x0001
    map2 FLAGS2     ,B,4        ! 126 0x0002
    map2 FLAGS3     ,B,4        ! 130 0x0004
    map2 FLAGS4     ,B,4        ! 134 0x0008
    map2 FLAGS5     ,B,4        ! 138 0x0010
    map2 FLAGS6     ,B,4        ! 142 0x0020
    map2 FLAGS7     ,B,4        ! 146 0x0040
    map2 FLAGS8     ,B,4        ! 150 0X0080
    map2 FMAPCTL    ,B,4        ! 154 1's enable FKEY to be sent to ctl
    map2 FMAPAPP    ,B,4        ! 158 1's enable FKEY to be sent to app
    map2 TOOLBARMASK,B,4	    ! 162 1's rmv obj from toolbar (TXTB_xxx)
    map2 FCOLOR     ,B,4	    ! 166 Override text color (&h80bbggrr)
    map2 BCOLOR     ,B,4	    ! 170 Override bg color (&h80bbggrr)
    map2 FPROTCLR   ,B,4	    ! 174 Override prot fg color (&h80bbggrr)
    map2 BPROTCLR   ,B,4	    ! 178 Override prot bg color (&h80bbggrr)
    map2 PROTROWS   ,B,4        ! 182 rows to prot (-1=all,-2=all but last, etc.)
    map2 TIMEOUT    ,B,4        ! 186 timeout (ms)
    map2 IDNAME     ,S,40       ! 190 ctlname or parentname>ctlname
    map2 MAXBYTES   ,B,2        ! 230 max bytes allowed (0=unlimited)
    map2 MENUMASK1  ,B,4        ! 232 menu disabling flags (TXFM1_xxx)
    map2 MENUMASK2  ,B,4        ! 236 more menu disabling flags (TXFM2_xxx)
    map2 HFLAGMASK  ,B,2        ! 240 HTML flag mask
    map2 HFLAGS1    ,B,4        ! 242 HTML flags
    map2 HFLAGS2    ,B,4        ! 246 HTML flags
    map2 MGNLEFT    ,B,2        ! 250 left margin (twips)
    map2 MGNRIGHT   ,B,2        ! 252 right margin (twips)
    map2 MGNTOP     ,B,2        ! 254 top margin (twips)
    map2 MGNBOTTOM  ,B,2        ! 256 margin margin (twips)
    map2 MGNHEADER  ,B,2        ! 258 header margin (twips)
    map2 MGNFOOTER  ,B,2        ! 260 footer margin (twips)
    map2 PAPERSIZE  ,B,2        ! 262 paper size (see DMPAPER_xxx)
    map2 ORIENT     ,B,1        ! 264
    map2 MAXROWS    ,B,1        ! 265 (266 total) [109] was unused
ENDSTRUCT

++else
++message                     Skipping XTEXT.SDF - already included
++endif
