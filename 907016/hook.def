!HOOK.DEF [114] - HOOK definitions (used by MX_FILEHOOK)
!-------------------.-------------------.-------------------.-------------------.
!EDIT HISTORY
! [114] 09-Sep-19 / jdm / Add ST_HOOKMASCB_ENV for xMASCB files
! [113] 18-May-19 / jdm / Add compound HFEX_xxx event flags 
! [112] 11-Mar-19 / jdm / Add HFE_ISAM2_xxx, HFEX_NO_FIELDS
! [111] 12-Sep-18 / jdm / Add subs and bkey to ST_HOOK_FLDCHG
! [110] 30-Aug-18 / jdm / ST_HOOK_FLDCHG
! [109] 15-Aug-18 / jdm / Add HMF_xxx flags for open file modes 
! [108] 04-Aug-18 / jdm / Add ST_HOOK_TIMESTAMP; more HFF_xxx flags;
!                           increase env length from 98 to 100 /jdm
! [107] 25-Nov-16 / jdm / Add chan, lineno, location to hook envelope
! [106] 08-Feb-15 / jdm / Add HFE_ISAM_ADD, HFE_ISAM_DEL
! [105] 09-Jun-11 / jdm / Add HFF_DATA_CHG_ONLY
! [104] 15-Dec-10 / jdm / Add HFF_DATA_WAS_NA
! [103] 13-Apr-10 / jdm / Added conditional compilation to allow nested include
! [102] 22-Jan-10 / jdm / HOOKOP_QRY
! [101] 20-Jan-10 / jdm / misc additions
! [100] 18-Jan-10 / jdm / created 
!-------------------.-------------------.-------------------.-------------------.

++ifndef HFE_PRE_OPEN 

++include'once ashinc:types.def         ! [111]
++include'once ashinc:dynstruct.def     ! [110]

! hook events
define HFE_PRE_OPEN     =  &h00000001  ! prior to open
define HFE_POST_OPEN    =  &h00000002  ! after open
define HFE_PRE_CLOSE    =  &h00000004  ! prior to close
define HFE_POST_CLOSE   =  &h00000008  ! after close
define HFE_PRE_READ     =  &h00000010  ! prior to read or get (any)
define HFE_POST_READ    =  &h00000020  ! after read or get (any)
define HFE_PRE_READL    =  &h00000040  ! prior to readl or getl
define HFE_POST_READL   =  &h00000080  ! after readl or getl
define HFE_PRE_WRITE    =  &h00000100  ! prior to write (or update'record)
define HFE_POST_WRITE   =  &h00000200  ! after write (or update'record)
define HFE_PRE_WRITEL   =  &h00000400  ! prior to writel (or create'record)
define HFE_POST_WRITEL  =  &h00000800  ! after writel (or create'record)
define HFE_PRE_ALLOC    =  &h00002000  ! prior to allocate
define HFE_POST_ALLOC   =  &h00004000  ! after allocate
define HFE_PRE_KILL     =  &h00008000  ! prior to kill [101]
define HFE_POST_KILL    =  &h00010000  ! after kill [101]
define HFE_PRE_SORT     =  &h00020000  ! prior to sort [101]
define HFE_POST_SORT    =  &h00040000  ! after sort [101]
define HFE_ISAM_ADD     =  &h00080000  ! ISAM 5 (add) [106]
define HFE_ISAM_DEL     =  &h00100000  ! ISAM 6 (delete) [106]
define HFE_PRE_ISAMA_DEL=  &h00200000  ! ISAMA pre delete'record [112]
define HFE_POST_ISAMA_DEL= &h00400000  ! ISAMA post delete'record [112]
define HFE_APP_EVENT1   =  &h10000000  ! custom application event

! compound event flags
define HFEX_OPEN        =  &h00000003  ! [113] any open
define HFEX_CLOSE       =  &h0000000C  ! [113] any close
define HFEX_OPEN_CLOSE  =  &h0000000F  ! [113] any open or close
define HFEX_READ        =  &h000000F0  ! [113] any read
define HFEX_WRITE       =  &h00000F00  ! [113] any write
define HFEX_KILL        =  &h00018000  ! [113] any kill
define HFEX_SORT        =  &h00060000  ! [113] any sort 
define HFEX_NO_FIELDS   =  &h1007E00F  ! event flags that don't provide field-level info [112]
                                       ! (entire buffer may be a message, or nothing) [112]

! hook flags
define HFF_PROG          = &h00000001  ! include program
define HFF_SBX           = &h00000002  ! include sbx
define HFF_DATA          = &h00000004  ! include data
define HFF_DATA_WAS      = &h00000008  ! include data (prior to write)
define HFF_RECNO         = &h00000010  ! include recno
define HFF_TIME          = &h00000020  ! include time
define HFF_DATE          = &h00000040  ! include date
define HFF_TIMESTAMP     = &h00000060  ! either TIME or DATE (used in LOGFIL: hooks)
define HFF_USER          = &h00000080  ! include user name
define HFF_PID           = &h00000100  ! include process id
define HFF_CHAN          = &h00000200  ! [107] include file channel
define HFF_LOCATION      = &h00000400  ! [107] include location counter           
define HFF_LINENO        = &h00000800  ! [107] include line #
define HFF_AUTO_DEL      = &h00001000  ! auto delete hook on prog exit
define HFF_DATA_WAS_NA   = &h00010000  ! [104] "was" data not available (set internally)
define HFF_DATA_CHG_ONLY = &h00020000  ! [105] skip post-write hook if no chg
!define HFF_ASYNC         = &h00040000  ! [108] handler is async (no response) (TCP: hooks) 
define HFF_SQZ_ENV       = &h00080000  ! [108] 'squeeze' env rec (LOGFIL: hooklogs)
define HFF_SQZ_DATA      = &h00100000  ! [108] 'squeeze' data recs (LOGFIL: hooklogs)
define HFF_DIF_DATA      = &h00200000  ! [108] use diff fmt for updated recs (LOGFIL: hooklogs)

! MX_FILEHOOK opcodes
define HOOKOP_DISABLE   = 0          ! disable one or all hooks (FSPEC="*")
define HOOKOP_ENABLE    = 1          ! enable one or all hooks (FSPEC="*")
define HOOKOP_ADD       = 2          ! add a hook
define HOOKOP_DEL       = 3          ! delete one or all hooks (FSPEC="*")
define HOOKOP_ADD2      = 4          ! add secondary hook (trigger)
define HOOKOP_DEL2      = 5          ! del secondary hook (trigger)
define HOOKOP_APPEVENT  = 6          ! call hook process with custom event
define HOOKOP_QRY       = 7          ! query hook (by fileid or fspec)

! [108] hooklog special control bytes

define SQUEEZE_CTL      = &hDD        ! leadin for a squeeze run of bytes (221.)

define HLOG_VERSION       = 1         ! log event format version (#1) (follows HLOG_EVENT_START)
define HLOG_MASCB_VERSION = 2         ! [114] xMASCB logs use version #2

define HLOG_EVENT_START = &h80        ! start of log entry (followed by version byte)  
define HLOG_TIMESTAMP   = &h82        ! timestamp  
define HLOG_ENV         = &h83        ! envelope  
define HLOG_ENV_SQZ     = &h84        ! start of squeezed log entry (followed by version byte)  
define HLOG_PREDATA     = &h85        ! prior ('was') data  
define HLOG_PREDATA_SQZ = &h86        ! prior ('was') data (squeezed)  
define HLOG_DATA        = &h87        ! new data  
define HLOG_DATA_SQZ    = &h88        ! new data (squeezed)  
define HLOG_DATA_DIFF   = &h89        ! new data (diff vs predata, then squeezed)  
define HLOG_APP_MSG     = &h8A        ! app msg (in rec buf; not squeezed) 
define HLOG_EVENT_END   = &h8F        ! end of log entry  

! [109] mode flags 
define HMF_FORCE_WRITE  = &h0001      ! Set if FORCE_WRITE required 
define HMF_FORCED       = &h0010      ! RANDOM'FORCED 
define HMF_SPANBLOCKS   = &h2000      ! SPAN'BLOCKS 
define HMF_END_FILE     = &h2000      ! END'FILE 
define HMF_W_RECORD     = &h4000      ! WAIT'RECORD 
define HMF_W_FILE       = &h8000      ! WAIT'FILE 
define HMF_W_IRECORD    = &h0080      ! ISAM WAIT'RECORD  (on GET statement)
define HMF_READONLY     = &h1000      ! READ'ONLY 
define HMF_LOCALCOPY    = &h0200      ! local copy 
define HMF_MEM          = &h0400      ! file is in user memory ->pmap 


deftype T_HOOK_FLDREP = S,256        ! [110] one string-formatted field value (for chg report)

defstruct ST_HOOK_ENV   ! information passed to hook routine
    map2 event,b,4      ! event code (see HPE_xxxx flags)
    map2 flags,b,4      ! event processing flags (see HFF_xxx)
    map2 fileid,b,4     ! unique file id #
    map2 recno,f,8      ! rec # (always 0-relative, regardless of FILEBASE!)
    map2 pid,b,4        ! process id
    map2 recsiz,b,4     ! record size
    map2 mode,b,4       ! mode (depends on event; see HMF_xxx)  [109]
    map2 prog,s,16      ! program name
    map2 sbx,s,16       ! sbx name (if appl)
    map2 user,s,24      ! user name
    map2 chan,b,4       ! [107] actual file channel (may not match id)
    map2 location,b,4   ! [107] location counter
    map2 lineno,b,2     ! [107] last line #
    map2 fill,x,2       ! [108] unused (but needed to bring size to multiple of 4)
endstruct

! [114] variation of the standard ST_HOOK_ENV structure tailored for xMASCB
! adds prerecsiz, since both copies of rec are compressed and thus variable size;
! adds key; removes mode and channel
defstruct ST_HOOKMASCB_ENV   ! information passed to hook routine
    map2 event,b,4      ! event code (see HPE_xxxx flags)
    map2 flags,b,4      ! event processing flags (see HFF_xxx)
    map2 fileid,b,4     ! unique file id #
    map2 recno,f,8      ! rec # (always 0-relative, regardless of FILEBASE!)
    map2 pid,b,4        ! process id
    map2 prog,s,12      ! program name
    map2 sbx,s,10       ! sbx name (if appl)
    map2 user,s,20      ! user name
    map2 lineno,b,2     ! last line #
    map2 recsiz,b,2     ! record size
    map2 prerecsiz,b,2  ! compressed size of prerec
    map2 location,b,4   ! location counter
    map2 key,s,52       ! external key
endstruct


defstruct ST_HOOK_TIMESTAMP     ! [108] used in FSPEC: time hook output
    map2 date,b,4               ! [108] separated date format
    map2 msecs,b,4              ! [108] milliseconds since midnight
endstruct

defstruct ST_HOOK_FLDCHG        ! [110] used for passing field-level chg info
    map2 bkey,BOOLEAN           ! [111] if true, this is the key field (may not have changed but helpful for logging)
    map2 flddef,ST_FLDDEF       ! [110] field name, type, size (from dynstruct.def)
    map2 subs(MAX_ARY_DIMS),b,4 ! [111] for array fields, list of subscripts
    map2 datapre,T_HOOK_FLDREP  ! [110] string representation of field contents prior to chg
    map2 datapost,T_HOOK_FLDREP ! [110] string representation of field contents prior to chg
endstruct    
    
++endif
