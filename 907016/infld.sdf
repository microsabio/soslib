!INFLD.SDF [103] - Standard INFLD structure definitions
!-------------------.-------------------.-------------------.-------------------.
!EDIT HISTORY
! VEDIT=103
! Note: history reformatted 9-Feb-17
! [103] 21-Aug-13 / jdm / Add flags field to ST_INFCLRX
! [102] 19-Aug-13 / jdm / Add ST_INFCLRX
! [101] 26-Jun-12 / jdm / Add IMXF_PIXCOORDS
! [100] 07-May-12 / jdm / Created (mainly for ST_INFXMAX, but added others as well)
!-------------------.-------------------.-------------------.-------------------.

++ifnmap VERSYS_INFLD_SDF

![100] This is only map statement here - needs to be a map so
![100] it is included in the run (to be displayed with VERSYS.LIT)
map1 VERSYS_INFLD_SDF,s,40,">>@VERSYS(1)->>infld.sdf[103]"


! new extended XMAX structure
defstruct ST_INFXMAX
    map2 erow,b,4           ! rows, millirows or pixels
    map2 ecol,b,4           ! columns, millicolumns or pixels
    map2 flags,b,4          ! see IMXF_xxx below
    map2 fontattr,b,4       ! same as in AUI_CONTROL
    map2 fontscale,b,4      ! " "   "  
    map2 fontface,s,32      ! " "   "  
endstruct

! ST_INFXMAX flags
define IMXF_FNTPTSIZE  = &h00000001       ! fontscale is in points*10
define IMXF_PIXCOORDS  = &h00000002       ! all coords in pixels [101]
define IMXF_XMAXONLY   = &h40000000       ! erow=xmax; all other fields ignored


defstruct ST_INFCLR
    map2 dfclr,B,1,1         ! display fg
    map2 dbclr,B,1,2         ! display bg
    map2 efclr,B,1,67        ! editing fg
    map2 ebclr,B,1,68        ! editing bg
    map2 nfclr,B,1,5         ! negatives fg
    map2 nbclr,B,1,6         ! negatives bg
    map2 ufclr,B,1,2         ! update fg
    map2 ubclr,B,1,0         ! update bg
    map2 mfclr,B,1,3         ! messages fg
    map2 mbclr,B,1,2         ! messages bg
    map2 ofclr,B,1,11        ! original messages fg
    map2 obclr,B,1,12        ! original messages bg
    map2 ffclr,B,1,13        ! forms fg
    map2 fbclr,B,1,14        ! forms bg
endstruct

!Note: to override std colors, set sig = "x1", else structure is ignored.
defstruct ST_INFCLRX         ! [102] new GUI version (6.1.1360+)
    map2 sig,s,2             ! [102] "x1"=enabled; else ignored (see note above)
    map2 flags,b,2           ! [103] see INFCLRXF_xxx (1=use d?rgb for edit too)
    map2 dfrgb,b,4           ! [102] display fg
    map2 dbrgb,b,4           ! [102] display bg
endstruct

![103] ST_INFCLRX flags flags
define INFCLRXF_EDTCLR = &h0001     ! [103] use d?rgb for edit mode too

++endif

