!EMAILX.DEF [110] - EMAILX definitions
!-------------------.-------------------.-------------------.-------------------.
!EDIT HISTORY
! VEDIT=109
! Note: history reformatted 9-Feb-17
! [110] 07-Mar-21 /jdm / Add ST_EM_ATTACHMENTS
! [109] 14-Apr-19 /jdm / Add a DEFXCALL; add ++ifndef
! [108] 09-Oct-13 /jdm / Add EMF_NOAUTOHDR, switch to hex notation
! [107] 12-Apr-10 /jdm / Add EMF_TLS, EMF_STARTTLS
! [106] 29-Sep-07 /jdm / Created from EMAILX.MAP
! [105] 13-Jul-01 /jdm / Add EMF_DEBUG
! [104] 09-Jul-01 /jdm / Add EMS_QUEUED, EMF_CHECKQ, EMF_SENDQ, define 
!                        CTE_ATT'ON'GW flags
! [103] 07-Feb-01 /jdm / Add EMS_EXPIR flag
! [102] 26-Jan-01 /jdm / Add flags related to MIME
! [101] 07-Jan-01 /jdm / Rearrange parameter list, introduce config file
! [100] 06-Jan-01 /jdm / Created
!-------------------.-------------------.-------------------.-------------------.
!NOTES
! Symbols used by EMAILX.SBX and applications calling it
! (Include this version or EMAILX.MAP, depending on whether you want
! to use the modern symbols or not)
!-------------------.-------------------.-------------------.-------------------.

++ifndef EMF_NORMAL

  ! [109] As of compiler 898, there is no conditional test for DEFXCALLs, so to
  ! [109] allow application to provide it's own version, it can set the symbol
  ! [109] DEFXCALL_XCALL and define it's own version before including this module.
  ! [109] (At some point the compiler should be updated to support ++ifndef for DEFXCALL)
  ++ifndef DEFXCALL_EMAILX
    define DEFXCALL_EMAILX = 1
    defxcall EMAILX, opflags, cfgfile$, to$, header$, body$, status {,from$="", attachments="", &
                    signature'filespec$="", errmsg$="", socket=0 ,server$="", host$="", &
                    username$="", password$=""}
  ++else
    ++message Skipping DEFXCALL EMAILX ... (already defined?)
  ++endif

!map1 EMF_OPFLAGS		! Definitions of  EM'OPFLAGS symbols
    define EMF_NORMAL      = &h0000    ! normal operation
    define EMF_LEAVEOPEN   = &h0001    ! don't close session
    define EMF_ALREADYOPEN = &h0002    ! don't open; (already open)
    define EMF_OPENONLY    = &h0004    ! open (don't send); ignore all but
                                  !   SERVER & HOST;
                                  ! WARNING: Does NOT imply EMF_LEAVEOPEN!!!
                                  ! (may be used just to test SMTP connection)
    define EMF_CLOSEONLY   = &h0008  ! close only (DOES imply EMF_ALREADYOPEN)
    define EMF_HDRCAT      = &h0010  ! concat HEADER+BODY (no CRLF)
    define EMF_HTMLBODY    = &h0020  ! BODY contains <HTML>
    define EMF_MIMEBODY    = &h0040  ! BODY is a self-contained MIME part
    define EMF_CHECKQ      = &h0080  ! Check if any messages waiting in queue
    define EMF_SENDQ       = &h0100  ! Attempt to dequeue messages
    define EMF_DEBUG       = &h0200  ! Display debug messages on screen
    define EMF_TLS         = &h0400  ! [107] use TLS
    define EMF_STARTTLS    = &h0800  ! [107] use STARTTLS
    define EMF_NOAUTOHDR   = &h1000  ! [108] don't add any automatic hdrs (good for SMS)

!map1 EMS_STATUS         ! definitions of certain error conditions
    define EMS_OK       = 0       ! ok
    define EMS_TOOFEW   = 71      ! too few params
    define EMS_TIMEOUT  = 72      ! timed out waiting for SMTP response
    define EMS_NOTO     = 73      ! no 'TO' address
    define EMS_NOBODY   = 74      ! no 'BODY' address
                                  ! (Note: this doesn't have to be an error
                                  !  but probably indicates a bug in app
                                  !  so we choose to abort message rather
                                  !  than sending an empty message)
    define EMS_BADRESP  = 75      ! Bad (non numeric) response from SMTP
    define EMS_NOSOCKET = 76      ! No socket open
    define EMS_NOCFG    = 77      ! Specifed CFG file not present
    define EMS_BADCFG   = 78      ! Syntax error within CFGFILE
    define EMS_BADSIG   = 79      ! Bad signature file
    define EMS_BADATT   = 80      ! Bad attachment (prob. not found)
    define EMS_B64ERR   = 81      ! Error during base64 encoding
    define EMS_EXPIR    = 82      ! Program license expired
    define EMS_QUEUED   = 83      ! Unable to contact gw (AMOS) or SMTP (ashell)
				                  ! so message left in queue (id in ERRMSG)
    define EMS_QUEREN   = 84      ! Unable to rename EMT to EMQ (AMOS)
    define EMS_OPENQO   = 85      ! Unable to open EMT file
    define EMS_OPENQR   = 86      ! Unable to reopen EMQ for transfer
    define EMS_GWCONF   = 87      ! Unable to connect to email gw (AMOS)

!map1 define CTE_FLAGS		! Content-Transfer-Encoding (ATT'ENCODING(X))
    define CTE_BASE64    = 0      ! Base64 (default)
    define CTE_7BIT      = 1      ! 7 bit unencoded text
    define CTE_8BIT      = 2      ! 8 bit unencoded text
    define CTE_BINARY    = 3      ! binary unencoded data
    define CTE_QUOTED    = 4      ! Quoted Printable encoding
    define CTE_ATT'ON'GW = 128    ! Attachment fspec is on gateway machine

    define MAX_EM_ATTACHMENTS = 10  ! [110] max # attachments
    
defstruct ST_EM_ATTACHMENTS         ! [110] attachments structure
    map2 att'count,b,2
    map2 attx(MAX_EM_ATTACHMENTS)
        map3 att'file,s,128
        map3 att'content'type,s,127
        map3 att'encoding,b,1
endstruct
    
++endif
