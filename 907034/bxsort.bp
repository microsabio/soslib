program BXSORT,1.0(100)  ! BASORT-compatible front end to OptTech sort
!------------------------------------------------------------------------
!BXSORT - Sort a file, using BASORT args but with asopsort (OptTech sort)
!
!Usage:
!	XCALL BXSORT,CH,RECCNT,RECSIZ,K1SIZ,K1POS,K1ORD{,K2SIZ,K2POS,K2ORD,
!           K3SIZ,K3POS,K3ORD,K1TYP,K2TYP,K3TYP}
!where
!	CH - if numeric, is file channel of file to sort.  if string, is
!        filespec of file.
!	RECCNT (numeric) number of records to sort
!	RECSIZ (numeric) record size
!   KxSIZ  (numeric) size of sort key x
!   KxPOS  (numeric) starting byte position of sort key x
!   KxORD  (numeric) 0=ascending, 1=descending
!   kxTYP  (numeric) 0=string, 1=floating point, 2=binary
!--------------------------------------------------------------------
! LIMITATIONS:
! 
! Although the Optech sort is way more powerful than Basort, 
! it is also so different that it is not easy to match up the options
! one-to-one with Basort.  So beware of the following limitations:
!
! 1. Currently only RANDOM files are supported (no sequential)
! 2. Record size must divide into 512 evenly, or the file must have
!    been created using span'blocks (so there is no empty space at
!    at the end of each physical block).
! 3. Only string sort keys (KxTYP=0)
! 4. Maximum allowed recsize is 4096 (but this could be expanded by
!    changing MAX_RECSIZ definition below and recompiling)
! 
!--------------------------------------------------------------------
!EDIT HISTORY   
![100] June 6, 2006 1:30 PM         Edited by Jack
!	Created
!--------------------------------------------------------------------
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"

++include ashinc:xcall.bsi
++include ashinc:ashell.def

define MAX_RECSIZ = 4096        ! maximum record size

MAP1 XCALL'PARAMS
    map2 CH,B,2
    map2 FNAME$,S,120
    map2 RECCNT,B,4
    map2 RECSIZ,B,2
    map2 K1SIZ,B,2
    map2 K1POS,B,2
    map2 K1ORD,B,1
    map2 K1TYP,B,1
    map2 K2SIZ,B,2
    map2 K2POS,B,2
    map2 K2ORD,B,1
    map2 K2TYP,B,1
    map2 K3SIZ,B,2
    map2 K3POS,B,2
    map2 K3ORD,B,1
    map2 K3TYP,B,1        
    map2 DUMMY,B,1
    
map1 SORT'PARAMS
    map2 FSPEC$,S,160
    map2 SORTCMD$,S,800
    map2 FOUT$,S,160
    map2 SORTKEYS$,S,50
    
MAP1 DDB
    MAP2 DEV,s,6         ! 6 characters AMOS device
    MAP2 FILNAM,s,32     ! 6 characters AMOS or 32 host
    MAP2 EXT,s,8         ! 3 characters AMOS or 8 host
    MAP2 PRJ,s,3         ! 3 character justified P
    MAP2 PRG,s,3         ! 3 character justified PN

map1 MISC
    map2 STATUS,F
    map2 ENVVAL$,S,120
    map2 CHOUT,F
    map2 CHIN,F
    map2 RECNOX,X,6
    map2 FBYTES,F
    map2 ADDRECS,F
    map2 RNO,F
    map2 REC,X,MAX_RECSIZ   
    map2 X,F
    map2 FLAGS,F
    map2 GUIFLAGS,F
    
define M_SPANBLOCKS = &h2000
                   
    if XCBCNT < 6 then
        xcall EVTMSG,"Insufficient number of params (%d) passed to BXSORT (min=6)",XCBCNT
        end
    endif
            
    filebase 0            
    on error goto TRAP
            
    ! Since Optech sort operates on filespec, we have to extract the file
    ! name from the channel (but don't close file to preserve locking)
    if XCB'PTYPE(1) >= XTYPE'FLOAT then
        xgetarg 1,CH
        xcall FILNAM,CH,FNAME$
        if FNAME$ = "" then
            xcall EVTMSG,"No file open on channel %d in BXSORT",CH
            end
        endif            
        ! dup the file info (just for the side effect that causes the caller
        ! to reset it on return)
        xcall MIAMEX,MX_FILECHINFO,CH,STATUS,FLAGS
        if STATUS # 3 then
            xcall EVTMSG,"BXSORT currently only supports random files (status=%d)",STATUS
            end
        endif 
                   
        !close #CH
    else
        xgetarg 1,FNAME$
    endif
    
    xcall MIAMEX,MX_FSPEC,FNAME$,FSPEC$,"dat",FS_FMA+FS_TOH,DDB,STATUS                   
    if LOOKUP(FSPEC$) = 0 then
        xcall EVTMSG,"File %s not found in BXSORT",FSPEC$
        end
    endif
    
    ! get the spec of the miame.ini file
    xcall MIAMEX,MX_GETENV,"MIAMEFILE",ENVVAL$
    
    ! write output to a similarly named file...
    X = instr(1,FSPEC$,".")
    
    FOUT$ = FSPEC$[1,X-1] + ".srx" 
    
    SORTCMD$ = "asopsort " + ENVVAL$ + " " + FSPEC$ + " " + FOUT$ + " ""/"
            
    xgetargs DUMMY,RECCNT,RECSIZ,K1SIZ,K1POS,K1ORD,K2SIZ,K2POS,K2ORD, &
        K3SIZ,K3POS,K3ORD,K1TYP,K2TYP,K3TYP

    IF (int(512/RECSIZ) # (512/RECSIZ)) and ((FLAGS and M_SPANBLOCKS) = 0) then
        if DEBUG then
            xcall EVTMSG,"BXSORT cannot handle files with block padding; calling BOSORT..."
        endif
        xcall BOSORT,CH,RECCNT,RECSIZ,K1SIZ,K1POS,K1ORD,K2SIZ,K2POS,K2ORD, &
            K3SIZ,K3POS,K3ORD,K1TYP,K2TYP,K3TYP
        end
    endif
    xcall AUI,AUI_ENVIRONMENT,0,GUIFLAGS
    if GUIFLAGS and AGF_ANYWIN then
        if DEBUG then
            xcall EVTMSG,"BXSORT doesn't support Windows; calling BOSORT..."
        endif
        xcall BOSORT,CH,RECCNT,RECSIZ,K1SIZ,K1POS,K1ORD,K2SIZ,K2POS,K2ORD, &
            K3SIZ,K3POS,K3ORD,K1TYP,K2TYP,K3TYP
        end
    endif
                            
   
    ! S(pos,size,type,order{pos,size,type,order...}) for sort keys
    SORTKEYS$ = "S(" + str(K1POS) + "," + str(K1SIZ)

    if (K1TYP=0) then SORTKEYS$ = SORTKEYS$ + ",C," &
    else if (K1TYP=1) then SORTKEYS$ = SORTKEYS$ + ",E," &
    else if (K1TYP=2) then SORTKEYS$ = SORTKEYS$ + ",W," &
    else xcall EVTMSG,"Invalid K1TYP in BXSORT: %d",K1TYP

    if (K1ORD = 0) then 
        SORTKEYS$ = SORTKEYS$ + "A"
    else
        SORTKEYS$ = SORTKEYS$ + "D"
    endif
    
    if K2SIZ then
        SORTKEYS$ = SORTKEYS$ + "," + str(K2POS) + "," + str(K2SIZ)
        if (K2TYP=0) then SORTKEYS$ = SORTKEYS$ + ",C," &
        else if (K2TYP=1) then SORTKEYS$ = SORTKEYS$ + ",E," &
        else if (K2TYP=2) then SORTKEYS$ = SORTKEYS$ + ",W," &
        else xcall EVTMSG,"Invalid K2TYP in BXSORT: %d",K2TYP

        if (K2ORD = 0) then 
            SORTKEYS$ = SORTKEYS$ + "A"
        else
            SORTKEYS$ = SORTKEYS$ + "D"
        endif
                
        if K3SIZ then
            SORTKEYS$ = SORTKEYS$ + "," + str(K3POS) + "," + str(K3SIZ) 
            if (K3TYP=0) then SORTKEYS$ = SORTKEYS$ + ",C," &
            else if (K3TYP=1) then SORTKEYS$ = SORTKEYS$ + ",E," &
            else if (K3TYP=2) then SORTKEYS$ = SORTKEYS$ + ",W," &
            else xcall EVTMSG,"Invalid K3TYP in BXSORT: %d",K3TYP

            if (K3ORD = 0) then 
                SORTKEYS$ = SORTKEYS$ + "A"
            else
                SORTKEYS$ = SORTKEYS$ + "D"
            endif
        endif
    endif                            
    SORTCMD$ = SORTCMD$ + SORTKEYS$ + ") "

    ! F(TYP,SIZ) specifies file type and recsize 
    SORTCMD$ = SORTCMD$ + "F(FIX," + str(RECSIZ)+") "
    
    ! LIMIT(#) sets the number of records to sort
    SORTCMD$ = SORTCMD$ + "LIMIT(" + str(RECCNT) +") "

    ! LOG(logfile,N) outputs message to log file    
    SORTCMD$ = SORTCMD$ + "LOG(bxsort.log,A)"""
    
    IF DEBUG then
        X = instr(1,SORTCMD$,"""")
        xcall EVTMSG,SORTCMD$[1,X-1]+chr(13)+chr(10)+SORTCMD$[X,-1]
    endif        
    ! do the sort...
100 xcall HOSTEX,SORTCMD$,STATUS 
    
    if STATUS # 0 then
        xcall EVTMSG,"Error code %d returned from OptTech sort in BXSORT.SBX",STATUS
        if lookup(FOUT$) # 0 then kill FOUT$
        end
    endif        
         
    ! OK, now we should have the sorted output in the FOUT$ file.  
    ! First we need to expand that file to contains the unsorted records
!     at the end of the original file; then rename it
!    xcall SIZE,FSPEC$,FBYTES
!    if FBYTES > RECCNT*RECSIZ then
!        CHOUT = 55000
!        do while eof(CHOUT) # -1    ! get next available unused channel
!            CHOUT = CHOUT + 1
!        loop
!
!        ADDRECS = int((FBYTES - (RECCNT*RECSIZ)) / RECSIZ)
!        
!        open #CHOUT, FOUT$, RANDOM, RECSIZ, RNO, span'blocks
!
!         expand the output file        
!        xcall MIAMEX,MX_EXPFIL, ADDRECS, STATUS
!        
!         now copy the unsorted recs from the input file...
!        RNO = int(FBYTES / RECCNT)
!        
!        open #CH, FSPEC$, RANDOM, RECSIZ, RNO, span'blocks
!        do while ADDRECS > 0
!            read #CH, REC
!            write #CH, REC[1,RECSIZ]
!            ADDRECS = ADDRECS - 1
!        loop
!        
!        close #CH
!        close #CHOUT
!    
!    endif
!    
!    
!     remove original file and rename output to it
!    kill FSPEC$
!    xcall RENAME,FOUT$,FSPEC$,STATUS

    ! alternate plan: open the sorted output and original input as streams
    ! and copy the sort data back on top of the original unsorted data
110 xcall SIZE,FOUT$,FBYTES
    IF (FBYTES # RECSIZ*RECCNT) then
        xcall EVTMSG,"Error in size of sorted output in BXSORT: %d vs %d (%d x %d)", &
            FBYTES,RECSIZ*RECCNT,RECSIZ,RECCNT
        end
    endif
    
    ! Reposition file cursor of original file at beginning and then copy
    ! new file on top of it
125 xcall MIAMEX,MX_FILEPOS,CH,1,0

    ! open sorted file for input
    CHIN = 55000
130 do while eof(CHIN) # -1    ! get next available unused channel
135     CHIN = CHIN + 1
140 loop
145 open #CHIN, FOUT$, INPUT
    
    ! now copy sorted data on top of original data
150 xcall MIAMEX,MX_COPYFILE,CHIN,CH
    
156 close #CHIN
160 kill FOUT$
170 end     ! return to Basic (no return args)
    
TRAP:
    xcall EVTMSG,"Basic error %d in BXSORT.SBX; line=%d, file=%d (%s)", &
        err(0),err(1),err(2),FNAME$
    end
                
