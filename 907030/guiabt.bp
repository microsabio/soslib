PROGRAM GUIABT,1.0 (100)

! Name: GUIABT
! Description: Gui A-Shell About Menu.  Windows A-Shell or if Client/Server environment then it's the server about information
! Author: Stephen Funkhouser
! Date: 08-06-2009
!
!----------------------------------------------------------------------------------------------------------------
!{history}
! 08-06-2009 --- 100 --- Stephen Funkhouser
! Date Created
! 
!----------------------------------------------------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------

!{pragmas}
!--------------------------
++PRAGMA sbx
!--------------------------

!{definitions zone}
!--------------------------
++INCLUDE ashinc:ashell.def

DEFINE dlg'about_btn'microsabio_EXITCODE = 501

!{includes}
!--------------------------
++INCLUDE guiabt.gen
!--------------------------

!{begin variable maps}
!--------------------------
MAP1 set'focus	,b,4
MAP1 exitcode	,f,6
MAP1 status	,f,6
MAP1 guiflags	,b,4
MAP1 sbxnam$	,s,10

MAP1 mx_about_params
	MAP2 prdname,s,64
	MAP2 version,s,64
	MAP2 serial,f,6
	MAP2 coname,s,64
	MAP2 key,s,64
	MAP2 licnodes,f,6
	MAP2 phynodes,f,6
	MAP2 lognodes,f,6
	MAP2 licoptions,s,64
	MAP2 expflags,f,6
	MAP2 reldate,s,64
	MAP2 expdate,s,64
	MAP2 inifile,s,200
!{end variable maps}
!--------------------------


!{eventwait}
!--------------------------
	XCALL MIAMEX,MX_SETMEMFLAGS,0,1		! delete from memory
	XCALL GETPRG,"",sbxnam$

	XCALL AUI,AUI_ENVIRONMENT,MXOP_GET,guiflags
	IF ((guiflags AND AGF_GUIEXT) = 0) THEN
		XCALL MSGBOX,"GUI Extensions must be enabled to run " + edit$(sbxnam$,40) + "!","Application Message",MBTN_OK,0,0
		CALL exit'program()
	ENDIF


	XCALL MIAMEX,MX_ABOUT,prdname,version,serial,coname,key,licnodes,phynodes,lognodes,licoptions,expflags,reldate,expdate,inifile

	!trim all string vars
	prdname	= edit$(prdname,40)
	version	= edit$(version,40)
	coname	= edit$(coname,40)
	key		= edit$(key,40)
	licoptions= edit$(licoptions,40)
	reldate	= edit$(reldate,40)
	expdate	= edit$(expdate,40)
	inifile	= edit$(inifile,40)

	dlg'about_title$ = "About " + prdname

	dlg'about_lbl'version_text$		= prdname + " Version: " + version
	dlg'about_lbl'licensed'to_text$	= "This copy of " + prdname + " is licensed to:"
	dlg'about_lbl'coname_text$		= coname
	dlg'about_lbl'ser'rel_text$		= "Serial # " + STR(serial) + CHR(9) + "Release date: " + reldate

	dlg'about_lbl'ini_text$			= inifile
	dlg'about_lbl'nodes_text$		= STR(licnodes)
	dlg'about_lbl'nodes'jobs_text$	= STR(phynodes) + " / " + STR(lognodes)
	dlg'about_lbl'licoptions_text$	= licoptions
	dlg'about_lbl'expdate_text$		= "Updates licensed thru " + expdate

	CALL dlg'about_load'dialog(dlg'about_dlgid,MBST_CENTER,dlg'about_title$)
	XCALL AUI, AUI_CONTROL, CTLOP_ADD, "dlg'about_ash'icon", "#1001", MBST_ENABLE, MBF_STATIC + MBF_ICON, "", "", NUL_CSTATUS, 1500, 2, 3500, 5

	DO
		XCALL AUI,AUI_EVENTWAIT,dlg'about_dlgid,set'focus,exitcode,EVW_DESCEND+EVW_EDIT+EVW_EXCDFOCUS+EVW_EXCDINOUT

		SWITCH ABS(exitcode)
			CASE dlg'about_btn'microsabio_EXITCODE
				XCALL MIAMEX,MX_SHELLEX,status,"http://microsabio.com","open","",""
				IF (status # 0) THEN
					XCALL MSGBOX,"Error Launching Microsabio Website." + CHR(13) + CHR(10) + "Status Code: " + STR(status) &
						,"Error Message",MBTN_OK,MBICON_EXCLAMATION,MBMISC_SYSMODAL

				ENDIF
				EXIT
		ENDSWITCH

	LOOP UNTIL (exitcode = 1)

	CALL exit'program()

PROCEDURE exit'program()
++PRAGMA AUTO_EXTERN

	XCALL AUI,AUI_CONTROL,CTLOP_DEL,dlg'about_dlgid
	END

ENDPROCEDURE
