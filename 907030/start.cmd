:<(INFO) Routine to retrieve/display misc system/program information

    ASHVER.SBX - Retrieve version info for ashell server and client
        SASVER - Test program for ASHVER.SBX
    ATEINS.SBX - GUI variation of SYSTAT (from Stephen Funkhouser)
    TSTATEINS  - Test program for ATEINS.SBX
    GUIABT.SBX - GUI Display displaying "about" info (Funkhouser)

    (See source files for usage notes)
>
