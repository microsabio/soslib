!************************************************************************
!ASHVER - Get Server/ATE Ashell Version.
!************************************************************************
! VERSION HISTORY
! 19/May/2005 - 4.2(100) - Steve - New SBX to get ashell versions.
! 24/May/2005 - 4.2(101) - Steve - Added ATE version support.
! 25/Nov/2005 - 4.2(500) - Steve - Madics 4.2a
! 04/Jan/2006 - 4.2(501) - Steve - Changed to cover Ashell 5 version format.
!
!PROBAS=X
program ASHVER,4.2(501)
!{Edit History}
!
!------------------------------------------------------------------------
!Usage:
! 	XCALL ASHVER,{Type},{Full},{version},{Major},{Minor},{Build},&
!                    {Patch},{Check},{EXE}
!
!
!	TYPE - "S" for Server
!	     - "C" for ATE Client (If Used)
!
! Returns:
!
!	FULL - Full Ashell version. Eg. A-Shell/32 Ver. 4.9(928-1)
!
!    VERSION - Version Only         Eg. 4.9(928-1)
!
!      MAJOR - Major Version.       Eg. 4
!
!      MINOR - Minor Version.       Eg. 9
!
!      BUILD - Build number.        Eg. 928
!
!      PATCH - Patch number.        Eg. 1
!
!      CHECK - Check version        Eg. 0004000909280001
!              Break down of the about if 4 digit segments.
!
!              This can be used to check the version before calling a
!              newer function not supported in previous versions.
!              For example:
!                IF CHECK=>"0004000909300001 THEN &
!		    XCALL MIAMEX,146,LAST'LNO
!
!      EXE   - ATE ashw32.exe location if TYPE "C" used.
!
!--------------------------------------------------------------------
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! NOTE: MUST COMPILE WITH COMPIL ASHVER/X:2
!
++INCLUDE xcall.bsi
!--------------------------------------------------------------------

MAP1 PARAMS
	MAP2 TYPE,S,1
	MAP2 ASHVER,S,30
	MAP2 VERSIONX,S,15
	MAP2 MAJOR,F,6
	MAP2 MINOR,F,6
	MAP2 BUILD,F,6
	MAP2 PATCH,F,6
	MAP2 CHECK,S,16

MAP1 MISC
 MAP2 X1,F,6
 MAP2 X2,F,6
 MAP2 X3,F,6
 MAP2 X4,F,6
 MAP2 POSX,F,6
 MAP2 ENVVAR$,S,40
 MAP2 EXEPATH,S,255
 MAP2 ATEVER,S,15
 MAP2 NA,S,1
 MAP2 SERVER'CHECK,S,16

MAP1 ATE'USED,S,1,"N"

! *** GUI FLAGS (used by MIAMEX,129) to get client information
	MAP1 AGF'GF,B,1                 ! separate var to retrieve into
	MAP1 AGF'GUIFLAGS		!
	    MAP2 AGF'LWG,B,1,1 		! Local Windows Gui
	    MAP2 AGF'LWN,B,1,2 		! Local Windows Non-gui
	    MAP2 AGF'ATE,B,1,4 		! ATE client
	    MAP2 AGF'RWN,B,1,8 		! Remote WiNdows (ATSD)
	    MAP2 AGF'TNT,B,1,16 	! TelNeT
	    MAP2 AGF'ASH,B,1,32 	! A-Shell (not AMOS)
	    MAP2 AGF'THEMES,B,1,64 	! XP themes active
	    MAP2 AGF'LOCWIN,B,1,3 	! Local Windows (GUI or non)
	    MAP2 AGF'ANYWIN,B,1,11 	! Windows (local or remote)
	    MAP2 AGF'GUIEXT,B,1,5 	! GUI EXTensions available
	    MAP2 AGF'LWNATE,B,1,7	! Local WiNdows or ATE

     	on error GOTO TRAP

	! pick up all args; any not passed will just be set to null
      	XGETARGS TYPE,ASHVER

	! Are we using ATE
	XCALL MIAMEX,129,0,AGF'GF
        IF AGF'GF AND AGF'ATE THEN ATE'USED="Y"

	IF TYPE="C" THEN
	   IF ATE'USED="Y" THEN
	      CALL GET'ATE'VERSION
	     ELSE
	      ASHVER="Not running ATE"
	   ENDIF
	 ELSE
	   XCALL MIAMEX,12,ASHVER
	   VERSIONX=ASHVER[17,15]
	   CALL BREAK'DOWN'VERSION
	 ENDIF

	! Pass back to program and exit.
      	XPUTARG 2,ASHVER
      	XPUTARG 3,VERSIONX
      	XPUTARG 4,MAJOR
      	XPUTARG 5,MINOR
      	XPUTARG 6,BUILD
      	XPUTARG 7,PATCH
      	XPUTARG 8,CHECK
      	XPUTARG 9,EXEPATH
	END
!
!====================================================================
! Break Down Version 4.8/4.9..
!====================================================================
BREAK'DOWN'VERSION:
        X1=INSTR(1,VERSIONX,"(")
        X2=INSTR(1,VERSIONX,"-")
        X3=INSTR(1,VERSIONX,")")
        X4=INSTR(1,VERSIONX,".")

        MAJOR=VERSIONX[X4-1;1]
	IF MAJOR=>5 THEN
	  CALL BREAK'DOWN'VERSION'5
	  GOTO GOT'VERSION
	ENDIF

        MINOR=VERSIONX[X4+1;1]

        IF X2=0 THEN POSX=X3 ELSE POSX=X2
        BUILD=VERSIONX[X1+1,POSX-1]

	IF X2<>0 THEN &
           PATCH=VERSIONX[X2+1;2]

GOT'VERSION:
        CHECK=STR(MAJOR) USING "#ZZZ"
        CHECK=CHECK+(STR(MINOR) USING "#ZZZ")
        CHECK=CHECK+(STR(BUILD) USING "#ZZZ")
        CHECK=CHECK+(STR(PATCH) USING "#ZZZ")
	RETURN
!
!====================================================================
! Break Down Version 5
!====================================================================
BREAK'DOWN'VERSION'5:
        X1=INSTR(1,VERSIONX,".")
        X2=INSTR(X1+1,VERSIONX,".")
        X3=INSTR(X2+1,VERSIONX,".")
        X4=INSTR(X3+1,VERSIONX," ")
        MAJOR=VERSIONX[1,X1-1]
        MINOR=VERSIONX[X1+1,X2-1]
        BUILD=VERSIONX[X2+1,X3-1]
        PATCH=VERSIONX[X3+1,X4-1]
 	RETURN
!
!====================================================================
! Get ATE Version and EXE location.
!====================================================================
GET'ATE'VERSION:
	!Use this XCALL with in it self to get Server Version.
	XCALL ASHVER,"S",NA,NA,NA,NA,NA,NA,SERVER'CHECK
	IF VAL(SERVER'CHECK)<0004000909300001 THEN &
  	   ASHVER="ashell 4.9(930)-1 required" : &
	   GOTO RTN'GET'ATE'VERSION

        ENVVAR$="%ATEEXE%"
        ? TAB(-10,45);ENVVAR$;chr(127);
        XCALL NOECHO
        Input "",EXEPATH
        XCALL ECHO

        ENVVAR$="%ATEVER%"
        ? TAB(-10,45);ENVVAR$;chr(127);
        XCALL NOECHO
        Input "",ATEVER
        XCALL ECHO

	ASHVER="ATE ashw32.exe "+ATEVER
	VERSIONX=ATEVER
	CALL BREAK'DOWN'VERSION
RTN'GET'ATE'VERSION:
	RETURN
!
!
!====================================================================
! Error Trap
!====================================================================
TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in ASHVER.SBX!!",2
	END			! return to caller

