!************************************************************************
! SASVER - Show Ashell/ATE Version
!************************************************************************
! VERSION HISTORY
! 19/May/2005 - 4.2(100) - Steve - New Program
!
program SASVER,4.2(100)
!
MAP1 ASHVER,S,30
MAP1 VERSIONX,S,15
MAP1 MAJOR,F,6
MAP1 MINOR,F,6
MAP1 BUILD,F,6
MAP1 PATCH,F,6
MAP1 CHECK,S,16
MAP1 EXE,S,255

	XCALL ASHVER,"S",ASHVER,VERSIONX,MAJOR,MINOR,BUILD,PATCH,CHECK

	PRINT ""
	PRINT "Server Ashell Version"
	PRINT "====================="
	PRINT "Full    : ";ASHVER
	PRINT "Version : "VERSIONX
	PRINT "Major   :";MAJOR
	PRINT "Minor   :";MINOR
	PRINT "Build   :";BUILD
	PRINT "Patch   :";PATCH
	PRINT "Check   : ";CHECK
	PRINT ""

	XCALL ASHVER,"C",ASHVER,VERSIONX,MAJOR,MINOR,BUILD,PATCH,CHECK,EXE

	PRINT "ATE Client Version"
	PRINT "=================="
	PRINT "Full    : ";ASHVER
	PRINT "Version : "VERSIONX
	PRINT "Major   :";MAJOR
	PRINT "Minor   :";MINOR
	PRINT "Build   :";BUILD
	PRINT "Patch   :";PATCH
	PRINT "Check   : ";CHECK
	PRINT "EXE     : ";EXE

	END

