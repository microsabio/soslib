!========================================================================
! ENCDEC - {Encryption/Decrypt Text using BLOFSH.SBR}
!========================================================================
!
!Usage:
!	XCALL ENCDEC,{opcode},{in},{out},{len},[key]
!	
!	OPCODE:	1 - Encrypt data
!		2 - Decrypt data
!	
!	IN:	string in.
!	OUT: 	string out
!
!	LEN:	amount of characters to encrypt. (divisible by 8)
!
!	KEY:	Key to use, if blank/not passed Madics default key used.
!
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include ashinc:xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps Zone} 
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARMS
 MAP2 X'OP'CODE,F,6
 MAP2 X'IN,X,512
 MAP2 X'OUT,X,512
 MAP2 X'FIELD'LEN,F,6
 MAP2 X'KEY,S,56                !4.2(503)

 
MAP1 BF'IMPURE,X,4168		! Internal encryption work area
MAP1 CLEARTEXT,X,512		! Unencrypted block (can contain binary data)
MAP1 CIPHERTEXT,X,512		! Encrypted block
MAP1 KEY,X,56			! Map at desired length from 4 to 56 bytes
				! (32 to 448 bits) (must be divisible by 4)
MAP1 I,F
MAP1 A$,X,1
MAP1 LENX,F,6

MAP1 WORK'OUT,X,512
MAP1 WORK'IN,X,512
!
!==============================================================================
! NOTE: CLEARTEXT AND CIPHERTEXT must be identical size that is divisible by 8
! KEY and Both data blocks MUST be mapped on an even word boundry
!
! XCALL BLOFSH,1,IMPURE,KEY			! LOAD KEY
! XCALL BLOFSH,2,IMPURE,CLEARTEXT,CIPHERTEXT	! ENCRYPT CLEAR TO CIPHER
! XCALL BLOFSH,3,IMPURE,CIPHERTEXT,CLEARTEXT	! DECRYPT CIPHER TO CLEAR
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
    	XGETARGS X'OP'CODE,X'IN,X'OUT,X'FIELD'LEN,X'KEY
	CALL ENCRYPT'DECRYPT
	XPUTARG 3,X'OUT
	END	
!
!==============================================================================
!{Encrypt/Decrypt}
!==============================================================================
ENCRYPT'DECRYPT:
  	X'OUT=""
   	WORK'IN=X'IN[1;X'FIELD'LEN]
  	LENX=LEN(X'IN)
  	
        !Set Key.
	RANDOMIZE
	IF X'KEY="" THEN 
	   KEY="ATTITUDEANDSUCCESS"
	  ELSE 
	   KEY=X'KEY 
	ENDIF
	xcall BLOFSH,1,BF'IMPURE,KEY

	! Encrypt text
	IF X'OP'CODE=1 then
	   CLEARTEXT=WORK'IN
	   xcall BLOFSH,2,BF'IMPURE,CLEARTEXT,CIPHERTEXT
	   WORK'OUT=CIPHERTEXT	   
	ENDIF

	! Decrypt text
	IF X'OP'CODE=2 then
	   CIPHERTEXT=WORK'IN
	   XCALL BLOFSH,3,BF'IMPURE,CIPHERTEXT,CLEARTEXT
	   WORK'OUT=CLEARTEXT 
	ENDIF

	X'OUT=WORK'OUT[1;X'FIELD'LEN]+X'IN[X'FIELD'LEN+1;LENX-X'FIELD'LEN]

 	RETURN
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
