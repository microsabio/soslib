# A-Shell Basic (ASB) Shared Open Source Library (SOSLIB) 

## Description: ##
This library contains utilities in the form of source code intended for re-use, as-is, by A-Shell developers: functions, procedures, SBXs, symbol and structure definitions.  It is is organized into several 907,xxx directories, each containing a descriptive START.CMD and related modules, as follows:

* Core includes used by nearly all ASB developers (907016, aka ASHINC:, e.g. ++INCLUDE ASHINC:ASHELL.DEF)
* Common library functions (907010, aka SOSFUNC:, e.g. ++INCLUDE SOSFUNC:FNDEC2HEX.BSI)
* Common library procedures (907012, aka SOSPROC:, e.g. ++INCLUDE SOSPROC:SYSLOG.BSI)
* External SBX subroutines (each in its own directory)
* LIT commands  (each in its own directory)

## History: ##
The original SOSLIB also contained a set of examples illustrating internal A-Shell capabilities, in the [908,*] directories.  These have been split off into a separate library, EXLIB, because of differences in its usage patterns and in order to keep the more heavily used SOSLIB smaller (and therefore easier to keep updated). The original SOSLIB was released as a series of zip files named sos###.zip (where ### was the release version). Later we adopted the Firmaware UpdateCenter mechanism to release updates using the SOSUPD front-end.  The last update of that series was version 165, in March 2012. The initial commit for this new repository is based on that 165 version.


## How do I get set up? ##
Traditionally the SOSLIB files were kept in DSK0:[907,*].  You can continue that scheme, but for maximum flexibility, we recommend that you create a separate device (e.g. call it SOS61: in honor of A-Shell version 6.1).  The only aspect of the location that is critical for program compatibility is that you create the following ersatz definitions:

* ASHINC:   (must point to the [907,16] directory)
* SOSFUNC:  (must point to the [907,10] directory)
* SOSPROC:  (must point to the [907,12] directory)
* SOSLIB:   (optional ersatz for the device where the [907,*] directories were stored)

For example, if you save the library's 907xxx directories beneath the physical directories c:\vm\miame\soslib61, then you would add a DEVICE statement to your miame.ini along the lines of:


```
#!plain

DEVICE=SOS64 c:\vm\miame\soslib64\
```


and the following ersatz definitions:


```
#!plain

ASHINC: =SOS64:[907,16]
SOSFUNC:=SOS64:[907,10]
SOSPROC:=SOS64:[907,12]
SOSLIB: =SOS64:

```

## Downloading / Updating ##
There are two approaches.  One is to just use the download link (below left), which gives you a zip file that you can then unzip into the desired directory tree. The other is to set up Mercurial (or the Windows GUI front-end Tortoise Hg) and then you can just sync the updates to your system.


## Testing ##
To verify that the library is set up properly, try compiling/running any .bp programs in the [907,11] directory.  (These are test/demo programs corresponding to the routines in the sosfunc: directory.)

## Usage ##
The most common way to use the library is to add the following to the start of every program:

    ++include ashinc:ashell.def

This makes available most of the standard symbols used for referencing ASB routines (particularly subroutines), as documented in the [A-Shell Reference](http://www.microsabio.net/dist/doc/conref/00ashref.htm).  Depending on the routines you are using, you may need to add additional ++include statements (hopefully documented).

Beyond the core symbols and other definitions in the ashinc: directory, the next most common way to use the library is to take advantage of functions and procedures in the sosfunc: and sosproc:  directories.  The best way to get an idea of what is available is to log to one of those directories at the dot prompt.  This will display an index of the files and routines.  You can then get the details for using any of them by displaying (in VUE, APN, or some other viewer/editor) the relevant file, which should hopefully contain detailed usage information.

## Contributing ##
To contribute bug reports or enhancement requests, please use the Issue Tracker. To contribute patches or new routines, please email them to MicroSabio (for verification/testing/editing) or otherwise contact us to set up a direct access account.
We'll grant direct updating privileges to developers who have the skills and interest to post their own updates directly to the library.  (Anyone can easily track those from the commits panel.)