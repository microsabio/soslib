!*************************************************************************
! SYSREG.SBX - Read/Save data to a text based INI File
!*************************************************************************
!--------------------------------------------------------------------
! VERSION HISTORY
! 12/Jan/2005 - 4.1(100) - Steve - New SBX
!
program SYSREG,4.1(100)
!
!------------------------------------------------------------------------
!  *** warning, no file locking is currently used **
!------------------------------------------------------------------------
!
! NOTE: MUST COMPILE WITH COMPIL SYSREG/X:2 
!
!
!Description:
!
! A windows registy style storage but stored in a random data file.
! works much like INIX.SBX the INI file version.
!
! Useful to store system wide setting or temp settings.
!
! DSK0:SYSREG.DAT[1,2] is the default data file but different file names
! can be used instead if required. also an option for a temp entry and 
! its removed when the purge flag is used.
!
!Usage:
!	XCALL SYSREG,Type,Section,Ident,Value,{temp,file,blocksize}
!
!	Type    - R=Read
!               - W=Write
!               - D=Delete
!               - P=Purge Temp Records
!               - S=Sort/Reorg
!	Section - Selection Name         (Max=6 Chr)
!	Ident   - Ident name             (Max=15 Chr)
!	Value   - Value to write/read    (max=40 chr)
!	Temp    - (Y)es or (N)o {Default is No}
! 	File    - File, if not DSK0:SYSREG.DAT[1,2] used.
! 	Blocksize - If File set, file block allocation size.
!
!Example:
!
! Stores Fred Blogs under  Full Name in FRED section
! XCALL SYSREG,"W","FRED","FULL NAME","Fred Bloggs","N"
!
! Gets Full Name from FRED section and puts it in THE'NAME variable.
! MAP1 THE'NAME,S,40
! XCALL SYSREG,"R","FRED","FULL NAME",THE'NAME
! PRINT THE'NAME
!
! Deletes Full Name from FRED section
! XCALL SYSREG,"D","FRED","FULL NAME"
!
! Reorg/sort File
! XCALL SYSREG,"S"
!
! Purge Temp Records
! XCALL SYSREG,"P"
!
! Store in a different data file called TESTX file and 50 blocks in size.
! XCALL SYSREG,"W","FRED","FULL NAME","Fred Bloggs","N","TESTX.DAT",50
!
!--------------------------------------------------------------------
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"

++include xcall.bsi

MAP1 SYSREG'HEADER
 MAP2 SYSREG'DUM,X,2
 MAP2 SYSREG'ORG,F,6		! Organized count
 MAP2 SYSREG'REC,F,6		! Record count
 MAP2 SYSREG'MAX,F,6		! Maximum number of records
 MAP2 SYSREG'DEL,F,6		! Delete count

MAP1 SYSREG
 MAP2 SYSREG'SECTION,S,6
 MAP2 SYSREG'IDENT,S,15
 MAP2 SYSREG'TEMP,S,1
 MAP2 SYSREG'VALUE,S,40
 MAP2 SYSREG'DEL'FLAG,B,2

MAP1 FSYSREG,F,6,4500
MAP1 RSYSREG,F,6,64
MAP1 KSYSREG,F,6

MAP1 PARAMS
	MAP2 TYPEX,S,1
	MAP2 SECTION,S,6
	MAP2 IDENT,S,15
	MAP2 VALUEX,S,40
	MAP2 TEMPX,S,1
	MAP2 FILEX,S,100
	MAP2 BLOCKSX,F,6

MAP1 MISC
 MAP2 MASTER'FILE,S,25,"DSK0:SYSREG.DAT[1,2]"
 MAP2 ALLO'SIZE,F,6
 MAP2 FF,F,6
 MAP2 XX,F,6
 MAP2 BRACKS,S,64
 MAP2 SEARCH'KEY,S,21
 MAP2 BSMID,F,6
 MAP2 SRCCTL,F,6
 MAP2 WRTCNT,F,6
 MAP2 RDCNT,F,6
!
!--------------------------------------------------------------
!
	on error goto TRAP

	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	FOR XX=1 TO 8 : BRACKS=BRACKS+"]]]]]]]]" : NEXT XX

	! pick up all args
     	XGETARGS TYPEX,SECTION,IDENT,VALUEX,TEMPX,FILEX,BLOCKSX
	SECTION=SECTION+SPACE(6)
	IDENT=IDENT+SPACE(15)
	IF TYPEX="" THEN TYPEX="R"
	IF FILEX="" THEN FILEX=MASTER'FILE
	IF TEMPX="" THEN TEMPX="N"
	CALL PROCESS'DATAFILE	

	! Pass back and exit.
	XPUTARG 4,VALUEX
     	END		
!
!=============================================================
! Search Data file for information.
!=============================================================
PROCESS'DATAFILE:
	CALL ALLOCATE'FILE
	OPEN #FSYSREG,FILEX,RANDOM'FORCED,RSYSREG,KSYSREG
	KSYSREG=1 : READ #FSYSREG,SYSREG'HEADER

	IF TYPEX="S" OR TYPEX="P" THEN CALL REORG'FILE : GOTO CLOSE'FILE

	IF TYPEX<>"D" AND TYPEX<>"R" AND TYPEX<>"W" THEN GOTO CLOSE'FILE

	SEARCH'KEY=SECTION+IDENT+SPACE(21)
    	XCALL SERCH,FSYSREG,SYSREG,SEARCH'KEY,1,21,SYSREG'ORG,BSMID,SRCCTL,6,&
  		63,64,0,0,0,0

	!
	! Read Mode
	!
     	IF TYPEX="R" THEN
	  IF SRCCTL=1 THEN VALUEX="" ELSE VALUEX=SYSREG'VALUE
	ENDIF
	! 
	! Write Mode
	!
	IF TYPEX="W" THEN
	  IF SRCCTL=1 THEN
             CALL ADD'NEW'RECORD
	   ELSE
             KSYSREG=BSMID : READ #FSYSREG,SYSREG
             SYSREG'VALUE=VALUEX+SPACE(40)
             WRITEL #FSYSREG,SYSREG
          ENDIF
	ENDIF
	! 
	! Delete Mode
	!
	IF TYPEX="D" THEN
	  IF SRCCTL=0 THEN
             KSYSREG=BSMID : READ #FSYSREG,SYSREG
             SYSREG'DEL'FLAG=0
             WRITEL #FSYSREG,SYSREG
          ENDIF
        ENDIF

CLOSE'FILE:
	CLOSE #FSYSREG

RTN'PROCESS'DATAFILE:
	RETURN
!
!---------------------
! New Record
!---------------------
ADD'NEW'RECORD:
	IF SYSREG'REC+1>SYSREG'MAX THEN &
	   XCALL MESAG,FILEX+" FILE FULL NO UPDATE",2 : &
	   GOTO RTN'ADD'NEW'RECORD	

	! update header
	KSYSREG=1 : READ #FSYSREG,SYSREG'HEADER
	SYSREG'REC=SYSREG'REC+1
	KSYSREG=1 : WRITEL #FSYSREG,SYSREG'HEADER

	! write new record.
	SYSREG'SECTION=SECTION
	SYSREG'IDENT=IDENT
	SYSREG'TEMP=TEMPX
	SYSREG'VALUE=VALUEX+SPACE(40)
	SYSREG'DEL'FLAG=9
	KSYSREG=SYSREG'REC
	WRITEL #FSYSREG,SYSREG

RTN'ADD'NEW'RECORD:
	RETURN
!
!
!=============================================================
! Allocate file
!=============================================================
ALLOCATE'FILE:
	LOOKUP FILEX,FF
	IF FF<>0 THEN GOTO RTN'ALLOCATE'FILE
	IF BLOCKSX=0 THEN 
	   IF FILEX=MASTER'FILE THEN ALLO'SIZE=3000 ELSE ALLO'SIZE=500
	 ELSE
	   ALLO'SIZE=BLOCKSX
	ENDIF

	ALLOCATE FILEX,ALLO'SIZE
	OPEN #FSYSREG,FILEX,RANDOM'FORCED,RSYSREG,KSYSREG
	SYSREG'DUM=CHR(0)+CHR(0)
	SYSREG'ORG=0 : SYSREG'REC=1 : SYSREG'DEL=0
	SYSREG'MAX=ALLO'SIZE*(512/RSYSREG)
	KSYSREG=1 : WRITEL #FSYSREG,SYSREG'HEADER
	FOR XX=2 TO SYSREG'MAX
	 KSYSREG=XX : WRITEL #FSYSREG,BRACKS
	NEXT XX
	CLOSE #FSYSREG

RTN'ALLOCATE'FILE:
	RETURN
!
!=============================================================
! Reorg File
!=============================================================
REORG'FILE:
	KSYSREG=1 : READ #FSYSREG,SYSREG'HEADER
    	RDCNT=1 : WRTCNT=1
LOOP'AROUND:
    	RDCNT=RDCNT+1
	IF RDCNT>SYSREG'REC GOTO SYSREG'PAD'OUT
	KSYSREG=RDCNT : READ #FSYSREG,SYSREG
	IF SYSREG'DEL'FLAG=0 GOTO LOOP'AROUND
	IF SYSREG[1;3]="]]]" GOTO LOOP'AROUND
   	IF TYPEX="P" AND SYSREG'TEMP="Y" THEN GOTO LOOP'AROUND
	WRTCNT=WRTCNT+1
	KSYSREG=WRTCNT : WRITE #FSYSREG,SYSREG
    	GOTO LOOP'AROUND

SYSREG'PAD'OUT:
    	IF WRTCNT=>SYSREG'REC GOTO SYSREG'SORT'FILE
	SYSREG=BRACKS[1;RSYSREG]
     	FOR KSYSREG=(WRTCNT+1) TO SYSREG'REC
  	    WRITEL #FSYSREG,SYSREG
     	NEXT KSYSREG

SYSREG'SORT'FILE:
	SYSREG'REC=WRTCNT : SYSREG'ORG=1 : SYSREG'DEL=0
	KSYSREG=1 : WRITEL #FSYSREG,SYSREG'HEADER
	XCALL BASORT,FSYSREG,SYSREG'REC,RSYSREG,21,1,0,0,0,0,0,0,0,0,0,0
    	SYSREG'ORG=WRTCNT
	KSYSREG=1 : WRITEL #FSYSREG,SYSREG'HEADER
	RETURN
!
!
!=============================================================
! On Error Trap
!=============================================================
      TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in SYSREG.SBX!!",2
     	end		


