program TSTINX2,1.0(100) ! Simple interactive INIX test
![100] May 31, 2011 06:08 PM            Edited by jack
!   created

MAP1 PARAMS
    MAP2 OPCODE,B,1
    MAP2 INIFILE$,S,100
    MAP2 SECTION$,S,50
    MAP2 IDENT$,S,50
    MAP2 VALUE$,S,300
    MAP2 DEFAULT$,S,300

    input line "INIFILE: ",INIFILE$
    input "op: ",OPCODE
    input "Section: ",SECTION$
    input "Ident: ",IDENT$
    input "Value: ",VALUE$
    input "Def: ",DEFAULT$
    xcall INIX,INIFILE$,OPCODE,SECTION$,IDENT$,VALUE$,DEFAULT$
    ? "VALUE$: ",VALUE$
END
