program GENTSX,1.0(101)
! Description:     Generate INI file import data for TSTINX test
! Author:          Jack McGregor
! Date:            19-Jun-07
!---------------------------------------------------------------------
!Notes:
!
!--------------------------------------------------------------------
!{Edit History}
!
! 29-11-2011 --- 101 --- Jack McGregor
! Chg INIX2 to INIX
!
! 19-06-2007 --- 100 --- Jack McGregor
! Created as alternate way to test INIX2
!
!---------------------------------------------------------------------
!{end history}

map1 SECTION$,S,60
map1 ITEM$,S,60
map1 VALUE$,S,1000
map1 SNO,F
map1 INO,F
map1 T1,F
map1 ELAPSE,F
map1 A$,S,1

define INIFILE$ = "TEST.GEN"

    ? "Generating TEST.GEN (can be used with TSTINX as data source)"
    if lookup(INIFILE$) then
        ? INIFILE$;" exists - replace? ";
        input "",A$
        if ucs(A$)#"Y" End
        kill INIFILE$
    endif

    T1 = TIME
    for SNO = 1 to 50
        SECTION$ = "Section #"+str(SNO)
        ? "[";SECTION$;"]"
        for INO = 1 to 50
            ITEM$ = "Item Number "+str(INO)
            VALUE$ = "Value of item #"+str(INO)+", section "+str(SNO)
            xcall INIX, INIFILE$, 1, SECTION$, ITEM$, VALUE$
            ? ".";
        next INO
        ELAPSE = (TIME - T1) max 1
        ? "( ";(SNO*51)/ELAPSE;"/sec )"
    next SNO
    ELAPSE = (TIME - T1) max 1

    ? "2500 entries added to ";INIFILE$;" in";ELAPSE;"secs ";
    ? "( ";2500/ELAPSE;"/sec )"
    xcall EZTYP,"TEST.GEN"


    End
