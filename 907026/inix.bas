 program INIX,5.0(521)      ! read/write INI files
!*************************************************************************
! New and Improved version by Jack McGregor - based on original by
! Steve Evans.  (Original was renamed to INIX1).
!*************************************************************************
!PROBAS=X
!--------------------------------------------------------------------
!{Edit History}
!
! 09-22-2-23 --- 521 --- Jack 
! Update traces, 
! close SEGV loophole in MX_USRIO logic (surfaced in 64 bit version)
!
! 07-28-2020 --- 520 --- Jack 
! Add INXOP_TIMEOUT_xxx opcodes and change the error trap message box
! to use MSGBOX instead of MESAG (give app more control) 
!
! 10-14-2019 --- 519 --- Jack 
! Adjust locking to cover situation where INI appears not to exist (because
! it was just renamed while another job is updating it.)
!
! 10-13-2019 --- 518 --- Jack 
! Add retry logic to additional routines; add retry logging to ashlog 
!
! 10-10-2019 --- 517 --- Jack 
! Retry the rename, and add error trapping/wait'file to the append to
! work around locking failures (in particular, sharing an INI file
! between different systems over SAMBA); modernize tracing (using
! tags "inix" for warnings only and "inixverbose" for verbose traces.
!
! 06-03-2019 --- 516 --- Jack 
! Change "unable to rename" messagebox to a trace message 
!
! 11-22-2015 --- 515 --- Jack 
! Move the temp file to %temp% to avoid read-only conflict
!
! 05-10-2015 --- 514 --- Jack 
! Change extension of temp file from .<jobno> to .x<jobno> to elimiante
! problem of conflict with an existing file.<jobno> causing "Illegal INI file
! extension" error.
!
! 05-10-2015 --- 513 --- Jack 
! Resolve problem with XLOCK using ,0 as the second part of the lock pair;
! adjust retry logic to be slightly more robust before display error msg 
!
! 19-10-2011 --- 512 --- Steve (Caliq/Madics)
! Fix 'unable to rename' error when backing up/taking a copy of the
! INI file, this was creating a backup file with the .bak at the end of
! the original INI file so creating files like:
! DSK0:STEVE.INI[1,2].bak (instread of DSK0:STEVE.bak[1,2]) this issue
! never showed up under unix as its a valid file name still but caused
! an error under Windows.
!
! 31-05-2011 --- 511 --- Jack
! Support +16 in the TYPE (opcode) to redirect operation to ATE client
! +32 in the TYPE cause INIX to sync itself with the client first.
!
! 31-05-2011 --- 510 --- Jack
! Use RETURN to make the read value available on the stack if called
! as a function.

! 26-10-2010 --- 509 --- Jack
! Fix problem with INI dirnames containing "." (was causing the
! backup to fail and go into an infinite loop of messages)
!
! 27-12-2008 --- 508 --- Jack
! Fix problem with INI filenames containing spaces (similar to 507
! fix but in this case we plug in underlines to fill in the spaces,
! lest the memory module copy just fail)
!
! 15-05-2008 --- 507 --- Jack
! Fix problems caused by the INI filename being longer than 10.3
! (We have to truncate to 10.3 for the module name).
! Note: the new limitation is that the final 10.3 of the name must
! be unique in memory. So you can't simultaneously work on two INI
! files like:
!    ABCDEFGHIJK.INI
!    ZBCDEFGHIJK.INI
! since they would both get loaded into memory as BCDEFGHIJK.INI
!
! 05-03-2008 --- 506 --- Jack
! Fix bug in which write operations did not update the module
! in memory, thus leaving open the possibility that the next access
! would falsely think that the module and disk were the same (since
! modtime resolution is 1 sec).  This led to one update sometimes
! causing the prior one to be reset to the original value.
!
! 03-01-2008 --- 505 --- Jack
! Don't bother with locking ops if in ATE client mode
!
! 16-12-2007 --- 504 --- Jack
! Wasn't returning the default value if the INI didn't exist
!
! 22-11-2007 --- 503 --- Steve (Madics)
! Reaplied the missing 01-08-2007 changes:
! Looks up the file first TO see IF it exists before reading from the
! INI previously IF the INI never existed the field was returned
! back with Line format error: in the string.
!
! 21-11-2007 --- 502 --- Jack McGregor
! Fix intermittent rename error when INI file is not in the current directory
!
! 28-06-2007 --- 501 --- Jack McGregor
! Add XLOCK locking to guard against simultaneous update;
! make .BAK copy when updating INI
!
! 19/Jun/2007 - 5.0(500) - Jack  - New version of Steve Evans' INIX
!                                  Uses memory modules to optimize I/O
!                                  See NOTEs below for info on differences.
!------------------------------------------------------------------------
!Usage:
!	XCALL INIX,INIFILE,TYPE,Section,Ident,Value,{default}
!
!			   TYPE=0 is Read from INI
!			   TYPE=1 is Write to INI
!			   TYPE=2 is Delete Ident
!			   TYPE=3 is Delete Complete Section
!
!           +16 to redirect the operation to the ATE client         [511]
!           +32 to first sync INIX on the server with the client    [511]
!
! Example Read:
! XCALL INIX,"TEST.INI",0,"LOCATION","TOWN",MY'TOWN,"ROYSTON"
! This will read TEST.INI under section LOCATION for TOWN, MY'TOWN will
! be set if found othewrise MY'TOWN is returned with the supplied default.
! Default does not need to be supplied and blank is returned.
!
! Example Write:
! XCALL INIX,"TEST.INI",1,"LOCATION","TOWN","CAMBRIDGE"
! This will write Cambridge to the INI under TOWN in the LOCATION section.
! If the INI file,section or ident is not found they will be created.
!
! Example INI File layout:
! [LOCATION]
! TOWN=CAMBRIDGE
!
! Example Delete Ident:
! XCALL INIX,"TEST.INI",2,"LOCATION","TOWN"
! TOWN line under section LOCATION is delete from the INI file.
!
! Example Delete Section
! XCALL INIX,"TEST.INI",3,"LOCATION"
! Section LOCATION and all idents with in it are deleted.
!
!NOTES (Comparison of INIX 5.0 and INIX 4.X) :
! This version (5.0) was designed to be a more efficient and flexible
! version of the original INIX by Steve Evans.  In the end, it turned
! out to be much more complex than hoped for, but it otherwise
! succeeds in the original goals.  Here's a comparison on various
! points:
!
!Complexity:  INIX 4.X wins, hands-down.  (Nice, clean, straightforward
!             implementation by Steve Evans.)
!
!Structure:   INIX 5.x is broken into reasonably nice functions and
!             procedures, none of which require global variables,
!             whereas INIX 4.x was a traditional model with GOSUBs
!
!Compatibility: As far as I can tell, they are completely compatible,
!             provided you have A-Shell 5.0.990.7 or higher.  For
!             A-Shell 5.0.989.0 to 5.0.990.6, INIX 5.0 operates in a
!             case sensitive manner (which could be either a major
!             or negligible compatibility issue, depending on the app._
!             Prior to 5.0.989, INIX 5.0 does not work at all (and will
!             report a messagebox saying so.)
!
!Performance: When dealing with large INI files, INIX 5.0 is about twice
!             as fast for writing and about 5 times as fast for
!             reading.  This is mainly because it caches a copy of
!             the INI in memory, eliminating the need to re-read the
!             file from disk for each read operation.  It is not
!             clear if the performance advantage carries over to
!             very small INI files though (difficult to measure).
!
!Flexibility: INIX 5.0 supports any size INI file, and individual lines
!             up to 2000 bytes.  (The 2000 byte limit could easily
!             be changed by doing a global replace of 2000 with the
!             desired value,
!             without a major impact on overall memory.)  INIX 4.2
!             (prior to edit 503) had a limit of 3500 lines of
!             up to 200 bytes each.  As of INIX 4.2(503), that limit
!             was removed by using DIMX to allocate a new memory
!             array for each call.  But that has its own issues
!             (see memory requirements).
!
!Memory Requirements: INIX 5.0 loads a copy of the INI file into a
!             memory module, but otherwise does not use much memory
!             of its own, and does not use DIMX either.   (Note that
!             the memory copy name will match the name on disk, but the
!             extension will be the job #.  Also, the memory copy has
!             an extra header line, and a bunch of nulls at the end, so
!             it should not be copied back to the INI on disk.)  By
!             contrast, INIX 4.x (prior to edit 503) contained an array
!             of 750K bytes.  In edit 503, the array was moved to DIMX
!             and sized dynamically as needed, but DIMX has at least
!             one downside:  it causes the SBX to be flushed
!             out of memory as it exits, so there is both
!             a performance hit (from reloading the SBX each time)
!             and a possible bit of confusion (from thinking that you
!             are running the copy you loaded into memory, when you
!             are actually pulling it off disk using the default
!             search path, for all the subsequent calls.
!
!User Interface:  Since the SBX has no user interface, this is not
!             much of an issue, except for error reporting.  INIX 5.0
!             uses MSGBOX to report errors, and gives the option of
!             hitting CANCEL to send a ^C to the caller, which can
!             be useful in situations where otherwise the caller
!             might continue to make dozens or hundreds of calls to
!             INIX even after an error (since the error status is
!             not returned to the caller).  INIX also has a bunch
!             of Debug.Print statements that can be activated
!             (hopefully never needed).
!
!Multi-User Issues:  INIX 5.0(601) uses XLOCK to prevent simultaneous
!             updating of the INI file, or loading of it by one user
!             while another is writing.  The danger was pretty minimal
!             but possibly a simultaneous update could have clobbered
!             the INI in the 4.x version.  Also, INIX 5.X makes a .BAK
!             copy of the INI whenever updating it.
!--------------------------------------------------------------------
++pragma SBX
++pragma ERROR_IF_NOT_MAPPED "ON"

!++INCLUDE ashinc:xcall.bsi
++include'once ashinc:ashell.def
++include'once ashinc:types.def          ! [515]

++include'once sosfunc:fnextch.bsi       ! Fn'NextCh(startch)
++include'once sosfunc:fnameext.bsi      ! Fn'Name'Ext$(fspec$)
++include'once sosfunc:fndec2hex.bsi     ! Fn'Dec2Hex$(value,width,flags)
++include'once sosfunc:fntempfile.bsi    ! [515] Fn'Temp'Dir$()

! opcode definitions
DEFINE INXOP_READ       = 0
DEFINE INXOP_WRITE      = 1
DEFINE INXOP_DELITM     = 2
DEFINE INXOP_DELSEC     = 3
DEFINE INXOP_ATE_READ   = &h10      ! [511]
DEFINE INXOP_ATE_WRITE  = &h11      ! [511]
DEFINE INXOP_ATE_DELITM = &h12      ! [511]
DEFINE INXOP_ATE_DELSEC = &h13      ! [511]
DEFINE INXOP_ATE_SYNC   = &h20      ! [511] add to sync SBX with ATE client
DEFINE INXOP_TIMEOUT_ABORT = &h40   ! [520] message boxes time out after 15 secs and then abort
DEFINE INXOP_TIMEOUT_RETRY = &h80   ! [520] message boxes time out after 15 secs and then retry

! definitions for MX_USRIO modes
DEFINE USRIO_READ = 0
DEFINE USRIO_WRITE = 1
DEFINE USRIO_SEARCHI = 4    ! case insensitive search
DEFINE USRIO_SEARCH = 5     ! case sensitive search

! define module header size
DEFINE MOD_HDR_SIZE = 22        ! ########,###########CRLF  (size,timestamp)

! define special code value for deleting an item
DEFINE DELETE_IDENT$ = "<<<DELETE THIS IDENT>>>"


!==============================================================================
!{Map Zone}
!==============================================================================

++pragma PRIVATE_BEGIN
    MAP1 TIMEOUT'RETRY,BOOLEAN          ! [520]
    MAP1 TIMEOUT'ABORT,BOOLEAN          ! [520] 
    MAP1 TIMEOUT'SECS,B,2               ! [520] 
++pragma PRIVATE_END

MAP1 PARAMS
	MAP2 INIFILE,S,512
	MAP2 TYPEX,B,1
	MAP2 SECTION,S,60
	MAP2 IDENT,S,60
	MAP2 VALUEX,S,2000
	MAP2 DEFAULTX,S,2000

MAP1 MISC
    MAP2 RC,B,1
    MAP2 AGPARMS,S,3000       ! [511]
    
!==============================================================================
!{We start for real here.}
!==============================================================================
 	on error GOTO TRAP

    ! pick up all args
    xgetargs INIFILE,TYPEX,SECTION,IDENT,VALUEX,DEFAULTX

    ! [511] check if we should try to sync ourselves with the client INIX first...
    IF TYPEX and INXOP_ATE_SYNC THEN
        IF Fn'Sync'SBX'to'ATE("INIX.SBX") < 0 THEN
            VALUEX = "-1"
            xputarg 5, VALUEX
            return(VALUEX)
            end
        ENDIF
        TYPEX = TYPEX and NOT INXOP_ATE_SYNC
    ENDIF

    ! [520] xfer INXOP_TIMEOUT_xx options to global and remove them from TYPEX
    if TYPEX and INXOP_TIMEOUT_RETRY then
        TIMEOUT'RETRY = .TRUE
        TIMEOUT'SECS = 15
    elseif TYPEX and INXOP_TIMEOUT_ABORT then
        TIMEOUT'ABORT = .TRUE
        TIMEOUT'SECS = 15
    endif
    TYPEX = TYPEX and not (INXOP_TIMEOUT_RETRY or INXOP_TIMEOUT_ABORT)

    switch TYPEX
        case INXOP_READ      ! read item
     	    VALUEX = Fn'Read'INI$(INIFILE,SECTION,IDENT,DEFAULTX)
            xputarg 5, VALUEX
            return(VALUEX)           ! [510]
            exit
        case INXOP_WRITE      ! write item
        	call Write'INI'File(INIFILE,SECTION,IDENT,VALUEX)
        	exit
        case INXOP_DELITM      ! delete an item
	        call Write'INI'File(INIFILE,SECTION,IDENT,DELETE_IDENT$)
	        exit
        case INXOP_DELSEC      ! delete an entire section
        	call Write'INI'File(INIFILE,SECTION,"","")
        	exit
        case INXOP_ATE_READ     ! [511] read from ATE
            AGPARMS = "INIX,"+INIFILE+",0,"+SECTION+","+IDENT+",,"+DEFAULTX
            XCALL MIAMEX, MX_AGWRAPPER, AG_XFUNCS, AGPARMS, VALUEX
            xputarg 5, VALUEX
            return(VALUEX)
            exit
        case INXOP_ATE_WRITE     ! [511] write to ATE
            AGPARMS = "INIX,"+INIFILE+",1,"+SECTION+","+IDENT+","+VALUEX
            ? TAB(-10,AG_XFUNC2);AGPARMS;chr(127);  ! no response to write
            exit
        case INXOP_ATE_DELITM     ! [511] delete item from ATE
            AGPARMS = "INIX,"+INIFILE+",2,"+SECTION+","+IDENT+","+DELETE_IDENT$
            ? TAB(-10,AG_XFUNC2);AGPARMS;chr(127);  ! no response to delete
            exit
        case INXOP_ATE_DELSEC     ! [511] delete entire section from ATE
            AGPARMS = "INIX,"+INIFILE+",3,"+SECTION+",,,"
            ? TAB(-10,AG_XFUNC2);AGPARMS;chr(127);  ! no response to write
            exit

        default
            XCALL MSGBOX,"Invalid opcode: "+TYPEX &
                + chr(13)+chr(10)+"(Use CANCEL for Control-C)", &
                "INIX.SBX",MBTN_OK_CANCEL,MBICON_STOP, &
                MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0), RC, 0, TIMEOUT'SECS      ! [520]
            IF RC = MBRC_CANCEL THEN
                XCALL ASFLAG,&h080  ! send ^C to parent
            ENDIF
    endswitch

    End     ! back to caller

!----------------------------------
! On Error Trap
!----------------------------------
TRAP:
    ![520] XCALL MESAG,"INIX ERROR: "+STR(ERR(0))+" ON CHAN "+STR(ERR(2))+ &
    ![520]         ", LINE "+STR(ERR(1))+", FILE: "+INIFILE+", LOC: "+Fn'Dec2Hex$(err(8)),2
    xcall MSGBOX,"Error #" + str(err(0)) + " channel " + str(err(2)) &
        + "; File: " + INIFILE + ", location " + str(err(8)), "INIX Error", &
        MBTN_OK, MBICON_STOP, MBMISC_TOPMOST, RC, &
        0, TIMEOUT'SECS            ! [520] 15 second timeout
        
    XCALL ASFLAG,&h080   ! send ^C to caller
	END			         ! return to caller

!-------------------------------------------------------------------------
!Function: Fn'Load'INI(fspec$)
!Parameters:
!   fspec [string, in] - ini file fspec
!
!Returns:
!   >0 = module index # if module in memory (for direct access)
!   -1 = fspec does not exist
!   -2 = module has bad header
!   -3 = unable to lock module [601]
!Globals:
!   none
!Module Variables:
!  none
!Error handling:
!   none
!Notes:
!   Checks if version in memory matches the one on disk.  If not, load
!   it.  Add 8K of extra space to end when loading a new copy.  If
!   refreshing an existing module, just reuse it if big enough.
!
!   First line of module will contain a header, which can be used to
!   determine if it matches the version on disk, and also to know how
!   to append:
!       <size>,<timestamp>
!
!   lock INI during load operation to make sure we don't load it while
!   it is being written
!-------------------------------------------------------------------------
FUNCTION Fn'Load'INI(fspec$ as T_NATIVEPATH:inputonly) as f

    DEFINE MODBUF_SIZE = 4096    ! misc buffer for loading data

    MAP1 locals
        MAP2 f'bytes,f           ! file size in bytes
        MAP2 m'bytes,f           ! module overall size in bytes
        MAP2 m'lbytes,f          ! size of module that is in use
        MAP2 bytes'read,f        ! bytes read in last file op
        MAP2 f'mtime,b,4         ! file mod time
        MAP2 m'mtime,b,4         ! module mod time
        MAP2 m'flags,b,4         ! module flags
        MAP2 modname$,s,64       ! module name [507] was s,14
        MAP2 save'modname$,s,14  ! saved copy
        MAP2 modhdr$,s,MOD_HDR_SIZE        ! module header ########,###########<CRLF>
        MAP2 midx,f              ! module index
        MAP2 status,f
        MAP2 ch,b,2
        MAP2 ch2,b,2
        MAP2 buf4k$,s,MODBUF_SIZE
        MAP2 bufsiz,b,4
        MAP2 x,f
        MAP2 moffset,b,4
        MAP2 jobno,b,2
        MAP2 temp'modname$,T_NATIVEPATH     ! [515] file on 

    ! get the stats on the file on disk
    XCALL MIAMEX, MX_FILESTATS, "L", fspec$, f'bytes, f'mtime
    trace.print (99, "inixverbose") "$# $P", ABC_CURRENT_ROUTINE$,fspec$,f'bytes,f'mtime
    IF f'bytes < 0 THEN
        Fn'Load'INI = -1
        EXITFUNCTION        ! return -1 = file not found
    ENDIF

    ! modname$ = Fn'Name'Ext$(fspec$)

    XCALL JOBNUM,jobno
    modname$ = Fn'Name'Ext$(fspec$)

    x = instr(1,modname$,".")
    IF x > 0 THEN                           ! temp file is <file>.<jobno>
       modname$ = modname$[1,x] + "x" +  (jobno using "#ZZ")  ! [514] was + str(jobno),  now fname.x#zz
    ELSE
       x = len(modname$) + 1                ! [507] x = pos of .
       modname$ = strip(modname$) + ".x" + (jobno using "#ZZ") ! [514] was "." + str(jobno)
    ENDIF

    ! [507] take last 10.4 of name if longer    [513] was 10.3
    IF x > 11 THEN                      ! if more than 10.x,
        modname$ = modname$[x-10,-1]    ! trim start of name
    ENDIF
    IF len(modname$) > 15 THEN          ! if still too long  ! [514] was > 14
        modname$ = modname$[1,15]       ! trim end off extension
    ENDIF

    ! [508] replace any spaces with _
    DO
        x = instr(1,modname$," ")
        IF x > 0 THEN
            modname$[x,x] = "_"
        ELSE
            exit
        ENDIF
    LOOP

    save'modname$ = modname$   ! save copy (MX_USRMAP may wipe it out)

    ! check if the memory module exists
    XCALL MIAMEX, MX_USRMAP, midx, modname$, m'bytes, m'flags
    modname$ = save'modname$   ! MX_USRMAP wipes out modname$ if not found
    trace.print (99, "inixverbose") "$#", midx,modname$,m'bytes,m'flags

    IF midx > 0 THEN
        ! if the module exists, get its timestamp
        XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, modhdr$, 0, MOD_HDR_SIZE
        trace.print (99, "inixverbose") "$#", status,modname$modhdr$
        x = instr(1,modhdr$,",")
        IF x > 0 THEN
            m'lbytes = modhdr$[1,x-1]
            m'mtime = modhdr$[x+1,-1]
        ELSE
            Fn'Load'INI = -2    ! invalid header
            EXITFUNCTION
        ENDIF

        ! if the module timestamp matches the file's, no action needed
        IF f'mtime = m'mtime and f'bytes = m'lbytes THEN
            trace.print (99, "inixverbose") "$# matches memory module size and timestamp; no reload needed"
            Fn'Load'INI = midx
            EXITFUNCTION
        ENDIF

        ! we need to load module into memory; but will it fit?
        IF f'bytes >=  (m'bytes - MOD_HDR_SIZE) THEN
            ! won't fit, delete current module
            XCALL MIAMEX,MX_USRDEL, midx, modname$, USRMEM_UNLOCK
            trace.print (99, "inixverbose") "$#",f'bytes,m'bytes,"deleting old module"
            m'bytes = 0
            midx = 0
        ENDIF
    ENDIF

    ! [601] Test for lock on the file before we start loading it from INI
    IF Fn'Lock'INI(fspec$, 4) THEN        ! check if lock available (without setting it)
        Fn'Load'INI = -3    ! unable to lock
        EXITFUNCTION
    ENDIF

    IF midx = 0 THEN     ! create new module from scratch

        !===============================================================================
        ! Method 1: use DIMX to create a buffer 8K larger than the file, then
        ! read the file into the buffer and the buffer into the memory module

            ! allocate buffer allowing 8K of expansion

            !            bufsiz = f'bytes + 8196 + MOD_HDR_SIZE
            !            dimx modbuf(1),x,bufsiz
            !            trace.print (99, "inixverbose") "$# Fn'Load'INI: dimx bufsiz=["+bufsiz+"] "
            !
                        ! load file into buffer
            !            ch = Fn'NextCh(60000)
            !            open #ch, fspec$, input
            !            xcall GET, modbuf(1), ch, f'bytes, bytes'read
            !            close #ch
            !
                        ! load the buffer into a memory module
            !            xcall MIAMEX, MX_USRLOD, midx, modname$, USRMEM_NOFILE+USRMEM_CACHE, modbuf(1), 1
            !            trace.print (99, "inixverbose") "$# Fn'Load'INI MX_USRLOD: midx=["+midx+"] modname$=["+modname$+"] "

            !        if midx <= 0 then
            !            Fn'Load'INI = -3        ! problem loading module
            !            ExitFUNCTION
            !        endif
            ! (fall through to load logic)
        !===============================================================================

        !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ! Method 2: copy the ini file to a temp file and append 8K of nulls, then load
        ! it directly into memory module.  (Avoids need for DIMX)
        ! WARNING: original INI file better not have a numeric extension matching job#!!!
        ch = Fn'NextCh(60000)
        open #ch, fspec$, input

        ! check for possible conflict between original INI and tmp copy
        IF ucs(Fn'Name'Ext$(fspec$)) = ucs(modname$) THEN
            XCALL MSGBOX, "Illegal INI file extension ("+modname$+")", &
                "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
                MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0),x, 0, TIMEOUT'SECS      ! [520]
            close #ch
            IF x = MBRC_CANCEL THEN
                XCALL ASFLAG,&h080  ! send ^C to parent
            ENDIF

            EXITFUNCTION
        ENDIF

        temp'modname$ = Fn'Temp'Dir$() + modname$       ! [515]
        trace.print (99, "inixverbose") "$# copying ",fspec$,temp'modname$
        ch2 = Fn'NextCh(ch)
        open #ch2, temp'modname$, output      ! [515]

        ! Add header to start of temp file
        modhdr$ = (f'bytes using "########") +"," + (f'mtime using "###########") + chr(13) + chr(10)
        XCALL PUTBYT, ch2, modhdr$, MOD_HDR_SIZE

        DO
            buf4k$=""
            XCALL GET, buf4k$, ch, MODBUF_SIZE, bytes'read
            IF bytes'read > 0 THEN
                XCALL PUTBYT, ch2, buf4k$, bytes'read
            ENDIF
        LOOP UNTIL bytes'read <= 0

        ! file now copied; add 8K worth of nulls
        close #ch
        buf4k$ = ""
        XCALL PUTBYT, ch2, buf4k$, MODBUF_SIZE
        XCALL PUTBYT, ch2, buf4k$, MODBUF_SIZE
        close #ch2

        ! now load the module directly into memory
        XCALL MIAMEX,MX_USRLOD, midx, temp'modname$,USRMEM_CACHE    ! [515]

        kill temp'modname$  ! [515] delete temp file

        Fn'Load'INI = midx
        trace.print (99, "inixverbose") "$# New module",modname$,modhdr$,f'bytes
        EXITFUNCTION
        !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    ENDIF

    ! load file into existing memory module

    ! first write a new header
    modhdr$ = (f'bytes using "########") +"," + (f'mtime using "###########") + chr(13) + chr(10)
    XCALL MIAMEX,MX_USRIO, status, midx, USRIO_WRITE, modhdr$, 0, MOD_HDR_SIZE
    moffset = MOD_HDR_SIZE
    trace.print (99, "inixverbose") "$# Update Module",modhdr$

    ! now load the file in 4K chunks and copy to module
    ch = Fn'NextCh(60000)
    open #ch, fspec$, input
    DO
        buf4k$=""
        XCALL GET, buf4k$, ch, MODBUF_SIZE, bytes'read
        trace.print (99, "inixverbose") "$#",bytes'read

        IF bytes'read > 0 THEN
            XCALL MIAMEX, MX_USRIO, status, midx, USRIO_WRITE, buf4k$, moffset, bytes'read
            moffset = moffset + bytes'read
        ENDIF
    LOOP UNTIL bytes'read <= 0
    close #ch

    ! last, if new module smaller than old one, write nulls to the end to clear out leftover bytes
    DO WHILE m'lbytes > f'bytes
        buf4k$ = ""
        m'bytes = (m'lbytes - f'bytes) min MODBUF_SIZE
        XCALL MIAMEX, MX_USRIO, status, midx, USRIO_WRITE, buf4k$, moffset, m'bytes
        trace.print (99, "inixverbose") "$# MX_USRIO nulls to clear old module data",m'bytes,moffset,status
        m'lbytes = m'lbytes - m'bytes
    LOOP

    Fn'Load'INI = midx  ! return module idx on success
    trace.print (99, "inixverbose") "$# New/updated ",modhdr$,f'bytes,bufsiz

End FUNCTION


!-------------------------------------------------------------------------
!Function: Fn'Read'INI$(inifile$,section$,ident$,default$)
!   Retrieve an item from the ini file
!Parameters:
!   inifile$,s,120      [in]
!   section$,s,60       [in]
!   ident$,s,60         [in]
!   default$,s,2000     [in]
!
!Returns:
!   value$
!Globals:
!   none
!Error handling:
!   none
!Notes:
!   Operates on the copy of the ini file in memory, loading/reloading
!   it (via Fn'Load'INI) if needed.
!-------------------------------------------------------------------------
FUNCTION Fn'Read'INI$(inifile$ as s120, section$ as s60, ident$ as s60, default$ as s2000) as s2000

    MAP1 locals
        MAP2 linebuf$,s,2000
        MAP2 sectionpos,f,6
        MAP2 itempos,f,6
        MAP2 nextsectionpos,f,6
        MAP2 status,f
        MAP2 x,f
        MAP2 midx,i,2
	MAP2 does'it'exist,f,6                                     ! [503]

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,inifile$,section$,ident$,default$

    LOOKUP inifile$,does'it'exist                                  ! [503]
    IF does'it'exist=0 THEN                                        ! [503]
        Fn'Read'INI$ = default$                                    ! [504]
        trace.print (99, "inix") inifile$,"$# INI did not exist."                 ! [503]
     EXITFUNCTION                          			   ! [503]
    ENDIF

    midx = Fn'Load'INI(inifile$)
    IF midx < 0 THEN
        trace.print (99, "inix") "$# Unable to load INI file!"
        Fn'Read'INI$ = default$
        EXITFUNCTION
    ENDIF

    ! now use MX_USRIO search facility to see if the section is there
    !xcall MIAMEX, MX_USRIO, sectionpos, midx, USRIO_SEARCH, "["+section$+"]", 0
    sectionpos = Fn'Module'Instr(midx,"["+section$+"]",0)
    trace.print (99, "inixverbose") "$#", sectionpos,midx

    IF sectionpos < 0 THEN      ! section not found
        Fn'Read'INI$ = default$
        EXITFUNCTION
    ENDIF

    ! OK, so we have the start of the section; let's see if the item
    ! can be found somewhere after that
    ! xcall MIAMEX, MX_USRIO, itempos, midx, USRIO_SEARCH, chr(10)+ident$+"=",sectionpos+1
    itempos = Fn'Module'Instr(midx,chr(10)+ident$+"=",sectionpos+1)
    trace.print (99, "inixverbose") "$#", itempos,midx,sectionpos

    IF itempos < 0 THEN         ! nope
        Fn'Read'INI$ = default$
        EXITFUNCTION
    ENDIF

    ! Now, the question is: is the item within the section?
    ! (We have to find the start of the next section to know)
    !xcall MIAMEX, MX_USRIO, nextsectionpos, midx, USRIO_SEARCH, chr(10)+"[", sectionpos+1
    nextsectionpos = Fn'Module'Instr(midx,chr(10)+"[",sectionpos+1)
    trace.print (99, "inixverbose") "$#",nextsectionpos,midx,sectionpos

    IF nextsectionpos > 0 and nextsectionpos < itempos THEN ! nope; item in other section
        Fn'Read'INI$ = default$
        EXITFUNCTION
    ENDIF

    ! OK, we know the item is present and where it is - just read the line now
    ! (we don't have a read-line function so read 4K and then truncate)
    linebuf$ = ""
    XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, linebuf$, itempos+1
    trace.print (99, "inixverbose") "$#",status,midx,itempos,linebuf$[1,40]

    x = instr(1,linebuf$, chr(10))
    IF x > 0 THEN
        IF linebuf$[x-1;1] = chr(13) THEN
            linebuf$ = linebuf$[1,x-2]      ! truncate at CRLF
        ELSE
            linebuf$ = linebuf$[1,x-1]      ! truncate at LF
        ENDIF
    ENDIF

    x = instr(1,linebuf$,"=")
    IF x > 0 THEN
        Fn'Read'INI$ = linebuf$[x+1,-1]
    ELSE
        Fn'Read'INI$ = "Line format error: "+linebuf$ ! this should be impossible
    ENDIF

End FUNCTION

!-------------------------------------------------------------------------
!Procedure: Write'INI'File(inifile$,section$,ident$,value$)
!   Update the INI (add/replace/delete an item or delete a section)
!Parameters:
!   inifile$,s,120 [in]
!   section$,s,60  [in]
!   ident$,s,60    [in]   If NULL, we delete the section$
!   value$,s,2000  [in]   If DELETE_IDENT$ then delete the item
!Globals:
!   none
!Error handling:
!   none
!Notes:
!   We use memory copy (loading/reloading it if not current) to check if
!   item is already there and as a source for updating the file, but
!   but we do all the updates to the file on disk.  (It will be left to
!   the next call to INIX to reload the updated file from disk.)
!   All actual writing, other than the one case of a new
!   INI file, is handled by the following procedures:
!
!       Write'New'Section()
!       Append'To'Section()
!       Replace'Item()
!       Delete'Section()
!       Fn'Mod'To'Disk()
!
!   We lock the file during the write, using Fn'Lock'INI
!   [506] Clear module time stamp to force a reload on next access!!!
!-------------------------------------------------------------------------
PROCEDURE Write'INI'File(inifile$ as s120, section$ as s60, ident$ as s60, value$ as s2000)

    MAP1 locals
        MAP2 f'bytes,f           ! file size in bytes
        MAP2 linebuf$,s,80
        MAP2 sectionpos,f,6
        MAP2 itempos,f,6
        MAP2 nextsectionpos,f,6
        MAP2 status,f
        MAP2 x,f
        MAP2 y,f
        MAP2 ch,b,2
        MAP2 midx,i,2
        MAP2 modhdr$,s,16       ! [506]
        MAP2 updated,b,1        ! [519]

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,inifile$,section$,ident$,value$

    ! [519] move this lock up before the file-exists test (it could be that
    ! [519] it appears not to exist in the sliver of time between it getting
    ! [519] renamed and rewritten by another job.
    IF Fn'Lock'INI(inifile$, 1) THEN
        EXITPROCEDURE       ! unable to place lock
    ENDIF

    ! see if file exists.  if not, create it and return
    if LOOKUP(inifile$)=0 THEN
        call Write'New'Section(inifile$, section$, ident$, value$)
        updated = 1         ![519] file updated
        EXITPROCEDURE       ![519]GOTO Procedure'exit
    ENDIF

    ! file exists, but let's operate on copy in memory
    midx = Fn'Load'INI(inifile$)
    IF midx < 0 THEN
        ! error - we probably should return an error code but
        ! since this is so rare a condition, we just print a messagebox
        XCALL MSGBOX, "Unable to load INI file ["+inifile$+"].  " &
            + chr(13)+chr(10)+"Can't write "+ident$+"="+value$ &
            + chr(13)+chr(10)+" in ["+section$+"]", &
            "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
            MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0), x, 0, TIMEOUT'SECS      ! [520]
            
        IF x = MBRC_CANCEL THEN
            XCALL ASFLAG,&h080  ! send ^C to parent
        ENDIF
        EXITPROCEDURE
    ENDIF

    ![601] place lock (no need if creating new file in above case)
    ![519] IF Fn'Lock'INI(inifile$, 1) THEN
    ![519]     EXITPROCEDURE       ! unable to place lock
    ![519] ENDIF

    ! now see if section exists
    !xcall MIAMEX, MX_USRIO, sectionpos, midx, USRIO_SEARCH, "["+section$+"]", 0
    sectionpos = Fn'Module'Instr(midx,"["+section$+"]",0)
    IF sectionpos < 0 THEN      ! no - add the entire section
        IF ident$ # "" THEN     !
            call Write'New'Section(inifile$, section$, ident$, value$)
            updated = 1         ![519] file updated
        ELSE
            ! we were asked to delete a section that doesn't exist
            ! (so do nothing)
        ENDIF
        EXITPROCEDURE       ![519]GOTO Procedure'exit
    ENDIF

    ! locate start of next section
    !xcall MIAMEX, MX_USRIO, nextsectionpos, midx, USRIO_SEARCH, chr(10)+"[", sectionpos+1
    nextsectionpos = Fn'Module'Instr(midx,chr(10)+"[",sectionpos+1)
    trace.print (99, "inixverbose") "$#",nextsectionpos,midx,sectionpos

    IF ident$ # "" THEN         ! if we are updating an item (rather than deleting section)...

        ! see if the ident is found in the section
        !xcall MIAMEX, MX_USRIO, itempos, midx, USRIO_SEARCH, chr(10)+ident$+"=",sectionpos+1
        itempos = Fn'Module'Instr(midx,chr(10)+ident$+"=",sectionpos+1)
        trace.print (99, "inixverbose") "$#",itempos,midx,sectionpos

        IF itempos > 0 and (itempos < nextsectionpos or nextsectionpos < 0) THEN ! item found in section; replace it
            call Replace'Item(midx, inifile$, itempos+1, ident$, value$)
            updated = 1         ![519] file updated
        ELSE
            IF ident$ # "" THEN
                ! The following adds the new item at the end of the section
                ! call Append'To'Section(midx, inifile$, nextsectionpos+1, ident$, value$)
                ! Alternatively, to match the old INIX, we can add it at the start...
                ! read a line starting at section head to get position of first item
                XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, linebuf$, sectionpos, 80
                x = instr(2,linebuf$,chr(10))
                IF x > 0 THEN
                    call Append'To'Section(midx, inifile$, sectionpos+x, ident$, value$)
                    updated = 1         ![519] file updated
                ELSE
                    XCALL MSGBOX, "Error in format of section header: " &
                        + chr(13)+chr(10)+linebuf$ &
                        + chr(13)+chr(10)+"Can't add "+ident$+"="+value$ &
                        + chr(13)+chr(10)+" in ["+section$+"]", &
                        "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
                        MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0),x, 0, TIMEOUT'SECS      ! [520]
                        
                    IF x = MBRC_CANCEL THEN
                        XCALL ASFLAG,&h080  ! send ^C to parent
                    ENDIF
                    EXITPROCEDURE       ![519]GOTO Procedure'exit
                ENDIF
            ELSE
                ! we were asked to delete an item that doesn't exist
                ! so do nothing
            ENDIF
        ENDIF

    ELSE                        ! we are deleting an entire section

        call Delete'Section(midx, inifile$, sectionpos, nextsectionpos+1)
        updated = 1         ![519] file updated
    ENDIF

![519] Procedure'exit:     ! ugly but convenient way to make sure we release lock
$EXIT:                      ! [519] modern way to force exiting handling

    if updated then         ![519]
        ![506] update the module header to force it to be reloaded, since the
        ![506] write operation didn't update the module in memory
        ![506] (skip over 1st 9 bytes where module size is)
        modhdr$ = (0 using "###########") + chr(13) + chr(10)
        XCALL MIAMEX,MX_USRIO, status, midx, USRIO_WRITE, modhdr$, 9, MOD_HDR_SIZE-9
        trace.print (99, "inixverbose") "$# Resetting module timestamp"
    endif
    
    call Fn'Lock'INI(inifile$, 2)    ! clear lock

End PROCEDURE


!-------------------------------------------------------------------------
!Procedure: Write'New'Section(inifile$,section$,ident$,value$)
!   Write a new section (possibly the first section)
!Parameters:
!   inifile$,s,120 [in]
!   section$,s,60  [in]
!   ident$,s,60    [in]
!   value$,s,2000  [in]
!Globals:
!   none
!Module Variables:
!  none
!Error handling:
!   Allow up to 20 retries (in case of file in use conflict) [517]
!Notes:
!   Since this is a new section, we can just append it to the end of the
!   file.  If the file didn't previously exist, this creates it.
!-------------------------------------------------------------------------
 PROCEDURE Write'New'Section(inifile$ as s120, section$ as s60, ident$ as s60, value$ as s2000)

    MAP1 locals
        MAP2 ch,b,2
        MAP2 errcnt,b,2         ! [517]
        MAP2 jobnum,b,2         ! [519]
        
    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,inifile$,section$,ident$,value$

    on error goto TRAP          ! [517]
    
    ch = Fn'NextCh(60000)
APPND:                          
    open #ch, inifile$, append, wait'file  ! note: append acts like output if file doesn't exist
                                           ! [517] also: wait for exclusive access (Windows only)
    ? #ch, "[";section$;"]"
    ? #ch, ident$;"=";value$
    close #ch

    exitprocedure
    
TRAP:                               ! [517]
    errcnt += 1                 
    if errcnt <= 20 then            ! allow up to 20 retries
        if err(0) > 3 then          ! anything but ^C, system error, out of memory
            trace.print (99, "inix") "$# Error "+err(0) + "; retrying ..."
            xcall MIAMEX, MX_ASHLOG, "Retry Write'New'Section #" + errcnt + " on error "+err(0)     ! [518]
            xcall JOBNUM, jobnum    ! [519]
            sleep 0.15 * jobnum     ! [519]
            resume APPND
        endif
    else
        trace.print (99, "inix") "$# Error "+err(0) + "; giving up"
    endif
    
    resume endprocedure with_error err(0)    ! else return error

End PROCEDURE

!-------------------------------------------------------------------------
!Procedure: Append'To'Section(midx,inifile$,pos,ident$,value$)
!   Append a new item to an existing section
!Parameters:
!   midx,i,2       [in]  module index
!   inifile$,s,120 [in]
!   pos,b,4        [in]  offset to end of existing section (or <=0)
!                        note: includes module header of MOD_HDR_SIZE bytes
!   ident$,s,60    [in]
!   value$,s,2000  [in]
!Globals:
!   none
!Module Variables:
!  none
!Error handling:
!   Allow up to 20 retries (in case of file in use conflict) [518]
!Notes:
!-------------------------------------------------------------------------
 PROCEDURE Append'To'Section(midx as i2, inifile$ as s120, pos as i4, ident$ as s60, value$ as s2000)

    MAP1 locals
        MAP2 ch,b,2
        MAP2 modhdr$,s,MOD_HDR_SIZE
        MAP2 status,f
        MAP2 modbytes,b,4
        MAP2 x,f
        MAP2 errcnt,b,2
        MAP2 jobnum,b,2         ! [519]

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,midx,inifile$,pos,ident$,value$

    on error goto TRAP          ! [518]
    
    ! get module size from header
    ! if the module exists, get its timestamp
    XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, modhdr$, 0, MOD_HDR_SIZE

    trace.print (99, "inixverbose") "$# MX_USRIO",status,midx,modhdr$
    x = instr(1,modhdr$,",")
    IF x > 0 THEN
        modbytes = modhdr$[1,x-1]
    ELSE
        XCALL MSGBOX, "Error reading from module # "+midx+".  " &
            + chr(13)+chr(10)+"Can't append "+ident$+"="+value$, &
            "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
            MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0),x, 0, TIMEOUT'SECS      ! [520]

        IF x= MBRC_CANCEL THEN
            XCALL ASFLAG,&h080  ! send ^C to parent
        ENDIF
        EXITPROCEDURE
    ENDIF

    call Backup'INI(inifile$)   ! [601] create .BAK copy

    ch = Fn'NextCh(60000)
APPND:                              
    open #ch, inifile$, output  ! we are replacing old file

    IF pos <= 0 THEN
        pos = modbytes + MOD_HDR_SIZE
    ENDIF

    ! copy from memory module start to End of section to file
    IF Fn'Mod'To'Disk(midx, ch, MOD_HDR_SIZE, pos) < 0 THEN
        close #ch
        EXITPROCEDURE
    ENDIF

    ! output the item
    PRINT #ch, ident$;"=";value$
    trace.print (99, "inixverbose") "$#",ch,ident$,value$


    ! output the remainder of the module to disk (if any)
    IF pos < (modbytes + MOD_HDR_SIZE) THEN
        call Fn'Mod'To'Disk(midx, ch, pos, modbytes+MOD_HDR_SIZE)
    ENDIF

    close #ch

    exitprocedure
    
TRAP:                               ! [518]
    errcnt += 1                 
    if errcnt <= 20 then            ! allow up to 20 retries
        if err(0) > 3 then          ! anything but ^C, system error, out of memory
            trace.print (99, "inix") "$# Error "+err(0) + "; retrying ..."
            xcall MIAMEX, MX_ASHLOG, "Retry Append'Section #" + errcnt + " on error "+err(0)     ! [518]
            xcall JOBNUM, jobnum    ! [519]
            sleep 0.25 * jobnum     ! [519]
            resume APPND
        endif
    else
        trace.print (99, "inix") "$# Error "+err(0) + "; giving up"
    endif
    
    resume endprocedure with_error err(0)    ! else return error


End PROCEDURE

!-------------------------------------------------------------------------
!Procedure: Replace'Item(midx,inifile$,pos,ident$,value$)
!   Replace (or delete) an individual item
!Parameters:
!   midx,i,2       [in]  module index
!   inifile$,s,120 [in]
!   pos,b,4        [in]  offset to insertion point (or -1 to append)
!                        note: includes module header of MOD_HDR_SIZE bytes
!   ident$,s,60    [in]
!   value$,s,2000  [in]  if DELETE_IDENT$ then delete the item
!Globals:
!   none
!Error handling:
!   Allow up to 20 retries (in case of file in use conflict) [518]
!Notes:
!   Similar to Append'To'Section() except that we don't output the
!   replaced item.
!-------------------------------------------------------------------------
 PROCEDURE Replace'Item(midx as i2, inifile$ as s120, pos as i4, ident$ as s60, value$ as s2000)

    MAP1 locals
        MAP2 ch,b,2
        MAP2 modhdr$,s,MOD_HDR_SIZE
        MAP2 linebuf$,s,2000
        MAP2 status,f
        MAP2 modbytes,b,4
        MAP2 x,f
        MAP2 errcnt,b,2
        MAP2 jobnum,b,2         ! [519]
        
    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,midx,inifile$,pos,ident$,value$
    
    on error goto TRAP          ! [518]
    
    ! get module size from header
    ! if the module exists, get its timestamp
    XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, modhdr$, 0, MOD_HDR_SIZE

    trace.print (99, "inixverbose") "$# MX_USRIO",status,midx,modhdr$
    x = instr(1,modhdr$,",")
    IF x > 0 THEN
        modbytes = modhdr$[1,x-1]
    ELSE
        XCALL MSGBOX, "Error reading from module # "+midx+".  " &
            + chr(13)+chr(10)+"Can't append "+ident$+"="+value$, &
            "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
            MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0),x, 0, TIMEOUT'SECS      ! [520]
        IF x = MBRC_CANCEL THEN
            XCALL ASFLAG,&h080  ! send ^C to parent
        ENDIF
        EXITPROCEDURE
    ENDIF

    call Backup'INI(inifile$)   ! [601] create .BAK copy

    ch = Fn'NextCh(60000)
REPLACE:    
    open #ch, inifile$, output  ! we are replacing old file

    ! copy first part of module to ini file on disk
    IF Fn'Mod'To'Disk(midx, ch, MOD_HDR_SIZE, pos) < 0 EXITPROCEDURE

    IF value$ # DELETE_IDENT$ THEN
        ! output the replacement item
        PRINT #ch, ident$;"=";value$
        trace.print (99, "inixverbose") "$#",ch,ident$,value$
    ENDIF

    ! now we have to find the end of the item being replaced, so we can skip it.
    XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, linebuf$, pos, 2000

    x = instr(1,linebuf$,chr(10))
    IF x > 0 THEN
        linebuf$ = linebuf$[1,x]        ! truncate past LF
    ENDIF
    pos = pos + len(linebuf$)

    ! now output the remainder of the module from memory
    IF pos < (modbytes+MOD_HDR_SIZE) THEN
        call Fn'Mod'To'Disk(midx, ch, pos, modbytes+MOD_HDR_SIZE)
    ENDIF

    close #ch

    exitprocedure
    
TRAP:                               ! [518]
    errcnt += 1                 
    if errcnt <= 20 then            ! allow up to 20 retries
        if err(0) > 3 then          ! anything but ^C, system error, out of memory
            trace.print (99, "inix") "$# Error "+err(0) + "; retrying ..."
            xcall MIAMEX, MX_ASHLOG, "Retry Replace'Item #" + errcnt + " on error "+err(0)     ! [518]
            xcall JOBNUM, jobnum    ! [519]
            sleep 0.3 * jobnum      ! [519]
            resume REPLACE
        endif
    else
        trace.print (99, "inix") "$# Error "+err(0) + "; giving up"
    endif
    
    resume endprocedure with_error err(0)    ! else return error

End PROCEDURE

!-------------------------------------------------------------------------
!Procedure: Delete'Section(midx,inifile$,sectpos,nextsectpos)
!   Delete an entire section
!Parameters:
!   midx,i,2        [in]  module index
!   inifile$,s,120  [in]  inifile spec
!   sectpos,i,4     [in]  offset to start of section (includes MOD_HDR_SIZE)
!   nextsectpos,i,4 [in]  offset to start of following section (or -1)
!Globals:
!   none
!Error handling:
!   Allow up to 20 retries (in case of file in use conflict) [518]
!Notes:
!-------------------------------------------------------------------------
 PROCEDURE Delete'Section(midx as i2, inifile$ as s120, sectpos as i4, nextsectpos as i4)

    MAP1 locals
        MAP2 ch,b,2
        MAP2 modhdr$,s,MOD_HDR_SIZE
        MAP2 status,f
        MAP2 modbytes,b,4
        MAP2 x,f
        MAP2 errcnt,b,2
        MAP2 jobnum,b,2         ! [519]
        
    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,midx,inifile$,sectpos,nextsectpos
    
    on error goto TRAP          ! [518]
    
    ! get module size from header
    ! if the module exists, get its timestamp
    XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, modhdr$, 0, MOD_HDR_SIZE
    trace.print (99, "inixverbose") "$# MX_USRIO",status,midx,modhdr$

    x = instr(1,modhdr$,",")
    IF x > 0 THEN
        modbytes = modhdr$[1,x-1]
    ELSE
        XCALL MSGBOX, "Error reading from module # "+midx+".  " &
            + chr(13)+chr(10)+"Can't delete section", &
            "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
            MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0),x, 0, TIMEOUT'SECS      ! [520]
        IF x = MBRC_CANCEL THEN
            XCALL ASFLAG,&h080  ! send ^C to parent
        ENDIF
        EXITPROCEDURE
    ENDIF

    call Backup'INI(inifile$)   ! [601] create .BAK copy

    ch = Fn'NextCh(60000)
REPLACE:    
    open #ch, inifile$, output  ! we are replacing old file

    ! copy first part of module to ini file on disk
    IF Fn'Mod'To'Disk(midx, ch, MOD_HDR_SIZE, sectpos) < 0 EXITPROCEDURE

    ! copy second part of module to ini file on disk (after section to be deleted)
    IF nextsectpos > sectpos THEN
        call Fn'Mod'To'Disk(midx, ch, nextsectpos, modbytes+MOD_HDR_SIZE)
    ENDIF

    close #ch

    exitprocedure
    
TRAP:                               ! [518]
    errcnt += 1                 
    if errcnt <= 20 then            ! allow up to 20 retries
        if err(0) > 3 then          ! anything but ^C, system error, out of memory
            trace.print (99, "inix") "$# Error "+err(0) + "; retrying ..."
            xcall MIAMEX, MX_ASHLOG, "Retry Delete'Section #" + errcnt + " on error "+err(0)     ! [518]
            xcall JOBNUM, jobnum    ! [519]
            sleep 0.1 * jobnum      ! [519]
            resume REPLACE
        endif
    else
        trace.print (99, "inix") "$# Error "+err(0) + "; giving up"
    endif
    
    resume endprocedure with_error err(0)    ! else return error

End PROCEDURE


!-------------------------------------------------------------------------
!Fn'Mod'To'Disk(midx,ch,frompos,topos)
!   Output a chunk of the memory module to the disk file
!Parameters:
!   midx,i,2       [in]  module index
!   ch,b,4         [in]  file channel, open for output
!   frompos,b,4    [in]  starting offset within memory module to output from
!   topos,b,4      [in]  ending offset within memory module to output from
!Returns
!   <0 for error
!Globals:
!   none
!Error handling:
!   none
!Notes:
!   For efficiency we write the entire chunk in a single print operation.
!   Display error message in msgbox if it can't write
!-------------------------------------------------------------------------

FUNCTION Fn'Mod'To'Disk(midx as i2, ch as b4, frompos as i4, topos as i4) as i4

    DEFINE IOBUF_SIZE = 4096

    MAP1 locals
        MAP2 bytes'to'output,i,4
        MAP2 bytes,b,4
        MAP2 offset,b,4
        MAP2 buffer$,s,IOBUF_SIZE
        MAP2 status,f

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,midx,ch,frompos,topos

    bytes'to'output = topos - frompos
    offset = frompos

    DO WHILE bytes'to'output > 0
        bytes = bytes'to'output min IOBUF_SIZE
        buffer$ = ""

        XCALL MIAMEX, MX_USRIO, status, midx, USRIO_READ, buffer$, offset, bytes
        trace.print (99, "inixverbose") "$#",status,midx,buffer$[1,40],offset,bytes
        IF status < bytes THEN
            ! error - we probably should return an error code but
            ! since this is so rare a condition, we just print a messagebox
            XCALL MSGBOX, "Error reading from module # "+midx+".  " &
                + chr(13)+chr(10)+"Can't copy module to file.", &
                "INIX.SBX Error",MBTN_OK_CANCEL,MBICON_STOP, &
                MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0), status, 0, TIMEOUT'SECS      ! [520]
            Fn'Mod'To'Disk = -1
            IF status = MBRC_CANCEL THEN
                XCALL ASFLAG,&h080  ! send ^C to parent
            ENDIF
            Fn'Mod'To'Disk = -1
            EXITFUNCTION
        ENDIF

        PRINT #ch, buffer$;
        bytes'to'output = bytes'to'output - status
        offset = offset + status
    LOOP
End FUNCTION


!-------------------------------------------------------------------------
!Fn'Module'Instr(midx,key$,stpos)
!   Do an instr() operation directly on a memory module
!Parameters:
!   midx,i,2       [in]  module index
!   key$,s,100     [in]  string to search for
!   stpos,b,4      [in]  starting positon (base 0)
!Returns
!   offset to found key$ (0 base, -1=not found)
!Globals:
!   none
!Error handling:
!   none
!Notes:
!   Requires A-Shell 5.0.990.7 for case insensitive search;
!   otherwise uses case sensitive search
!
!-------------------------------------------------------------------------

FUNCTION Fn'Module'Instr(midx as i2, key$ as s100, stpos as b4) as f

    MAP1 locals
        MAP2 verstr$,s,30
        MAP2 vmajor,b,1
        MAP2 vminor,b,1
        MAP2 vedit,b,2
        MAP2 vpatch,b,1
        MAP2 ashver,f
        MAP2 rc,b,1

    STATIC MAP1 searchmode,b,1  ! avoids fetching version more than needed

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,midx,key$,stpos

    IF searchmode = 0 THEN
        ! get version of A-Shell to see if we can use the case insensitive search
        XCALL MIAMEX, MX_GETVER, verstr$, vmajor, vminor, vedit, vpatch
        ashver = vedit + (vpatch/10)

        IF ashver >= 990.7 THEN
            searchmode = USRIO_SEARCHI
        ELSE
            IF ashver >= 989
                searchmode = USRIO_SEARCH
            ELSE
                ! (We "could" implement our own module search by reading chunks at a time,
                ! or even by using DIMX to copy the entire module to memory, but for now
                ! we'll just complain.)

                XCALL MSGBOX,"Sorry, you need at least A-Shell 5.0.989 to support searching within modules, " &
                    + "and at least 5.0.990.7 to support case insensitive searching.  " &
                    + "Use a 4.x version of INIX.SBX for older verions of A-Shell.", "INIX Error", &
                    MBTN_OK_CANCEL, MBICON_STOP, &
                    MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0), rc, 0, TIMEOUT'SECS      ! [520]
                IF rc = MBRC_CANCEL THEN
                    XCALL ASFLAG,&h080  ! send ^C to parent
                ENDIF
                Fn'Module'Instr = -1
            ENDIF
        ENDIF
        trace.print (99, "inixverbose") "$#",ashver
    ENDIF

    IF searchmode THEN
        XCALL MIAMEX, MX_USRIO, Fn'Module'Instr, midx, searchmode, key$, stpos
    ENDIF

    trace.print (99, "inixverbose") "$#",searchmode,Fn'Module'Instr

End FUNCTION


!-------------------------------------------------------------------------
!Function Fn'Lock'INI(inifile$, op)
!   Set or clean an access lock to the INI file
!Parameters:
!   inifile$,s,160 [in]  INI fspec
!   op,b,1         [in]  operation (XLOCK compatible):
!                           1 = set lock.  If not available, wait up to
!                                   5 seconds. If still not available,
!                                   display warning message about conflict
!                                   and let user retry or cancel (cancel
!                                   returns >0)
!                           2 = release lock
!                           5 = same as 1 but don't actually set lock
!Returns
!   0 for success.
!Globals:
!Error handling:
!   none
!Notes:
!
!-------------------------------------------------------------------------

FUNCTION Fn'Lock'INI(inifile$ as s160, op as b2) as b2

    MAP1 locals
        MAP2 mode,b,2
        MAP2 class,b,2,150          ! lock pool id (avoid conflicts with apps)
        MAP2 lock1,b,4,999999       ! fixed lock1 value
        MAP2 ininame$,s,4           ! 1st 4 chars of INI name
        MAP2 lock2,b,4,@ininame$    ! (interpreted as number for lock id)
        MAP2 t1,b,4
        MAP2 rc,b,1
        MAP2 guiflags,b,4           ! [505]
        MAP2 wait,f                 ! [513]

    XCALL AUI, AUI_ENVIRONMENT, 2, guiflags    ! [505]
    IF (guiflags and AGF_ATECLI) THEN          ! [505]
        EXITFUNCTION                           ! [505]
    ENDIF                                      ! [505]

    ininame$ = Fn'Name'Ext$(inifile$)   ! get 1st 4 chars of name.ext [513] was ininame$
    mode = op
    IF mode = 1 mode = 0                ! turn off wait so we can handle it ourselves
    t1 = time

    DO
        XCALL XLOCK, mode, lock1, lock2, class
        trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,mode,lock1,lock2,ininame$
        
        IF op = 2 mode = 0        ! on clear, always assume success

        ! if we can't succeed after 8 seconds, display a message and
        ! give user chance to retry or cancel
        IF mode # 0 THEN    ! lock failed

            IF  (time-t1) > 8 THEN    ! [513] increase from 5

                XCALL MSGBOX, "Lock ("+lock1+","+lock2+") for " &
                    + Fn'Name'Ext$(inifile$) + " has been reserved by job #" &
                    + mode + " and appears to be stuck.  You may need to clear " &
                    + "it manually, using QUTL on another job.", "INIX.SBX Lock Error", &
                    MBTN_RETRY_CANCEL, MBICON_STOP, &
                    MBMISC_TOPMOST+ifelse(TIMEOUT'ABORT,MBMISC_DFLT2,0), rc, 0, TIMEOUT'SECS      ! [520]

                IF rc # MBRC_RETRY exit
                t1 = time       ! reset timer on retry
            ELSE
                
                IF wait = 0 THEN    ! [513] if first failure, randomize 
                    randomize       ! [513] (get unique sequence of wait times)
                ENDIF

                wait = rnd(0) / 10  ! [513] set wait time between 0 and 0.1 secs
                sleep wait          ! [513] was 0.1       ! sleep 1/10 sec before trying again
            ENDIF
            mode = op
        ELSE
            exit                ! if success, exit
        ENDIF
    LOOP

    Fn'Lock'INI = mode

End FUNCTION

!-------------------------------------------------------------------------
!Procedure Backup'INI(inifile$)
!   Rename inifile$ to .bak (prior to recreating it)
!Parameters:
!   inifile$,s,160 [in]  INI fspec
!Error handling:
!   none
!Notes:
!   This is just a safety valve, in case something horrible
!   goes wrong with the update
!-------------------------------------------------------------------------

PROCEDURE Backup'INI(inifile$ as s160)

    MAP1 locals
        MAP2 bakfile$,s,160
        MAP2 status,f
        MAP2 x,f                        ! [502]
        MAP2 ppn$,s,10
        MAP2 brackets,f		            ! [512]
        MAP2 retries,b,2                ! [517]
        
    brackets=instr(1,inifile$,"[")	! [512]
    IF brackets=0 THEN			    ! [512]
        x = len(inifile$) - 5                   ! [509] start search near end
    ELSE
        x = brackets - 5                        ! [512]
        IF x<1 THEN x=1			 ! [512]
    ENDIF

    status = instr(x,inifile$,".")          ! [509] chg 1 to x

    IF status > 0 THEN
        x = instr(status,inifile$,"[")      ! [502] check for [p,pn]
        IF x > 0 THEN                       ! [502]
            ppn$ = inifile$[x,-1]           ! [502]
        ENDIF                               ! [502]
        bakfile$ = inifile$[1,status] + "bak" + ppn$     ![502]
    ELSE
        bakfile$ = inifile$ + ".bak"
    ENDIF

    DO                                      ! [517] retry rename multiple times
        IF LOOKUP(bakfile$) kill bakfile$

        XCALL RENAME, inifile$, bakfile$, status

        IF status THEN
            retries += 1
            if retries <= 5 then
                trace.print (99,"inix") "Error/conflict renaming ",inifile$,bakfile$,", retrying..."   ! [516][517]
                REPEAT
            else
                trace.print (99,"inix") "Error/conflict renaming ",inifile$,bakfile$,", giving up."    ! [517]
            endif
        ENDIF
        
!>!        XCALL MSGBOX, "Unable to rename " &
!>!            + chr(10)+chr(13)+chr(10)+chr(13)+inifile$ &
!>!            + chr(10)+chr(13)+chr(10)+chr(13)+"to" &
!>!            + chr(10)+chr(13)+chr(10)+chr(13)+bakfile$, &
!>!            "INIX.SBX error", MBTN_OK, MBICON_EXCLAMATION, MBMISC_TASKMODAL
        EXIT
    LOOP

End PROCEDURE


![511]
!-------------------------------------------------------------------------
!Function Fn'Sync'SBX'To'ATE()
!   Sync file from server to client
!Parameters:
!   fspec$ [s,in] fname.sbx  (assumed to be in BAS:)
!Returns:
!    0 for success
!   -1 client is not ATE
!   -2 file doesn't exist on server
!   -3 ATEAPX not available for transfer
!   -4 client is not ATE
!Error handling:
!   none
!Notes:
!
!
!-------------------------------------------------------------------------

FUNCTION Fn'Sync'SBX'to'ATE(fspec$ as s0) as f6

    map1 locals
        map2 guiflags,F
        map2 svr'mtime,B,4              ! file mtime on server
        map2 svr'ctime,B,4              ! file ctype on server
        map2 svr'bytes,F                ! size of file on server
        map2 svr'mode,B,2               ! file type/mode on server
        map2 svr'ver$,s,24              ! verson on server a.b.c{.d{.e}}
        map2 svr'hash$,s,15             ! ###-###-###-###
        map2 cli'mtime,B,4              ! file mtime on ATE client
        map2 cli'ctime,B,4              ! file ctype on ATE client
        map2 cli'bytes,F                ! size of file on ATE client
        map2 cli'mode,B,2               ! file type/mode on ATE client
        map2 cli'ver$,s,24              ! verson on server a.b.c{.d{.e}}
        map2 cli'hash$,s,15             ! ###-###-###-###
        map2 status,f
        map2 midx,f

    ! check our environment
    xcall AUI, AUI_ENVIRONMENT, 2, guiflags

    ! if not running ATE, can't transfer
    if ((guiflags and AGF_ATE)=0) then
        Fn'Sync'SBX'to'ATE = -1
        EXITFUNCTION
    endif

    ! first get status of ATEPRT.SBX on server
    xcall MIAMEX, MX_FILESTATS, "L", "BAS:"+fspec$, svr'bytes, &
        svr'mtime, svr'ctime, svr'mode, svr'ver$, svr'hash$

    trace.print (99, "inixverbose") "$#",ABC_CURRENT_ROUTINE$,fspec$,svr'bytes,svr'mtime,svr'ver$,svr'hash$

    if (svr'bytes < 0) then
        Fn'Sync'SBX'to'ATE = -2
        EXITFUNCTION
    endif

    ! now compare to status of file on ATE
    xcall MIAMEX, MX_FILESTATS, "R", "BAS:"+fspec$, cli'bytes, &
        cli'mtime, cli'ctime, cli'mode, cli'ver$, cli'hash$

    trace.print (99, "inixverbose") "$# Client ",fspec$,cli'bytes,cli'mtime,cli'ver$,cli'hash$

    if (svr'hash$ # cli'hash$) or (svr'ver$ # cli'ver$) then
        trace.print (99, "inixverbose") "$# transferring to client via ATEAPX...",fspec$

        ! check if it exists...
        if lookup("BAS:ATEAPX.SBX")=0 then
            ! if not on disk, then in user mem?
            xcall MIAMEX, MX_USRMAP, midx, "ATEAPX.SBX", svr'bytes
            if svr'bytes = 0 then
                Fn'Sync'SBX'to'ATE = -3
                EXITFUNCTION
            endif
        endif

        xcall ATEAPX, "BAS:"+fspec$, "%MIAME%\DSK0\007006", fspec$, 0, status
        if status < 0 then
            Fn'Sync'SBX'to'ATE = status
            EXITFUNCTION
        endif
    endif

ENDFUNCTION


