program TSTINX,1.0(103)
! Description:     Tester for INIX.SBX
! Author:          Jack McGregor
! Date:            19-Jun-07
!---------------------------------------------------------------------
!Notes:
!
!--------------------------------------------------------------------
!{Edit History}
! 29-11-2011 --- 103 --- Jack McGregor
! Improve clarity of read mismatch error
!
! 28-06-2007 --- 102 --- Jack McGregor
! Rename INIX2 to INIX
!
! 20-06-2007 --- 101 --- Jack McGregor
! Add option to load ZTERM.INI
!
! 19-06-2007 --- 100 --- Jack McGregor
! Created to test new version of INIX
!
!---------------------------------------------------------------------
!{end history}


++include ashinc:ashell.def

map1 PARAMS
    map2 TMODE,B,1
    map2 COUNT,F
    map2 I,F
    map2 TVALUE,S,60
    map2 INIFILE,S,20,"TEST.INI"
!   map2 INIFILE,S,200 ,"c:\documents and settings\joaquin\UserData\LONGFILENAMETEST.INI"
    map2 SRC,F
    map2 LINBUF$,S,400
    map2 CUR'SECTION,S,60
    map2 CUR'ITEM,S,60
    map2 CUR'VALUE,S,100
    map2 X,F
    map2 T1,F
    map2 LOADINI$,S,100
    map2 ELAPSE,F

define MAX_ITEMS = 4000
map1 INIDATA(MAX_ITEMS)
    map2 SECTION,S,40
    map2 ITEM,S,40
    map2 VALUE,S,80


! some data for a small-scale test
data "PRINTER","Manufacturer","Gutenberg"
data "PRINTER","Model","Lead Stamp Express"
data "PRINTER","Port","LPT0"
data "PRINTER","Paper","Acid Free Parchment"
data "CONNECTION","Type","Telnet"
data "CONNECTION","Address","1.2.3.4"
data "CONNECTION","Port","23"
data "CONNECTION","Keepalive","TRUE"
data "TERMINAL","Emulation","AM62C"
data "TERMINAL","Rows",24
data "TERMINAL","Columns",80
data "TERMINAL","Colors",16
data "TERMINAL","Type","Field"
data "","",""


    ? tab(-1,0);"TSTINX.BAS - Test INIX.SBX"

    input "Choose data source: 1) Small File, 2) Large (you specify): ",SRC

    if SRC = 1 then
        ! load up some options
        do
            COUNT = COUNT + 1
            read SECTION(COUNT),ITEM(COUNT),VALUE(COUNT)
        loop until SECTION(COUNT)=""
        COUNT = COUNT - 1
    else
        input line "Enter file to load from: ",LOADINI$
        open #1, LOADINI$, input
        do
            input line #1, LINBUF$
            if LINBUF$ # "" then
                if LINBUF$[1,1] = "[" then  ! new section
                    CUR'SECTION = LINBUF$[2,-2]
                else
                    COUNT = COUNT + 1
                    if COUNT > MAX_ITEMS then
                        ? "Exceeded ";MAX_ITEMS;"items"
                        exit
                    endif

                    SECTION(COUNT) = CUR'SECTION
                    X = instr(1,LINBUF$,"=")
                    ITEM(COUNT) = LINBUF$[1,X-1]
                    VALUE(COUNT) = LINBUF$[X+1,-1]
                endif
            endif
        loop until eof(1) = 1
    endif

MAIN:
    TMODE=0
    input "Mode: 1)Create TEST.INI, 2)Read, 3)Del Item, 4)Del Sect, 5)Add Item : ",TMODE
    if TMODE=0 End
    on TMODE call TCREATE,TREAD,TDELITEM,TDELSECT,TADDITEM
    goto MAIN


TCREATE:
    if lookup(INIFILE) # 0 kill INIFILE
    ? "Creating ";INIFILE;"..."
    T1 = TIME
    for I = 1 to COUNT
        if SRC = 1 then
            ? "Writing ";SECTION(I);",";ITEM(I);",";VALUE(I)
        else
            ! for big load, output more selective lifesigns
            if I=1
                ? "[";SECTION(I);"]";
            else
                if SECTION(I) # SECTION(I-1) then
                    ELAPSE = (TIME-T1) max 1
                    ? "(";I/ELAPSE using "####";"/sec)"
                    ? "[";SECTION(I);"]";
                else
                    ? ".";
                endif
            endif
        endif
        xcall INIX,INIFILE,1,SECTION(I),ITEM(I),VALUE(I)
    next I
    ?
    ? "OK, file created in";TIME-T1;"seconds"
    return

TREAD:
    if lookup(INIFILE) = 0 ? INIFILE;" not found" : END
    ? "Reading ";INIFILE;"..."
    T1 = TIME
    for I = 1 to COUNT
        ? "Reading ";SECTION(I);",";ITEM(I);"...";tab(-1,254);

        xcall INIX,INIFILE,0,SECTION(I),ITEM(I),TVALUE
        !xcall TSTSBX,INIFILE,0,SECTION(I),ITEM(I),TVALUE
        ? TVALUE;

        if TVALUE # VALUE(I) then
            ? " [Error!]"
            ? "Retrieved value (";TVALUE;") does not match expectation (";VALUE(I);")" ! [103]
            stop
        else
            ? " [OK]"
            if SRC = 2 and I > 1 then
                ! for big load, output more performance per section
                if I > 1
                    if SECTION(I) # SECTION(I-1) then
                        ELAPSE = (TIME-T1) max 1
                        ? "(";I/ELAPSE using "####";"/sec)"
                    endif
                endif
            endif
        endif
    next I

    ? "Now test one that should fail..."
    ? "Reading BadSection,BadItem...";tab(-1,254);
    xcall INIX,INIFILE,0,"BadSection","BadItem",TVALUE
    if TVALUE # "" then
        ? TVALUE;" [ERROR]"
    else
       ?"<not found>  [OK]"
    endif

    ? "Done reading in";TIME-T1;"seconds"
    return

TDELITEM:
    CUR'SECTION = "?"
    do while CUR'SECTION="?"
        input "Enter section,item to delete (?,? to view TEST.INI): ",CUR'SECTION,CUR'ITEM
        if CUR'SECTION = "?" then
            xcall MIAMEX, MX_VUE, INIFILE, ""
        endif
    loop
    xcall INIX,INIFILE,2,CUR'SECTION,CUR'ITEM

    ! now verify that item was deleted by trying to retrieve it...
    ? "Item deleted; now checking by looking it up...";
    xcall INIX,INIFILE,0,CUR'SECTION,CUR'ITEM,TVALUE,"[Not Found]"
    if TVALUE = "[Not Found]" then
        ? " [OK]"
    else
        ? TVALUE;" [Error, item found]"
    endif
    return

TDELSECT:
    CUR'SECTION = "?"
    do while CUR'SECTION="?"
        input "Enter section (? to view TEST.INI): ",CUR'SECTION
        if CUR'SECTION = "?" then
            xcall MIAMEX, MX_VUE, INIFILE, ""
        endif
    loop
    xcall INIX,INIFILE,3,CUR'SECTION

    ! now verify that item was deleted by trying to retrieve it...
    ? "Section deleted; use read test to verify"
    return

TADDITEM:
    CUR'SECTION = "?"
    do while CUR'SECTION="?"
        input "Enter section,item,value to add (?,?,? to view TEST.INI): ", &
                CUR'SECTION,CUR'ITEM,CUR'VALUE
        if CUR'SECTION = "?" then
            xcall MIAMEX, MX_VUE, INIFILE, ""
        endif
    loop
    xcall INIX,INIFILE,1,CUR'SECTION,CUR'ITEM,CUR'VALUE

    ! now verify that item was added by trying to retrieve it...
    ? "Item added; now checking by looking it up...";
    xcall INIX,INIFILE,0,CUR'SECTION,CUR'ITEM,TVALUE,"[Not Found]"
    if TVALUE = "[Not Found]" then
        ? " [ERROR]"
    else
        if TVALUE # CUR'VALUE then
            ? TVALUE;" [MISMATCH!]"
        else
            ? TVALUE;" [OK]"
        endif
    endif
    return
