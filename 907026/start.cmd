:<(IO) Parameter storage/retrieve routines (from Steve Evans / Madics)
    INIX.SBX   - New version of INIX (faster, more flexible than original)
    SYSREG.SBX - Using a random file (much faster, but not as convenient)
    TSTINX.RUN - Tester for INIX.SBX
    GENTSX.RUN - Will generate a large TEST.GEN file, both for direct
                 testing of INIX and for use as an input to TSTINX.
    (See source files for usage notes)
>
