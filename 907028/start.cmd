:<(UI) Box drawing, screen saving, msg routines (from Steve Evans / Madics)
    MADSCR.SBX - Advanced screen save/restore screen wrapper; allows
                 for direct restore of screens out of order, handles 
                 details to ensure clean XCALL AMOS/HOSTEX with GUI screens.
                 take care of issues when using XCALL AMOS or HOSTEX
    MADBOX.SBX - Various types of box drawing with GUI/text support
    TSTMBX.BP  - Sample program for MADBOX.SBX
    MADMES.SBX - Message display routine (replacement for MESAG.SBR)
    (See source files for usage notes)
>
