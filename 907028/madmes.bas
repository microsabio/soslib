!========================================================================
!MADMES.SBX - Madics Version of XCALL MESAG 
!========================================================================
!Usage:
!	XCALL MADMES,{message},{type}
!
!To replace the standard XCALL MESAG with this version system wide with 
!no program changes add the following line to miame.ini.
!
! ALIAS=MESAG,MADMES
!
!========================================================================
!
! VERSION HISTORY
! 17/Mar/2005 - 4.1(100) - Steve - New Madics MESAG to replace current
!                                  standard XCALL MESAG, this has no extra
!                                  features like not showing a GUI message
!                                  box if the message line is blank.
!
!                                  Also the bottom line 24 is restored
!                                  after the message has been displayed.
! 07/Apr/2005 - 4.2(100) - Steve - Madics 4.2
! 25/Nov/2005 - 4.2(500) - Steve - Madics 4.2a 
!
program MADMES,4.2(500)

++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! NOTE: MUST COMPILE WITH COMPIL MADMES/X:2 
!
++include xcall.bsi
!--------------------------------------------------------------------
! This code example will do multi lines...if needed later..
!FSPEC="OPR:MENU0.MNU"
!HEADER="1~85~Title~S~Font=Lucida Console~~"
!FLAGS=2
!xcall XTRMSG,"This is the title",8,8,18,65,FLAGS,FSPEC,EXITCODE,HEADER,XFLAGS
!--------------------------------------------------------------------


MAP1 PARAMS
  	MAP2 X'MESSAGE,S,100
  	MAP2 X'TYPE,B,1 

MAP1 WIN'OK,S,1,"N"
MAP1 CHARX,B,1
MAP1 RTNCDE,F,6
MAP1 MSGBOX'TYPE,F,6,48
MAP1 TYPE'DESCR,S,14,"CR TO CONTINUE"
MAP1 PRGNAM,S,10
MAP1 SBXNAM,S,10

! *** GUI FLAGS (used by MIAMEX,129) to get client information
	MAP1 AGF'GF,B,1                 ! separate var to retrieve into
	MAP1 AGF'GUIFLAGS		! 
	    MAP2 AGF'LWG,B,1,1 		! Local Windows Gui
	    MAP2 AGF'LWN,B,1,2 		! Local Windows Non-gui
	    MAP2 AGF'ATE,B,1,4 		! ATE client
	    MAP2 AGF'RWN,B,1,8 		! Remote WiNdows (ATSD)
	    MAP2 AGF'TNT,B,1,16 	! TelNeT
	    MAP2 AGF'ASH,B,1,32 	! A-Shell (not AMOS)
	    MAP2 AGF'THEMES,B,1,64 	! XP themes active
	    MAP2 AGF'LOCWIN,B,1,3 	! Local Windows (GUI or non)
	    MAP2 AGF'ANYWIN,B,1,11 	! Windows (local or remote)
	    MAP2 AGF'GUIEXT,B,1,5 	! GUI EXTensions available
	    MAP2 AGF'LWNATE,B,1,7	! Local WiNdows or ATE 

     	on error goto TRAP

	! pick up all args; any not passed will just be set to null
      	XGETARGS X'MESSAGE,X'TYPE
	!
	! Are we using ATE or Windows
	!
	XCALL MIAMEX,129,0,AGF'GF
        IF AGF'GF AND AGF'LWNATE THEN WIN'OK="Y"
	!
	! What Message Type
	!
	! 16 = <STOP>, 32 = <question>, 48 = <exclamation>, 64 = <info>
 	IF X'TYPE=1 THEN TYPE'DESCR="CR TO RECOVER"  : MSGBOX'TYPE=48
 	IF X'TYPE=2 THEN TYPE'DESCR="CR TO CONTINUE" : MSGBOX'TYPE=64
 	IF X'TYPE=3 THEN TYPE'DESCR="CR TO CONTINUE" : MSGBOX'TYPE=32
	!
	! GUI Message Box Version, if we Can...
	!
    	IF WIN'OK="Y" AND X'MESSAGE<>"" THEN
	    !Get Calling program name use as title
	    XCALL GETPRG,PRGNAM,SBXNAM	 
            XCALL MSGBOX,X'MESSAGE,PRGNAM,0,MSGBOX'TYPE,8192,RTNCDE
	  ELSE
	   !
	   ! GUI not available or if the blank message is blank.
	   ! Emulate XCALL MESAG as much as we can.
	   !
	   PRINT TAB(-1,202);	! Save Screen
 	   PRINT TAB(24,1);TAB(-1,9);X'MESSAGE;
	   PRINT TAB(24,65);TYPE'DESCR;TAB(-1,9);TAB(24,79);
	   XCALL ACCEPN,CHARX
	   PRINT TAB(24,1);
	   PRINT TAB(-1,203);	! Restore Screen
    	ENDIF	   	   

	END

TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in MADMES.SBX!!",2
	END			! return to caller
 
