!========================================================================
!MADSCR - Save/Restore Screen
!========================================================================
! Supports existing (LIFO stack order) TCRTs 202 and 203 if no 
! screen ID is supppled, otherwise if supported (ATE/Windows) the new
! TAB(-10,40); is used.
!
!Usage:
!	XCALL MADSCR,Command,{screen id},{clear screen}
!
!	COMMAMND:
!	  S (save screen)
!	  R (restore previously saved screen)
!	  P (pop previously saved screen without redisplay)
!
!	SCREEN ID - Zero for existing TAB(-1,202) mode.
!
!	CLEAR SCREEN - "Y" or "N" to clear screen on restore.
!
!
!Examples:
! ATE/Windows Only:
!	XCALL MADSCR,"S",1	! save screen with ID 1
!	XCALL MADSCR,"S",5	! save screen with ID 5
!	XCALL MADSCR,"R",1	! restore screen saved as ID 1
!	XCALL MADSCR,"R",0	! same as TAB(-1,203) 
!				! (i.e. restore last saved screen)
!	XCALL MADSCR,"P",3	! pop/discard screen saved as ID 3
!	XCALL MADSCR,"P",0   	! pop/discard last saved screen
!
! Full Support:
!	XCALL MADSCR,"S"	! Saves screen 1
!	XCALL MADSCR,"S"	! Saves screen 2
!	XCALL MADSCR,"R","Y"	! restores screen 1
!	XCALL MADSCR,"R","Y"	! restores screen 2
!
!========================================================================
!
! VERSION HISTORY
! 18/Feb/2005 - 4.1(100) - Steve - New SBX
!
program MADSCR,4.1(100)

++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!
! NOTE: MUST COMPILE WITH COMPIL MADSCR/X:2 
!
++include xcall.bsi
!--------------------------------------------------------------------

MAP1 PARAMS
	MAP2 X'COMMAND,S,1
	MAP2 X'SCREEN,F,6
	MAP2 X'CLR,S,1

MAP1 WIN'OK,S,1,"N"

! *** GUI FLAGS (used by MIAMEX,129) to get client information
	MAP1 AGF'GF,B,1                 ! separate var to retrieve into
	MAP1 AGF'GUIFLAGS		! 
	    MAP2 AGF'LWG,B,1,1 		! Local Windows Gui
	    MAP2 AGF'LWN,B,1,2 		! Local Windows Non-gui
	    MAP2 AGF'ATE,B,1,4 		! ATE client
	    MAP2 AGF'RWN,B,1,8 		! Remote WiNdows (ATSD)
	    MAP2 AGF'TNT,B,1,16 	! TelNeT
	    MAP2 AGF'ASH,B,1,32 	! A-Shell (not AMOS)
	    MAP2 AGF'THEMES,B,1,64 	! XP themes active
	    MAP2 AGF'LOCWIN,B,1,3 	! Local Windows (GUI or non)
	    MAP2 AGF'ANYWIN,B,1,11 	! Windows (local or remote)
	    MAP2 AGF'GUIEXT,B,1,5 	! GUI EXTensions available
	    MAP2 AGF'LWNATE,B,1,7	! Local WiNdows or ATE 

     	on error goto TRAP

	! pick up all args; any not passed will just be set to null
      	XGETARGS X'COMMAND,X'SCREEN

	! Are we using ATE or Windows
	XCALL MIAMEX,129,0,AGF'GF
        IF AGF'GF AND AGF'LWNATE THEN WIN'OK="Y"

	! Check if supported
	IF X'SCREEN>0 AND WIN'OK="N" THEN
	   XCALL MESAG,"SCREEN SAVE "+STR(X'SCREEN)+" FUNCTION NOT SUPPORTED, ATE/Windows required",2
	   X'SCREEN=0
	ENDIF	   

	! clear screen on restore?
	IF X'COMMAND="R" AND X'CLR="Y" THEN PRINT TAB(-1,0);

	!Now lets save/restore the old way frist if not supported or 
	! its the first screen.
	IF X'SCREEN=0 THEN
	  IF X'COMMAND="S" THEN PRINT TAB(-1,202);
	  IF X'COMMAND="R" THEN PRINT TAB(-1,203);
	ENDIF

	! otherwise..lets go for the multi save/restore sceen option.
	IF X'SCREEN>0 AND WIN'OK="Y" THEN
	   PRINT TAB(-10,40);X'COMMAND;",";STR(X'SCREEN);chr(127);
	ENDIF

	! back to program we go...
	END

TRAP:
	XCALL MESAG,"Error #"+STR(err(0))+" in MADSCR.SBX!!",2
	END			! return to caller

