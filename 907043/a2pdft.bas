program A2PDFT, 1.0(100)     ! test for A2PDF.SBX

++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!------------------------------------------------------------------------
! NOTE: MUST COMPILE WITH COMPIL A2PDFT/X:2 
!--------------------------------------------------------------------
!{Edit History} 
![100] February 5, 2008 12:29 PM       Edited by jdm
!   Created to test Miro's A2PDF.SBX
!--------------------------------------------------------------------

++include ashinc:ashell.def

map1 amosfile$,s,200
map1 pdffile$,s,200
map1 a$,s,1
map1 status,f

    ? "Test A2PDF.SBX... "

    input line "Enter AMOS filespec (ASCII file) to convert: ",amosfile$
    xcall A2PDF,amosfile$,pdffile$
    ? "Output is ";pdffile$
    end
