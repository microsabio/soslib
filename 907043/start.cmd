:<(File conversion subroutines)

    A2PDF.BAS/SBX   - Convert ASCII text file to PDF, using UNIX utilities
                         a2ps (ascii to postscript) and p2pdf (postscript
                         to pdf using ghostscript).  From Miro Ceperkovic
    A2PDFT.BAS/RUN  - Simple test/example of A2PDF.SBX
>
