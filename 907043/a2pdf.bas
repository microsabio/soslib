program A2PDF,1.0(111)    ! Convert text to pdf (via postscript & ghostscript)
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
! ------------------------------------------------------------------------
! PDF.SBR - Convert Amos file name PDF
! Author:         Miro Ceperkovic
! Date:           Feb. 2008
! Usage:
!   XCALL A2PDF,amosFile,pdfFile
!   amosFile    = Amos file             - input
!   pdfFile     = pdf file              - output
! --------------------------------------------------------------------
! NOTE: MUST COMPILE WITH COMPIL A2PDF.BAS/X:2 
! --------------------------------------------------------------------
! {Edit History}                        Edited by
! [110] Feb, 2008                  Miro Ceperkovic
! This could be extended with parameters to format different .ps files
! [111] Feb 5, 2008                Jack McGregor
! Change name from PDF to A2PDF
! --------------------------------------------------------------------
++pragma ERROR_IF_NOT_MAPPED "TRUE"
++include ashinc:xcall.bsi     
++include ashinc:ashell.def
++include sosfunc:fnfqfs.bsi

        MAP1 params
                MAP2 amosFile,s,256
                MAP2 psFile,s,256
                MAP2 pdfFile,s,256
                MAP2 txtFile,s,256


        MAP1 FILE'NAME,S,256   ! no extension
        MAP1 HOSTEX'PARAM,S,256
        MAP1 A2PS,S,23,"a2ps -q -1 -f 10 -B -o"    ! portrait font size 10
        MAP1 HOSTEX'STATUS,F,6
        
        on error goto trap


        ! see how many args we got, just for fun
        debug.print "A2PDF.SBX received "+XCBCNT+" arguments"   ! [111]


        ! GET FIRST PARAMETER
        xgetarg 1,amosFile
        txtFile = Fn'FQFS$(amosFile)
            
        ! Name files
        FILE'NAME = txtFile[1;INSTR(1,txtFile,".")-1] ! No extension        
        psFile = FILE'NAME + ".ps"
        pdfFile = FILE'NAME + ".pdf"


        ! Create ps file from our text file
        ! a2ps -q -1 -f 10 -B -o <>.ps <>.txt
        HOSTEX'PARAM = A2PS + " " + psFile + " " + txtFile         
        XCALL HOSTEX, HOSTEX'PARAM, HOSTEX'STATUS
        
        ! Create pdf file from ps
        ! ps2pdf <>.ps <>.pdf     
        HOSTEX'PARAM = "ps2pdf " +  psFile + " " + pdfFile
        XCALL HOSTEX, HOSTEX'PARAM, HOSTEX'STATUS
      
        ! XCALL HOSTEX, psFile
        ! XCALL HOSTEX, txtFile   


return'to'caller:
    xputarg 2,pdfFile
    
    end         


! -------------------------------------------------------------------
trap:
    ? "Error #";err(0);" in A2PDF.SBX!!!"
    xcall SLEEP,5
    xcall ASFLAG,128    ! set ^C in caller
    end                 ! return to caller

