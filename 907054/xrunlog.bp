program XRUNLOG, 1.0(103)  ! Sample SBX called by A-Shell to log RUNs
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 23-Dec-22 / jdm / created
! [101] 28-Dec-22 / jdm / deactivate xlocking; revise consolidate log format
! [102] 28-Dec-22 / jdm / include prior archived log (to minimize orphaned end events)
! [103] 03-Apr-23 / jdm / fix the possible file ch conflict (channels are
!                           shared with caller)
!------------------------------------------------------------------------
!REQUIREMENTS
!   A-Shell 6.5.1723.0+
!NOTES
!   XRUNLOG.SBX must be in BAS: (Given that a program may change directories,
!   allowing the normal SBX search path would introduce too many ambiguities.)
!   If present, it is automatically called on startup and shutdown of the 
!   session, as well as at the start end end of each RUN (not including LITs).
!
! Calling syntax:
!
!       XCALL XRUNLOG, EVT, JCBREC, STATUS
!   or
!       XCALL XRUNLOG  (with no args to run the consolidation routine)
!
! Where
!
!       EVT (num) [in] - Event code:
!                        1=start of session
!                        2=end of session
!                        4=start of RUN
!                        8=end of RUN
!       JCBREC (ST_JCBREC) [in] - copy of current JOBTBL rec
!       STATUS (i4) [in/out] - integer status we returned on previous call.
!                               Provides way for each call to this SBX
!                               to pass info to the next call, especially
!                               for communicating between the start and 
!                               end RUN events). Should be -1
!                               if no prior info. Will be 0 for the start 
!                               session event; after that it depends on you.
!
!       If no args passed, operates in interactive mode (intended for
!       utility operations such as pairing the start/end records or
!       reporting.
!
!   This sample SBX simply writes out log records to OPR:XRUNLOG.LOG in
!   the format:
!
!       dd-mon-yr,hh:mm:ss,pid,evt,seqnum,usrnam,prgnam,prgedit,totreads,totwrites,totinstr,totkeys
!
!   Field notes:
!    The totxxxx fields are running totals for the session, so
!       if you want to get totals for each RUN, you have to match up the 
!       start and end RUN events and take the difference.
!    Seqnum is a sequence #, unique to the pid, that can be used to pair
!       the start and end event records for each session/RUN. It is 
!       implemented via the in/out STATUS parameter, illustrating one 
!       of the ways that parameter can be used.  
!
!------------------------------------------------------------------------

++pragma SBX

significance 11

++include'once ashinc:ashell.def
++include'once ashinc:jobtbl.sdf
++include'once sosfunc:fnlogarc.bsi
++include'once sosfunc:fndatetime.bsi
++include'once sosfunc:fnextch.bsi      ! [103]

define XRUN_LOG_FILE$ = "opr:xrunlog.log"

define XLOCK_LOCK1 = 40000      ! XLOCK lock 1 value used for exclusive lock
define XLOCK_LOCK2 = 40001      !       lock 2
define XLOCK_CLASS = 253        !       lock class

map1 PARAMS
    map2 EVT,B,2
    map2 JCB,ST_JCBREC
    map2 STATUS,I,4
    
map1 MISC    
    map2 OJCB,ST_JCBREC
    map2 ODATE,S,10
    map2 OTIME,S,10
    map2 ACTION,S,10
    map2 XLOCK1,B,4
    map2 XLOCK2,B,4
    map2 XCLASS,B,2
    map2 XMODE,B,2
    map2 CH,B,2                         ! [104]
    
    if .ARGCNT = 0 then                 ! if no params, call
        call Fn'XRUNLOG'Interactive()   ! interactive operations
        end
    endif
    
    xgetargs EVT, JCB, STATUS
    trace.print (99,"xrunlog") EVT, JCB.PRGNAM, STATUS

    ! [101] If we're just appending to the log file, there's no need for
    ! locking. This code here in reserve for more advanced operations.
    ! XLOCK1 = XLOCK_LOCK1
    ! XLOCK2 = XLOCK_LOCK2
    ! XCLASS = XLOCK_CLASS
    ! XMODE = 1               ! lock with wait
    ! xcall XLOCK, XMODE, XLOCK1, XLOCK2, XCLASS  ! get an exclusive lock on log file
    
    if EVT = 4 then     ! start RUN
        STATUS += 1     ! increment our sequence counter for start events
    elseif EVT = 2 then ! end A-Shell
        STATUS = 0      ! manually set STATUS to 0 to match start A-Shell event
    endif
        
    switch EVT
        case 1               ! start of A-Shell session
        case 2               ! terminate A-Shell
        case 4               ! start of RUN
        case 8               ! end of RUN
        default              ! not sure what this would be but log it anyway
            CH = Fn'NextCh(44000)       ! [103]
            open #CH, XRUN_LOG_FILE$, append
            writecd #CH, odtim(0,0,&h200), odtim(0,0,&h1), abs(JCB.PID), EVT, STATUS, &
                JCB.PRGNAM, JCB.PRGEDIT, JCB.USRNAM, JCB.TOTREADS, JCB.TOTWRITES, &
                JCB.TOTINSTR, JCB.TOTKEYS
            close #CH
            exit
    endswitch    
   
    ! [101] 
    ! Clear the lock
    ! XMODE = 2
    ! xcall XLOCK, XMODE, XLOCK1, XLOCK2, XCLASS  ! clear the lock on log file

    xputarg @STATUS
    trace.print (99,"xrunlog") STATUS
    end
    
    

!---------------------------------------------------------------------
!Function:
!   Interactive operations
!Params:
!Returns:
!Globals:
!Notes:
!   In this example we create a new log file OPR:XRUNLOG2.LOG which
!   combines the start/end lines for easier analysis. Format of 
!   resulting lines:
!
!       sdate,stime,elapsed'time,pid,evt,seqnum,usrnam,prgnam,prgedit,totreads,totwrites,totinstr,totkeys
!    
!   Differences vs xlogrun.log lines:
!
!       if start event not seen, sdate will be first seen stime (from .l01) with "<" prefix
!       elapsed'time added; if start event not seen, will be based on stime shown with "+" suffix
!       evt will be the ending event (2 or 8), +16 if start event not seen
!       tot fields will indicate activity for the event duration (end totals minus start totals)
!           unless start time not seen, in which case they will just be current totals (for entire session)
!
!   We archive xrunlog.log -> .l01 -> .l02 -> etc at start of process.
!   But we scan the original .l01 (prior archive) for orphaned start events
!       and include them in our analysis of the current log archive.
!---------------------------------------------------------------------
Function Fn'XRUNLOG'Interactive() as i4

defstruct ST_XRUNLOG         ! struct used for reporting (adjust per your needs)
    map2 date,s,10           ! dd-mon-yy (starting date for RUN or session)
    map2 time,s,10           ! hhhh:mm:ss (elapsed time for RUN or session)
    map2 pid,b,4
    map2 type,b,1            ! 0=ashell session, 1=RUN
    map2 seqnum,b,4
    map2 prgnam,s,10
    map2 prgedit,b,2
    map2 usrnam,s,20
    map2 totreads,i,4        ! reads during RUN or session
    map2 totwrites,i,4       ! writes during RUN or session
    map2 totinstr,i,4        ! instructions during RUN or session
    map2 totkeys,i,4         ! keystrokes during RUN or session
endstruct

    map1 locals
        map2 op,b,1
        map2 evt0,ST_XRUNLOG
        map2 evt,ST_XRUNLOG
        map2 logfile1$,s,100
        map2 logfile2$,s,100
        map2 key$,s,20              ! pppppppppp-########
        map2 days,b,2
        map2 seconds,b,4
        map2 sruncnt,i,4
        map2 eruncnt,i,4
        map2 sashcnt,i,4
        map2 eashcnt,i,4
        map2 sorphancnt,i,4
        map2 eorphancnt,i,4
        map2 sdate1,s,10            ! [102]
        map2 stime1,s,10            ! [102]
        map2 sdate2,s,10            ! [102]
        map2 stime2,s,10            ! [102]
        map2 elapsed$,s,12          ! [102] {hhh}h:mm:ss{+}
        
    dimx $evt, ordmap(varstr;varx)  ! <pid><seq> -> ST_XRUNLOG
        
    ? tab(-1,0);"XRUNLOG Interactive Utilites"
    ?
    input "1) Consolidate start/end events into OPR:XRUNLOG2.LOG or 0) Exit: ", op
    if op = 1 then
        ? "This operation archives the current OPR:XRUNLOG.LOG (to .L01) "
        ? "and appends consolidated records to OPR:XRUNLOG2.LOG"
        input "1) To proceed or 0) Exit: ",op
        if op = 1 then
    
            ! [102] scan the existing .L01 for start events we can query
            ! [102] to match with end events in current log
            logfile1$ = XRUN_LOG_FILE$[1,-5] + ".l01"
            if lookup(logfile1$) then
                ? "Scanning ";logfile1$;" for unterminated events..."
                open #1, logfile1$, input
                do while eof(1) = 0
                    input csv #1, evt.date, evt.time, evt.pid, evt.type, evt.seqnum, &
                        evt.prgnam, evt.prgedit, evt.usrnam, evt.totreads, evt.totwrites, &
                        evt.totinstr, evt.totkeys
                    
                    if sdate1 = "" then      ! save first timestamp for final stats
                        sdate1 = evt.date
                        stime1 = evt.time
                    endif
                    key$ = (evt.pid using "#ZZZZZZZZZ") + "-" + (evt.seqnum using "#ZZZZZZZ")
                    if evt.type = 1 or evt.type = 4 then    ! start session or RUN
                        $evt(key$) = evt
                    elseif evt.type = 2 or evt.type = 8     ! end session or RUN
                        if not .isnull($evt(key$)) then     ! if start event found...
                            $evt(key$) = .NULL              ! remove it
                        endif
                    endif
                loop
                close #1
                ? .extent($evt());" unmatched start events carried forward"
            endif
            ! now $evt() contains just start events with no matching end event
            
            ? "Rolling over ";XRUN_LOG_FILE$;" to ";logfile1$;" ..."
            call Fn'LogArchive(logfile$=XRUN_LOG_FILE$)
            
            logfile2$ = XRUN_LOG_FILE$[1,-5] + "2.log"
            open #1, logfile1$, input
            open #2, logfile2$, append
            
            do while eof(1) = 0
                input csv #1, evt.date, evt.time, evt.pid, evt.type, evt.seqnum, &
                    evt.prgnam, evt.prgedit, evt.usrnam, evt.totreads, evt.totwrites, &
                    evt.totinstr, evt.totkeys
                
                trace.print (99,"xrunlog") evt.date, evt.time, evt.pid, evt.type, evt.seqnum, &
                    evt.prgnam, evt.prgedit, evt.usrnam, evt.totreads, evt.totwrites, &
                    evt.totinstr, evt.totkeys

                if sdate2 = "" then          ! [102] save first timestamp for final stats
                    sdate2 = evt.date
                    stime2 = evt.time
                endif

                if evt.date # "" then       ! if we got something
                    
                    switch evt.type
                        case 1
                            sashcnt += 1        ! start sessions
                            exit
                        case 2
                            eashcnt += 1        ! end sessions
                            exit
                        case 4
                            sruncnt += 1        ! start runs
                            exit
                        case 8
                            eruncnt += 1        ! end runs
                            exit
                    endswitch
                            
                    key$ = (evt.pid using "#ZZZZZZZZZ") + "-" + (evt.seqnum using "#ZZZZZZZ")
                    
                    trace.print (99,"xrunlog") key$, sashcnt, eashcnt, sruncnt, eruncnt

                    if evt.type = 2 or evt.type = 8     ! end ashell session or end RUN

                        evt0 = $evt(key$)               ! try to find the start record

                        if .isnull(evt0) then           ! if no start rec, add 16 to event type and set elapsed time based on span of log
                            evt.type |= 16              ! set orphan flag
                            eorphancnt += 1             ! count orphans
                            evt0.date = sdate1          ! use first time in .l01 log as start time
                            evt0.time = stime1
                            elapsed$ = Fn'Elapsed'Time$(date1=evt0.date, time1=evt0.time, &
                                            date2=evt.date, time2=evt.time) + "+"
                                            
                            evt0.time = "<" + evt0.time ! insert < prefix to indicate actual time prior
                            
                            evt.totreads  = -1          ! set counters to -1 since we have no idea
                            evt.totwrites = -1
                            evt.totinstr  = -1
                            evt.totkeys   = -1
                            
                        else                            ! start event found...
                            
                            elapsed$ = Fn'Elapsed'Time$(date1=evt0.date, time1=evt0.time, &
                                            date2=evt.date, time2=evt.time)

                            evt.totreads  -= evt0.totreads  ! replace ending counter with count
                            evt.totwrites -= evt0.totwrites ! for the session
                            evt.totinstr  -= evt0.totinstr
                            evt.totkeys   -= evt0.totkeys
                            
                            $evt(key$) = .NULL  ! [102] remove matched start events from map
                        endif
                        
                        ! note: here evt0.time is start event time, evt.time is elapsed time
                        writecd #2, evt0.date, evt0.time, elapsed$, abs(evt.pid), evt.type, evt.seqnum, &
                            evt.prgnam, evt.prgedit, evt.usrnam, evt.totreads, evt.totwrites, &
                            evt.totinstr, evt.totkeys
                    
                        trace.print (99,"xrunlog") key$, evt0.date
                    else            ! for start events, just save the event record in the ordmap
                        $evt(key$) = evt
                    endif
                endif
            loop
            
            close #1
            close #2
            
            ? "Consolidation complete for events logged since ";sdate2;" ";stime2       ! [102]
            ? (sashcnt using "########");   " A-Shell sessions started"
            ? (eashcnt using "########");   " A-Shell sessions completed"
            ? (sruncnt using "########");   " RUN executions started"
            ? (eruncnt using "########");   " RUN executions completed" 
            ? (eorphancnt using "########");" Completion events without start event";ifelse$(eorphancnt," *","") ! [102]
            
            foreach $$i in $evt()       ! [102] count remaining start events
                sorphancnt += 1
            next $$i
            ? (sorphancnt using "########");" Start events without end event";ifelse$(sorphancnt," *,**","") ! [102]
            
            if sorphancnt or eorphancnt then    ! [102] 
                ? " * Includes events that started since ";sdate1;" ";stime1
            endif
            if sorphancnt then    ! [102] 
                ? "** Orphaned start events not included in consolidated log"
            endif

        endif
        
    endif
    
EndFunction


!---------------------------------------------------------------------
!Function:
!   Return {hhh}h:mm:ss difference between two date/time pairs
!Params:
!   date1  (flexdate) [in] - starting date
!   time1  (flextime) [in] - starting time
!   date2  (flexdate) [in] - starting date
!   time2  (flextime) [in] - starting time
!Returns:
!   {hhh}h:mm:ss
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'Elapsed'Time$(date1 as T_FLEX_DATE:inputonly, &
                               time1 as T_FLEX_TIME:inputonly, &
                               date2 as T_FLEX_DATE:inputonly, &
                               time2 as T_FLEX_TIME:inputonly) as s30
    map1 locals
        map2 ft1, T_FILETIME
        map2 ft2, T_FILETIME
       
    ft1 = Fn'Date'Time'To'FILETIME(flexdate$=date1, flextime$=time1)
    ft2 = Fn'Date'Time'To'FILETIME(flexdate$=date2, flextime$=time2)
    .fn = odtim$(0,(ft2-ft1),&h01)
EndFunction
