program PRINT1, 1.0(100)     ! test for PRINT2.SBX

++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!------------------------------------------------------------------------
! NOTE: MUST COMPILE WITH COMPIL PRINT1/X:2 
!--------------------------------------------------------------------
!{Edit History} 
![100] December 11, 2007 11:29 AM       Edited by jdm
!   Created
!--------------------------------------------------------------------

++include ashinc:ashell.def

map1 params
    map2 lincnt,b,2
    map2 pgcnt,b,4
    map2 pline,s,512
    map2 title,s,160
    map2 hdr1,s,160
    map2 hdr2,s,160
    map2 hdr3,s,160
    map2 legnd1,s,160
    map2 legnd2,s,160
    map2 legnd3,s,160
    map2 linctl,b,1
    map2 pagsiz,b,2
    map2 prctl2,b,2
    map2 lgnctl,b,1
    map2 pchanl,b,2
    map2 pcn,s,40
    map2 pff,b,1

map1 misc
    map2 i,f
    map2 totlines,f

    ? "Testing PRINT2.SBX..."

    ! initialize the params

    lincnt = 61         ! force new page
    pgcnt = 0
    title = "This is the title"
    hdr1 = "This is hdr1"
    hdr2 = "This is hdr2"
    hdr3 = "This is hdr3"
    legnd1 = "This is legnd1"
    legnd2 = "This is legnd2"
    legnd3 = "This is legnd3"
    linctl = 0              ! single space
    pagsiz = 80
    prctl2 = 80
    lgnctl = 0              ! legends only on page 1
    pchanl = 74             ! print channel
    pcn = "sys:coname.dat"  ! coname.dat spec
    pff = 0                 ! use FF chars

    open #pchanl, "print1.prt", output
    input "Enter # of lines of output to generate: ",totlines
        
    for i = 1 to totlines
        pline = "This is line # "+ str(i)
    
        xcall PRINT2,lincnt,pgcnt,pline,title,hdr1,hdr2,hdr3,legnd1,legnd2, &
                legnd3,linctl,pagsiz,prctl2,lgnctl,pchanl,pcn,pff

    next i  

    close #pchanl
    xcall EZTYP, "print1.prt"
    end
