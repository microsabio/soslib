program PRINT2,1.1(111) 	! SBX version of PRINT.SBR
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
! ------------------------------------------------------------------------
! PRINT2.SBR - Routine to process printed output, making it easier to 
!   use systematic headers.  The original version of PRINT.SBR
!   was part of the AlphaACCOUNTING package under AMOS, and then 
!   was included as an embedded subroutine in A-Shell.  The idea behind
!   re-implementing it as an SBX is to allow it to be easily enhanced.
!
! Usage:
!   XCALL PRINT,LINCNT,PGCNT,PLINE,TITLE,HDR1,HDR2,HDR3,LEGND1,LEGND2,
!                LEGND3,LINCTL,PAGSIZ,PRCTL2,LGNCTL,PCHANL,PCN,PFF
!
! where
!   LINCNT [num, in/out] is the current line count.  (Set > 60 to force
!       a header on the first call)
!    
!   PGCNT [num, in /out] current page number.
!
!   PLINE [str, in] is the string to be printed. 
!
!   TITLE [str, in] title to print on 3rd line of first page and top  
!           line of subsequent pages.  If TITLE="NO TITLE", the title
!           line and following blank line are not printed.   
!           If TITLE="BLANKS", two blank lines are printed.
!           Title is printed in place of the company name on pages
!           after the first.
!
!   HDR1 [str, in] a header line 
!   HDR2 [str, in] another header line 
!   HDR3 [str, in] another header line 
!           Header lines are printed after the legends.  If "NO HDR",
!           no further headers are printed.
!
!   LEGND1 [str, in] a legend line
!   LEGND2 [str, in] another legend line 
!   LEGND3 [str, in] another legend line 
!           Legend lines are printed after the title.  If "NO LEGEND",
!           no further legend lines are printed.
!
!   LINCTL [num, in] number of lines to skip between print lines     
!
!   PAGSIZ [num, in] page width in columns (affects header format)
!
!   PRCTL2 [num, in] max characters allowed in a print line 
!
!   LGNCTL [num, in] 1=print legend on every page; 0=first page only
!
!   PCHANL [num, in] output file channel (must be opened by caller) 
!                    (default = 14) 
!
!   PCN [str, in] file spec of the CONAME.DAT file. 
!               (default = "DSK:CONAME.DAT")
!
!   PFF [num, in] forms control. If 0 (or not specified), form-feed 
!               chars are used; else line-feeds are used to simulate 
!
! --------------------------------------------------------------------
! NOTE: MUST COMPILE WITH COMPIL PRINT2/X:2 
! --------------------------------------------------------------------
! {Edit History}                        Edited by
! [100] December 11, 2007 11:29 AM      jdm
        !   Created
! [110] December, 2007                  Miro Ceperkovic
        ! - Title has 2 lines now and they repeat on each page:
        !   <date time>             <Company>                  <page num>
        !                         <Report Title>                     
        ! - Spaces between Title and Legend and Legend and Hdr lines are removed.
        !   If you need space use one of the legend or header lines.
        !   If you need underline define it as one of the legend or header lines.
        ! - Option to keep legend on first page or print on all pages (LGNCTL) is kept.
        !   LINCTL will affect all printed lines - if you specify 1 there ..
        !     will be single spece between each line including title
        !     If you have better way of dealing with blank lines, please post
        !   LGNCTL could be expended to pass more than just 0 and 1 and could be
        !   codded to print legend lines and header lines centered, for example.
        !   I will leave this to your imagination.  Please post any usefull samples.
        ! - Header lines will print on all pages.

! [111] January 7, 2008 03:55 PM      jdm
        !   Merge in relevant parts of CKPPN.INC (from Miro) so it 
        !   compiles.  Routine doesn't exactly match embedded PRINT.SBR
        !   but may serve as a good starting point for anyone wanting
        !   to customize the way PRINT.SBR works.
! --------------------------------------------------------------------

++include ashinc:xcall.bsi           ! (only needed for XCBCNT)
++include ashinc:ashell.def

map1 params
    map2 lincnt,b,2
    map2 pgcnt,b,4
    map2 pline,s,512
    map2 title,s,160
    map2 hdr1,s,160
    map2 hdr2,s,160
    map2 hdr3,s,160
    map2 legnd1,s,160
    map2 legnd2,s,160
    map2 legnd3,s,160
    map2 linctl,b,1
    map2 pagsiz,b,2
    map2 prctl2,b,2
    map2 lgnctl,b,1
    map2 pchanl,b,2
    map2 pcn,s,40
    map2 pff,b,1
    





    on error goto trap

    ! see how many args we got, just for fun
    debug.print "PRINT2.SBX received "+XCBCNT+" arguments"

    ! although it would be simpler to just grab all the args, let's
    ! first check lincnt to see if we really need them all...
    xgetarg 1,lincnt

    if lincnt <= 57 then    ! just need a few params for most lines

        xgetargs lincnt,pgcnt,pline
        if XCBCNT >= 11    xgetarg 11,linctl
        if XCBCNT >= 15    xgetarg 15,pchanl

    else                    ! for headers, we need them all

        xgetargs lincnt,pgcnt,pline,title,hdr1,hdr2,hdr3, &
            legnd1,legnd2,legnd3,linctl,pagsiz,prctl2,lgnctl, &
            pchanl,pcn,pff
    endif

    ! debugging...
    debug.print "lincnt=["+lincnt+"], pgcnt=["+pgcnt+"], title=["+title+"]" 
   
    if lincnt > 57    call New'Page()               ! handle new page

    lincnt = Fn'Print'Line(pchanl, pline, lincnt, linctl, pagsiz)   ! output the line

return'to'caller:
    xputarg 1,lincnt        ! update the line count
    xputarg 2,pgcnt         ! update the page count
    end		

! -------------------------------------------------------------------
trap:
    ? "Error #";err(0);" in PRINT2.SBX!!!"
    xcall SLEEP,5
    xcall ASFLAG,128	! set ^C in caller
    end			! return to caller

! -------------------------------------------------------------------
! Function: lincnt = Fn'Print'Line(pchanl, pline, lincnt, linctl, pagsiz)
! Outputs a single line to the specified channel
! Params:
  ! ch [num, in] file channel
  ! prtlin [str, in] string to output
  ! lincnt [num, in] lincnt
  ! linctl [num, in] number of blank lines in between
  ! pagsiz [num, in] page size
! Returns
!   new lincnt
! Notes:
!   This is perhaps overkill as a function, but does serve to 
!   consolidate the lincnt arithmetic, the stripping of trailing
!   blanks, and the defaulting of the print channel. DOES NOT
!   HANDLE PAGE HEADERS! 

! Miro - print blank lines, if required
! ------------------------------------------------------------------

Function Fn'Print'Line(ch as b2, prtlin as s512, lincnt as b2, linctl as b2, pagsiz as b2) as b2
    map1 N,b,1   
    if ch = 0 then ch = 14      ! default print channel  
    print #ch, strip(prtlin)
    lincnt = lincnt + 1  ! return updated line count  
    ! Print any blank lines, if required
    if linctl > 1 then
        Print'Blank'Lines:
            print #ch, space(pagsiz)
            N = N + 1
            lincnt = lincnt + 1  ! return updated line count
            IF N <= linctl - 2 then goto Print'Blank'Lines    
    endif 
    Fn'Print'Line = lincnt
End Function
    
! -------------------------------------------------------------------
! Procedure: New'Page()
! Handles all the details of generating a new page and headers.
! Params:
!   All are global (not worth passing them individually from the 
!   main body for such a simple module.)  The only point of even 
!   using a Procedure rather than a global subroutine is to allow
!   compartmentalizing of local temp variables 
! Updates:
!   lincnt, pgcnt 
! -------------------------------------------------------------------
Procedure New'Page()

    ++PRAGMA AUTO_EXTERN        ! allow global vars
![111]    ++INCLUDE CKPPN1.INC

![111] 
MAP1 JOBNO,B,2

MAP1    JOB'STATS
    MAP2    LOG'DEV,S,3             ! device
    MAP2    LOG'LUN,S,3             ! logical unit
    MAP2    PROJECT,S,3             ! project number
    MAP2    PROGRMR,S,3             ! programmer number
    MAP2    EXTRA'STATS             
        MAP3    PARTITION,S,6   ! job name
        MAP3    PROG'ID,S,6     ! program name
        MAP3    TERM'NAME,S,6   ! terminal name
        MAP3    TERM'DRVR,S,6   ! terminal driver name
        MAP3    INTERFACE,S,6   ! idv name
        MAP3    JOB'NMBR,S,3    ! job number
        MAP3    JOB'TYPE,B,2    ! job type flags
        MAP3    CMD'FLSZ,B,2    ! cmd file size (0=no cmd file)
        MAP3    JOBPPN,B,2      ! [p,pn] as a binary
        MAP3    JOBEXP,B,1      ! job experience
    MAP2 MORE'STATS
        MAP3    JOBLVL,B,1      ! job level
        MAP3    JOBUID,S,3      ! user id
        MAP3    JOBUSN,S,20     ! user name
        MAP3    JOBCPU,B,4      ! cpu time used
        MAP3    JOBDSR,B,4      ! # of reads
        MAP3    JOBDSW,B,4      ! # of writes
        MAP3    JOBATT,S,6      ! Parent job name
        MAP3    JOBSTS,B,2      ! [2] job status
        MAP3    TRKVER,S,3      ! [3] tracking version



    XCALL PLYJOB,"",JOB'STATS	! This call returns userid and filename

    map1 locals
        map2 coname'ch,b,2,61111
        map2 coname'text,s,80,pcn
        map2 workline,s,256
        map2 datestring,s,40
        map2 x,b,2

    pgcnt = pgcnt + 1

    ! generate a page break (either with a FF or several LF chars)
    if (pff = 0) then
        if pgcnt > 1 then 
           ? #pchanl,chr(12)
        endif
        lincnt = 1
    else
        do while lincnt < 60
            ? #pchanl
            lincnt = lincnt + 1
        loop
    endif
    
    ! get date (needed for every page)
    xcall ODTIM, datestring, 0, TIME, 0 ! &h00200  ! (dd-mon-yr)     

    if lookup(pcn) = 0 then 
       pcn = pcn + "coname.dat"
    endif 
    if lookup(pcn) <> 0 then
        open #coname'ch, pcn, input
        input line #coname'ch, coname'text
        close #coname'ch
    endif     
    ! -----------------------------------------------------------    
    ! print first line 
    ! <date time>             <Company>                  <page num>    
    ! -----------------------------------------------------------
    workline = space(pagsiz)
    workline[1;19] = datestring
    workline[pagsiz-8;9] ="PAGE "+(pgcnt using "####")
    xcall trim, coname'text
    x = pagsiz/2 - len(coname'text)/2
    workline[x;len(coname'text)] = coname'text

    lincnt = Fn'Print'Line(pchanl, workline, lincnt, linctl, pagsiz)

    ! -----------------------------------------------------------
    ! Print second line
    ! <userid>               <Report Title>               <program>
    ! ------------------------------------------------------------
    workline = space(pagsiz)
    workline[1;len(JOBUSN)] = JOBUSN
    workline[pagsiz-len(PROG'ID)+1;len(PROG'ID)] = PROG'ID
    x = pagsiz/2 - len(title)/2
    workline[x;len(title)] = title

    lincnt = Fn'Print'Line(pchanl, workline, lincnt, linctl, pagsiz)
    
    ! Print blank line 
    workline = space(pagsiz)
    lincnt = Fn'Print'Line(pchanl, workline, lincnt, linctl, pagsiz)


    ! ----  Legends ----------------------------------------------------------------        
    if pgcnt = 1 then  
        if legnd1[1;9] <> "NO LEGEND" then 
            lincnt = Fn'Print'Line(pchanl, legnd1, lincnt, linctl, pagsiz)   
            if legnd2[1;9] <> "NO LEGEND" then 
                lincnt = Fn'Print'Line(pchanl, legnd2, lincnt, linctl, pagsiz)
                if legnd3[1;9] <> "NO LEGEND" then 
                    lincnt = Fn'Print'Line(pchanl, legnd3, lincnt, linctl, pagsiz)
                endif ! if legnd3[1;9] <> "NO LEGEND" then
             endif ! if legnd2[1;9] <> "NO LEGEND" then
        endif ! if legnd1[1;9] <> "NO LEGEND" then
       
    else  ! for rest of pages > 1  
        ! if legends are only on the first page
        if lgnctl <> 0 then 
            if legnd1[1;9] <> "NO LEGEND" then 
                lincnt = Fn'Print'Line(pchanl, legnd1, lincnt, linctl, pagsiz)   
                if legnd2[1;9] <> "NO LEGEND" then 
                    lincnt = Fn'Print'Line(pchanl, legnd2, lincnt, linctl, pagsiz)
                    if legnd3[1;9] <> "NO LEGEND" then 
                        lincnt = Fn'Print'Line(pchanl, legnd3, lincnt, linctl, pagsiz)
                    endif ! if legnd3[1;9] <> "NO LEGEND" then
                 endif ! if legnd2[1;9] <> "NO LEGEND" then
            endif ! if legnd1[1;9] <> "NO LEGEND" then
        endif ! if lgnctl <> 0 then
    endif ! for rest of pages > 1
    
    ! ------- Headers - on every page ----------------------------------
    if hdr1[1;3] <> "NO " then 
         lincnt = Fn'Print'Line(pchanl, hdr1, lincnt, linctl, pagsiz)   
         if hdr2[1;3] <> "NO " then 
             lincnt = Fn'Print'Line(pchanl, hdr2, lincnt, linctl, pagsiz)
             if hdr3[1;3] <> "NO " then 
                 lincnt = Fn'Print'Line(pchanl, hdr3, lincnt, linctl, pagsiz)
             endif 
         endif 
    endif 

    ! Print blank line 
    workline = space(pagsiz)
    lincnt = Fn'Print'Line(pchanl, workline, lincnt, linctl, pagsiz)

End Procedure

