! Nome: repstr
! Autor: Jorge Tavares
! Descri��o: replace strings
! Data: 2005.08.30
!---------------------------------------------------------------------------------------
! Hist�rico de altera��es
!---------------------------------------------------------------------------------------
! 2005.10.15 - 1.0(001) - replace by "empty"
! 2005.10.29 - 1.0(002) - resize of the in variable for 2000
! 2005.09.07 - 1.1(000) - was not handling correctly the replace by NULL
!			   - use of "DO LOOP" instead of "FOR NEXT" because the value of len(strall) do not change during the FOR loop
!			   - if any of the in variable or replace string are NULL, exits w/o doing anything
!			   - replace WORDS, the word to replace can be of different size of the new word
!				  e.g.: replace A by AA in ANALFABETO, result = AANAALFAABETO
!			   NOTE: if the new word contain the replacing word, it's not processed due to a possible inifinite loop
!			   - replace a group of characters by a single character, e.g.: ".-_;:" by ""
!			   - replace corresponding characters of two lists, needs option=1 and both lists must be of the same size
!			      e.g.: ".-_;:" by ",+�:" ;the "." (dot) is replaced the  "," (comma); the "-" (hiphen) by the "+" (plus sign) an so on until the end of the list
!---------------------------------------------------------------------------------------

++pragma sbx

       				PROGRAM repstr,1.1(000)



map1 strall,s,10000
map1 oldv$,s,100
map1 newv$,s,100
map1 option,b,1
map1 i,F,6
map1 j,f,6
map1 x,f,6
map1 y,f,6
map1 vazio,b,1
map1 old'char,s,1
map1 new'char,s,1


	XGETARGS strall,oldv$,newv$,option

	if (len(strall)=0 OR len(oldv$)=0) then                       ! base string and sub-string to be replaced, can't be empty
	   goto end'repstr
	endif

	if len(newv$)=0 then                                          ! identify empty new sub-string to replace 
	   vazio = 1
	endif
	
	if (len(oldv$)=1 AND len(newv$)<2) then	                    ! if sub-string to be replaced is single character and
	   old'char = oldv$                                           ! new sub-string also or empty, proceed for single character
	   new'char = newv$	                                         ! replacement and exit
	   call replace'one'character                                 
	   
	   goto end'repstr
	endif
	
	if (len(oldv$)>1 AND len(newv$)<2 AND option=0) then	      ! if sub-string to be replaced is a word and
	   for j=1 to len(oldv$)                                      ! new sub-string ia a single character or empty and
	       old'char = oldv$[j;1]                                  ! option=0 (4th parameter) means that any single character
		new'char = newv$                                       ! from the sub-string to be replaced will be replaced by
	       call replace'one'character                             ! the single character new sub-string
	   next j                                                     ! e.g.: in "-A 123.456:789" subtitute "- .:" by "" (null) 
	                                                              ! result= "A123456789"
	   goto end'repstr                                            
	endif


	if (len(oldv$)>1 AND len(oldv$)=len(newv$) AND option=1) then ! if sub-string to be replaced is a word as well as the
	   for j=1 to len(oldv$)                                      ! new sub-string both with the same size and option=1
	       old'char = oldv$[j;1]                                  ! means that any single character in sequence
		new'char = newv$[j;1]                                  ! from the sub-string to be replaced will be replaced by
	       call replace'one'character                             ! the corresponding single character in the new sub-string
	   next j                                                     ! e.g.: in "-A 123.456:789" subtitute "- .:" by "+@|#"  
	                                                              ! result= "+A@123|456#789"
	   goto end'repstr                                            
	endif

                                                                     ! word substitution will take place in under two conditions:
       x = len(newv$)                                                ! - both, old and new sub-strings are not single characters
       y = len(oldv$)	                                         !   and option<>1
       call replace'words						      ! - option>1	
	           

end'repstr:	
	return(strall)

end



replace'one'character:	
	i = 1
	do
	   if i>len(strall) then
	      exit
	   endif

	   if strall[i;1]=old'char then
	      if vazio then
		  if i=len(strall) then
		     if i=1 then
		        strall = ""
		     else
		        strall = strall[1,-2]
		     endif
		  else 
		     if i=1 then
		        strall = strall[i+1,-1]
		     else	
		        strall = strall[1,i-1] + strall[i+1,-1]
		     endif
		     i = i - 1
		  endif
	      else
		  strall[i;1] = new'char
	      endif
	   endif
	   
	   i = i + 1
	loop
return


replace'words:
	j = 1
	do
	   i = instr(j,strall,oldv$[1;y])
	   if i=0 then
	      exit
	   endif
	   
	   if (i+y)>len(strall) then
	      if i=1 then
	         strall = newv$[1;x]
	      else
	         strall = strall[1,i-1] + newv$[1;x]
	      endif
	      
	      exit
	   else
	      if i=1 then
	         strall = newv$[1;x] + strall[i+y,-1]
	      else
	         strall = strall[1,i-1] + newv$[1;x] + strall[i+y,-1]
	      endif

	      j = i + y
	   endif	   
	loop
return

