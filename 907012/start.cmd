:<A-Shell Source Procedures
(These get overwritten frequently so if you want to make your own 
personal modifications, it is advised that you move your copy elsewhere.)

See [907,13] for test programs (generally named same as procedure bsi)

Procedure   Description

StsMsgBox   Output floating, auto-closing status message to screen

SysLog      Output formatted/time-stamped messages to log file, trace 
                file and/or screen.  (Similar to EVTWIN.SBX with with
                no GUI display.)
xtr32bits   Present up to 32 option flags inside of an XTREE using
                checkboxes.
>
SET LONGDIR
