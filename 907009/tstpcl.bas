
REM	**************  TSTPCL.BAS  ****************************************

REM	Program for demo'ing some of the features of PCLUTL.BSI

REM	v1.0	Original  				SWS	01/15/08
REM     v1.1    Minor mods for SOSLIB                   JDM     01/15/08

		++INCLUDE PCLUTL.BSI[907,8]


		!-------------------!
		! Letterhead fields !
		!-------------------!

		MAP1  STR'NAM, S, 60
		MAP1  STR'AD1, S, 60
		MAP1  STR'AD2, S, 60
		MAP1  STR'PH1, S, 60
		MAP1  STR'FAX, S, 60
		MAP1  STR'MAI, S, 60

		MAP1  FNT'NAM, S, 50		! PCL strings to set fonts
		MAP1  FNT'ADR, S, 50
		MAP1  FNT'PHN, S, 50
		MAP1  FNT'MAI, S, 50

		MAP1  POS'NAM, S, 50		! PCL string to set Horiz and
		MAP1  POS'AD1, S, 50		!   Vertical position of flds
		MAP1  POS'AD2, S, 50
		MAP1  POS'PH1, S, 50
		MAP1  POS'FAX, S, 50
		MAP1  POS'MAI, S, 50

		MAP1  POS'COLN,  F		! Horiz pos of Name
		MAP1  POS'COLA1, F		! Horiz pos of Addr 1
		MAP1  POS'COLA2, F		! Horiz pos of Addr 2
		MAP1  POS'COLP1, F		! Horiz pos of Phone 1
		MAP1  POS'COLF,  F		! Horiz pos of Fax
		MAP1  POS'COLM,  F		! Horiz pos of email addr

		MAP1  POS'ROWN,  F		! Vert pos of Name
		MAP1  POS'ROWA1, F		! Vert pos of Addr line 1
		MAP1  POS'ROWA2, F		! Vert pos of Addr line 2
		MAP1  POS'ROWP1, F		! Vert pos of Phone 1
		MAP1  POS'ROWF,  F		! Vert pos of Fax
		MAP1  POS'ROWM,  F		! Vert pos of email addr

		!---------------------!
		! Line drawing fields !
		!---------------------!

		MAP1  HL00'ROW,	 F	! Horizontal line fields
		MAP1  HL01'ROW,	 F
		MAP1  HL02'ROW,	 F
		MAP1  HL03'ROW,	 F
		MAP1  HL04'ROW,	 F
		MAP1  HL05'ROW,	 F
		MAP1  HL06'ROW,	 F
		MAP1  HL07'ROW,	 F
		MAP1  HL08'ROW,	 F
		MAP1  HL09'ROW,	 F
		MAP1  HL10'ROW,	 F
		MAP1  HL11'ROW,	 F
		MAP1  HL12'ROW,	 F
		MAP1  HL13'ROW,	 F
		MAP1  HL14'ROW,	 F
		MAP1  HL15'ROW,	 F

		MAP1  HL00'COL,	 F
		MAP1  HL01'COL,	 F
		MAP1  HL02'COL,	 F
		MAP1  HL03'COL,	 F
		MAP1  HL04'COL,	 F
		MAP1  HL05'COL,	 F
		MAP1  HL06'COL,	 F
		MAP1  HL07'COL,	 F
		MAP1  HL08'COL,	 F
		MAP1  HL09'COL,	 F
		MAP1  HL10'COL,	 F
		MAP1  HL11'COL,	 F
		MAP1  HL12'COL,	 F
		MAP1  HL13'COL,	 F
		MAP1  HL14'COL,	 F
		MAP1  HL15'COL,	 F

		MAP1  HL00'LEN,	 F
		MAP1  HL01'LEN,	 F
		MAP1  HL02'LEN,	 F
		MAP1  HL03'LEN,	 F
		MAP1  HL04'LEN,	 F
		MAP1  HL05'LEN,	 F
		MAP1  HL06'LEN,	 F
		MAP1  HL07'LEN,	 F
		MAP1  HL08'LEN,	 F
		MAP1  HL09'LEN,	 F
		MAP1  HL10'LEN,	 F
		MAP1  HL11'LEN,	 F
		MAP1  HL12'LEN,	 F
		MAP1  HL13'LEN,	 F
		MAP1  HL14'LEN,	 F
		MAP1  HL15'LEN,	 F

		MAP1  VL01'ROW,	 F	! Vertical line fields
		MAP1  VL02'ROW,	 F
		MAP1  VL03'ROW,	 F
		MAP1  VL04'ROW,	 F
		MAP1  VL05'ROW,	 F
		MAP1  VL06'ROW,	 F
		MAP1  VL07'ROW,	 F
		MAP1  VL08'ROW,	 F
		MAP1  VL09'ROW,	 F
		MAP1  VL10'ROW,	 F
		MAP1  VL11'ROW,	 F
		MAP1  VL12'ROW,	 F

		MAP1  VL01'COL,	 F
		MAP1  VL02'COL,	 F
		MAP1  VL03'COL,	 F
		MAP1  VL04'COL,	 F
		MAP1  VL05'COL,	 F
		MAP1  VL06'COL,	 F
		MAP1  VL07'COL,	 F
		MAP1  VL08'COL,	 F
		MAP1  VL09'COL,	 F
		MAP1  VL10'COL,	 F
		MAP1  VL11'COL,	 F
		MAP1  VL12'COL,	 F

		MAP1  VL01'LEN,	 F
		MAP1  VL02'LEN,	 F
		MAP1  VL03'LEN,	 F
		MAP1  VL04'LEN,	 F
		MAP1  VL05'LEN,	 F
		MAP1  VL06'LEN,	 F
		MAP1  VL07'LEN,	 F
		MAP1  VL08'LEN,	 F
		MAP1  VL09'LEN,	 F
		MAP1  VL10'LEN,	 F
		MAP1  VL11'LEN,	 F
		MAP1  VL12'LEN,	 F

		MAP1  TL01'ROW,	 F	! Title fields
		MAP1  TL02'ROW,	 F
		MAP1  TL03'ROW,	 F
		MAP1  TL04'ROW,	 F
		MAP1  TL05'ROW,	 F
		MAP1  TL06'ROW,	 F
		MAP1  TL07'ROW,	 F
		MAP1  TL08'ROW,	 F
		MAP1  TL09'ROW,	 F
		MAP1  TL10'ROW,	 F
		MAP1  TL11'ROW,	 F
		MAP1  TL12'ROW,	 F
		MAP1  TL13'ROW,	 F
		MAP1  TL14'ROW,	 F
		MAP1  TL15'ROW,	 F
		MAP1  TL16'ROW,	 F
		MAP1  TL17'ROW,	 F
		MAP1  TL18'ROW,	 F

		MAP1  TL01'COL,	 F
		MAP1  TL02'COL,	 F
		MAP1  TL03'COL,	 F
		MAP1  TL04'COL,	 F
		MAP1  TL05'COL,	 F
		MAP1  TL06'COL,	 F
		MAP1  TL07'COL,	 F
		MAP1  TL08'COL,	 F
		MAP1  TL09'COL,	 F
		MAP1  TL10'COL,	 F
		MAP1  TL11'COL,	 F
		MAP1  TL12'COL,	 F
		MAP1  TL13'COL,  F
		MAP1  TL14'COL,  F
		MAP1  TL15'COL,  F
		MAP1  TL16'COL,	 F
		MAP1  TL17'COL,	 F
		MAP1  TL18'COL,	 F

		MAP1  SH01'ROW,	 F	! Shade fields
		MAP1  SH02'ROW,	 F
		MAP1  SH03'ROW,	 F
		MAP1  SH04'ROW,	 F
		MAP1  SH05'ROW,	 F
		MAP1  SH06'ROW,	 F

		MAP1  SH01'COL,	 F
		MAP1  SH02'COL,	 F
		MAP1  SH03'COL,	 F
		MAP1  SH04'COL,	 F
		MAP1  SH05'COL,	 F
		MAP1  SH06'COL,	 F

		MAP1  SH01'LEN,	 F
		MAP1  SH02'LEN,	 F
		MAP1  SH03'LEN,	 F
		MAP1  SH04'LEN,	 F
		MAP1  SH05'LEN,	 F
		MAP1  SH06'LEN,	 F

		MAP1  LTHK,	 F	! Line thickness
		MAP1  COL1,	 F	! Starting col
		MAP1  ROW1,	 F	! Starting row

		MAP1  UCOL,	 F
		MAP1  UROW,	 F
		MAP1  ULEN,	 F
		MAP1  UHGT,	 F

		MAP1  COPY'DESC, S,20
		MAP1  BFAX'DESC, S,40		! BROKER FAX:   1-20 chars
		MAP1  BEML'DESC, S,47		! BROKER Email: 1-40 chars

		MAP1  STXT,	 S,300

		MAP1  PAYM'MSG1, S, 43,					   &
		  "FULL PAYMENT IS DUE UPON RECEIPT OF INVOICE"		   !
		MAP1  PAYM'MSG2, S, 54,					   &
		  "INSTALLMENT PAYMENTS ARE DUE ON OR BEFORE THE DUE DATE" !

		!-----------------------------------!
		! Other fields used by this program !
		!-----------------------------------!

		MAP1  SAVE'HOUSE'NUM,S,2,"01"	! House number
		MAP1  POL'TRANS'CODE,S,2,"01"	! Transaction Type Code
		MAP1  INSTAL'FLG,    S, 1, "N"	! Installments: Y or N
		MAP1  PCNT'MAIN,     F, 6, 10	! # of amount lines
		MAP1  PCNT'INST,     F, 6,  0	! # of extra lns for install
		MAP1  N,	     F, 6	! Work field

! ***************************************************************************

	MAIN'RTN:
                ? "Creating PCL report file TSTPCL.LST using PCLUTL.BSI..."
		OPEN  #15, "TSTPCL.LST", OUTPUT
		PRT'FNO = 15 : GOSUB INIT'PORT10	! (see: PCLUTL.BSI)

!		--------------------------
		GOSUB INIT'PRINT'POSITIONS
!		--------------------------

!		----------------
		GOSUB PRINT'FORM
!		----------------

		GOSUB END'PRINTOUTS			! (see: PCLUTL.BSI)

		XCALL ECHO
		PRINT TAB(23,1)
		END

! ***************************************************************************

	INIT'PRINT'POSITIONS:
3000		COL1     = 0 + (1/32)		! Starting column (in inches)
		ROW1     = 1         		! Starting row    (in inches)

		!---------------------------------------------!
		! Define start positions for Horizontal Lines !
		!---------------------------------------------!

3010		HL00'COL = COL1 + 0 + ( 1/16) : HL00'ROW = ROW1 + 0 - (10/16)
		HL01'COL = COL1 + 6 + ( 2/16) : HL01'ROW = ROW1 + 0 + ( 7/16)
		HL02'COL = COL1 + 6 + ( 2/16) : HL02'ROW = ROW1 + 0 + (10/16)
		HL03'COL = COL1 + 6 + ( 2/16) : HL03'ROW = ROW1 + 0 + (15/16)
		HL04'COL = COL1 + 6 + ( 2/16) : HL04'ROW = ROW1 + 1 + ( 2/16)
		HL05'COL = COL1 + 6 + ( 2/16) : HL05'ROW = ROW1 + 1 + ( 5/16)
		HL06'COL = COL1 + 6 + ( 2/16) : HL06'ROW = ROW1 + 1 + (10/16)
		HL07'COL = COL1 + 0 + ( 1/16) : HL07'ROW = ROW1 + 2 + ( 0/16)
		HL08'COL = COL1 + 0 + ( 1/16) : HL08'ROW = ROW1 + 2 + ( 3/16)
		HL09'COL = COL1 + 0 + ( 1/16) : HL09'ROW = ROW1 + 2 + ( 8/16)
		HL10'COL = COL1 + 0 + ( 1/16) : HL10'ROW = ROW1 + 2 + (11/16)
		HL11'COL = COL1 + 0 + ( 1/16) : HL11'ROW = ROW1 + 3 + ( 0/16)
		HL12'COL = COL1 + 0 + ( 1/16) : HL12'ROW = ROW1 + 3 + ( 3/16)
		HL13'COL = COL1 + 0 + ( 1/16) : HL13'ROW = ROW1 + 3 + ( 1/32)
		HL14'COL = COL1 + 0 + ( 1/16) : HL14'ROW = ROW1 + 3 + ( 7/32)
		HL15'COL = COL1 + 0 + ( 1/16) : HL15'ROW = ROW1 + 4 + (14/16)

		!-----------------------------------!
		! Define length of Horizontal Lines !
		!-----------------------------------!

3022		HL00'LEN = 7 + (26/32)
		HL01'LEN = 1 + ( 5/16)
		HL02'LEN = 1 + ( 5/16)
		HL03'LEN = 1 + ( 5/16)
		HL04'LEN = 1 + ( 5/16)
		HL05'LEN = 1 + ( 5/16)
		HL06'LEN = 1 + ( 5/16)
		HL07'LEN = 7 + (26/32)
		HL08'LEN = 7 + (26/32)
		HL09'LEN = 7 + (26/32)
		HL10'LEN = 7 + (26/32)
		HL11'LEN = 7 + (26/32)
		HL12'LEN = 7 + (26/32)
		HL13'LEN = 7 + (26/32)
		HL14'LEN = 7 + (26/32)
		HL15'LEN = 7 + (26/32) + (3/720)	! (close the box)

		!-------------------------------------------!
		! Define start positions for Vertical Lines !
		!-------------------------------------------!

3030		VL01'ROW = ROW1 + 0 - (17/32) : VL01'COL = COL1 + 0 + ( 1/16)
		VL02'ROW = ROW1 + 2 + ( 8/16) : VL02'COL = COL1 + 1 + (11/16)
		VL03'ROW = ROW1 + 2 + ( 0/16) : VL03'COL = COL1 + 2 + (11/16)
		VL04'ROW = ROW1 + 2 + ( 8/16) : VL04'COL = COL1 + 3 + ( 4/16)
		VL05'ROW = ROW1 + 3 + ( 0/16) : VL05'COL = COL1 + 4 + ( 0/16)
		VL06'ROW = ROW1 + 2 + ( 8/16) : VL06'COL = COL1 + 4 + (13/16)
		VL07'ROW = ROW1 + 2 + ( 0/16) : VL07'COL = COL1 + 5 + ( 6/16)
		VL08'ROW = ROW1 + 0 + ( 7/16) : VL08'COL = COL1 + 6 + ( 2/16)
		VL09'ROW = ROW1 + 1 + ( 2/16) : VL09'COL = COL1 + 6 + ( 2/16)
		VL10'ROW = ROW1 + 0 + ( 7/16) : VL10'COL = COL1 + 7 + ( 7/16)
		VL11'ROW = ROW1 + 1 + ( 2/16) : VL11'COL = COL1 + 7 + ( 7/16)
		VL12'ROW = (300/720)	      : VL12'COL = 7 + (645/720)

		!---------------------------------!
		! Define length of Vertical Lines !
		!---------------------------------!

3040		VL01'LEN = 5 + (13/32)
		VL02'LEN = 0 + ( 8/16)
		VL03'LEN = 0 + ( 8/16)
		VL04'LEN = 0 + ( 8/16)
		VL05'LEN = 1 + (14/16)
		VL06'LEN = 0 + ( 8/16)
		VL07'LEN = 0 + ( 8/16)
		VL08'LEN = 0 + ( 8/16)
		VL09'LEN = 0 + ( 8/16)
		VL10'LEN = 0 + (366/720) + LTHK
		VL11'LEN = 0 + (366/720) + LTHK
		VL12'LEN = 5 + (15/32)   + LTHK

		!----------------------------------------!
		! Define start positions for Title Lines !
		!----------------------------------------!

3050		ROW1 = ROW1 + (1/64)		! Adjust for titles

		TL01'COL = COL1 + 3 + ( 7/16) : TL01'ROW = ROW1 + 0 + ( 4/16)
		TL02'COL = COL1 + 6 + ( 5/16) : TL02'ROW = ROW1 + 0 + ( 9/16)
		TL03'COL = COL1 + 6 + (10/16) : TL03'ROW = ROW1 + 1 + ( 4/16)
		TL04'COL = COL1 + 0 + (14/16) : TL04'ROW = ROW1 + 2 + ( 2/16)
		TL05'COL = COL1 + 3 + ( 7/16) : TL05'ROW = ROW1 + 2 + ( 2/16)
		TL06'COL = COL1 + 5 + (14/16) : TL06'ROW = ROW1 + 2 + ( 2/16)
		TL07'COL = COL1 + 0 + ( 7/16) : TL07'ROW = ROW1 + 2 + (10/16)
		TL08'COL = COL1 + 2 + ( 0/16) : TL08'ROW = ROW1 + 2 + (10/16)
		TL09'COL = COL1 + 3 + ( 9/16) : TL09'ROW = ROW1 + 2 + (10/16)
		TL10'COL = COL1 + 5 + (11/16) : TL10'ROW = ROW1 + 2 + (10/16)
		TL11'COL = COL1 + 1 + ( 8/16) : TL11'ROW = ROW1 + 3 + ( 2/16)
		TL12'COL = COL1 + 5 + (11/16) : TL12'ROW = ROW1 + 3 + ( 2/16)
		TL13'COL = COL1 + 3 + ( 5/16) : TL13'ROW = ROW1 + 3 + ( 7/32)
		TL14'COL = COL1 + 0 + ( 5/32) : TL14'ROW = ROW1 + 4 + (13/16)
		TL15'COL = COL1 + 7 + (19/32) : TL15'ROW = ROW1 + 4 + (13/16)
		TL16'COL = COL1 + 2 + ( 3/16) : TL16'ROW = ROW1 + 5 + ( 3/16)
		TL17'COL = COL1               : TL17'ROW = ROW1 + 5 + ( 7/16)
		TL18'COL = COL1 + 0 + (11/16) : TL18'ROW = ROW1 + 1 + (27/32)

		ROW1 = ROW1 - (1/64)		! Back to normal

		!------------------------------------!
		! Define start positions for Shading !
		!------------------------------------!

3060		SH01'COL = COL1 + 6 + ( 2/16) : SH01'ROW = ROW1 + 0 + ( 7/16)
		SH02'COL = COL1 + 6 + ( 2/16) : SH02'ROW = ROW1 + 1 + ( 2/16)
		SH03'COL = COL1 + 0 + ( 1/16) : SH03'ROW = ROW1 + 2 + ( 0/16)
		SH04'COL = COL1 + 0 + ( 1/16) : SH04'ROW = ROW1 + 2 + ( 8/16)
		SH05'COL = COL1 + 0 + ( 1/16) : SH05'ROW = ROW1 + 3 + ( 0/16)
		SH06'COL = COL1 + 0 + ( 1/16) : SH06'ROW = ROW1 + 3 + ( 3/32)

		!--------------------------!
		! Define length of Shading !
		!--------------------------!

3070		SH01'LEN = 1 + ( 5/16)
		SH02'LEN = 1 + ( 5/16)
		SH03'LEN = 7 + (26/32)
		SH04'LEN = 7 + (26/32)
		SH05'LEN = 7 + (26/32)
		SH06'LEN = 7 + (26/32)

		RETURN

! ***************************************************************************

! Draw the form, line by line.  Specify size/pos using inches

	PRINT'FORM:
!		----------------
		GOSUB PRT'LTRHED
!		----------------

		!------------------!
		! Horizontal lines !
		!------------------!

4000		LTHK = ( 7/720)				! Line thickness

		UCOL = HL01'COL : UROW = HL01'ROW : ULEN = HL01'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  1

		UCOL = HL02'COL : UROW = HL02'ROW : ULEN = HL02'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  2

		UCOL = HL03'COL : UROW = HL03'ROW : ULEN = HL03'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  3

		UCOL = HL04'COL : UROW = HL04'ROW : ULEN = HL04'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  4

		UCOL = HL05'COL : UROW = HL05'ROW : ULEN = HL05'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  5

		UCOL = HL06'COL : UROW = HL06'ROW : ULEN = HL06'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  6

		UCOL = HL07'COL : UROW = HL07'ROW : ULEN = HL07'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  7

		UCOL = HL08'COL : UROW = HL08'ROW : ULEN = HL08'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  8

		UCOL = HL09'COL : UROW = HL09'ROW : ULEN = HL09'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line  9

		UCOL = HL10'COL : UROW = HL10'ROW : ULEN = HL10'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 10

		UCOL = HL11'COL : UROW = HL11'ROW : ULEN = HL11'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 11

		UCOL = HL12'COL : UROW = HL12'ROW : ULEN = HL12'LEN
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 12

		IF INSTAL'FLG = "Y"					&
		  UCOL = HL13'COL : UROW = HL13'ROW : ULEN = HL13'LEN : &
		  UROW = UROW + ((PCNT'MAIN+1) / 6) + ( 1/16)	      :	&
		  UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 13 !

		IF INSTAL'FLG = "Y"					&
		  UCOL = HL14'COL : UROW = HL14'ROW : ULEN = HL14'LEN : &
		  UROW = UROW + ((PCNT'MAIN+1) / 6) + ( 1/16)	      :	&
		  UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 14 !

		LTHK = (10/720)				! Line thickness

		!--------------------------------------------------------!
		! The bottom line's position varies depending on:	 !
		!  a) If there are > 10 amts, the line needs to be lower !
		!     than it's default position			 !
		!  b) If there is an installment section, the line needs !
		!     to be printed below that				 !
		!							 !
		! Likewise, we have to adjust the height of the vertical !
		!     lines that connect to it: L01, L05, and L12	 !
		!--------------------------------------------------------!

		UCOL = HL15'COL : UROW = HL15'ROW : ULEN = HL15'LEN
		IF PCNT'MAIN > 10    THEN  UROW = UROW + ((PCNT'MAIN-10)/6)
		IF INSTAL'FLG = "Y"  THEN  UROW = UROW + ((PCNT'INST+2)/ 6)
		UHGT = LTHK     : GOSUB DRAW'LINE	! Horiz line 15

		LTHK = ( 7/720)				! Line thickness

		!-------------------------------------------------------!
		! Vertical lines					!
		!							!
		! Note:  Add a line thickness (LTHK) to vertical lines	!
		!	 which close a box				!
		!-------------------------------------------------------!

4100		LTHK = (10/720)				! Line thickness

		UCOL = VL01'COL : UROW = VL01'ROW : UHGT = VL01'LEN
		IF PCNT'MAIN > 10    THEN  UHGT = UHGT + ((PCNT'MAIN-10)/6)
		IF INSTAL'FLG = "Y"  THEN  UHGT = UHGT + ((PCNT'INST+2)/ 6)
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  1

		LTHK = ( 7/720)				! Line thickness

		UCOL = VL02'COL : UROW = VL02'ROW : UHGT = VL02'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  2

		UCOL = VL03'COL : UROW = VL03'ROW : UHGT = VL03'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  3

		UCOL = VL04'COL : UROW = VL04'ROW : UHGT = VL04'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  4

		UCOL = VL05'COL : UROW = VL05'ROW : UHGT = VL05'LEN
		IF PCNT'MAIN > 10  THEN  UHGT = UHGT + ((PCNT'MAIN-10)/6)
		IF INSTAL'FLG = "Y"					&
			UHGT = (PCNT'MAIN / 6) + ( 4/16) + LTHK		!
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  5

		UCOL = VL06'COL : UROW = VL06'ROW : UHGT = VL06'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  6

		UCOL = VL07'COL : UROW = VL07'ROW : UHGT = VL07'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  7

		UCOL = VL08'COL : UROW = VL08'ROW : UHGT = VL08'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  8

		UCOL = VL09'COL : UROW = VL09'ROW : UHGT = VL09'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line  9

		UCOL = VL10'COL : UROW = VL10'ROW : UHGT = VL10'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line 10

		UCOL = VL11'COL : UROW = VL11'ROW : UHGT = VL11'LEN
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line 11

		LTHK = (10/720)				! Line thickness

		IF SAVE'HOUSE'NUM = "09"		&
			N = (3/64)			&
		ELSE					&
			N = 0				! Adjust if NIF PRO

		UCOL = VL12'COL : UROW = VL12'ROW + N : UHGT = VL12'LEN - N
		IF PCNT'MAIN > 10    THEN  UHGT = UHGT + ((PCNT'MAIN-10)/6)
		IF INSTAL'FLG = "Y"  THEN  UHGT = UHGT + ((PCNT'INST+2)/ 6)
		ULEN = LTHK     : GOSUB DRAW'LINE	! Vert line 12

		LTHK = ( 7/720)				! Line thickness

		!--------------!
		! Field Titles !
		!--------------!

4200		IF SAVE'HOUSE'NUM = "03"	OR	&
		   SAVE'HOUSE'NUM = "05"	OR	&
		   SAVE'HOUSE'NUM = "09"		&
			N = (2/16)			&
		ELSE					&
			N = 0				!Adjust if 4 line addr

		PRINT #PRT'FNO, FONT06;

		UCOL = TL01'COL : UROW = TL01'ROW + N
		STXT = "INVOICE"
		GOSUB PRINT'TITLE			! Print title  1

		PRINT #PRT'FNO, FONT01;

		UCOL = TL02'COL : UROW = TL02'ROW
		STXT = "INVOICE NUMBER"
		GOSUB PRINT'TITLE			! Print title  2

		UCOL = TL03'COL : UROW = TL03'ROW
		STXT = "DATE"
		GOSUB PRINT'TITLE			! Print title  3

		UCOL = TL04'COL : UROW = TL04'ROW
		STXT = "NAME OF ASSURED"
		GOSUB PRINT'TITLE			! Print title  4

		UCOL = TL05'COL : UROW = TL05'ROW
		STXT = "INSURANCE COMPANY"
		GOSUB PRINT'TITLE			! Print title  5

		UCOL = TL06'COL : UROW = TL06'ROW
		STXT = "TRANSACTION DESCRIPTION"
		GOSUB PRINT'TITLE			! Print title  6

		UCOL = TL07'COL : UROW = TL07'ROW
		STXT = "BROKER NUMBER"
		GOSUB PRINT'TITLE			! Print title  7

		UCOL = TL08'COL : UROW = TL08'ROW
		STXT = "EFFECTIVE DATE"
		GOSUB PRINT'TITLE			! Print title  8

		UCOL = TL09'COL : UROW = TL09'ROW
		STXT = "EXPIRATION DATE"
		GOSUB PRINT'TITLE			! Print title  9

		UCOL = TL10'COL : UROW = TL10'ROW
		STXT = "POLICY/BINDER NUMBER"
		GOSUB PRINT'TITLE			! Print title 10

		UCOL = TL11'COL : UROW = TL11'ROW
		STXT = "POLICY DESCRIPTION"
		GOSUB PRINT'TITLE			! Print title 11

		UCOL = TL12'COL : UROW = TL12'ROW
		STXT = "AMOUNT"
		GOSUB PRINT'TITLE			! Print title 12

		IF INSTAL'FLG = "Y"			&
		    UCOL = TL13'COL : UROW = TL13'ROW :	&
		    UROW = UROW + ((PCNT'MAIN+1) /6)  :	&
		    STXT = "INSTALLMENT SCHEDULE"     :	&
		    GOSUB PRINT'TITLE			! Print title 13

		PRINT #PRT'FNO, FONT12;

		UCOL = TL14'COL : UROW = TL14'ROW
		IF PCNT'MAIN > 10    THEN  UROW = UROW + ((PCNT'MAIN-10)/6)
		IF INSTAL'FLG = "Y"  THEN  UROW = UROW + ((PCNT'INST+2)/ 6)
		STXT = COPY'DESC
		GOSUB PRINT'TITLE			! Print title 14

		!------------------!
		! Payment Messages !
		!------------------!

4300		IF POL'TRANS'CODE = "04"   OR		&
		   POL'TRANS'CODE = "05"   OR		&
		   POL'TRANS'CODE = "10"   OR		&
		   POL'TRANS'CODE = "12"   		&
			GOTO  PF'PAY'X			! Skip if Cancel or RP

		PRINT #PRT'FNO, FONT09;

		UCOL = TL16'COL : UROW = TL16'ROW
		IF PCNT'MAIN > 10  THEN  UROW = UROW + ((PCNT'MAIN-10)/6)
		STXT = PAYM'MSG1

4350		IF INSTAL'FLG = "Y"				&
			UCOL = COL1 + 1 + (10/16)	      :	&
			UROW = UROW + ((PCNT'INST+2)/ 6)      :	&
			STXT = PAYM'MSG2			!

		GOSUB PRINT'TITLE			! Print title 16

	PF'PAY'X:
4380		PRINT #PRT'FNO, FONT09;

		UCOL = TL18'COL : UROW = TL18'ROW
		STXT = BFAX'DESC + SPACE(6) + BEML'DESC
		GOSUB PRINT'TITLE			! Print title 18

		PRINT #PRT'FNO, DEFLT'FONT;

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		!-------------!
		! Add shading !
		!-------------!

4400		UCOL = SH01'COL : UROW = SH01'ROW : ULEN = SH01'LEN
		UHGT = ( 3/16)  : GOSUB ADD'SHADING	! Shade fm horiz ln 01

		UCOL = SH02'COL : UROW = SH02'ROW : ULEN = SH02'LEN
		UHGT = ( 3/16)  : GOSUB ADD'SHADING	! Shade fm horiz ln 04

		UCOL = SH03'COL : UROW = SH03'ROW : ULEN = SH03'LEN
		UHGT = ( 3/16)  : GOSUB ADD'SHADING	! Shade fm horiz ln 07

		UCOL = SH04'COL : UROW = SH04'ROW : ULEN = SH04'LEN
		UHGT = ( 3/16)  : GOSUB ADD'SHADING	! Shade fm horiz ln 09

		UCOL = SH05'COL : UROW = SH05'ROW : ULEN = SH05'LEN
		UHGT = ( 3/16)  : GOSUB ADD'SHADING	! Shade fm horiz ln 11

		IF INSTAL'FLG = "Y"					  &
		    UCOL = SH06'COL : UROW = SH06'ROW : ULEN = SH06'LEN : &
		    UROW = UROW + ((PCNT'MAIN+1) /6)			: &
		    UHGT = ( 3/16)  : GOSUB ADD'SHADING			  !
							! Shade fm horiz ln 13
		RETURN

REM *************************************************************************

! At entry:   UROW, UCOL, ULEN, and UHGT  (specified in inches)

	DRAW'LINE:
5000		DCOL = UCOL : DROW = UROW : GOSUB SET'POS'I
		DHGT = UHGT : DLEN = ULEN : GOSUB SET'DRAW'I
		PRINT #PRT'FNO, POS'CMD; DRAW'CMD;		! Draw a line
		RETURN

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

! At entry:   UROW, UCOL, ULEN, and UHGT  (specified in inches)
!
! Note:  This rtn prints a 10% shaded rectangle (in transparent mode -- i.e.,
!	 it won't overlay anything already printed).  The cursor is in the
!	 upper left corner of the rectangle.  Shading is done towards the
!	 right and down.

	ADD'SHADING:
5100		DCOL   = UCOL : DROW   = UROW : GOSUB SET'POS'I
		BOXHGT = UHGT : BOXLEN = ULEN : BSHD = PCT'10
						GOSUB SET'BOX'I
		PRINT #PRT'FNO, POS'CMD; BOX'CMD;		! Draw 10% shaded box
 		RETURN

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

! At entry:   UROW, UCOL (specified in inches), and STXT

	PRINT'TITLE:
5200		DCOL = UCOL : DROW = UROW : GOSUB SET'POS'I
		PRINT #PRT'FNO, POS'CMD; STXT;		! Print the title
		RETURN

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

! At entry:   UCOL (specified in inches) and STXT

	PRINT'TITLE'IN'CURR'ROW:
5300		DCOL = UCOL : GOSUB SET'COL'I
		PRINT #PRT'FNO, POS'CMD; STXT;		! Print the title
		RETURN

! ***************************************************************************

	PRT'LTRHED:
		STR'NAM	 = "AESOPS Corporation"
		STR'AD1  = "130 West 57th Street"		
		STR'AD2  = "New York, New York  10019"
		STR'PH1  = "Tel: (123) 444-5555"
		STR'FAX  = "Fax: (123) 444-6666"
		STR'MAI  = "Email:  SShatz@AesopsCorp.com"

		FNT'TYPE  = FNT'ARIAL : FNT'SIZE = 14 : GOSUB SET'FNT'BOTH
		FNT'NAM	  = FNT'CMD		! Arial 14 Italic Bold
		FNT'ADR   = FONT12		! CG Times 11 Regular
		FNT'TYPE  = FNT'ARIAL : FNT'SIZE =  8.5 : GOSUB SET'FNT'NORM
		FNT'PHN	  = FNT'CMD		! Arial 8.5 Regular
		FNT'MAI   = FNT'PHN		! Arial 8.5 Regular

		POS'COLN  = 1150
		POS'COLA1 = POS'COLN
		POS'COLA2 = POS'COLN
		POS'COLP1 = 4000		! Align phone #s before edge
		POS'COLF  = POS'COLP1 + 900
		POS'COLM  = POS'COLP1 + 5

		POS'ROWN  = 250
		POS'ROWA1 = 425
		POS'ROWA2 = 535
		POS'ROWP1 = POS'ROWA1
		POS'ROWF  = POS'ROWA1
		POS'ROWM  = POS'ROWA2

!		----------------
		GOSUB LTRHED'RTN
!		----------------

		RETURN

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	LTRHED'RTN:
	        FNT'NAM   = FONT06
			! Print Name lines using Arial 18 Bold

		FNT'TYPE  = FNT'TIMESNEW : FNT'SIZE = 12 : GOSUB SET'FNT'ITAL
		FNT'ADR = FNT'CMD
			! Print Address lines using CG Times 12 Italic

		DCOL =   67 : DROW =  300 : GOSUB SET'POS
		DHGT =   10 : DLEN = 5620 : GOSUB SET'DRAW

		PRINT #PRT'FNO, POS'CMD; DRAW'CMD	! Draw horizontal line

		DCOL =   67 : DROW =  300 : GOSUB SET'POS
		DHGT =   40 : DLEN =   10 : GOSUB SET'DRAW
		PRINT #PRT'FNO, POS'CMD; DRAW'CMD
			! Close border box with small vertical line

		XCALL STRIP, STR'NAM			! Strip trailing blanks
		XCALL STRIP, STR'AD1
		XCALL STRIP, STR'AD2
		XCALL STRIP, STR'PH1
		XCALL STRIP, STR'FAX
		XCALL STRIP, STR'MAI

    DCOL = POS'COLN   : DROW = POS'ROWN   : GOSUB SET'POS : POS'NAM = POS'CMD
    DCOL = POS'COLA1  : DROW = POS'ROWA1  : GOSUB SET'POS : POS'AD1 = POS'CMD
    DCOL = POS'COLA2  : DROW = POS'ROWA2  : GOSUB SET'POS : POS'AD2 = POS'CMD
    DCOL = POS'COLP1  : DROW = POS'ROWP1  : GOSUB SET'POS : POS'PH1 = POS'CMD
    DCOL = POS'COLF   : DROW = POS'ROWF   : GOSUB SET'POS : POS'FAX = POS'CMD
    DCOL = POS'COLM   : DROW = POS'ROWM   : GOSUB SET'POS : POS'MAI = POS'CMD

		PRINT #PRT'FNO, FNT'NAM;		! Set Name Font
		PRINT #PRT'FNO, POS'NAM; STR'NAM

		PRINT #PRT'FNO, FNT'ADR;		! Set Address Font
		PRINT #PRT'FNO, POS'AD1; STR'AD1
		PRINT #PRT'FNO, POS'AD2; STR'AD2

		PRINT #PRT'FNO, FNT'PHN;		! Set Phone Font
		PRINT #PRT'FNO, POS'PH1; STR'PH1
		PRINT #PRT'FNO, POS'FAX; STR'FAX

		PRINT #PRT'FNO, FNT'MAI;		! Set Email Font
		PRINT #PRT'FNO, POS'MAI; STR'MAI

		DROW = 0 : GOSUB SET'ROW		! Back to row 0 ...
		PRINT #PRT'FNO, POS'CMD; DEFLT'FONT	! ... & end last line

		RETURN

! ***************************************************************************
!			       END OF TSTPCL.BAS
! ***************************************************************************
