!========================================================================
! FILDIR.SBX - {Scan Directory}			Steve	15/1/2018
!========================================================================
!
! Compile Command: COMPIL FILDIR/X:2/P
!
!------------------------------------------------------------------------
! CHANGE HISTORY
!------------------------------------------------------------------------
!  Date     by     Version     Description
!  ----     --     -------     -----------
! 15/01/18  Steve  4.2(500)     New SBX
! 19/01/18  Jack   4.2(501)     Minor adjustments to source to be 
!                                   more "SOSLIB standard"
PROGRAM FILDIR,4.2(501)
!
!------------------------------------------------------------------------
!
!Usage:
!	XCALL FILDIR,{switches},{Directory},{File mask},{Ouput folder}
!
!	Switches: 	1 = Run on Client PC (otherwise scans Ashell Server)
!
!========================================================================
!PROBAS=X
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
![501] $COPY XCALL.BSI                   ! [501] not needed  
++include'once ashinc:ashell.def         ! [501] was $COPY ASHELL.DEF
++include'once sosfunc:fnatesbx.bsi      ! [501] was $COPY fnatesbx.bsi
++include'once sosfunc:fndirscan.bsi     ! [501] was $COPY fndirscan.bsi
!
!--------------------------------------------------------------------
!{Global Maps Zone} 
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARM'SWITCHES,F,6
MAP1 PARM'DIR,S,0
MAP1 PARM'MASK,S,0
MAP1 PARM'OUTPUT'FILE,S,0

MAP1 GLOBAL'ATE'MODE,S,1
MAP1 GLOBAL'SPAWN'LOCAL,S,1
MAP1 GLOBAL'OUTPUT'CHAN,F,6,63512
MAP1 GLOBAL'FILE'COUNT,F,6

!>!MAP1 PARM'JSON'FILE,S,0
!>!MAP1 PARM'OUTPUT'FILE,S,0
!>!MAP1 PARM'STATUS,F,6
!>!MAP1 PARM'DELIMITER,S,1
!>!MAP1 GLB'ATE'MODE,S,1
!>!MAP1 GLB'SPAWNED'LOCAL,S,1
!>!MAP1 GLB'AUTO'DECIDE,S,1
!>!DIMX $JSONMAP, ORDMAP (VARSTR; VARSTR)

DEFINE GLOBAL'SPAWN'LOCALLY	= &h00000001    ! (1)
DEFINE SWITCH'2			= &h00000002    ! (2)
DEFINE SWITCH'4			= &h00000004    ! (4)
DEFINE SWITCH'8 		= &h00000008    ! (8)
DEFINE SWITCH'16		= &h00000010    ! (16) 

! *** GUI FLAGS (used by MIAMEX,MX_GUIFLG) to get client information
	MAP1 GUIFLAGS,B,1                 ! separate var to retrieve into
	MAP1 AGF'GUIFLAGS		! 
	    MAP2 AGF'LWG,B,1,1 		! Local Windows Gui
	    MAP2 AGF'LWN,B,1,2 		! Local Windows Non-gui
	    MAP2 AGF'ATE,B,1,4 		! ATE client
	    MAP2 AGF'RWN,B,1,8 		! Remote WiNdows (ATSD)
	    MAP2 AGF'TNT,B,1,16 	! TelNeT
	    MAP2 AGF'ASH,B,1,32 	! A-Shell (not AMOS)
	    MAP2 AGF'THEMES,B,1,64 	! XP themes active
	    MAP2 AGF'LOCWIN,B,1,3 	! Local Windows (GUI or non)
	    MAP2 AGF'ANYWIN,B,1,11 	! Windows (local or remote)
	    MAP2 AGF'GUIEXT,B,1,5 	! GUI EXTensions available
	    MAP2 AGF'LWNATE,B,1,7	! Local WiNdows or ATE 
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
      	XGETARGS PARM'SWITCHES,PARM'DIR,PARM'MASK,PARM'OUTPUT'FILE
	IF PARM'SWITCHES AND GLOBAL'SPAWN'LOCALLY THEN GLOBAL'SPAWN'LOCAL="Y" ELSE GLOBAL'SPAWN'LOCAL="N"
	XCALL AUI,AUI_ENVIRONMENT,0,GUIFLAGS  
        IF GUIFLAGS AND AGF'ATE THEN GLOBAL'ATE'MODE="Y" ELSE GLOBAL'ATE'MODE="N"
	IF PARM'OUTPUT'FILE<>"" THEN 
	   IF LOOKUP(PARM'OUTPUT'FILE)<>0 THEN KILL PARM'OUTPUT'FILE
	ENDIF
	IF GLOBAL'SPAWN'LOCAL="Y" AND GLOBAL'ATE'MODE="Y" THEN 
	   CALL FN'EXECUTE'LOCALLY()
	 ELSE
	   CALL FN'SCAN'DIRECTORY(PARM'DIR,PARM'MASK)
	ENDIF
	END	
!
!==============================================================================
!{Scan Directory}
!==============================================================================
FUNCTION FN'SCAN'DIRECTORY(F'PATH AS S0:INPUTONLY,F'WILD AS S0:INPUTONLY) 
ON ERROR GOTO LOCAL'TRAP
++PRAGMA AUTO_EXTERN "TRUE"
MAP1 LOCAL'STATS,F,6
!>!	TRACE.PRINT "F'PATH="+F'PATH
!>!	TRACE.PRINT "F'WILD="+F'WILD
	GLOBAL'FILE'COUNT=0
	IF PARM'OUTPUT'FILE<>"" THEN OPEN #GLOBAL'OUTPUT'CHAN,PARM'OUTPUT'FILE,OUTPUT
	CALL Fn'Dir'Scan(F'PATH,F'WILD,LOCAL'STATS,@Fn'Do'File(),@Fn'Do'Dir())
	IF PARM'OUTPUT'FILE<>"" THEN CLOSE #GLOBAL'OUTPUT'CHAN
	EXITFUNCTION
LOCAL'TRAP:
 	XCALL MESAG,"Function Error "+STR(ERR(0))+" in FN'SCAN'DIRECTORY",2
	RESUME ENDFUNCTION WITH_ERROR 99
ENDFUNCTION
!
!------------------------------------------------
!{Process File}
!------------------------------------------------
Function Fn'Do'File(fspec$ as T_NATIVEPATH:inputonly) as i4
++PRAGMA AUTO_EXTERN "TRUE"
	map1 bytes,i,4
	xcall SIZE, fspec$, bytes
	DEBUG.PRINT fspec$+"  ("+str(bytes)+" bytes)"
	Fn'Do'File = 1
	GLOBAL'FILE'COUNT=GLOBAL'FILE'COUNT+1
	IF PARM'OUTPUT'FILE<>"" THEN
	   WRITECD #GLOBAL'OUTPUT'CHAN,fspec$,STR(bytes)
	ENDIF
EndFunction
!
!------------------------------------------------
!{Process Directory}
!------------------------------------------------
Function Fn'Do'Dir(dir$ as T_NATIVEPATH:inputonly, dirend as BOOLEAN:inputonly) as i4
       DEBUG.PRINT "Fn'Dir'Process'Dir("+dir$+","+dirend+")"
EndFunction
!
!==============================================================================
!{Execute via Local ATE/Ashell}
!==============================================================================
FUNCTION FN'EXECUTE'LOCALLY()
ON ERROR GOTO LOCAL'TRAP
++PRAGMA AUTO_EXTERN "TRUE"
MAP1 LOCAL'RTNCDE,F,6
MAP1 LOCAL'PARMS,F,6
MAP1 LOCAL'OUTPUT'FILE,S,0
	! Execute SBX locally.
!>!	TRACE.PRINT "FN'EXECUTE'LOCALLY"
	LOCAL'PARMS=PARM'SWITCHES
	LOCAL'PARMS = LOCAL'PARMS AND NOT GLOBAL'SPAWN'LOCALLY	! Turn off runs locally. (as thats what we about to do)
	LOCAL'OUTPUT'FILE="%ATECACHE%\"+PARM'OUTPUT'FILE
	IF LOOKUP(LOCAL'OUTPUT'FILE)<>0 THEN KILL LOCAL'OUTPUT'FILE
	LOCAL'RTNCDE=Fn'ATE'SBX("FILDIR",PARM'SWITCHES,PARM'DIR,PARM'MASK,LOCAL'OUTPUT'FILE)
!>!	TRACE.PRINT "LOCAL'RTNCDE="+STR(LOCAL'RTNCDE)
	FN'EXECUTE'LOCALLY=LOCAL'RTNCDE
	IF LOCAL'RTNCDE<0 THEN 
	   TRACE.PRINT "FILDIR.SBX - Fn'ATE'SBX error "+STR(LOCAL'RTNCDE)
	 ELSE
	   ! Now get CSV results file back...
!>!	   TRACE.PRINT "LOCAL'OUTPUT'FILE="+LOCAL'OUTPUT'FILE
!>!	   IF LOOKUP(LOCAL'OUTPUT'FILE)<>0 THEN 
	      DEBUG.PRINT "get LOCAL'OUTPUT'FILE="+LOCAL'OUTPUT'FILE
	      DEBUG.PRINT "save as PARM'OUTPUT'FILE="+PARM'OUTPUT'FILE
	      XCALL ATEGFK, PARM'OUTPUT'FILE, LOCAL'OUTPUT'FILE, 16, LOCAL'RTNCDE
	      IF LOCAL'RTNCDE<0 THEN 
	         IF LOCAL'RTNCDE=-3 THEN 
	            TRACE.PRINT "FILDIR.SBX - ATEGFK file transferred but failed to verify afterwards"
	          ELSE

	            TRACE.PRINT "FILDIR.SBX - ATEGFK File transfer error "+STR(LOCAL'RTNCDE)
		 ENDIF
	      ENDIF
!>!	   ENDIF
	ENDIF

	EXITFUNCTION
LOCAL'TRAP:
 	XCALL MESAG,"Function Error "+STR(ERR(0))+" in FN'EXECUTE'LOCALLY",2
	RESUME ENDFUNCTION WITH_ERROR 99
ENDFUNCTION
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
