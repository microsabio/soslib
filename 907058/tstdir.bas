!============================================================
! Test FILDIR.SBX
!
!------------------------------------------------------------
!PROBAS=X
! COMPIL TSTDIR/X:2
!============================================================
MAP1 MY'SWITCHES,F,6
MAP1 MY'DIRECTORY,S,0
MAP1 MY'MASK,S,0
MAP1 MY'OUTPUT'FILE,S,0

	! UNIX Directory
!>!	MY'DIRECTORY="/inhouse/miame/dsk99"
!>!	MY'MASK=".err"
!>!	MY'SWITCHES=0
!>!	MY'OUTPUT'FILE="STEVE.CSV"

	! WINDOWS Directory
	MY'DIRECTORY="C:\TEMP"
	MY'MASK=".csv"
	MY'SWITCHES=1
	MY'OUTPUT'FILE="STEVE.csv"

	PRINT TAB(-1,0);
	XCALL DELMEM,"*.SBX"
	XCALL FILDIR,MY'SWITCHES,MY'DIRECTORY,MY'MASK,MY'OUTPUT'FILE
	PRINT "MY'OUTPUT'FILE="+MY'OUTPUT'FILE
	END
