program TSTQST,1.0(101)		! test MAPQST.SBX
!---------------------------------------------------------------------------
!EDIT HISTORY
![100] November 28, 2004 10:57 PM       Edited by Jack
!	Created to test MAPQST.SBX
!
![101] January 10, 2005 12:28 PM        Edited by Jack
!	Add option to use Yahoo maps; option for both
!---------------------------------------------------------------------------

MAP1 MAP'PARAMS
    MAP2 ADDR,S,40
    MAP2 CITY,S,40
    MAP2 STATE,S,10
    MAP2 ZIP,S,10
    MAP2 FLAGS,F
    MAP2 ADDR2,S,40
    MAP2 CITY2,S,40
    MAP2 STATE2,S,10
    MAP2 ZIP2,S,10
    MAP2 OPTION,F
    MAP2 A,F
    MAP2 RTNSTS,F

	? "Input address to look up, or to start from:"
	INPUT LINE  "Enter Addr:  ", ADDR
	INPUT LINE  "Enter City:  ", CITY
	INPUT LINE  "Enter State: ", STATE
	INPUT LINE  "Enter Zip:   ", ZIP
	?
	? "Input 'to' address for directions (or ENTER for just lookup of above addr)"
	ADDR2 = ""
	INPUT LINE  "Enter 'to' Addr:  ", ADDR2
	if ADDR2 = "" goto MAPIT
	
	INPUT LINE  "Enter 'to' City:  ", CITY2
	INPUT LINE  "Enter 'to' State: ", STATE2
	INPUT LINE  "Enter 'to' Zip:   ", ZIP2
	
MAPIT:
	INPUT "0)Mapquest, 1)Yahoo, 2)Both: ",OPTION	! [101]

	if OPTION=1 then FLAGS = 1	! [101]
MAP'AGAIN:				! [101]
	if ADDR2#"" then
		RTNSTS = XFUNC("MAPQST",ADDR,CITY,STATE,ZIP,FLAGS, &
			ADDR2,CITY2,STATE2,ZIP2)
	else
		RTNSTS = XFUNC("MAPQST",ADDR,CITY,STATE,ZIP,FLAGS)
	endif
	
	if FLAGS=0 ? "MapQuest "; else ? "Yahoo "; ! [101]
	? "RTNSTS = ";RTNSTS;
	if RTNSTS = 0 ? " [OK]" else ? " [Error]"

	! [101] pause before launching 2nd one, because if launched too
	! [101] fast, Windows may cancel/replace the first launch
	if OPTION=2 and FLAGS=0 then 
	    input "Hit ENTER to proceed with Yahoo lookup: ",A
	    FLAGS=1 
	    goto MAP'AGAIN  
	endif
END
