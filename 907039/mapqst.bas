program MAPQST,1.0(101) 	! launch Mapquest with specified address(es)
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
!------------------------------------------------------------------------
!MAPQST - For a specified address, formulate the mapquest query string and
!	  use it to launch mapquest to map the address or give directions
!	  from the first address to the second.
!
!Prototype worked out by Tom Heim (hmoPro) and donated to the A-Shell 
!community;!converted to SBX format by Jack McGregor
!
!Usage:
!	XCALL MAPQST,FLAGS,ADDRESS,CITY,STATE,ZIP{,ADD2,CITY2,ST2,ZIP2)
!or
!	RTNSTS = XFUNC("MAPQST",FLAGS,ADDRESS,CITY,STATE,ZIP{,ADD2,CITY2,ST2,ZIP2})
!where
!	RTNSTS = 0 for success or -1 for error.  (Note, success only
!	means that the launch command did not abort.)
!
!	FLAGS (numeric) sum of:
!	   1 = Use yahoo maps instead of mapquest
!--------------------------------------------------------------------
! NOTE: MUST COMPILE WITH COMPIL MAPQST/X:2 
!--------------------------------------------------------------------
!EDIT HISTORY   
![100] November 28, 2002 11:08 AM         Edited by Jack
!	Take Tom's prototype, convert to SBX	
!
![101] January 10, 2005 12:22 PM        Edited by Jack
!	Flag 1 for Yahoo Maps; chg MAP'QUEST to QUERY$; support	
!	canada (at least in yahoo mode) 
!--------------------------------------------------------------------

++include ashinc:xcall.bsi

MAP1 PARAMS
	MAP2 FLAGS,F
	MAP2 ADDR,S,40
	MAP2 CITY,S,40
	MAP2 STATE,S,10
	MAP2 ZIP,S,10
	MAP2 ADDR2,S,40
	MAP2 CITY2,S,40
	MAP2 STATE2,S,10
	MAP2 ZIP2,S,10

MAP1 MISC
	MAP2 QUERY$,S,2048
	MAP2 MQUEST,B,1,1		! just 1 address now
	MAP2 INPUT'STRING,S,100
	MAP2 A$,S,1
	MAP2 WAS$,S,5
	MAP2 IS$,S,5
	MAP2 L,F
	MAP2 CCSTATUS,F
	MAP2 RTNSTS,F
	MAP2 COUNTRY$,S,2,"us"
	MAP2 ZIP$,S,12

MAP1 MQF'FLAGS				! [101]
	MAP2 MQF'YAHOO,B,1,1		! [101] use Yahoo maps

MAP1 CANADIAN'PROVINCES$,S,40,"AB\BC\MB\NB\NL\NT\NS\NU\ON\PE\QC\SK\YT" ![101]

64000	on error goto TRAP

	! pick up all args; any not passed will just be set to null
64010	XGETARGS ADDR,CITY,STATE,ZIP,FLAGS,ADDR2,CITY2,STATE2,ZIP2

	! determine if we are mapping one address or showing directions
	! from the first to the second
64020	if XCBCNT >= 9 and ADDR2 # "" then MQUEST = 2 else MQUEST = 1


64030   ON MQUEST CALL MQADDR,MQDIR

!	This version requires that the explicit path to the 
!	browser executable be given; also HOSTEX only works under
!	Windows in this case; but it does allow adding the " &" to
!	launch it minimized...
!	QUERY$="C:\progra~1\intern~1\iexplore.exe " + QUERY$
!	XCALL HOSTEX,QUERY$+" &":RETURN

!	This version uses the "Shell Execute" function, which eliminates
!	the need for specifying the name or path of the browser, and
!	also works remotely over ATE, but doesn't allow the specification
!	of an initial window state.  The function returns chr(13) or 
!	chr(3) in the keyboard, indicating if the launch failed or not
!	(but not whether the website found, address found, etc.)

64100	xcall MIAMEX,4,CCSTATUS		! check if ^C enabled
64110	if CCSTATUS # 0 then 		! yes; disable temporarily
64120		xcall CCOFF
64130	endif

	! Shell Execute version 
64140	? TAB(-10,24);
64150	? QUERY$;CHR(127);

64160	xcall NOECHO
64170	xcall ACCEPN,A$
64180	if CCSTATUS # 0 then xcall CCON
64190	xcall ECHO
		
64200	if asc(A$)=3 then RTNSTS = -1 else RTNSTS = 0
64210	return(RTNSTS)		! (return status if called via XFUNC)
64220	end		

!-------------------------------------------------------------------------
! Map a single address...
! Inputs:
!	ADDR,CITY,STATE,ZIP
! Output:
!	QUERY$ string ready to pass to Shell Execute
!-------------------------------------------------------------------------
64300 MQADDR:
	if (FLAGS and MQF'YAHOO) goto MQADDR'YAHOO
64310	QUERY$ = "http://www.MAPQUEST.com/maps/map.adp?country=US&"
64320   QUERY$=QUERY$+"country=US&countryid=US&searchtype=address&cat="
64325	QUERY$ = lcs(QUERY$)
64330   INPUT'STRING=ADDR
64340   CALL MSTRIP:QUERY$=QUERY$+"&address="+INPUT'STRING
64350   INPUT'STRING=CITY :CALL MSTRIP:QUERY$=QUERY$+"&city="+INPUT'STRING
64360   INPUT'STRING=STATE:CALL MSTRIP:QUERY$=QUERY$+"&state="+STATE
64370   QUERY$=QUERY$+"&zipcode="+ZIP
64380   RETURN

64400 MQADDR'YAHOO:		! [101]
	QUERY$ = "http://maps.yahoo.com/maps_result?addr="
	INPUT'STRING=ADDR
	xcall STRIP,INPUT'STRING
	call MSTRIP'YAHOO
	QUERY$ = QUERY$ + INPUT'STRING
        ZIP$ = ZIP : call FIX'ZIP
	INPUT'STRING = CITY + ", " + STATE + " " + ZIP$
	call MSTRIP'YAHOO
	QUERY$ = QUERY$ + "&csz=" + INPUT'STRING
	if instr(1,CANADIAN'PROVINCES$,ucs(STATE)) > 0 or &
        (ucs(ZIP$[1,1])>="A" and ucs(ZIP$[1,1])<="Z") then 
	    COUNTRY$ = "ca"
	else 
	    COUNTRY$ = "us"
	endif
	QUERY$ = QUERY$ + "&country=" + COUNTRY$ + "&cat=&trf="
64499	RETURN
!-------------------------------------------------------------------------
! Get directions from first address to second address
! Inputs:
!	ADDR,CITY,STATE,ZIP ('from' address)
!	ADDR2,CITY2,STATE2,ZIP2 ('to' address)
! Output:
!	QUERY$ string ready to pass to Shell Execute
!-------------------------------------------------------------------------

64500 MQDIR:
	if (FLAGS and MQF'YAHOO) goto MQDIR'YAHOO

64510   QUERY$="HTTP://WWW.MAPQUEST.COM/DIRECTIONS/MAIN.ADP?GO=1"
64520   QUERY$=QUERY$+"&DO=NW&UN=M&2TABVAL=ADDRESS"
64530   QUERY$=QUERY$+"&CL=EN&CT=NA&1V=ADDRESS&1TABVAL=ADDRESS&1Y=US"
64535	QUERY$ = lcs(QUERY$)
64540   INPUT'STRING=ADDR:CALL MSTRIP
64550   QUERY$=QUERY$+"&1A="+INPUT'STRING
64560   INPUT'STRING=CITY :CALL MSTRIP
64570   QUERY$=QUERY$+"&1C="+INPUT'STRING
64580   QUERY$=QUERY$+"&1S="+STATE
64590   QUERY$=QUERY$+"&1Z="+ZIP
64600   INPUT'STRING=ADDR2:CALL MSTRIP
64610   QUERY$=QUERY$+"&2A="+INPUT'STRING
64620   INPUT'STRING=CITY2 :CALL MSTRIP
64630   QUERY$=QUERY$+"&2C="+INPUT'STRING
64640   QUERY$=QUERY$+"&2S="+STATE2
64650   QUERY$=QUERY$+"&2Z="+ZIP2
64660   return

64700 MQDIR'YAHOO:		! [101]
	! 1st part is just like addr lookup but starts with dd?
	QUERY$ = "http://maps.yahoo.com/dd?addr="
	INPUT'STRING = ADDR
	call MSTRIP'YAHOO
	QUERY$ = QUERY$ + INPUT'STRING

        ZIP$ = ZIP : call FIX'ZIP
	INPUT'STRING = CITY + ", " + STATE + " " + ZIP$
	call MSTRIP'YAHOO
	QUERY$ = QUERY$ + "&csz=" + INPUT'STRING
	if instr(1,CANADIAN'PROVINCES$,ucs(STATE)) > 0 or &
        (ucs(ZIP$[1,1])>="A" and ucs(ZIP$[1,1])<="Z") then 
	    COUNTRY$ = "ca"
	else 
	    COUNTRY$ = "us"
	endif
	QUERY$ = QUERY$ + "&country=" + COUNTRY$

	! 2nd part similar but with taddr=
	QUERY$ = QUERY$ + "&taddr="
	INPUT'STRING=ADDR2
	call MSTRIP'YAHOO 
	QUERY$ = QUERY$ + INPUT'STRING + "&tcsz="
	ZIP$ = ZIP2 : call FIX'ZIP
	INPUT'STRING=CITY2 + "," + STATE2 + " " + ZIP$
	call MSTRIP
	QUERY$ = QUERY$ + INPUT'STRING
	if instr(1,CANADIAN'PROVINCES$,ucs(STATE2)) > 0 or &
        (ucs(ZIP$[1,1])>="A" and ucs(ZIP$[1,1])<="Z") then 
	    COUNTRY$ = "ca"
	else 
	    COUNTRY$ = "us"
	endif
	QUERY$ = QUERY$ + "&tcountry=" + COUNTRY$
	
64799   return
	

!------------------------------------------------------------------------
! Remove illegal characters from string
! Inputs:
!	INPUT'STRING
!	WAS$ (string to remove)
!	IS$  (replacement string)
! Output:
!	INPUT'STRING (stripped, folded to lower case)
!------------------------------------------------------------------------
64800 MSTRIP:
	if FLAGS and MQF'YAHOO goto MSTRIP'YAHOO	! [101]
64810	INPUT'STRING = LCS(INPUT'STRING)
64820	XCALL TRIM,INPUT'STRING,1
64830   WAS$="." :IS$=" "  :CALL N3
64840   WAS$="," :IS$=" " :CALL N3	
64850   WAS$="  ":IS$=" "  :CALL N3
64860   WAS$=" " :IS$="%20":CALL N3
64870   RETURN

64900 MSTRIP'YAHOO:						! [101]
	INPUT'STRING = LCS(INPUT'STRING)
	XCALL TRIM,INPUT'STRING,1
	if INPUT'STRING[1,1] = "," then INPUT'STRING = INPUT'STRING[2,-1]
	WAS$="." :IS$=""  :CALL N3
	WAS$="," :IS$="%2C" :CALL N3	
	WAS$="  ":IS$=" "  :CALL N3
	WAS$=" " :IS$="+":CALL N3
64999	RETURN

65000 N3:
65010	L=INSTR(1,INPUT'STRING,"#"):REM IS A BAD CHAR TO MAPQUEST
65020   IF L>0 THEN INPUT'STRING=INPUT'STRING[1;L-1]
65030 N4:
65040	L=INSTR(1,INPUT'STRING,WAS$)
65050   IF L>0 THEN INPUT'STRING=INPUT'STRING[1;L-1]+IS$+INPUT'STRING[L+LEN(WAS$);9999]:GOTO N4
65060   RETURN

65100 FIX'ZIP:		! remove space from ZIP (canadian)
	if ZIP$[1,3] # "" and ZIP$[4,4] = " " then &
		ZIP$ = ZIP$[1,3] + ZIP$[5,7]
65199	return

65200 TRAP:
65210	? "Error #";err(0);" in MAPQST.SBX!!!"
65220	xcall SLEEP,5
65230	xcall ASFLAG,128	! set ^C in caller
65240	end			! return to caller
