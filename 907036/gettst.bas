program GETTST,1.0(101)     ! sample test of TCPGET.SBX

MAP1 HOST,S,50,"198.66.212.196"     ! microsabio.com
MAP1 PORT,F,6,80
MAP1 COMMAND,S,50,"GET /index.html"
MAP1 FILE,S,25,"STEVE.PRT"
MAP1 ERROR'TYPE,F,6
MAP1 STATUS,F,6
 
PRINT TAB(-1,0);
!DELMEM clears memory modules (removed here in case DELMEM not avail)
!XCALL DELMEM,"*.SBX"

XCALL TCPGET,HOST,PORT,COMMAND,FILE,ERROR'TYPE,STATUS,20000

IF ERROR'TYPE<>2 THEN &
   PRINT "ERROR:";ERROR'TYPE,STATUS; &
 ELSE &
   PRINT "COMPLETED OK (Page saved to ";FILE;")"

END
