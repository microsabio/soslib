program XECURR,1.0(100)     ! Get EX.COM currency exchange rate file
!========================================================================
! XECURR - GET EX.COM Currency file.
!========================================================================
!Usage:
!	XCALL XECURR,{filename},X'ERROR'TYPE,X'STATUS
!
!	See TCPGET for returned error type and status.
!
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps Zone} 
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARMS
 MAP2 X'FILE,S,255
 MAP2 X'ERROR'TYPE,F,6
 MAP2 X'STATUS,F,6
 
MAP1 MISC
 MAP2 INIFILE,S,25,"DSK0:MADSYS.INI[1,2]"
 
MAP1 XE
 MAP2 XE'HOST,S,255,"216.220.38.20"
 MAP2 XE'PORT,F,6,80
 MAP2 XE'CMD,S,255
 MAP2 XE'LOGIN,S,255
!
!==============================================================================
!{Start here}
!==============================================================================
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
   	XGETARGS X'FILE,X'ERROR'TYPE,X'STATUS

	XCALL INIX,INIFILE,0,"XE","LOGIN",XE'LOGIN
	IF XE'LOGIN="" THEN &
	   XCALL MESAG,"XE.COM LOGIN NOT FOUND IN MADSYS.INI",2 : &
	   GOTO EXITOUT

	XE'CMD="GET /dfs/datafeed2.cgi?"+XE'LOGIN
	XCALL TCPGET,XE'HOST,XE'PORT,XE'CMD,X'FILE,X'ERROR'TYPE,X'STATUS,20000
 
EXITOUT:
	XPUTARG 2,X'ERROR'TYPE
	XPUTARG 2,X'STATUS
	END	
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
