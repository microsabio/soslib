!========================================================================
! TCPGET - {Get a file via HTTP Get}- Thanks to Jack for the example.
!========================================================================
! The file is pulled back in chunks of 1024 into an array, this is then
! saved to a file when everything is received or the connection closed.
!
!Usage:
!	XCALL TCPGET,{host},{socket/port},{command},{file},&
!                    {error type},{status},{file text size}
!
!	Error Types:	-1	No Host/IP Set.
!			-2 	No Socket number set
!			-3	No command set.
!			-4	No file set.
!			-5      File text size too small.
!			 1 	Connect error
!			 2	Disconnect eorr
!			 3	Read error
!			 4	Write error
!
! File text size:	Large enough to hold the complete file received	
!
!========================================================================
! 02/Aug/2006 - 4.2(501) - Steve
!	      Changed to pull the buffer into an array before saving the
!             text to a file, previously it was adding an extra C/R into
!             the saved file after the 1024 buffer was full.
! 02/Aug/2006 - 4.2(502) - Steve
!	      Fix connection timeout problem, was checking sending STATUS
!	      to XCALL TCPX but checking for X'STATUS on its return. 
!========================================================================
++PRAGMA SBX
++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++include xcall.bsi
!PROBAS=X  {Madics PROBAS will auto compile with /X if this line is here)
!
!--------------------------------------------------------------------
!{Maps Zone} 
!--------------------------------------------------------------------
MAP1 SBX'NAME,S,6
MAP1 PRGNAM,S,6

MAP1 PARMS
 MAP2 X'HOSTNAME,S,80		! IP Address can also be used.
 MAP2 X'SOCKETPORT,F,6
 MAP2 X'COMMAND,S,255
 MAP2 X'FILE,S,255
 MAP2 X'ERROR'TYPE,F,6
 MAP2 X'STATUS,F,6
 MAP2 X'FILE'SIZE,F,6
 
MAP1 TCP'PARMS
 MAP2 FLAGS,F,6,1		! blocking mode
 MAP2 BUFFER,S,1024
 MAP2 TIMER,F,6,5000	        ! 5 second timeout
 MAP2 OPCODE,F,6
 MAP2 STATUS,F,6 
  
MAP1 MISC
 MAP2 TMP'CHAN,F,6,63017
 MAP2 TMP'ERROR,F,6
!
!==============================================================================
!{Start here}
!==============================================================================
START'HERE:
     	on error goto TRAP
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME

      	XGETARGS X'HOSTNAME,X'SOCKETPORT,X'COMMAND,X'FILE,X'ERROR'TYPE,&
      	         X'STATUS,X'FILE'SIZE
      	         
	X'ERROR'TYPE=0 : X'STATUS=0
	IF X'FILE'SIZE=0 THEN X'FILE'SIZE=20000
	
	DIMX MY'TEXT(1),S,X'FILE'SIZE
	
 	! Check we have what we need.
	IF X'HOSTNAME="" THEN 
	   XCALL MESAG,"TCPGET: NO HOSTNAME SET",2
	   X'ERROR'TYPE=-1
	   GOTO EXIT'OUT
	ENDIF
	
	IF X'SOCKETPORT=0 THEN 
	   XCALL MESAG,"TCPGET: NO SOCKETPORT SET",2
	   X'ERROR'TYPE=-2
	   GOTO EXIT'OUT
	ENDIF

	IF X'COMMAND="" THEN 
	   XCALL MESAG,"TCPGET: NO COMMAND SET",2
	   X'ERROR'TYPE=-3
	   GOTO EXIT'OUT
	ENDIF

	IF X'FILE="" THEN 
	   XCALL MESAG,"TCPGET: NO FILENAME SET",2
	   X'ERROR'TYPE=-4
	   GOTO EXIT'OUT
	ENDIF

        ! OK now lets get to work..
	CALL GET'FILE'VIA'HTTP
	
        !Save to file..
	IF (X'ERROR'TYPE=0) OR (X'ERROR'TYPE=2) THEN &
	   CALL SAVE'TO'FILE

EXIT'OUT:
	XPUTARG 5,X'ERROR'TYPE
	XPUTARG 6,X'STATUS
	END	
!
!==============================================================================
!{Lets go and get the file via HTTP}
!==============================================================================
GET'FILE'VIA'HTTP:
	TMP'ERROR=0
	! Open connection.
	OPCODE=9 : CALL TCP                                     
	IF X'STATUS<>0 THEN GOTO RTN'GET'FILE'VIA'HTTP

        !Send GET command.
	BUFFER = X'COMMAND + chr(13) + chr(10)    ! (must have CFLF!)
	FLAGS = len(BUFFER)     ! # bytes to send
	OPCODE=2 : CALL TCP				  ! send GET command
	if X'STATUS<0 goto RTN'GET'FILE'VIA'HTTP
	!
        ! Loop around getting each time of the file til the end.
        !
NEXT'LINE:		     ! retrieve the page, one buffer at a time
	FLAGS = 0            ! retrieve one packet at a time (up to BUFFER size)
	OPCODE=4 : CALL TCP                                         
	if X'STATUS<0 THEN GOTO DONE

	IF LEN(MY'TEXT(1)+BUFFER)>X'FILE'SIZE THEN
  	   TMP'ERROR=-5
	   GOTO DONE
	ENDIF

	MY'TEXT(1)=MY'TEXT(1)+BUFFER
	goto NEXT'LINE

DONE:
	OPCODE=6 : CALL TCP  ! Close (warning, this may of been closed by server
			     ! so X'ERROR'TYPE=2 may want to be ignored on the
			     ! calling program)
RTN'GET'FILE'VIA'HTTP:
	IF TMP'ERROR<>0 THEN X'ERROR'TYPE=TMP'ERROR
 	RETURN
 	
!
!==============================================================================
!{Save received data to file}
!==============================================================================
SAVE'TO'FILE:
	OPEN #TMP'CHAN,X'FILE,OUTPUT
	PRINT #TMP'CHAN,MY'TEXT(1)
	CLOSE #TMP'CHAN
	RETURN
!
!==============================================================================
!{TCP Connection}
!==============================================================================	
!	OPCODE:	2 - Write	
!		4 - Read
!		6 - Close	
!		9 - Connect	
TCP: 	
	X'ERROR'TYPE=0 : X'STATUS=0
	xcall TCPX,OPCODE,STATUS,BUFFER,X'SOCKETPORT,FLAGS,TIMER,X'HOSTNAME
	IF OPCODE=9 AND STATUS<0 THEN X'ERROR'TYPE=1 : X'STATUS=STATUS
	IF OPCODE=6 AND STATUS=<0 THEN X'ERROR'TYPE=2 : X'STATUS=STATUS
	IF OPCODE=2 AND STATUS<=0 THEN X'ERROR'TYPE=3 : X'STATUS=STATUS
	IF OPCODE=4 AND STATUS<=0 THEN X'ERROR'TYPE=4 : X'STATUS=STATUS
	RETURN
!
!==============================================================================
!{Trap}
!==============================================================================
TRAP:
	XCALL MESAG,SBX'NAME+".SBX ERROR: "+STR(ERR(0))+" ON FILE "+STR(ERR(2))+ &
              ", LINE "+STR(ERR(1)),2
	END			! return to caller
