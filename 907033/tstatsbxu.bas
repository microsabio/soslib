program TSTATSBXU,1.0(100)   ! test ATSBXU.SBX
!-------------------------------------------------------------
![100] January 14, 2013 10:09 AM        Edited by jack
!   Created
!-------------------------------------------------------------

MAP1 STATUS,F
MAP1 SBXNAME$,S,50

    ? tab(-1,0);"Test ATSBXU.SBX"

    input "Enter SBX to sync: ",SBXNAME$
    xcall ATSBXU,SBXNAME$,STATUS
    ? "status: ";STATUS
    end
