program ATESYC,1.0(102)             ! SBX to sync a whole directory to ATE
!NOTE: this now made obsolete by ATSYNC.LIT/SBX 1.2(112) which can be 
!      executed as either a LIT or SBX (same binary) - see ATSYNC.BAS 
!------------------------------------------------------------------------
!SBX version of ATSYNC.LIT utility
!------------------------------------------------------------------------
!EDIT HISTORY   
![100] April 29, 2006 11:08 PM         Edited by Jack
!    Created    
![101] June 19, 2006 11:08 PM         Edited by Jack
!    Fold host dir to lower case 
!
![SBX] June 20, 2006		      Edited by Steve (Madics)
!      Taken ATSYNC and added a small SBX wrapper around it.
!      Uses all the same parameters as ATSYNC.
!      Recommend using /L for silent mode.
!
![102] June 23, 2009 11:08 AM         Edited by Jack
!    Use new MX_AGWRAPPER if >= 1153
!
!--------------------------------------------------------------------
!NOTES ON ATSYNC.LIT:
!
!For a specified server directory and PC directory, copies all of the 
!server files to the PC, as needed, so that the PC has the latest.
!
!   .ATSYNC {HostDir}{,PCDir}{/Switches}
!
!HostDir - AMOS or Native; if not specified, use current directory
!PCDir - Windows Native; if not specified, use %ATEPERMCACHE%
!Switches:
!
!   /C{ont} - Continue after most file transfer errors
!   /P{rgdlg} - Display progress dialog
!   /D{elete} - Delete files not present on server (not yet implemented)
!   /NOERR - Suppress FTP error dialogs
!   /L{ist}{:listfile} - Output status info to file [ATSYNC.LST]
!
!NOTES:
!Use SET DEBUG to run with verbose debugging
!
!The assumption here is that the FTP root directory and A-Shell root
!directory on the server are the same.  If they are not, then you will
!have to insert some translation logic to convert HOSTNATIVE$ to 
!the equivalent needed for FTP.  (For example, the FTP server 'root'
!might be /vm/miame in which case you would need to strip the leading
!"/vm/miame" from the HOSTNATIVE$ (native spec$) before using TAB(-10,22).
!
!Exit Status:
!Because there are a lot of things that can go wrong (or right) with 
!this command, and because it might be invoked under control of a
!program that wants to check its results from the ATSYNC.LST file, 
!the last message output to screen or file will be:
!   Status=# [result] <...>
!Where:
!   0=OK (possibly irrelevant - not needed on A-Shell/Windows local)
!   1-200 = basic error
!  <0=errors in setting up the TAB commands (AG_ATSYNC)
!  30000+ =FTPDLX (wodFTPDlx) errors 
!
!--------------------------------------------------------------------

++PRAGMA ERROR_IF_NOT_MAPPED "ON"
++PRAGMA SBX                                                       ! [SBX]

++include ashinc:ashell.def     ! [102] was ashell.bsi
++include ashinc:ashell.sdf     ! [102] (for ST_DDB struct)

    map1 DDB,ST_DDB             ! [102]

! define the TAB(-10,x) command symbol (should be AG_ATSYNC from tcrtex)
define AGX_SYNC = 55               ! (but this eliminates outside dependency)
define MX_AGWRAPPER = 177          ! [102]

    map1 PARAMETERS
        map2 STATUS,F,6
        map2 HOSTDIR$,S,160
        map2 PCDIR$,S,160
        map2 FILES,F
        map2 BYTES,F
        map2 SYNCFLAGS,B,4
        map2 DELFILES,F
        map2 DELBYTES,F
        
    map1 MISC
        map2 X,F
        map2 Y,F
        map2 HOSTNATIVE$,S,160
        map2 PCPATH$,S,160
        map2 GUIFLAGS,F
        map2 FDDB,X,52       
        map2 FNAME'EXT$,S,50            ! name.ext
        map2 DIRSEP$,S,1
        map2 PC'BYTES,F                 ! size of file on pc
        map2 PC'MTIME,B,4               ! file mtime on PC
        map2 PC'CTIME,B,4               ! file ctype on PC
        map2 PC'MODE,B,2                ! file type/mode on server
        map2 SVR'MTIME,B,4              ! file mtime on PC
        map2 SVR'CTIME,B,4              ! file ctype on PC
        map2 SVR'BYTES,F                ! size of file on server
        map2 SVR'MODE,B,2               ! file type/mode on server
        map2 CH,F
        map2 AGCMD$,S,200               ! [102]
        map2 AGRESPONSE$,S,200          ! [102]
        map2 I,F                        ! [102]
        map2 J,F                        ! [102]

    map1 OTHER
        map2 FNAME$,S,40
        map2 ATTR,B,4
        map2 CTIME,S,6
        map2 ATIME,S,6
        map2 MTIME,S,6
        map2 CDATE,S,10
        map2 ADATE,S,10
        map2 MDATE,S,10
        map2 TX'FILES,F
        map2 TX'BYTES,F
        map2 SIZE,F
        map2 STATE,F
        map2 L,F
        map2 EMSG$,S,100
        map2 LISTFILE$,S,32,"atsync.lst"

    map1 VERSION'INFO               ! [102]
        map2 VERSTR$,S,16           ! [102]        
        map2 VMAJOR,B,1             ! [102]
        map2 VMINOR,B,1             ! [102]
        map2 VEDIT,B,2              ! [102]
        map2 VPATCH,B,1             ! [102]



define MAX_SWITCHES = 5

! define the sync flags (can be used on xcall or any of the tabs)
define ATSF_NOERR   = &h0001        ! /NOERR suppress client error dialogs
define ATSF_PRGDLG  = &h0002        ! /PROGRESS display in-PRoGress DiaLoG
define ATSF_CONTIN  = &h0008        ! /CONTINUE after an xfer error
define ATSF_LIST    = &h0010        ! /LIST output status info to ATSYNC.LST
define ATSF_DELETE  = &h0020        ! /DELETE files not present on server

    map1 PARSER
        map2 CLINE,S,150
        map2 SWITCHSTR$,S,50
        map2 XSW,F                  ! position of first switch
        map2 XCOMMA,F               ! position of comma
        map2 OP,B,1
        map2 RDELIM$,S,2
        map2 SW$(MAX_SWITCHES),S,10
        map2 SWPARAM$,S,32
        map2 SWITCHES(MAX_SWITCHES)
            map3 SWNAME,S,42
            map3 SWVALUE,B,4
                   
SWNAME(1) = "CONT"            : SWVALUE(1) = ATSF_NOERR
SWNAME(2) = "PRGDLG"          : SWVALUE(2) = ATSF_PRGDLG
SWNAME(3) = "DELETE"          : SWVALUE(3) = ATSF_DELETE
SWNAME(4) = "NOERR"           : SWVALUE(4) = ATSF_NOERR
SWNAME(5) = "LIST"            : SWVALUE(5) = ATSF_LIST
        
define DBG_MSG$ = " [atsync debug] > "  ! use for debug msg leadin

MAP1 SBX'NAME,S,6                                                  ! [SBX]
MAP1 PRGNAM,S,6                                                    ! [SBX]
MAP1 WHAT'JOB,S,6                                                  ! [SBX]
!==============================================================================
!{Start here}                                                      ! [SBX]
!==============================================================================
	XCALL NOECHO : FILEBASE 1 : SIGNIFICANCE 11
	XCALL GETPRG,PRGNAM,SBX'NAME
	XCALL WHOAMI,WHAT'JOB : XCALL STRIP,WHAT'JOB
	LISTFILE$=WHAT'JOB+".PRT"
      	XGETARGS CLINE,STATUS
	CALL ATSYNC
	XPUTARG 2,STATUS
	END	
!
!-------------------------------------------------------------------
! Orginal ATSYNC..
!-------------------------------------------------------------------
ATSYNC:   
    ! [102] get ashell ver to see if we can use MX_AGWRAPPER
    xcall MIAMEX, MX_GETVER, VERSTR$, VMAJOR, VMINOR, VEDIT, VPATCH 

    !  check for /? help display
    ! if instr(1,ucs(A2S[A2,-1]),"/?") then goto HELPOUT           ! [SBX]
    !CLINE = ucs(A2S[A2,-1])                                       ! [SBX]
    xcall TRIM,CLINE

    ! see if we have a quoted hostdir...
    if CLINE[1,1] = """" then
        X = instr(2,CLINE,"""")
        if X < 1 goto SYNTAX'ERROR      ! unmatched quotes
        HOSTDIR$ = CLINE[2,X-1]    
        CLINE = CLINE[X+1,-1]
        xcall TRIM,CLINE
    else
        XCOMMA = instr(1,CLINE,",")
        if XCOMMA > 0 then
            if XCOMMA = 1 then
                HOSTDIR$ = ""
            else                
                HOSTDIR$ = CLINE[1,XCOMMA-1]
            endif
            CLINE = CLINE[XCOMMA,-1]
        else
            XSW = instr(1,CLINE,"/")
            IF XSW > 1 then
                HOSTDIR$ = CLINE[1,XSW-1]
                CLINE = CLINE[XSW,-1] 
            endif           
        endif
    endif
         
    xcall TRIM,HOSTDIR$
    HOSTDIR$ = lcs(HOSTDIR$)        ! [101]
    if HOSTDIR$ = "" then
    	xcall MIAMEX,MX_FSPEC,"q.q",HOSTDIR$,DDB.EXT,FS_TOH+FS_FMA,DDB,STATUS ![102]
	    X = instr(1,HOSTDIR$,"q.q")
	    if X > 0 then HOSTDIR$ = HOSTDIR$[1,X-2]
	    STATUS = 0
    endif         
    
    ! now HostDir is set or "" for current directory
    ! and CLINE starts just past HostDir
    xcall TRIM,CLINE
    
    if CLINE[1,1] = "," then        ! we have a PCDIR
        XSW = instr(1,CLINE,"/")
        if XSW > 1 then
            PCDIR$ = CLINE[2,XSW-1]
            xcall TRIM,PCDIR$
            if PCDIR$[1,1] = """" then
                X = instr(2,PCDIR$,"""")
                if X < 1 goto SYNTAX'ERROR      ! unmatched quotes
                PCDIR$ = PCDIR$[2,X-1]
            endif      
        else
            PCDIR$ = CLINE                        
        endif
    endif
    xcall TRIM,PCDIR$
    
    if PCDIR$ = "" then
        PCDIR$ = "%ATEPERMCACHE%"
    endif
        
    ! Now we have PCDIR$, and CLINE is stripped down to switches, if any
    XSW = instr(1,CLINE,"/")
    if XSW > 0 then  
        SWITCHSTR$ = CLINE[XSW+1,-1]
        
        ! Use STRTOK to parse out switches    
        ! WARNING: number of SW$() must not exceed MAX_SWITCHES!!!   
        xcall STRTOK,OP,SWITCHSTR$,"/;",SW$(1),RDELIM$, &
            SW$(2),SW$(3),SW$(4),SW$(5)
            
        ! now set the flags
        for I = 1 to MAX_SWITCHES
            xcall STRIP,SW$(I)
            ! check for : (as in /L:temp.lst)
            X = instr(1,SW$(I),":")
            if X > 0 then
                SWPARAM$ = SW$(I)[X+1,-1]
                SW$(I) = SW$(I)[1,X-1]
            endif                
            if (SW$(I) # "") then
                L = len(SW$(I))
                J = 1
                do while J <= MAX_SWITCHES
                    if SW$(I) = SWNAME(J)[1,L] then 
                        SYNCFLAGS = SYNCFLAGS or SWVALUE(J)
                        if SWNAME(J)="LIST" then
                            if (SWPARAM$ # "") then
                                LISTFILE$ = lcs(SWPARAM$)    ! optional list filename
                            endif                                
                            ! remove ATSYNC.LST if present
                            if lookup(LISTFILE$)>0 then
                                kill LISTFILE$
                            endif        
                        endif                            
                        exit
                    endif                    
                    J = J + 1
                loop
            endif
        next I
    endif                        

    if DEBUG then 
        ? DBG_MSG$;" Hostdir [";HOSTDIR$;"], PCDir [";PCDIR$;"]"
        ? DBG_MSG$;" Switches: [";SWITCHSTR$;"] Sw: "; SW$(1);", ";SW$(2);", ";SW$(3)
    endif
            
    if SYNCFLAGS and ATSF_LIST then
        CH = 9
        open #CH, LISTFILE$, output
    else
        CH = 0
    endif

    ! first get status of dir on server
    xcall MIAMEX, MX_FILESTATS, "L", HOSTDIR$, SVR'BYTES, &
        SVR'MTIME, SVR'CTIME, SVR'MODE

    if DEBUG then 
        ? DBG_MSG$;"Server stats: mode=";
        xcall MIAMEX,MX_OCVT,SVR'MODE,0,OT_TRM+OT_HEX
        ?
    endif
    
    if ((SVR'MODE and FSTS_DIR) = 0) then
        STATUS = -101         ! not a directory
        goto HELPOUT
    endif

    xcall NOECHO
    
    ! Next, find out if we are running ATE...
    xcall AUI,AUI_ENVIRONMENT,0,GUIFLAGS

    if DEBUG then 
        ? DBG_MSG$;"GUIFLAGS = ";
        xcall MIAMEX,MX_OCVT,GUIFLAGS,0,OT_TRM+OT_HEX
        ?
    endif        

    ! if local windows no xfer needed; return 0
    if (GUIFLAGS and AGF_LOCWIN) then
        STATUS = 1
        goto DONE
    endif

    ! if not ATE, return error -1
    if ((GUIFLAGS and AGF_ATE) = 0) then
        STATUS = -102
        ? "ATSYNC requires ATE client."
        goto DONE
    endif

    ! get the directory separator
    xcall MIAMEX,MX_DIRSEP,DIRSEP$

    ! at this point, host dir exists and we are running ATE, so xfer it
    ! start by converting HOSTDIR$ to HOSTNATIVE$

    xcall STRIP,HOSTDIR$
    IF instr(1,HOSTDIR$,DIRSEP$) > 0 then
        if HOSTDIR$[-1,-1] # DIRSEP$ then
            HOSTDIR$ = HOSTDIR$ + DIRSEP$
        endif
    endif        
    HOSTDIR$ = HOSTDIR$ + "a.a"
    xcall MIAMEX,MX_FSPEC,HOSTDIR$,HOSTNATIVE$,"",FS_FMA+FS_TOH, &
        FDDB,STATUS

    if DEBUG then ? DBG_MSG$;"Native host spec=";HOSTNATIVE$;", STATUS=";STATUS

!    if STATUS # 0 then
!        STATUS = -2     ! does not exist
!        goto DONE
!    endif

    HOSTDIR$ = HOSTNATIVE$[1,-4]    ! strip off "a.a"

    ! send the initialization command
    
    if DEBUG then 
        ? DBG_MSG$;"? tab(-10,AGX_SYNC);0,";SYNCFLAGS;",";HOSTDIR$;",";PCDIR$
    endif        

    AGCMD$ = "0,"+str(SYNCFLAGS)+","+HOSTDIR$+","+PCDIR$    ! [102]
    if VEDIT >= 1153 then       ! [102] use MX_AGWRAPPER if avail
        xcall MIAMEX,MX_AGWRAPPER,AGX_SYNC,AGCMD$,STATUS
    else
        ? tab(-10,AGX_SYNC);AGCMD$;chr(127);        ! [102]
        input "",STATUS
    endif
    if DEBUG then ? DBG_MSG$;"STATUS <-- ";STATUS
    if STATUS # 0 goto INIT'ERROR
    
    xcall MIAMEX,MX_FINDFIRST,HOSTDIR$,STATUS,FNAME$,SIZE,ATTR, &
        CDATE,CTIME,MDATE,MTIME

    IF (STATUS) then        ! problem with FINDFIRST
        STATUS = -101
        goto DONE
    endif
                
    ! now scan the directory
    TX'FILES = 0
    TX'BYTES = 0
    IF (CH # 0) then
        ? #CH,"Examining files";
        if (CH = 0) then ? tab(-1,254);
    endif        
    do 
        if DEBUG then
            ? DBG_MSG$;" FNAME$=";FNAME$;", ATTR=";
            xcall MIAMEX,MX_OCVT,ATTR,0,OT_TRM+OT_HEX
            ?
        endif   
        IF (lcs(FNAME$) # LISTFILE$) then         
            if (ATTR and FATR_NORMAL) then
                ! use MX_FILESTATS to get integer (rather than string) times
                xcall MIAMEX,MX_FILESTATS,"L",HOSTDIR$+FNAME$,SVR'BYTES, &
                    SVR'MTIME, SVR'CTIME, SVR'MODE
                if DEBUG then ? DBG_MSG$;"? tab(-10,AGX_SYNC);1,";SYNCFLAGS;",";FNAME$;",";SVR'BYTES;",";SVR'MTIME
                ? tab(-10,AGX_SYNC);"1,";str(SYNCFLAGS);",";FNAME$;","; &
                    str(SVR'BYTES);",";SVR'MTIME;chr(127);
                TX'FILES = TX'FILES + 1
                TX'BYTES = TX'BYTES + SVR'BYTES
                IF (CH # 0) then
                    ? #CH,".";
                    if (CH = 0) then ? tab(-1,254);
                endif        
            endif
        endif
        ! get next file...
        xcall MIAMEX,MX_FINDNEXT,STATUS,FNAME$,SIZE,ATTR, &
            MDATE,MTIME,CDATE,CTIME

    loop until STATUS # 0

    IF (CH # 0) then
        ? #CH
    endif        

    ? #CH, "Files examined:";TX'FILES;" (";str(TX'BYTES);" bytes)"
    TX'FILES = 0
    TX'BYTES = 0

    ! tell ATE that's all the files

    if DEBUG then ? DBG_MSG$;"? tab(-10,AGX_SYNC);2,";SYNCFLAGS
    AGCMD$ = "2,"+str(SYNCFLAGS)    ! [102]
    if VEDIT >= 1153 then           ! [102] use MX_AGWRAPPER if avail
        xcall MIAMEX,MX_AGWRAPPER,AGX_SYNC,AGCMD$,AGRESPONSE$
        xcall STRTOK,0,AGRESPONSE$,",",STATUS,RDELIM$,TX'FILES,TX'BYTES
    else
        ? tab(-10,AGX_SYNC);AGCMD$;chr(127);   ! [102]
        input "",STATUS,TX'FILES,TX'BYTES      ! # files, bytes to xfer  
    endif
    if DEBUG then ? DBG_MSG$;"files to xfer = ";TX'FILES;" bytes = ";TX'BYTES
    if STATUS goto DONE
    
    ? #CH,"Files needing update:";TX'FILES;"(";str(TX'BYTES);" bytes)"

    if (TX'FILES = 0) and ((SYNCFLAGS and ATSF_DELETE)=0) goto DONE
    
    if (TX'FILES > 0) ? #CH,"Transferring..."
        
    ? tab(-10,AGX_SYNC);"3,";str(SYNCFLAGS);chr(127);  

    do 
        if DEBUG then ? DBG_MSG$;"<-- "; tab(-1,254);
        input "",STATE,STATUS,FILES,BYTES,DELFILES,DELBYTES,FNAME'EXT$
        if DEBUG then ? DBG_MSG$;"STATE=";STATE;" STATUS=";STATUS;" FILES,BYTES="; &
            FILES;",";BYTES
            
        if (STATE < 3) then
            ? #CH, FNAME'EXT$; " (";str(BYTES);" bytes)"
        endif            
                        
    loop until STATE = 3

    if DEBUG then 
        ? DBG_MSG$;"Total files xferred:" ;FILES;", bytes:";BYTES
        if (SYNCFLAGS and ATSF_DELETE) then
            ? DBG_MSG$;"Total files deleted:";DELFILES;"(";str(DELBYTES);" bytes)"    
        endif
    endif                        
    
    
    
DONE:
    
    if (GUIFLAGS and AGF_LOCWIN) then
        ? #CH, "ATSYNC doesn't apply when running A-Shell/Windows locally."
    else        
        ? #CH, "Files Transferred:";FILES;"(";str(BYTES);" bytes)"            
        if (SYNCFLAGS and ATSF_DELETE) then
            ? #CH,"Total files deleted:";DELFILES;"(";str(DELBYTES);" bytes)"    
        endif

        if (STATUS = 0) then 
            ? #CH, "[OK] "            
        else
            ? #CH, "Status =";STATUS;"[Error] ";
            call GET'EMSG   
            ? #CH, EMSG$
        endif            
    endif        
        
    !!!if CH then ? "See ";LISTFILE$;" for output messages"        ! [SBX]
ENDIT:
    if CH then close #CH
    xcall ECHO
    RETURN
    
TRAP:
    STATUS = err(0)
    resume DONE

INIT'ERROR:
    goto DONE

SYNTAX'ERROR:
    ? "Syntax error "
    ?
HELPOUT:
    ? "Syntax: "
    ? 
    ? "    .ATSYNC {HostDir}{,PCDir}{/Switches}"
    ? 
    ? "HostDir is optional host directory (default is current directory)"
    ? "PCDir is optional PC directory (default is %ATEPERMCACHE%)"
    ? "Switches: "
    ? 
    ? "  /C{ont} - Continue after most file transfer errors"
    ? "  /P{rgdlg} - Display progress dialog"
    ? "  /D{elete} - Delete files not present on server (not yet implemented)"
    ? "  /NOERR - Suppress FTP error dialogs"
    ? "  /L{ist}{:file} - Output status info to file [ATSYNC.LST]"
    goto ENDIT

GET'EMSG:   
    if STATUS >= 30000 then EMSG$ = "FTP transfer error"
    if STATUS < 0 and STATUS > -100 then EMSG$ = "ATE returned error"
    if STATUS = -101 then EMSG$ = "Host dir ("+HOSTDIR$+") not a directory"
    if STATUS = -102 then EMSG$ = "Requires ATE client"
    if STATUS > 1 and STATUS < 500 then EMSG$ = "Basic Error"
    if STATUS = 1 then EMSG$ = "^C interrupt"
    return   
