:<(ATE UTIL) SBX's related to ATE file operations (transfers, printing, etc)

    ATECCH.SBX - Copy/refresh file to/in ATE cache
    TSTACH      - Test program for ATECCH.SBX
    ATEAPX.SBX  - Export file to ATE workstation via terminal channel
    TSTAPX      - Test program for ATEAPX.SBX
    ATEGFK.SBX  - Get file from PC via terminal channel
    TSTATEGFK   - Test program for ATEGFK.SBX
    ATESYC.SBX  - SBX version (wrapper) for ATSYNC.LIT (sync entire dir)
    FTPDXX.SBX  - Wrapper for FTPDLX (for indirect call via ATE)
    FTPDXT      - Test program for FTPDLX, FTPDXX
    FTPTST      - Test program for FTP2, FTPDLX
    APXPRF.SBX  - Modify APEX Preview Preferences (in Registry) from host
    APXPRF.BSI  - ++include containing function to call APXPRF.SBX
    TSTAPXPRF.BP- Sample using APXPRF.BSI to invoke APXPRF.SBX remotely.
    ATEPRT.SBX  - Invoke XCALL SPOOL remotely on ATE
    TSTAPT.BP   - Sample/test for ATEPRT.SBX
    FILVER.SBX  - ATE (AG_XFUNC) compatible routine to get a file version.
    ATEREG.SBX  - Make changes to ATE's registry from server
    ATEREGTST.BP- Test for ATEREG.SBX
    SETATEREG.BP- Command line utility using ATEREG.SBX /jdm
    OSVERCLI.SBX- XCALL to get ATE MX_OSVER info /jdm
>
