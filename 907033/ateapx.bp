program ATEAPX,1.0(127)             ! SBX for Ate Aux Port eXPort (of file)
!------------------------------------------------------------------------
!Exports a file to an arbitrary location on ATE client via aux port.
!Requires ATE 4.9.952.5+
!
!XCALL ATEAPX,HOSTSPEC,PCDIR,PCFILE,FLAGS,STATUS
!
!HOSTSPEC: spec of (server) file to copy (AMOS or native)
!PCDIR: directory where you want it to go on PC
!PCFILE: file.ext for PC (if blank, use name from host)
!        NOTE: if using format file.ext and <file> longer than 10 chars,
!       then prepend ".\" to it.
!FLAGS: 0=binary, 2=ASCII, +4 for 'NULL' printer (more efficient)
!       +8 for FTP (if server not Windows) [105]
!       +16 to skip post-transfer verification [107]
!       +32 pre-verify (skip transfer if file already current `on PC) [108]
!       +64 ignore version 0
!       +128 put log in current dir rather than opr: [117]
!       +256 use hash (if avail) rather than bytes for verification (AIX pre 1430 workaround) [123]
!STATUS:
!   >0 = File transferred
!    0 = File transfer not needed or applicable
!   -1 = ATE required
!   -2 = host file doesn't exist
!   -3 = file tranfserred but failed to verify afterwards [105]
!   -4 = FTP transfer failure [105]
!   -98= server ashell version not high enough (for binary xfer)
!   -99= ATE version not high enough
!NOTES:
!Use SET DEBUG to run with verbose debugging (but it all goes to OPR:ATEAPX.LOG [107])
!
!--------------------------------------------------------------------
!EDIT HISTORY
![100] March 21, 2007 01:08 PM         Edited by Jack
!    Created
![101] January 07, 2008 02:53 PM        Edited by joaquin
!    Add SSW_NOPREVIEW switch to prevent transferred file
!    from getting intercepted by previewer /jdm
![102] March 25, 2008 03:24 PM        Edited by joaquin
!    Increase max line length for ASCII mode from 512 to 8192 /jdm
![103] August 03, 2008 09:11 AM         Edited by joaquin
!    Increase PCFILE$ from 32 to 128
![104] February 03, 2009 08:59 AM         Edited by joaquin
!    Support FLAGS +4 for "NULL" printer
![105] February 10, 2009 02:49 PM         Edited by joaquin
!    Create a log in OPR:ATEAPX.LOG and double-check that the
!    file is actually transferred; support +8 mode for FTP
![106] May 3, 2009 05:45 PM         Edited by joaquin
!    Fix post-transfer check (was looking at host instead of client)
![107] May 5, 2009 09:15 AM         Edited by joaquin
!    Clean up debug mode, add flag option to eliminate post-verify,
!    add pre-verify, strip quotes
![108] May 5, 2009 01:15 PM         Edited by joaquin
!    Output "(no hash)" when hash not avail; if hashes match ignore size
![109] September 19, 2009 03:10 PM  Edited by joaquin
!    Correct log msg - said "PC filesize" but was really "Server filesize" /jdm
![110] October 14, 2009 02:27 PM  Edited by joaquin
!    Retry on failure /jdm
![111] October 22, 2009 04:06 PM        Edited by jacques
!    New flag +64 disables the version check (if version 0)
![112] November 2, 2009 08:11 AM        Edited by jacques
!    Fix bug: was treating (file already current) as error and retrying
![113] November 02, 2009 09:01 AM       Edited by jacques
!    Fix bug: was allowing bogus hash match to override obvious bytes mismatch /jdm
![114] January 27, 2010 10:45 AM       Edited by jacques
!    Log more details about errors validating filesize on pc
![115] January 28, 2010 10:45 AM       Edited by jacques
!    On post transfer validation error, try re-syncing ate/kbd
![116] mars 05, 2010 04:32 PM           Edited by jacques
!   Fix bug in post-verify (causing it to fail with 0 bytes)
![117] october 20, 2010 03:52 PM           Edited by jacques
!   Remove elseif to compile under 5.0; add +128 flag option
![118] January 28, 2011 02:48 PM           Edited by jacques
!   Minor cleanup to ateapx.log messages and eliminate redundant
!   retry when file already exists on PC.
![119] Febuary 4, 2011 11:04 AM           Edited by jacques
!   Recompile with patched Fn'Name'Ext to support dev#:[p,pn]name.ext format
![120] September 18, 2011 03:34 PM      Edited by jack
!   Close pre-verify loophole allowing unequal files to pass as equal
![121] June 27, 2012 11:34 AM           Edited by jack
!   Fix pre-transfer verify bug relating to PCFILE param not "" and 
!   post-transfer verify failure if not traditional 8.3 format;
!   Force +4 mode always (no downside, fixes problems with normal
!   mode with extended format filenames) /jdm
![122] March 10, 2015 09:07 PM           Edited by jack
!   Minor improvements to the logging; if a non-FTP transfer fails,
!   don't retry; just try to reset the PC as best as possible.
![123] March 24, 2017 11:11 AM           Edited by jack
!   Add flag +256 (APXF_HASHVERIFY)
![124] August 09, 2017 15:48 AM           Edited by jack
!   Check for SIGHUP signal manually, and if so, abort
![125] August 09, 2017 15:48 AM           Edited by jack
!   Add process id to the log entries, slight reformatting; also
!   log errors to ashlog. (This was actually [123] from ATEAPX2)
!   Also: avoid inf loop in pre 1400 sync if no response (10 sec timeout)
![126] December 07, 2017 17:03 PM           Edited by jack
!   Convert source spec with leading ./ or .\ to absolute to avoid
!   problems with server context mismatch
![127] December 07, 2017 17:03 PM           Edited by jack
!   Use FTP2 (flag 4096) if ATE client ver >= 1380
!--------------------------------------------------------------------
++pragma SBX
++pragma ERROR_IF_NOT_MAPPED "ON"

!++include'once ashinc:xcall.bsi     ! [123] (don't need this any more)
++include'once ashinc:ashell.def
++include'once ashinc:types.def      ! [123]
++include'once ashinc:tcrtex.def
++include'once sosfunc:fnextch.bsi   ! [105]
++include'once sosproc:syslog.bsi    ! [105]
++include'once sosfunc:fnsprintf.bsi ! [105]
++include'once sosfunc:fnfileage.bsi ! [105]
++include'once sosfunc:fnameext.bsi  ! [106]
++include'once sosfunc:fnistype.bsi  ! [123]
++include'once sosfunc:fnfqfs.bsi    ! [126]

define APXF_BINARY       = &h0000 ! [104] binary (MIME) transfer
define APXF_ASCII        = &h0002 ! [104] ascii
define APXF_NULL         = &h0004 ! [104] add to use "NULL" printer
define APXF_FTP          = &h0008 ! [105] use FTP
define APXF_NOPOSTVERIFY = &h0010 ! [107] don't post-verify
define APXF_PREVERIFY    = &h0020 ! [107] pre-verify
define APXF_NOVER        = &h0040 ! [111] ignore version 0
define APXF_LOGCURDIR    = &h0080 ! [117] put log in current dir
define APXF_HASHVERIFY   = &h0100 ! [123] use hash rather than bytes for verify

![104] Note: NULL printer mode differs from the DISK: mode in that
![104] ATE writes the output directly to that file and the file is
![104] never passed through the spooling subsystem.  It's more
![104] efficient but you don't get some of the features of spooling,
![104] such as preview potential, backup copy, possible logging, etc.

    map1 PARAMETERS
        map2 STATUS,F,6
        map2 HOSTSPEC$,S,160
        map2 PCDIR$,S,160
        map2 PCFILE$,S,128      ! [103]
        map2 FLAGS,B,4

    map1 MISC
        map2 I,F
        map2 X,F
        map2 Y,F
        map2 HOSTNATIVE$,S,160
        map2 HOSTDIR$,S,160
        map2 PCPATH$,S,260      ! [105] full filespec on PC
        map2 GUIFLAGS,F
        map2 FDDB,X,52
        map2 FNAME'EXT$,S,50            ! name.ext
        map2 DIRSEP$,S,1
        map2 ICH,F
        map2 PLINE,S,8192               ! [102] was 512
        map2 SVR'MTIME,B,4              ! file mtime on server
        map2 SVR'CTIME,B,4              ! file ctype on server
        map2 SVR'BYTES,F                ! size of file on server
        map2 SVR'MODE,B,2               ! file type/mode on server
        map2 PC'BYTES,F                 ! [105] size of file on PC
        map2 ATE'VER$,S,20
        map2 ASH'VER$,S,20
        map2 SVR'VER$,S,20              ! [107]
        map2 PC'VER$,S,20               ! [107]
        map2 MTIME,B,4                  ! [107]
        map2 CTIME,B,4                  ! [107]
        map2 SVR'HASH$,S,16             ! [107]
        map2 PC'HASH$,S,16              ! [107]
        map2 PC'MODE,B,2                ! [107]
        map2 VFLAGS,F
        map2 VMAJOR,B,1                 ! [104]
        map2 VMINOR,B,1                 ! [104]
        map2 VEDIT,B,2                  ! [104]
        map2 VPATCH,B,1                 ! [104]
        map2 ATE'VMAJOR,B,1             ! [104]
        map2 ATE'VMINOR,B,1             ! [104]
        map2 ATE'VEDIT,B,2              ! [104]
        map2 ATE'VPATCH,B,1             ! [104]
        map2 USRNAM$,S,20               ! [105]
        map2 MACHINE$,S,20              ! [105]
        map2 ATE'USRNAM$,S,20           ! [105]
        map2 ATE'MACHINE$,S,20          ! [105]
        map2 RC$,S,80                   ! [105]
        map2 ECHON,B,1                  ! [105]
        map2 CCON,B,1                   ! [105]
        map2 A,B,1                      ! [105]
        map2 RETRIES,B,1                ! [110]
        map2 MSG$,S,100                 ! [114]
        map2 ENVRES$,S,100              ! [114]
        map2 T1,F                       ! [115]
        map2 C,B,1                      ! [115]
        map2 LOGFILE$,S,32,"OPR:ATEAPX.LOG"  ! [117]
        map2 BYTES'MATCH,BOOLEAN        ! [123]
        map2 HASH'MATCH,BOOLEAN         ! [123]
        map2 SIGMASK,B,2                ! [124]
        map2 PID,B,4                    ! [123]
        
define DBG_MSG$ = " [ateapx.sbx debug] > "  ! use for debug msg leadin

!-------------------------------------------------------------------
    significance 11

    on error goto TRAP
    
    xcall MIAMEX, MX_GETSIG, SIGMASK                ! [124] retrieve signals
    if SIGMASK and SR_HUP then                      ! [124] SIGHUP received
        xcall MIAMEX, MX_ASHLOG, "Detected SIGHUP received in ATEAPX: terminating session!"
        ? "Prior SIGHUP detected - cannot proceed - terminating session...";chr(7)
        sleep 0.5
        xcall MIAMEX, MX_EXITSBXX                  ! [124] terminate to dot (hopefully disconnect from there)
        end
    endif
    
    xcall MIAMEX, MX_GETPID, PID                    ! [125] for traces    
    
BEGIN:
    if .ARGCNT < 5 then     ! [123] was XCBCNT
        print "Too few args in ATEAPX.SBX"
        xcall MIAMEX,MX_EXITSBXX
    endif

    xgetargs HOSTSPEC$,PCDIR$,PCFILE$,FLAGS,STATUS

    FLAGS = FLAGS or APXF_NULL              ! [121]

    if FLAGS and APXF_LOGCURDIR then        ! [117]
        LOGFILE$ = "ATEAPX.LOG"             ! [117] put log in current dir
    endif                                   ! [117] else dflt is opr:

    if HOSTSPEC$[1,1]="""" then         ! [107] remove quotes (nuisance)
        xcall TRIM, HOSTSPEC$
        HOSTSPEC$ = HOSTSPEC$[2,-2]
    endif

    ! [107] establish log file name so we can output debug messages to it
    call SysLog(SLF_CTL+SLF_RESTART,"LOGFIL",LOGFILE$)  ![117] was "OPR:ATEAPX.LOG"

    ! first get status of file on server
    SVR'HASH$ = "(no svr hash)"
    xcall MIAMEX, MX_FILESTATS, "L", HOSTSPEC$, SVR'BYTES, &
        SVR'MTIME, SVR'CTIME, SVR'MODE, SVR'VER$, SVR'HASH$ ! [107] add VER$, HASH$

    ![107] if DEBUG then ? DBG_MSG$;"Server stats: file=";HOSTSPEC$;" bytes=";SVR'BYTES
    call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART, &
        "%t ATEAPX Dbg: server stats: file=%s, bytes=%d, retries=%d",HOSTSPEC$,SVR'BYTES,RETRIES)    ![107][118]

    if (SVR'BYTES < 0) then
        STATUS = -2
        goto DONE
    endif

    ! Next, find out if we are running ATE...
    xcall AUI,AUI_ENVIRONMENT,2,GUIFLAGS

    ![107] if DEBUG then ? DBG_MSG$;"GUIFLAGS = ";GUIFLAGS;" (AGF_LOCWIN=";AGF_LOCWIN;", AGF_ATE=";AGF_ATE;")"
    call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART, " Dbg: GUIFLAGS=0x%x",GUIFLAGS)

    ! if local windows and file exists, no xfer needed; return 0
    if (GUIFLAGS and AGF_LOCWIN) then
       if SVR'BYTES >= 0 then
            STATUS = 0
            goto DONE
        endif
    endif

    ! if not ATE, return error -1
    if ((GUIFLAGS and AGF_ATE) = 0) then
        STATUS = -1
        goto DONE
    endif

    if (GUIFLAGS and AGF_RWN) then      ! [105] clear FTP flag if ATSD
        FLAGS = FLAGS and not APXF_FTP
    endif

    ! check versions
    xcall MIAMEX,MX_GETVER,ASH'VER$,VMAJOR,VMINOR,VEDIT,VPATCH, &
        ATE'VMAJOR,ATE'VMINOR,ATE'VEDIT,ATE'VPATCH      ! [104] get ATE ver too

    ![104] xcall AUI,AUI_ENVIRONMENT,0,VFLAGS,ATE'VER$
    ![104] if FLAGS = 0 then       ! we need 4.9.952.5+ for binary
    if (FLAGS and APXF_ASCII) = 0 then  ! [104] we need 4.9.952.5+ for binary
        if ASH'VER$ < "4.9(952)-5" then
            STATUS = -98
            goto DONE
        endif
        ![104] if ATE'VER$ < "4.9(952)-5" then
        if (ATE'VEDIT > 0) or ((FLAGS and APXF_NOVER) = 0) then  ![111]
            if ATE'VEDIT < 952 or (ATE'VEDIT=952 and ATE'VPATCH<5) then ! [104]
                STATUS = -99
                goto DONE
            endif
        endif
    else                    ! else we only need 4.9.952.3+ on ATE
        if ASH'VER$ < "4.9(952)-3" then
            STATUS = -99
            goto DONE
        endif
    endif

    ! [105] build complete PC filespec
    PCPATH$ = PCDIR$
    if PCDIR$#"" and PCDIR$[-1,-1]#"\" then
        PCPATH$ = PCPATH$ + "\"
    endif
    if PCFILE$="" then                              ! [121]  
        PCPATH$ = PCPATH$ + Fn'Name'Ext$(HOSTSPEC$)    
    else                                            ! [121]
        PCPATH$ = PCPATH$ + PCFILE$                 ! [121]   
    endif

    ! [107] see if file already on client
    if FLAGS and APXF_PREVERIFY then
        PC'HASH$ = "(no pc hash)"
        xcall MIAMEX, MX_FILESTATS, "R", PCPATH$, PC'BYTES, MTIME, CTIME, PC'MODE, PC'VER$, PC'HASH$
        if PC'BYTES = -1 then
            PC'HASH$="0-0-0-0"     ! [118] MX_FILESTATS should do this but doesn't
        endif
        call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART, &
            " Dbg: Pre-verify %s:\n       (svr vs pc): %d vs %d bytes,    %s vs %s hash", &
            PCPATH$, SVR'BYTES, PC'BYTES, SVR'HASH$, PC'HASH$)  ! [107]

![120]  if (PC'BYTES = SVR'BYTES) &
![120]  or ((SVR'HASH$ # "") and (SVR'HASH$ = PC'HASH$) and (PC'BYTES > 0)) then ![108][113] add PC'BYTES>0

        ![123]    if (SVR'HASH$ = PC'HASH$) or (SVR'HASH$ = "") or (PC'HASH$ = "") then ![120]
        ![123]        if PC'HASH$ = "" or SVR'HASH$ = "" or PC'HASH$ = SVR'HASH$ then    ! if hash supported and same
        if Fn'FilesMatch(SVR'BYTES, SVR'HASH$, PC'BYTES, PC'HASH$, FLAGS) then
            STATUS = 0  ! no transfer needed
            call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART, &
                " Dbg: File already on client, xfer skipped")    ! [107]
            RC$ = "OK (Transfer skipped, file already current on PC)" ! [107][118] prefix with "OK"
            goto DONE2
        endif
    endif

    ! [104] +4 (NULL printer) flag requires ATE 1137
    if (FLAGS and APXF_NULL) then
        if (ATE'VEDIT > 0) or ((FLAGS and APXF_NOVER) = 0) then  ![111]
          if ATE'VEDIT < 1137 then ! [104]
            ! just switch back to normal mode rather than abort
            ![107] if DEBUG then ? DBG_MSG$;"ATE client < 1136.2; ignoring +4 (NULL) flag"
            call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART," Dbg: ATE client < 1136.2; ignoring +4 (NULL) flag") ![107]
            FLAGS = FLAGS and not APXF_NULL
          endif
        endif
    endif

    ! at this point, host file exists and we are running ATE, so xfer it

    ! [105] handle FTP option...
    if FLAGS and APXF_FTP then
        ! convert hostspec to native format
        xcall MIAMEX,MX_FSPEC,HOSTSPEC$,HOSTNATIVE$,"",FS_FMA+FS_TOH, &
            FDDB,STATUS

        ? tab(-10,AG_FTP);
        if FLAGS and APXF_ASCII then
            ? "C";              ! host to pc silent ASCII
        else
            ? "A";              ! host to pc silent BINARY
        endif
        if HOSTNATIVE$[1,2] = "./" or HOSTNATIVE$[1,2] = ".\" then  ! [126]
            HOSTNATIVE$ = Fn'FQFS$(HOSTNATIVE$,FNTF_ABSOLUTE)
        endif
        ? HOSTNATIVE$;"~";PCPATH$;          ! [127]
        if ATE'VEDIT >= 1380 THEN           ! [127] if client supports FTP2
            ? "~4096";                      ! [127] ask for it
        endif
        ? chr(127);
        xcall MIAMEX, MX_GETECHO, ECHON     ! get echo status
        xcall MIAMEX, MX_GETCTRLC, CCON     ! get ^c status
        xcall NOECHO
        xcall CCOFF                         ! turn off ^C
        xcall ACCEPN,A
        if ECHON then xcall ECHO            ! restore echo
        if CCON then xcall CCON             ! restore ^C status
        if A = 3 then
            STATUS = -4
        else
            STATUS = 1
        endif
        goto DONE
    endif


    if PCFILE$ = "" then        ! get file.ext from source
        ! convert hostspec to native format
        xcall MIAMEX,MX_FSPEC,HOSTSPEC$,HOSTNATIVE$,"",FS_FMA+FS_TOH, &
            FDDB,STATUS
        ![107] if DEBUG then ? DBG_MSG$;"Native host spec=";HOSTNATIVE$;", STATUS=";STATUS
        call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART, &
            " Dbg: Native hostspec=%s, STATUS=%d",HOSTNATIVE$,STATUS) ![107]
        if STATUS # 0 then
            STATUS = -2     ! does not exist
            goto DONE
        endif
                            ! strip off file.ext
        xcall MIAMEX,MX_DIRSEP,DIRSEP$
        ! strip the name.ext off of the complete native spec
        Y = 0
        do
            X = Y
            Y = instr(X+1,HOSTNATIVE$,DIRSEP$)
        loop until Y = 0
        PCFILE$ = HOSTNATIVE$[X+1,-1]
    endif

    ![107]if DEBUG then ? DBG_MSG$;"PCDIR$=";PCDIR$;",PCFILE$=";PCFILE$
    !]107]if DEBUG then ? "Flags = ";FLAGS;" filesize = ";SVR'BYTES : STOP
    call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART," Dbg: PCDIR$=%s, PCFILE$=%s, FLAGS=%d, svr filesize=%d", &
        PCDIR$,PCFILE$,FLAGS,SVR'BYTES) ! [107]

    ! First, send AG_SPOOLCFG command
    ? tab(-10,AG_SPOOLCFG);

    ![104] for normal mode, we send:
    ![104]    <PCFILE$>,DISK:<PCDIR$>,...
    ![104] for NULL mode, we send
    ![104]    <PCDIR$>\<PCFILE$>,NULL,...
    if (FLAGS and APXF_NULL) then   ! [104] combine PCDIR$ and PCFILE$
        ? PCDIR$;                   ! [104]
        if PCDIR$#"" and PCDIR$[-1,-1]#"\" then
            ? "\";
        else                 ! [117]
            if PCDIR$="" and instr(1,PCFILE$,":")<1 &
            and instr(1,PCFILE$,"\")<1 and len(PCFILE$)>14 then
                ? ".\";
            endif
        endif
        ? PCFILE$;",NULL";

    else
        ? PCFILE$;                  ! filename override
        ? ",DISK:";PCDIR$;          ! use pseudo printer DISK:<path>
![121]        PCPATH$ = PCDIR$
    endif


    ![104]if FLAGS=0 then
   if (FLAGS and APXF_ASCII) = 0 then      ![104]
        ? ",";str(SVR'BYTES);   ! binary flag (size)
    else
        ? ",0";                 ! [101]
    endif
    ? ",,1048576";              ! [101] no PDF license, NO PREVIEW
    ? chr(127);


    ? tab(-1,82);               ! open aux port

    ! if binary, use B64ENC to output file
    ![104]if FLAGS=0 then

    if (FLAGS and APXF_ASCII) = 0 then      ![104]
        ! xlat to mime and output to stdout
        xcall B64ENC,HOSTSPEC$,0,STATUS
        if STATUS < 0 goto DONE
    else
        ICH = 55000             ! get 1st open file channel >= 55000
        do while eof(ICH)#-1
            ICH = ICH + 1
        loop
        open #ICH, HOSTSPEC$, input
        do
            input line #ICH, PLINE
            if (PLINE#"" or eof(ICH)#1)
                ? PLINE
            endif
        loop until eof(ICH)=1
        close #ICH
    endif
    ? tab(-1,83);               ! close aux port
    STATUS = 1                  ! (file transferred)

DONE:
    ![107] if DEBUG then ? DBG_MSG$;"Returning STATUS=";STATUS;" PCFILE$ = "+PCFILE$
    call SysLog(SLF_LOG+SLF_DBG+SLF_RESTART," Dbg: Returning STATUS=%d, PCFILE$=%s",STATUS,PCFILE$) ![107]

    ! [105] if status indicates success, double check target size
    if STATUS >= 0 then
        ![106] xcall MIAMEX, MX_FILESTATS, "L", HOSTSPEC$, PC'BYTES

![121] PCPATH$ should still be good...
![121]        if PCFILE$="" or PCPATH$[-1,-1]="\" then
![121]            PCPATH$ = PCPATH$ + Fn'Name'Ext$(HOSTSPEC$)     ! [106]
![121]        ! [116] append file to path if needed for verify
![121]        else    ![117]
![121]            if PCFILE$#"" and instr(1,lcs(PCPATH$),lcs(PCFILE$)) < 1 then ! [116] append PCPATH$
![121]                if PCPATH$[-1,-1] # "\" then
![121]                    PCPATH$ = PCPATH$ + "\"
![121]                endif
![121]                PCPATH$ = PCPATH$ + PCFILE$
![121]            endif
![121]        endif

        if (FLAGS and APXF_NOPOSTVERIFY) = 0 then           ! [107]
            xcall MIAMEX, MX_FILESTATS, "R", PCPATH$, PC'BYTES, MTIME, CTIME, PC'MODE, PC'VER$, PC'HASH$  ! [106][123]

            if FLAGS and APXF_ASCII then
                if PC'BYTES <= 0 then
                    RC$ = "Post-transfer verify error!"
                    STATUS = -3
                else
                    RC$ = "OK"
                    if PC'BYTES # SVR'BYTES
                        RC$ = RC$ + " (ASCII xfer; PC file size = "+PC'BYTES+")"
                    endif
                endif
            else        ! for binary transfer, should be exact
                if not Fn'FilesMatch(SVR'BYTES, SVR'HASH$, PC'BYTES, PC'HASH$, FLAGS)
                    
                    ! [123] we failed - just fine tune the error message
                    STATUS = -3
                    if ((FLAGS and APXF_HASHVERIFY) = 0) and PC'BYTES # SVR'BYTES then
                        RC$ = "Error: File Size doesn't match ("+PC'BYTES+")"
                        
                    else
                        RC$ = "Error: File Hash doesn't match ("+PC'HASH$+")"
                        STATUS = -3
                    endif
                    
                    ! [115] try re-syncing kbd  ---
                    if ATE'VEDIT > 1153 then
                       call SysLog(SLF_LOG+SLF_RESTART,"p%d %s - trying resync",PID,RC$) ![115][125]
                       T1 = time + 3
                        ? tab(-10,AG_ACK);chr(210);chr(127);
                        do while C # 210 and TIME <= T1
                            C = getkey(0)
                        loop
                        if C = 210 then
                            call SysLog(SLF_LOG+SLF_RESTART,"p%d Resync successful - trying verify again ...",PID) ![125]
                            STATUS = 1
                            xcall MIAMEX, MX_FILESTATS, "R", PCPATH$, PC'BYTES  ! [106]
                        else
                            call SysLog(SLF_LOG+SLF_RESTART,"p%d Resync failed (ATE may require reset)",PID)  ![125]
                        endif
                    else
                        call SysLog(SLF_LOG+SLF_RESTART,"p%d Cannot resync ATE versions <= 1153",PID)   ![125]
                    endif
                endif           ! ---[115]

                ! try the verification test one more time (in case it was just a messed up from file xfer hangover)
                if not Fn'FilesMatch(SVR'BYTES, SVR'HASH$, PC'BYTES, PC'HASH$, FLAGS) then
                    STATUS = -3
                    ! [123] we failed - just fine tune the error message
                    STATUS = -3
                    if ((FLAGS and APXF_HASHVERIFY) = 0) and PC'BYTES # SVR'BYTES then
                        RC$ = "Error: File Size doesn't match ("+PC'BYTES+")"
                        
                    else
                        RC$ = "Error: File Hash doesn't match ("+PC'HASH$+")"
                        STATUS = -3
                    endif
                    
                    ! Special case of a negative value indicating system error which we can report
                     if (PC'BYTES < 0) and (PC'BYTES > -20000) then     ! [123][114] log details of failure to verify
                        xcall MIAMEX, MX_ERRNOMSG, abs(PC'BYTES), MSG$
                        if VEDIT > 1155 and ATE'VEDIT > 1155 then
                            xcall MIAMEX, MX_AGWRAPPER, AG_GETENV, "ATECACHE,1", ENVRES$
                            ENVRES$ = "%ATECACHE% = " + ENVRES$
                        else
                            ENVRES$ = ""
                        endif
                        call SysLog(SLF_LOG+SLF_RESTART," Error %d verifying file on PC (%s)\n  %s", &
                                    PC'BYTES,MSG$,ENVRES$) ![114]
                    endif
                else
                    RC$ = "OK"
                endif
            endif
        else
            RC$ = "not verified"        ! [107]
        endif
    else
        RC$ = "Error # "+STATUS
        if PCPATH$ = "" then PCPATH$ = PCDIR$ + " + " + PCFILE$
    endif

DONE2:          ! [107]

    ! [105] log a message to OPR:SYSLOG.LOG
    xcall GETUSN,USRNAM$,MACHINE$,ATE'USRNAM$,ATE'MACHINE$
    ![107] if DEBUG then ? DBG_MSG$;"logging flags="+FLAGS+" ["+HOSTSPEC$+"] to ["+PCPATH$+"] RC = "+RC$

    if RC$[1,2] # "OK" and RETRIES < 1 and STATUS = 0 then  ! [110][118] check for OK at start of msg [122]
        if (FLAGS and APXF_FTP) then         ! [122] retry only if FTP
            RC$ = RC$ + " - Retrying..."     ! [110]
        endif
    endif                                    ! [110]

    call SysLog(SLF_LOGCLS, &
        "%t p%d (flags=%d)\n  [%s] to [%s]\n  User=%s, PC=%S, ATE Ver=%d.%d.%d.%d\n  Server filesize=%s [%s]  Status=%d", &
        PID,FLAGS,HOSTSPEC$,PCPATH$,USRNAM$,ATE'MACHINE$, &
        ATE'VMAJOR,ATE'VMINOR,ATE'VEDIT,ATE'VPATCH,str(SVR'BYTES),RC$,STATUS)  ![109] (was "PC filesize=") ![110] use str() [123][125]

![107] call SysLog(SLF_CTL+SLF_RESTART,"LOGFIL",LOGFILE$) ![117]
![112]   if RC$[1,2] # "OK" and RETRIES < 1 then  ! [110]
    if STATUS = 0 and RETRIES < 1 and RC$[1,2] # "OK" then  ! [112][118] add OK test
        if (FLAGS and APXF_FTP) then            ! [122] retry only if FTP        
            RETRIES = RETRIES + 1               ! [110]
            STATUS = 0                          ! [110]
            sleep 1                             ! [110]
            goto BEGIN                          ! [110]
        endif
    endif                                       ! [110]

    if STATUS < 0 then                          ! [122] log failure code explicitly
        ! [122] attempt to clear/reset channel        
        if ATE'VEDIT >= 1400 then               ! [122]        
            xcall MIAMEX, MX_CLRINBUF, 0        ! [122]   
        else
            T1 = time + 10                      ! [125] 10 second time out
            ? tab(-10,AG_RELEASEKBD);chr(127);  ! [122]
            do while getkey(0) = -1             ! [122] clear out input buffer
                if time > T1 then               ! [125]
                    call SysLog(SLF_LOG+SLF_RESTART,"p%d Timeout during pre-1400 resync after status %d",PID,STATUS)   ![125]
                    xcall MIAMEX, MX_ASHLOG, "Timeout during pre-1400 resync after error!"             ! [125]
                    exit                        ! [125]
                endif                           ! [125]
            loop                                ! [122]
        endif                                   ! [122]
        call SysLog(SLF_LOGCLS, "%t p%d ATEAPX return status=%d",PID,STATUS)    ! [125]
        xcall MIAMEX, MX_ASHLOG, "Error "+STATUS+", file "+PCFILE$              ! [125]
    endif
    
    xputarg 5,STATUS
    End

TRAP:
    ? "Basic error ";err(0);" in ATEAPX.SBX"
    ![105] xputarg 5,-(err(0)+1000)
    ![105]End2
    STATUS = -(err(0)+1000)     ! [105]
    goto DONE                   ! [105]

!---------------------------------------------------------------------
!Function:
!   Check if files match
!Params:
!   svr'bytes  (num) [in] - size of file on server
!   svr'hash   (s16) [in] - possible hash on server
!   cli'bytes  (num) [in] - size of file on client
!   cli'hash   (s16) [in] - possible hash on client
!   flags      (num) [in] - APXF_xxx
!Returns:
!   .TRUE if they match
!Globals:
!Notes:
!   flags determine which things are significant - in particular
!   if APXF_HASHVERIFY and we have two hashes, then a match is sufficient
!   (we can ignore size, as a workaround to problem with AIX pre 1430 where 
!   MX_FILESTATS returned size was corrupt.)  Otherwise size has to match
!   (unless APXF_ASCII)
!---------------------------------------------------------------------
Function Fn'FilesMatch(svr'bytes as i4:inputonly, svr'hash$ as s16:inputonly, &
                       cli'bytes as i4:inputonly, cli'hash$ as s16:inputonly, &
                       flags as b4:inputonly) as BOOLEAN
                       
    if (flags and APXF_HASHVERIFY) then
        if svr'hash$ = cli'hash$ then
            if fn'isdigit(svr'hash$[15;1]) and fn'isdigit(cli'hash$[15;1]) then
                Fn'FilesMatch = .TRUE
                exitfunction
            else
                ! fall thru - hashes are not valid (let size determine)
            endif
        else
            exitfunction ! failure - hashes don't match
        endif
    endif

    if flags and APXF_ASCII then        ! for ASCII transfer, size may increase
        if cli'bytes >= svr'bytes then  ! on PC side
            Fn'FilesMatch = .TRUE
        endif
    elseif svr'bytes = cli'bytes then   ! otherwise it should be exact
        Fn'FilesMatch = .TRUE
    endif

EndFunction
