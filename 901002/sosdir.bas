program SOSDIR,1.0(101)	! A-Shell Source DIRectory
!---------------------------------------------------------------------------
!EDIT HISTORY
![100] February 09, 2005 10:03 PM       Edited by joaquin
!	created
![101] May 16, 2005 09:20 PM            Edited by joaquin
!	Rename to SOSDIR to match SOSLIB naming convention
!---------------------------------------------------------------------------
!Scans all of the [9??,*] directories for START.CMD files and prints the
!text between the initial :< and >
!---------------------------------------------------------------------------

MAP1 DLINE,S,100
MAP1 PLINE,S,100
MAP1 X,F	
MAP1 CH,F,6,1		! change to 0 for direct output to screen
MAP1 STATE,F

    xcall AMOS,"DIR SOSDIR.DIR=START.CMD[9??,*]/D/K"
    lookup "SOSDIR.DIR",X
    if X=0 ? "No START.CMD[9??,*] files found" : END

    if CH # 0 then
        open #CH, "SOSDIR.LST", output
	? #CH,"Directory of Source Code in [9??,*]..."
	? #CH,"==========================================================="
    endif

    open #99, "SOSDIR.DIR", input
    do while eof(99)#1 or DLINE#""
        input line #99, DLINE
        if DLINE # "" and instr(1,DLINE,"[901,2]")<1 then
            X = instr(1,DLINE,"[")
            if X > 0 then 
                ? #CH, DLINE[X,-1]
                
                open #2, DLINE, input
                STATE = 0
                do while STATE<2 and eof(1)#99
                    input line #2, PLINE
                    if STATE = 0 then
                        X = instr(1,PLINE,":<")
                        if X > 0 then 
                            STATE = 1
                            PLINE = PLINE[X+2,-1]
                        endif
                    endif
                    if STATE = 1 then
                        X = instr(1,PLINE,">")
                        if X > 0 then
                            STATE = 2
                            if X > 1 then
                                PLINE = PLINE[1,X-1]
                                ? #CH, tab(4);PLINE
                            endif
                        else                
                            ? #CH, tab(4);PLINE
                        endif
                    endif
                loop
                close #2
            endif       
        endif
    loop
    close #99
    close #CH

    if CH # 0 then	
        xcall EZTYP,"SOSDIR.LST"
    endif
    end
