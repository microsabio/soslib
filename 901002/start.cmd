:R
:<SOSLIB PPNs:

  [901,2]	Administrative files, utilities
  [901,14]      Source to A-Shell LITs
  [907,0]       INI.VUE, other lib files 
  [907,6]       Compiled SBXs (copy to BAS: or LOAD for use)  (ASHBAS:)
  [907,10]      Function library (SOSFUNC:)
  [907,11]      Function library test programs
  [907,12]      Procedure library (SOSPROC:)
  [907,13]      Procedure library test rograms
  [907,15]      Firmaware includes (define as FMWMAP: FMWBPI: and FMWDEF:)
  [907,16]      Core A-Shell includes -  DEFs, MAPs, SDFs (ASHINC:)
  [907,20-999]  Individual SBX source (one SBX or related group per ppn)

NOTES: 
- The examples library [908,*] relocated to EXLIB:
- Visit http://www.bitbucket.org/microsabio for updates to SOSLIB and EXLIB

RETURN for directory list
>
:K
RUN SOSDIR
